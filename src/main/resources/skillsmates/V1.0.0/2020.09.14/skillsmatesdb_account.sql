-- MariaDB dump 10.17  Distrib 10.4.13-MariaDB, for Win64 (AMD64)
--
-- Host: 127.0.0.1    Database: skillsmatesdb
-- ------------------------------------------------------
-- Server version	10.4.13-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `account`
--

DROP TABLE IF EXISTS `account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `active` bit(1) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `deleted` bit(1) DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  `revision` bigint(20) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `birthdate` datetime DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `phone_number` varchar(20) DEFAULT NULL,
  `city` varchar(30) DEFAULT NULL,
  `follower` int(11) DEFAULT 0,
  `following` int(11) DEFAULT 0,
  `profile_picture` varchar(255) DEFAULT NULL,
  `publication` int(11) DEFAULT 0,
  `biography` varchar(5000) DEFAULT NULL,
  `id_country` int(11) DEFAULT NULL,
  `id_gender` int(11) DEFAULT NULL,
  `id_status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_iyeg6x6r8s959nhdt76efqc6u` (`id_server`),
  KEY `FKk7k6inityad67q6d6dauvq7se` (`id_country`),
  KEY `FKimysbk21p49j16863efigckk2` (`id_gender`),
  KEY `FKaghw7d40th0m83glnh9iqrvp4` (`id_status`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account`
--

LOCK TABLES `account` WRITE;
/*!40000 ALTER TABLE `account` DISABLE KEYS */;
INSERT INTO `account` VALUES (21,'205285312177400','','2020-08-24 18:35:43','\0',NULL,0,NULL,NULL,NULL,'innocent.ntafor@gmail.com','Innocent','NTAFOR','b9e4c748061f2c1962f8fee2055bdf92f3ecf4e89cc8e19afb992a8a9121e5b035add71568f5bb6801746ab543d605b55fd8bed141b8facf0ff60a9f3d7f2f52',NULL,'Buea',0,0,NULL,0,NULL,107,NULL,109),(62,'94568172733300','','2020-09-03 19:21:40','\0',NULL,0,NULL,'2020-05-04 02:00:00',NULL,'belinda.mengue@yopmail.com','Belinda','MENGUE','9e00068ca1fe3558a1e85c241fb05fcb6e4d0464effd36d1eb8a985817bb9df0839b1cbb1c71d86bda1b5e71ae1a29cf2c2bd6953314a6f1e776e02e908f268a',NULL,'Douala',0,0,NULL,0,'A quand remonte la dernière fois où vous avez mis à jour votre biographie Instagram ?\n\nParce que la présentation de votre profil est fondamentale pour susciter l’envie des utilisateurs de s’abonner à votre compte, proposer une biographie bien rédigée et attractive est plus important que jamais.\n\nDe la même manière que convertir les visiteurs de votre site Internet en clients, l’objectif de votre profil Instagram est de convertir des visiteurs en nouveaux followers. Cela signifie donc que vous devez vous assurer que votre biographie Instagram comprend tout ce qu’il faut pour faire une excellente première impression.\n\nJe vous expliquerai tout au long de cet article ce qu’il faut savoir pour réussir cet espace fondamental de votre profil, avec de nombreux exemples concrets pour que vous puissiez vous aussi reproduire cela pour votre marque.',107,106,110),(77,'176189985379000','','2020-09-04 18:01:50','\0',NULL,0,NULL,NULL,NULL,'marie.magne@yopmail.com','Marie','MAGNE','86002024197f55ff14d2f58d7230d6ff319944c3c8af2f82ecf14c6cc5e334aa215ac14b572a45f82a3416bd0f02c922744e661878f6b7a2d724b45c291345c0',NULL,NULL,0,0,NULL,0,NULL,NULL,NULL,110),(111,'18773924290900','','2020-09-07 17:14:53','\0',NULL,0,NULL,NULL,NULL,'vanessa.djou@yopmail.com','Vanessa','DJOU','34a8b78a2867c0daf2b08e8cafd74abe9b724549daf0f68cf1948f131f6d58831cef9b74d0382f3ea06a09bc668f131a539b3aa1662df38f4428df7ccd9128be',NULL,'Yaounde',0,0,NULL,0,NULL,107,106,110),(125,'114775214089500','','2020-09-11 17:19:20','\0',NULL,0,NULL,'2020-09-01 02:00:00',NULL,'solange.nana@yopmail.com','Solange','NANA','e7060722640fb48a42c4a3535ae5889ff202fb07c61fe4d2fa608f9a266a9809b6b784491e792703d4ebb7105c8ebdae495111e730cb4290ad652fd359cf1e09',NULL,'Buea',0,0,NULL,0,NULL,107,106,109);
/*!40000 ALTER TABLE `account` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-09-14  9:17:14
