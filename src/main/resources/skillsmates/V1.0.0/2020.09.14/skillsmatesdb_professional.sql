-- MariaDB dump 10.17  Distrib 10.4.13-MariaDB, for Win64 (AMD64)
--
-- Host: 127.0.0.1    Database: skillsmatesdb
-- ------------------------------------------------------
-- Server version	10.4.13-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `professional`
--

DROP TABLE IF EXISTS `professional`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `professional` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `active` bit(1) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `deleted` bit(1) DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  `revision` bigint(20) DEFAULT NULL,
  `city` varchar(20) NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  `establishment_name` varchar(50) NOT NULL,
  `id_account` int(11) DEFAULT NULL,
  `end_date` datetime NOT NULL,
  `job_title` varchar(30) NOT NULL,
  `start_date` datetime NOT NULL,
  `id_activity_area` int(11) DEFAULT NULL,
  `id_activity_sector` int(11) DEFAULT NULL,
  `current_position` bit(1) DEFAULT NULL,
  `id_establishment_type` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_jhtn3j550trbpd1t9gvk99ni4` (`id_server`),
  KEY `FK_plu2dumjg24sopl79b4d8cig3` (`id_establishment_type`),
  KEY `FKqqiq4iq4hjtu2w9xo76g8kbr5` (`id_activity_area`),
  KEY `FKrb9ipw4jpaovdyfl3b43ql3vw` (`id_activity_sector`),
  KEY `FK_d7boe4m4rripu794c5j4kl65y` (`id_account`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `professional`
--

LOCK TABLES `professional` WRITE;
/*!40000 ALTER TABLE `professional` DISABLE KEYS */;
INSERT INTO `professional` VALUES (89,'262136211683700','','2020-09-05 17:54:36','',NULL,0,'yaounde','awesome','yamo',62,'2020-09-06 02:00:00','informaticien','2020-09-01 02:00:00',84,88,'\0',NULL),(90,'262710190968700','','2020-09-05 18:03:40','',NULL,0,'yaounde','great','yamo',62,'2020-10-11 02:00:00','marketing','2020-08-04 02:00:00',84,87,'\0',NULL),(91,'265426228222800','','2020-09-05 18:48:56','\0',NULL,0,'Yaounde',NULL,'YAMO GROUP',62,'2020-09-06 02:00:00','Logistics','2020-09-05 02:00:00',83,87,'\0',NULL),(98,'266629868776400','','2020-09-05 19:09:00','\0',NULL,0,'Yaounde',NULL,'YAMO GROUP SARL',62,'2020-09-06 02:00:00','Agriculteur','2020-09-01 02:00:00',83,86,'\0',NULL),(126,'115764976863500','','2020-09-11 17:35:50','\0',NULL,0,'Yaounde','awesome job','YAMO',125,'2020-09-10 02:00:00','Informaticien','2020-09-01 02:00:00',84,88,'\0',NULL),(127,'115967337635800','','2020-09-11 17:39:12','\0',NULL,0,'Douala','great','YAMO GROUP',125,'2020-08-31 02:00:00','Agriculteur','2020-08-01 02:00:00',83,86,'\0',NULL);
/*!40000 ALTER TABLE `professional` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-09-14  9:17:15
