/*
 Navicat Premium Data Transfer

 Source Server         : skillmates
 Source Server Type    : MariaDB
 Source Server Version : 100505
 Source Host           : localhost:3306
 Source Schema         : skillsmatesdb

 Target Server Type    : MariaDB
 Target Server Version : 100505
 File Encoding         : 65001

 Date: 31/12/2020 12:49:03
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for account
-- ----------------------------
DROP TABLE IF EXISTS `account`;
CREATE TABLE `account`  (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `active` bit(1) NULL DEFAULT NULL,
  `created_at` datetime(0) NOT NULL,
  `deleted` bit(1) NULL DEFAULT NULL,
  `modified_at` datetime(0) NULL DEFAULT NULL,
  `revision` bigint(20) NULL DEFAULT NULL,
  `address` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `biography` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `birthdate` datetime(0) NULL DEFAULT NULL,
  `city` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `email` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `firstname` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `follower` int(11) NULL DEFAULT 0,
  `following` int(11) NULL DEFAULT 0,
  `lastname` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `phone_number` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `profile_picture` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `publication` int(11) NULL DEFAULT 0,
  `country` int(11) NULL DEFAULT NULL,
  `gender` int(11) NULL DEFAULT NULL,
  `status` int(11) NULL DEFAULT NULL,
  `connected` tinyint(1) NULL DEFAULT 0,
  `fcm_registration_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `receivers` int(11) NULL DEFAULT NULL,
  `account` int(11) NULL DEFAULT NULL,
  `message` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_iyeg6x6r8s959nhdt76efqc6u`(`id_server`) USING BTREE,
  UNIQUE INDEX `UK_q0uja26qgu1atulenwup9rxyr`(`email`) USING BTREE,
  INDEX `FKi7rut9n9vfl3ac9cxuegckab9`(`country`) USING BTREE,
  INDEX `FKge5lb1w9xvo78dg80k6s3ni27`(`gender`) USING BTREE,
  INDEX `FKny0dyt6qv6m9y7qlcbfdwms58`(`status`) USING BTREE,
  CONSTRAINT `FKge5lb1w9xvo78dg80k6s3ni27` FOREIGN KEY (`gender`) REFERENCES `gender` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FKi7rut9n9vfl3ac9cxuegckab9` FOREIGN KEY (`country`) REFERENCES `country` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FKny0dyt6qv6m9y7qlcbfdwms58` FOREIGN KEY (`status`) REFERENCES `professional_status` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for activity_area
-- ----------------------------
DROP TABLE IF EXISTS `activity_area`;
CREATE TABLE `activity_area`  (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `label` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `specified` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_8kp9xgj172mu3tjwkq340if7x`(`id_server`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for activity_sector
-- ----------------------------
DROP TABLE IF EXISTS `activity_sector`;
CREATE TABLE `activity_sector`  (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `label` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `specified` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_g3clcptstkdfkbdsa9s5pxatc`(`id_server`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for batch_job_execution
-- ----------------------------
DROP TABLE IF EXISTS `batch_job_execution`;
CREATE TABLE `batch_job_execution`  (
  `JOB_EXECUTION_ID` bigint(20) NOT NULL,
  `VERSION` bigint(20) NULL DEFAULT NULL,
  `JOB_INSTANCE_ID` bigint(20) NOT NULL,
  `CREATE_TIME` datetime(0) NOT NULL,
  `START_TIME` datetime(0) NULL DEFAULT NULL,
  `END_TIME` datetime(0) NULL DEFAULT NULL,
  `STATUS` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `EXIT_CODE` varchar(2500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `EXIT_MESSAGE` varchar(2500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `LAST_UPDATED` datetime(0) NULL DEFAULT NULL,
  `JOB_CONFIGURATION_LOCATION` varchar(2500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`JOB_EXECUTION_ID`) USING BTREE,
  INDEX `JOB_INST_EXEC_FK`(`JOB_INSTANCE_ID`) USING BTREE,
  CONSTRAINT `JOB_INST_EXEC_FK` FOREIGN KEY (`JOB_INSTANCE_ID`) REFERENCES `batch_job_instance` (`JOB_INSTANCE_ID`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for batch_job_execution_context
-- ----------------------------
DROP TABLE IF EXISTS `batch_job_execution_context`;
CREATE TABLE `batch_job_execution_context`  (
  `JOB_EXECUTION_ID` bigint(20) NOT NULL,
  `SHORT_CONTEXT` varchar(2500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `SERIALIZED_CONTEXT` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`JOB_EXECUTION_ID`) USING BTREE,
  CONSTRAINT `JOB_EXEC_CTX_FK` FOREIGN KEY (`JOB_EXECUTION_ID`) REFERENCES `batch_job_execution` (`JOB_EXECUTION_ID`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for batch_job_execution_params
-- ----------------------------
DROP TABLE IF EXISTS `batch_job_execution_params`;
CREATE TABLE `batch_job_execution_params`  (
  `JOB_EXECUTION_ID` bigint(20) NOT NULL,
  `TYPE_CD` varchar(6) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `KEY_NAME` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `STRING_VAL` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `DATE_VAL` datetime(0) NULL DEFAULT NULL,
  `LONG_VAL` bigint(20) NULL DEFAULT NULL,
  `DOUBLE_VAL` double NULL DEFAULT NULL,
  `IDENTIFYING` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  INDEX `JOB_EXEC_PARAMS_FK`(`JOB_EXECUTION_ID`) USING BTREE,
  CONSTRAINT `JOB_EXEC_PARAMS_FK` FOREIGN KEY (`JOB_EXECUTION_ID`) REFERENCES `batch_job_execution` (`JOB_EXECUTION_ID`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for batch_job_execution_seq
-- ----------------------------
DROP TABLE IF EXISTS `batch_job_execution_seq`;
CREATE TABLE `batch_job_execution_seq`  (
  `ID` bigint(20) NOT NULL,
  `UNIQUE_KEY` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  UNIQUE INDEX `UNIQUE_KEY_UN`(`UNIQUE_KEY`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for batch_job_instance
-- ----------------------------
DROP TABLE IF EXISTS `batch_job_instance`;
CREATE TABLE `batch_job_instance`  (
  `JOB_INSTANCE_ID` bigint(20) NOT NULL,
  `VERSION` bigint(20) NULL DEFAULT NULL,
  `JOB_NAME` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `JOB_KEY` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`JOB_INSTANCE_ID`) USING BTREE,
  UNIQUE INDEX `JOB_INST_UN`(`JOB_NAME`, `JOB_KEY`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for batch_job_seq
-- ----------------------------
DROP TABLE IF EXISTS `batch_job_seq`;
CREATE TABLE `batch_job_seq`  (
  `ID` bigint(20) NOT NULL,
  `UNIQUE_KEY` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  UNIQUE INDEX `UNIQUE_KEY_UN`(`UNIQUE_KEY`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for batch_step_execution
-- ----------------------------
DROP TABLE IF EXISTS `batch_step_execution`;
CREATE TABLE `batch_step_execution`  (
  `STEP_EXECUTION_ID` bigint(20) NOT NULL,
  `VERSION` bigint(20) NOT NULL,
  `STEP_NAME` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `JOB_EXECUTION_ID` bigint(20) NOT NULL,
  `START_TIME` datetime(0) NOT NULL,
  `END_TIME` datetime(0) NULL DEFAULT NULL,
  `STATUS` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `COMMIT_COUNT` bigint(20) NULL DEFAULT NULL,
  `READ_COUNT` bigint(20) NULL DEFAULT NULL,
  `FILTER_COUNT` bigint(20) NULL DEFAULT NULL,
  `WRITE_COUNT` bigint(20) NULL DEFAULT NULL,
  `READ_SKIP_COUNT` bigint(20) NULL DEFAULT NULL,
  `WRITE_SKIP_COUNT` bigint(20) NULL DEFAULT NULL,
  `PROCESS_SKIP_COUNT` bigint(20) NULL DEFAULT NULL,
  `ROLLBACK_COUNT` bigint(20) NULL DEFAULT NULL,
  `EXIT_CODE` varchar(2500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `EXIT_MESSAGE` varchar(2500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `LAST_UPDATED` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`STEP_EXECUTION_ID`) USING BTREE,
  INDEX `JOB_EXEC_STEP_FK`(`JOB_EXECUTION_ID`) USING BTREE,
  CONSTRAINT `JOB_EXEC_STEP_FK` FOREIGN KEY (`JOB_EXECUTION_ID`) REFERENCES `batch_job_execution` (`JOB_EXECUTION_ID`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for batch_step_execution_context
-- ----------------------------
DROP TABLE IF EXISTS `batch_step_execution_context`;
CREATE TABLE `batch_step_execution_context`  (
  `STEP_EXECUTION_ID` bigint(20) NOT NULL,
  `SHORT_CONTEXT` varchar(2500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `SERIALIZED_CONTEXT` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`STEP_EXECUTION_ID`) USING BTREE,
  CONSTRAINT `STEP_EXEC_CTX_FK` FOREIGN KEY (`STEP_EXECUTION_ID`) REFERENCES `batch_step_execution` (`STEP_EXECUTION_ID`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for batch_step_execution_seq
-- ----------------------------
DROP TABLE IF EXISTS `batch_step_execution_seq`;
CREATE TABLE `batch_step_execution_seq`  (
  `ID` bigint(20) NOT NULL,
  `UNIQUE_KEY` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  UNIQUE INDEX `UNIQUE_KEY_UN`(`UNIQUE_KEY`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for chat_room
-- ----------------------------
DROP TABLE IF EXISTS `chat_room`;
CREATE TABLE `chat_room`  (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `active` bit(1) NULL DEFAULT NULL,
  `created_at` datetime(0) NOT NULL,
  `deleted` bit(1) NULL DEFAULT NULL,
  `modified_at` datetime(0) NULL DEFAULT NULL,
  `revision` bigint(20) NULL DEFAULT NULL,
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_6yy8c1jy28uxn6ns8v9lkac8r`(`id_server`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for comment
-- ----------------------------
DROP TABLE IF EXISTS `comment`;
CREATE TABLE `comment`  (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `active` bit(1) NULL DEFAULT NULL,
  `created_at` datetime(0) NOT NULL,
  `deleted` bit(1) NULL DEFAULT NULL,
  `modified_at` datetime(0) NULL DEFAULT NULL,
  `revision` bigint(20) NULL DEFAULT NULL,
  `body` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `account` int(11) NULL DEFAULT NULL,
  `post` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_br19krw6o9vo598yx94h63wxd`(`id_server`) USING BTREE,
  INDEX `FKg4pifdpsrn19a8hubl96s2ncp`(`account`) USING BTREE,
  INDEX `FKomrdwc0ub3x7hvvlyu6htn8ti`(`post`) USING BTREE,
  CONSTRAINT `FKg4pifdpsrn19a8hubl96s2ncp` FOREIGN KEY (`account`) REFERENCES `account` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FKomrdwc0ub3x7hvvlyu6htn8ti` FOREIGN KEY (`post`) REFERENCES `post` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for country
-- ----------------------------
DROP TABLE IF EXISTS `country`;
CREATE TABLE `country`  (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `label` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_bs66m8npqqbrydipm9k2ko187`(`id_server`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for degree_obtained
-- ----------------------------
DROP TABLE IF EXISTS `degree_obtained`;
CREATE TABLE `degree_obtained`  (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `active` bit(1) NULL DEFAULT NULL,
  `created_at` datetime(0) NOT NULL,
  `deleted` bit(1) NULL DEFAULT NULL,
  `modified_at` datetime(0) NULL DEFAULT NULL,
  `revision` bigint(20) NULL DEFAULT NULL,
  `city` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `current_position` bit(1) NULL DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `establishment_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `specialty` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `account` int(11) NULL DEFAULT NULL,
  `end_date` datetime(0) NOT NULL,
  `start_date` datetime(0) NOT NULL,
  `diploma` int(11) NULL DEFAULT NULL,
  `establishment_type` int(11) NULL DEFAULT NULL,
  `teaching_area` int(11) NULL DEFAULT NULL,
  `another_diploma` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `another_establishment_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_it7wk6xlws8hdylq3dieky5ut`(`id_server`) USING BTREE,
  INDEX `FKfk1i6e7r5qsqj3w741eluxj48`(`diploma`) USING BTREE,
  INDEX `FKkiktrv9qgmrh2sthbfx0hkd7u`(`establishment_type`) USING BTREE,
  INDEX `FK75axn0e4e11xu6ny1kvaebh4g`(`teaching_area`) USING BTREE,
  INDEX `FK_kx2faddje0w3cixn027phkl7b`(`account`) USING BTREE,
  CONSTRAINT `FK75axn0e4e11xu6ny1kvaebh4g` FOREIGN KEY (`teaching_area`) REFERENCES `teaching_area` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_kx2faddje0w3cixn027phkl7b` FOREIGN KEY (`account`) REFERENCES `account` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FKfk1i6e7r5qsqj3w741eluxj48` FOREIGN KEY (`diploma`) REFERENCES `diploma` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FKkiktrv9qgmrh2sthbfx0hkd7u` FOREIGN KEY (`establishment_type`) REFERENCES `establishment_type` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for diploma
-- ----------------------------
DROP TABLE IF EXISTS `diploma`;
CREATE TABLE `diploma`  (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `label` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `education` int(11) NULL DEFAULT NULL,
  `specified` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_gi7a8g81d35pkdo1xabrrc294`(`id_server`) USING BTREE,
  INDEX `FKtc8s0lrn5ie925rom0xka5hp`(`education`) USING BTREE,
  CONSTRAINT `FKtc8s0lrn5ie925rom0xka5hp` FOREIGN KEY (`education`) REFERENCES `education` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for discipline
-- ----------------------------
DROP TABLE IF EXISTS `discipline`;
CREATE TABLE `discipline`  (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `label` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_5hj653mpeo2wjao494f7p1w2w`(`id_server`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for education
-- ----------------------------
DROP TABLE IF EXISTS `education`;
CREATE TABLE `education`  (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `code` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `label` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_rxld3r7vdl36kn9gej7eu65b7`(`id_server`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for establishment_type
-- ----------------------------
DROP TABLE IF EXISTS `establishment_type`;
CREATE TABLE `establishment_type`  (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `label` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `education` int(11) NULL DEFAULT NULL,
  `specified` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_8tc237adw2hrai8twqk5lu5kf`(`id_server`) USING BTREE,
  INDEX `FK49cpx0mji0tomjon7caxro4eg`(`education`) USING BTREE,
  CONSTRAINT `FK49cpx0mji0tomjon7caxro4eg` FOREIGN KEY (`education`) REFERENCES `education` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for follower
-- ----------------------------
DROP TABLE IF EXISTS `follower`;
CREATE TABLE `follower`  (
  `account_id` int(11) NOT NULL,
  `follower_id` int(11) NOT NULL,
  PRIMARY KEY (`account_id`, `follower_id`) USING BTREE,
  INDEX `FK1ychsfw8wfahvaewkb8oiowii`(`follower_id`) USING BTREE,
  CONSTRAINT `FK1ychsfw8wfahvaewkb8oiowii` FOREIGN KEY (`follower_id`) REFERENCES `account` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FKtblw5bbu6gynu89dd9e39vr68` FOREIGN KEY (`account_id`) REFERENCES `account` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for gender
-- ----------------------------
DROP TABLE IF EXISTS `gender`;
CREATE TABLE `gender`  (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `label` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_pmsrnxn4kayxewyfw2vp1rht4`(`id_server`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for hibernate_sequence
-- ----------------------------
DROP TABLE IF EXISTS `hibernate_sequence`;
CREATE TABLE `hibernate_sequence`  (
  `next_val` bigint(20) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for higher_education
-- ----------------------------
DROP TABLE IF EXISTS `higher_education`;
CREATE TABLE `higher_education`  (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `active` bit(1) NULL DEFAULT NULL,
  `created_at` datetime(0) NOT NULL,
  `deleted` bit(1) NULL DEFAULT NULL,
  `modified_at` datetime(0) NULL DEFAULT NULL,
  `revision` bigint(20) NULL DEFAULT NULL,
  `city` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `current_position` bit(1) NULL DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `establishment_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `specialty` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `account` int(11) NULL DEFAULT NULL,
  `establishment_type` int(11) NULL DEFAULT NULL,
  `study_level` int(11) NULL DEFAULT NULL,
  `targeted_diploma` int(11) NULL DEFAULT NULL,
  `teaching_area` int(11) NULL DEFAULT NULL,
  `another_targeted_diploma` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `another_study_level` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `another_establishment_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_h3tahd3n1q4kp7v3xwxxueoud`(`id_server`) USING BTREE,
  INDEX `FKa908thtmqa8crsydwj32f1jjl`(`targeted_diploma`) USING BTREE,
  INDEX `FK8mst42dw0j0ejjp0qh831ty29`(`teaching_area`) USING BTREE,
  INDEX `FK_1owtti119f95r1vv9oc7xnbml`(`establishment_type`) USING BTREE,
  INDEX `FK_kiwsm8pa9yd00er0xivjymk20`(`study_level`) USING BTREE,
  INDEX `FK_th7r3t3j275wb1g1mmxkko829`(`account`) USING BTREE,
  CONSTRAINT `FK8mst42dw0j0ejjp0qh831ty29` FOREIGN KEY (`teaching_area`) REFERENCES `teaching_area` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_1owtti119f95r1vv9oc7xnbml` FOREIGN KEY (`establishment_type`) REFERENCES `establishment_type` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_kiwsm8pa9yd00er0xivjymk20` FOREIGN KEY (`study_level`) REFERENCES `study_level` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_th7r3t3j275wb1g1mmxkko829` FOREIGN KEY (`account`) REFERENCES `account` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FKa908thtmqa8crsydwj32f1jjl` FOREIGN KEY (`targeted_diploma`) REFERENCES `diploma` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for infos_media_type
-- ----------------------------
DROP TABLE IF EXISTS `infos_media_type`;
CREATE TABLE `infos_media_type`  (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `color` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `position` tinyint(4) NOT NULL,
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_666irr8e1spjsdlf4en8q0i2m`(`id_server`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for level
-- ----------------------------
DROP TABLE IF EXISTS `level`;
CREATE TABLE `level`  (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `description` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `grade` int(11) NOT NULL,
  `icon` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `label` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `mastered` bit(1) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_avbprobsmujuclg54t1crl3ip`(`id_server`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for like_post
-- ----------------------------
DROP TABLE IF EXISTS `like_post`;
CREATE TABLE `like_post`  (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `account` int(11) NULL DEFAULT NULL,
  `post` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_q3fmv0m2w7d8ntnmn3hv3wo37`(`id_server`) USING BTREE,
  INDEX `FKn3fih30t2357eiijmd0etsrl9`(`account`) USING BTREE,
  INDEX `FKrasyhkem81o3qsxvm2t8do4t4`(`post`) USING BTREE,
  CONSTRAINT `FKn3fih30t2357eiijmd0etsrl9` FOREIGN KEY (`account`) REFERENCES `account` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FKrasyhkem81o3qsxvm2t8do4t4` FOREIGN KEY (`post`) REFERENCES `post` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for link
-- ----------------------------
DROP TABLE IF EXISTS `link`;
CREATE TABLE `link`  (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `active` bit(1) NULL DEFAULT NULL,
  `created_at` datetime(0) NOT NULL,
  `deleted` bit(1) NULL DEFAULT NULL,
  `modified_at` datetime(0) NULL DEFAULT NULL,
  `revision` bigint(20) NULL DEFAULT NULL,
  `link_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `account` int(11) NULL DEFAULT NULL,
  `link` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_muo7t66qy7bjxt2mpgk9u8doq`(`id_server`) USING BTREE,
  INDEX `FKgcicrmmae2kfvim5u85a5n8b`(`account`) USING BTREE,
  INDEX `FKbe6t25ltf7ig4hla2jqo437ss`(`link`) USING BTREE,
  CONSTRAINT `FKbe6t25ltf7ig4hla2jqo437ss` FOREIGN KEY (`link`) REFERENCES `post` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FKgcicrmmae2kfvim5u85a5n8b` FOREIGN KEY (`account`) REFERENCES `account` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for media_subtype
-- ----------------------------
DROP TABLE IF EXISTS `media_subtype`;
CREATE TABLE `media_subtype`  (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `label` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `media_type` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_fpk6ncx36twffmkqpvpie81o`(`id_server`) USING BTREE,
  INDEX `FKgjun9jfynng9l72q9v8kkj6y9`(`media_type`) USING BTREE,
  CONSTRAINT `FKgjun9jfynng9l72q9v8kkj6y9` FOREIGN KEY (`media_type`) REFERENCES `media_type` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for media_type
-- ----------------------------
DROP TABLE IF EXISTS `media_type`;
CREATE TABLE `media_type`  (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `label` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_at21ep413yoo6ja3927db4x94`(`id_server`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for message
-- ----------------------------
DROP TABLE IF EXISTS `message`;
CREATE TABLE `message`  (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `active` bit(1) NULL DEFAULT NULL,
  `created_at` datetime(0) NOT NULL,
  `deleted` bit(1) NULL DEFAULT NULL,
  `modified_at` datetime(0) NULL DEFAULT NULL,
  `revision` bigint(20) NULL DEFAULT NULL,
  `content` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `account` int(11) NULL DEFAULT NULL,
  `chat_room` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_9uaa6xfb0ls864o6hw0dspxuy`(`id_server`) USING BTREE,
  INDEX `FKgl02ife6trs9dhm9bjnokj6j`(`account`) USING BTREE,
  INDEX `FK3ydah2qgeg9o0gl4wnbf4i13y`(`chat_room`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for message_receiver
-- ----------------------------
DROP TABLE IF EXISTS `message_receiver`;
CREATE TABLE `message_receiver`  (
  `message_id` int(11) NOT NULL,
  `receiver_id` int(11) NOT NULL,
  INDEX `FK672ecccq4c37r2ycydqpavt82`(`receiver_id`) USING BTREE,
  INDEX `FKohdwj2yfmb91ww1f9rtx9ggl2`(`message_id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Fixed;

-- ----------------------------
-- Table structure for multimedia
-- ----------------------------
DROP TABLE IF EXISTS `multimedia`;
CREATE TABLE `multimedia`  (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `active` bit(1) NULL DEFAULT NULL,
  `created_at` datetime(0) NOT NULL,
  `deleted` bit(1) NULL DEFAULT NULL,
  `modified_at` datetime(0) NULL DEFAULT NULL,
  `revision` bigint(20) NULL DEFAULT NULL,
  `checksum` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `directory` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `extension` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `mime_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `account` int(11) NULL DEFAULT NULL,
  `multimedia` int(11) NULL DEFAULT NULL,
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_fjsq1euaq26h44sxodq1duswj`(`id_server`) USING BTREE,
  INDEX `FKjp62ph2bdbrb6kpcxswhvhy85`(`account`) USING BTREE,
  INDEX `FK51vq8lev5q0usgnbgb3abbb9p`(`multimedia`) USING BTREE,
  CONSTRAINT `FK51vq8lev5q0usgnbgb3abbb9p` FOREIGN KEY (`multimedia`) REFERENCES `post` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FKjp62ph2bdbrb6kpcxswhvhy85` FOREIGN KEY (`account`) REFERENCES `account` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for notification
-- ----------------------------
DROP TABLE IF EXISTS `notification`;
CREATE TABLE `notification`  (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `active` bit(1) NULL DEFAULT NULL,
  `created_at` datetime(0) NOT NULL,
  `deleted` bit(1) NULL DEFAULT NULL,
  `modified_at` datetime(0) NULL DEFAULT NULL,
  `revision` bigint(20) NULL DEFAULT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `account` int(11) NULL DEFAULT NULL,
  `notification_type` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_p0mtvr8el2y24evbc4luohyse`(`id_server`) USING BTREE,
  INDEX `FK7x83li73i5gcugpvreigaxb70`(`account`) USING BTREE,
  INDEX `FK7uj37jdhyuu7kwi93udpthnlt`(`notification_type`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for notification_map
-- ----------------------------
DROP TABLE IF EXISTS `notification_map`;
CREATE TABLE `notification_map`  (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `id_element` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `notification` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_ouimwrjucxm7phmm4pcel1mrl`(`id_server`) USING BTREE,
  INDEX `FK2a1uqng2jt9u2ts5nbojw5u1f`(`notification`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for notification_type
-- ----------------------------
DROP TABLE IF EXISTS `notification_type`;
CREATE TABLE `notification_type`  (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `label` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_fvwb72ftjxlt87a2k3wybw868`(`id_server`) USING BTREE,
  UNIQUE INDEX `UK_mn9ocefheprdywd5nfhu6ne0v`(`label`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for post
-- ----------------------------
DROP TABLE IF EXISTS `post`;
CREATE TABLE `post`  (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `active` bit(1) NULL DEFAULT NULL,
  `created_at` datetime(0) NOT NULL,
  `deleted` bit(1) NULL DEFAULT NULL,
  `modified_at` datetime(0) NULL DEFAULT NULL,
  `revision` bigint(20) NULL DEFAULT NULL,
  `description` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `keywords` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `account` int(11) NULL DEFAULT NULL,
  `media_subtype` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_hj56xjem2k1cs8nwm1nsh2xjx`(`id_server`) USING BTREE,
  INDEX `FKhldpsq033xe7e406jkt2ovuno`(`account`) USING BTREE,
  INDEX `FKchy449augo3lewkdmh8e9juh4`(`media_subtype`) USING BTREE,
  CONSTRAINT `FKchy449augo3lewkdmh8e9juh4` FOREIGN KEY (`media_subtype`) REFERENCES `media_subtype` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FKhldpsq033xe7e406jkt2ovuno` FOREIGN KEY (`account`) REFERENCES `account` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for professional
-- ----------------------------
DROP TABLE IF EXISTS `professional`;
CREATE TABLE `professional`  (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `active` bit(1) NULL DEFAULT NULL,
  `created_at` datetime(0) NOT NULL,
  `deleted` bit(1) NULL DEFAULT NULL,
  `modified_at` datetime(0) NULL DEFAULT NULL,
  `revision` bigint(20) NULL DEFAULT NULL,
  `city` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `current_position` bit(1) NULL DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `establishment_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `specialty` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `account` int(11) NULL DEFAULT NULL,
  `end_date` datetime(0) NULL DEFAULT NULL,
  `job_title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `start_date` datetime(0) NOT NULL,
  `activity_area` int(11) NULL DEFAULT NULL,
  `activity_sector` int(11) NULL DEFAULT NULL,
  `another_activity_area` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `another_activity_sector` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_jhtn3j550trbpd1t9gvk99ni4`(`id_server`) USING BTREE,
  INDEX `FKi5t5gwrrxhgyjedhvxcekbyxc`(`activity_area`) USING BTREE,
  INDEX `FKqn9wbhf8i3wxe4tfu4x2lo8pm`(`activity_sector`) USING BTREE,
  INDEX `FK_otn62bgvcnopnav5abccfgiu3`(`account`) USING BTREE,
  CONSTRAINT `FK_otn62bgvcnopnav5abccfgiu3` FOREIGN KEY (`account`) REFERENCES `account` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FKi5t5gwrrxhgyjedhvxcekbyxc` FOREIGN KEY (`activity_area`) REFERENCES `activity_area` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FKqn9wbhf8i3wxe4tfu4x2lo8pm` FOREIGN KEY (`activity_sector`) REFERENCES `activity_sector` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for professional_status
-- ----------------------------
DROP TABLE IF EXISTS `professional_status`;
CREATE TABLE `professional_status`  (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `label` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_fc9uvppyqis203m9mqheyyvc6`(`id_server`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for secondary_education
-- ----------------------------
DROP TABLE IF EXISTS `secondary_education`;
CREATE TABLE `secondary_education`  (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `active` bit(1) NULL DEFAULT NULL,
  `created_at` datetime(0) NOT NULL,
  `deleted` bit(1) NULL DEFAULT NULL,
  `modified_at` datetime(0) NULL DEFAULT NULL,
  `revision` bigint(20) NULL DEFAULT NULL,
  `city` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `current_position` bit(1) NULL DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `establishment_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `specialty` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `account` int(11) NULL DEFAULT NULL,
  `establishment_type` int(11) NULL DEFAULT NULL,
  `study_level` int(11) NULL DEFAULT NULL,
  `prepared_diploma` int(11) NULL DEFAULT NULL,
  `another_prepared_diploma` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `another_study_level` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `another_establishment_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_nhxvw6ieoh23429mwaq1qkmsj`(`id_server`) USING BTREE,
  INDEX `FKk3ptaihbdqfncqbwwou95xust`(`prepared_diploma`) USING BTREE,
  INDEX `FK_o2ypntl0oqre1t4gkekwfvtse`(`establishment_type`) USING BTREE,
  INDEX `FK_sbaqi5vxvcxexqg00nj4kubhc`(`study_level`) USING BTREE,
  INDEX `FK_h91sw2cbvc4og7hptcd4fw3ua`(`account`) USING BTREE,
  CONSTRAINT `FK_h91sw2cbvc4og7hptcd4fw3ua` FOREIGN KEY (`account`) REFERENCES `account` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_o2ypntl0oqre1t4gkekwfvtse` FOREIGN KEY (`establishment_type`) REFERENCES `establishment_type` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_sbaqi5vxvcxexqg00nj4kubhc` FOREIGN KEY (`study_level`) REFERENCES `study_level` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FKk3ptaihbdqfncqbwwou95xust` FOREIGN KEY (`prepared_diploma`) REFERENCES `diploma` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for skill
-- ----------------------------
DROP TABLE IF EXISTS `skill`;
CREATE TABLE `skill`  (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `active` bit(1) NULL DEFAULT NULL,
  `created_at` datetime(0) NOT NULL,
  `deleted` bit(1) NULL DEFAULT NULL,
  `modified_at` datetime(0) NULL DEFAULT NULL,
  `revision` bigint(20) NULL DEFAULT NULL,
  `discipline` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `keywords` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `label` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `skill_mastered` bit(1) NULL DEFAULT NULL,
  `account` int(11) NULL DEFAULT NULL,
  `level` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_mgwf4f5epvhru88djnobni1vl`(`id_server`) USING BTREE,
  INDEX `FK25xg7gyx32jtdiuek1fpp4xx9`(`account`) USING BTREE,
  INDEX `FKirhnfwdwcegn8fstgix349moy`(`level`) USING BTREE,
  CONSTRAINT `FK25xg7gyx32jtdiuek1fpp4xx9` FOREIGN KEY (`account`) REFERENCES `account` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FKirhnfwdwcegn8fstgix349moy` FOREIGN KEY (`level`) REFERENCES `level` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for study_level
-- ----------------------------
DROP TABLE IF EXISTS `study_level`;
CREATE TABLE `study_level`  (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `label` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `education` int(11) NULL DEFAULT NULL,
  `specified` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_jmdv843pnvonbtfhom3i794u1`(`id_server`) USING BTREE,
  INDEX `FK19p7f1vp5j6un4jr0u5svnvwp`(`education`) USING BTREE,
  CONSTRAINT `FK19p7f1vp5j6un4jr0u5svnvwp` FOREIGN KEY (`education`) REFERENCES `education` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for subscription
-- ----------------------------
DROP TABLE IF EXISTS `subscription`;
CREATE TABLE `subscription`  (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `followee` int(11) NOT NULL,
  `follower` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_944u2nqdi59b2k1ng769xhmxj`(`id_server`) USING BTREE,
  INDEX `FKryq9bww08kfsgp1jn06ecqwr6`(`followee`) USING BTREE,
  INDEX `FKg2grnwsk37ktk7ixmgm219dr`(`follower`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for teaching_area
-- ----------------------------
DROP TABLE IF EXISTS `teaching_area`;
CREATE TABLE `teaching_area`  (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `label` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `teaching_area_group` int(11) NULL DEFAULT NULL,
  `education` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_ox8ks35vdkc9nd83in6u23a1i`(`id_server`) USING BTREE,
  INDEX `FKr5km3wq5qmfmu05ttam0ojqu3`(`education`) USING BTREE,
  INDEX `FKh0g8a3pq339qtc9oqllryu034`(`teaching_area_group`) USING BTREE,
  CONSTRAINT `FKh0g8a3pq339qtc9oqllryu034` FOREIGN KEY (`teaching_area_group`) REFERENCES `teaching_area_group` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for teaching_area_group
-- ----------------------------
DROP TABLE IF EXISTS `teaching_area_group`;
CREATE TABLE `teaching_area_group`  (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `label` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `teaching_area_set` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_f4h4ji0wfcyp7g7kufslxwbrx`(`id_server`) USING BTREE,
  INDEX `FKqsyfcods699dixtgn4en0q5hd`(`teaching_area_set`) USING BTREE,
  CONSTRAINT `FKqsyfcods699dixtgn4en0q5hd` FOREIGN KEY (`teaching_area_set`) REFERENCES `teaching_area_set` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for teaching_area_set
-- ----------------------------
DROP TABLE IF EXISTS `teaching_area_set`;
CREATE TABLE `teaching_area_set`  (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `label` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `education` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_9qi7qjgv1nw3ocd1b59ph6ah6`(`id_server`) USING BTREE,
  INDEX `FK2s5sq1scene9139o2ysijp6ij`(`education`) USING BTREE,
  CONSTRAINT `FK2s5sq1scene9139o2ysijp6ij` FOREIGN KEY (`education`) REFERENCES `education` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for token
-- ----------------------------
DROP TABLE IF EXISTS `token`;
CREATE TABLE `token`  (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `created_at` datetime(0) NOT NULL,
  `token` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `account` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_g6cqlajolqvqq1ft3y2wsfquj`(`id_server`) USING BTREE,
  UNIQUE INDEX `UK_pddrhgwxnms2aceeku9s2ewy5`(`token`) USING HASH,
  INDEX `FKck8dkegqatv6k8iq37s50v8dv`(`account`) USING BTREE,
  CONSTRAINT `FKck8dkegqatv6k8iq37s50v8dv` FOREIGN KEY (`account`) REFERENCES `account` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
