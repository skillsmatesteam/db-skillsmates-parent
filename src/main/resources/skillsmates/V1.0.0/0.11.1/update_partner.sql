
-- ----------------------------
-- Table structure for partner_type
-- ----------------------------
DROP TABLE IF EXISTS `partner_type`;
CREATE TABLE `partner_type`  (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `label` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_qf3o9mh8w0wfnmv7435sud6m5`(`id_server`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of partner_type
-- ----------------------------
INSERT INTO `partner_type` VALUES (1, '106065089548800', 'Partenaire');
INSERT INTO `partner_type` VALUES (2, '106065089548900', 'Ambassadeur');


-- ----------------------------
-- Table structure for partner
-- ----------------------------
DROP TABLE IF EXISTS `partner`;
CREATE TABLE `partner`  (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `active` bit(1) NULL DEFAULT NULL,
  `created_at` datetime(0) NOT NULL,
  `deleted` bit(1) NULL DEFAULT NULL,
  `modified_at` datetime(0) NULL DEFAULT NULL,
  `revision` bigint(20) NULL DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `city` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `phone_number` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `profile_picture` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `activity_area` int(11) NULL DEFAULT NULL,
  `category` int(11) NULL DEFAULT NULL,
  `country` int(11) NULL DEFAULT NULL,
  `partner_type` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_gtl7mpq4t5v5tulxmd74cn7rs`(`id_server`) USING BTREE,
  UNIQUE INDEX `UK_kjhx5hhn0xq0h5lcn987ugv2o`(`email`) USING BTREE,
  INDEX `FKmusx7dedvv62aln9386eaf4fk`(`activity_area`) USING BTREE,
  INDEX `FK17ti6e5w1ftxnbefwdspctvvu`(`category`) USING BTREE,
  INDEX `FK6ioseyip4k7rpgh07l6c7fpx9`(`country`) USING BTREE,
  INDEX `FK16uqtcuv1bs71acia7ilmpue4`(`partner_type`) USING BTREE,
  CONSTRAINT `FK16uqtcuv1bs71acia7ilmpue4` FOREIGN KEY (`partner_type`) REFERENCES `partner_type` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK17ti6e5w1ftxnbefwdspctvvu` FOREIGN KEY (`category`) REFERENCES `category` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK6ioseyip4k7rpgh07l6c7fpx9` FOREIGN KEY (`country`) REFERENCES `country` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FKmusx7dedvv62aln9386eaf4fk` FOREIGN KEY (`activity_area`) REFERENCES `activity_area` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of partner
-- ----------------------------
INSERT INTO `partner` VALUES (1, '11308326636800', b'1', '2021-06-07 15:43:18', b'0', '2021-06-07 15:43:24', 0, 'Mombiman', 'Yaoundé', 'Yamo Group', '', 'Yamo', '699997744', 'avatar_e48193cc.png', 737, 3, 11, 1);
INSERT INTO `partner` VALUES (2, '1732808833502544', b'1', '2021-06-07 15:59:03', b'0', '2021-06-07 15:56:53', 0, 'Marché Central', 'Yaoundé', 'Luckas est une jeune startup de nouvelle technologie', 'luckas@gmail.com', 'Luckas', '222568874', 'avatar_d1d9b580.png', 741, 2, 11, 1);


-- ----------------------------
-- Table structure for category
-- ----------------------------
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category`  (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `label` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_l0yowkkrpomqexldk59av50k3`(`id_server`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of category
-- ----------------------------
INSERT INTO `category` VALUES (1, '106065088539600', 'Ecole');
INSERT INTO `category` VALUES (2, '106065088539700', 'Faculté');
INSERT INTO `category` VALUES (3, '106065088539800', 'Entreprise');

