--
-- Delete fcm_registration_id in account table
--

ALTER TABLE skillsmatesdb.account DROP COLUMN fcm_registration_id;