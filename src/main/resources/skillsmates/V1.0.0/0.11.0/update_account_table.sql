--
-- Delete role in account table
--

ALTER TABLE skillsmatesdb.account ADD role VARCHAR(255) DEFAULT 'USER';
