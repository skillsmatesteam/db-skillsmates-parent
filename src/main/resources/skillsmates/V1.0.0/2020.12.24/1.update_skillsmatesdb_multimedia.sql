-- rename multimedia to multimedia_old
ALTER TABLE skillsmatesdb.multimedia  RENAME TO multimedia_old;  

-- create metadata table
---- run: skillsmatesdb_metadata.sql

-- create multimedia table
---- run: skillsmatesdb_multimedia.sql

-- insert multimedia_old data into multimedia 
INSERT INTO skillsmatesdb.multimedia(id, id_server, active, created_at, deleted, modified_at, revision, checksum, directory, extension, mime_type, name, type, url, account, multimedia)
SELECT id, id_server, active, created_at, deleted, modified_at, revision, checksum, directory, extension, mime_type, name, type, url, account, multimedia FROM skillsmatesdb.multimedia_old;

-- delete multimedia_old table
DROP TABLE skillsmatesdb.multimedia_old;