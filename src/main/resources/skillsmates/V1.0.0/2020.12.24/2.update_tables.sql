 -- deletes link
DROP TABLE skillsmatesdb.link;

 -- add discipline in post
ALTER TABLE skillsmatesdb.post ADD COLUMN `discipline` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;

 -- change picture to image in media_type et multimedia
SET SQL_SAFE_UPDATES = 0;
UPDATE skillsmatesdb.media_type SET label = 'IMAGE' WHERE label = 'PICTURE';
UPDATE skillsmatesdb.multimedia SET type = 'IMAGE' WHERE type = 'PICTURE';
UPDATE skillsmatesdb.multimedia SET type = 'DOCUMENT' WHERE type is null;

 -- add notifiedAccount in notification
---- rune: skillsmates_notification.sql
---- rune: skillsmates_notification_map.sql
---- rune: skillsmates_notification_type.sql
