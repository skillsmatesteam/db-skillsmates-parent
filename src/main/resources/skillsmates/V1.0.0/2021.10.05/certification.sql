/*
 Navicat Premium Data Transfer

 Source Server         : skillmates
 Source Server Type    : MariaDB
 Source Server Version : 100505
 Source Host           : localhost:3306
 Source Schema         : skillsmatesdb

 Target Server Type    : MariaDB
 Target Server Version : 100505
 File Encoding         : 65001

 Date: 08/10/2021 08:39:11
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for certification
-- ----------------------------
DROP TABLE IF EXISTS `certification`;
CREATE TABLE `certification`  (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `active` bit(1) NULL DEFAULT NULL,
  `created_at` datetime(0) NOT NULL,
  `deleted` bit(1) NULL DEFAULT NULL,
  `modified_at` datetime(0) NULL DEFAULT NULL,
  `revision` bigint(20) NULL DEFAULT NULL,
  `another_activity_area` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `another_establishment_certification_type` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `description` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `entitled_certification` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `expiration_date` datetime(0) NULL DEFAULT NULL,
  `is_permanent` tinyint(1) NULL DEFAULT 0,
  `is_temporary` tinyint(1) NULL DEFAULT 0,
  `issuing_institution_name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `obtained_date` datetime(0) NOT NULL,
  `account` int(11) NULL DEFAULT NULL,
  `activity_area` int(11) NULL DEFAULT NULL,
  `establishment_certification_type` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_4lwiaq4c55xgufq2tr0qopq97`(`id_server`) USING BTREE,
  INDEX `FKoy1lftnberkpdr6e8s5ydtdls`(`account`) USING BTREE,
  INDEX `FK864jq4jeli330yslltwefdkd5`(`activity_area`) USING BTREE,
  INDEX `FKcwd4ejo91w2hti1e8m11npyk4`(`establishment_certification_type`) USING BTREE,
  CONSTRAINT `FK864jq4jeli330yslltwefdkd5` FOREIGN KEY (`activity_area`) REFERENCES `activity_area` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FKcwd4ejo91w2hti1e8m11npyk4` FOREIGN KEY (`establishment_certification_type`) REFERENCES `establishment_certification_type` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FKoy1lftnberkpdr6e8s5ydtdls` FOREIGN KEY (`account`) REFERENCES `account` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
