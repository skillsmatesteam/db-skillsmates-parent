/*
 Navicat Premium Data Transfer

 Source Server         : skillmates
 Source Server Type    : MariaDB
 Source Server Version : 100505
 Source Host           : localhost:3306
 Source Schema         : skillsmatesdb

 Target Server Type    : MariaDB
 Target Server Version : 100505
 File Encoding         : 65001

 Date: 05/10/2021 17:23:28
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for establishment_certification_type
-- ----------------------------
DROP TABLE IF EXISTS `establishment_certification_type`;
CREATE TABLE `establishment_certification_type`  (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `description` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `label` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `specified` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_1h2thyvp4xglempesb1q63wm2`(`id_server`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of establishment_certification_type
-- ----------------------------
INSERT INTO `establishment_certification_type` VALUES (721, '105976758462400', 'Instituts et centres de formation', 'Instituts et centres de formation', 0);
INSERT INTO `establishment_certification_type` VALUES (722, '105976766287800', 'Site internet / Plateforme en ligne', 'Site internet / Plateforme en ligne', 0);
INSERT INTO `establishment_certification_type` VALUES (723, '105976768321100', 'Association', 'Association', 0);
INSERT INTO `establishment_certification_type` VALUES (724, '105976770256800', 'Entreprise / Société', 'Entreprise / Société', 0);
INSERT INTO `establishment_certification_type` VALUES (725, '105976772225400', 'Universités et établissements d\'enseignement supérieur', 'Universités et établissements d\'enseignement supérieur', 0);
INSERT INTO `establishment_certification_type` VALUES (726, '105976774834200', 'Lycée général professionnel', 'Lycée général professionnel', 0);
INSERT INTO `establishment_certification_type` VALUES (727, '105976777300700', 'Autre types d\'établissements', 'Autre types d\'établissements', 1);

SET FOREIGN_KEY_CHECKS = 1;
