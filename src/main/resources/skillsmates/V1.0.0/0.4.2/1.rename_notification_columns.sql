-- rename notification to notification_old
ALTER TABLE skillsmatesdb.notification  RENAME TO notification_old;  

-- create notification table
---- run: skillsmatesdb_notification.sql

-- insert notification_old data into notification 
INSERT INTO skillsmatesdb.notification(id, id_server, active, created_at, deleted, modified_at, revision, title, emitter_account, notification_type, receiver_account)
SELECT id, id_server, active, created_at, deleted, modified_at, revision, title, account, notification_type, notified_account FROM skillsmatesdb.notification_old;

-- delete notification_old table
DROP TABLE skillsmatesdb.notification_old;