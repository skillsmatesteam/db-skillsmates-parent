-- rename social_interaction to social_interaction_old
ALTER TABLE skillsmatesdb.social_interaction  RENAME TO social_interaction_old;  

-- create social_interaction table
---- run: skillsmatesdb_social_interaction.sql

-- insert social_interaction_old data into social_interaction 
INSERT INTO skillsmatesdb.social_interaction(id, id_server, active, created_at, deleted, modified_at, revision, content, emitter_account, social_interaction_type, receiver_account)
SELECT id, id_server, active, created_at, deleted, modified_at, revision, content, account, social_interaction_type, NULL FROM skillsmatesdb.social_interaction_old;

-- delete social_interaction_old table
DROP TABLE skillsmatesdb.social_interaction_old;