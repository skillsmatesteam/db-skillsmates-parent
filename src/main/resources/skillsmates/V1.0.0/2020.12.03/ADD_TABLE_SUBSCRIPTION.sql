--
-- Table structure for table `subscription`
--

DROP TABLE IF EXISTS `subscription`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subscription` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `followee` int(11) NOT NULL,
  `follower` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_944u2nqdi59b2k1ng769xhmxj` (`id_server`),
  KEY `FKryq9bww08kfsgp1jn06ecqwr6` (`followee`),
  KEY `FKg2grnwsk37ktk7ixmgm219dr` (`follower`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;