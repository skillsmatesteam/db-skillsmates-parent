-- change size of column biography for account
ALTER TABLE skillsmatesdb.account MODIFY biography VARCHAR (10000);

-- change size of column description for professional
ALTER TABLE skillsmatesdb.professional MODIFY description VARCHAR (5000);

-- change size of column description for secondaryEducation
ALTER TABLE skillsmatesdb.secondaryEducation MODIFY description VARCHAR (5000);

-- change size of column keywords for skill
ALTER TABLE skillsmatesdb.skill MODIFY keywords VARCHAR (5000);
