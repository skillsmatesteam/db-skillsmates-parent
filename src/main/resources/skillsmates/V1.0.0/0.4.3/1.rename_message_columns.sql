-- rename message to message_old
ALTER TABLE skillsmatesdb.message  RENAME TO message_old;  

-- create message table
---- run: skillsmatesdb_message.sql

-- delete notification_old table
DROP TABLE skillsmatesdb.message_old;

-- delete message_receivers
DROP TABLE skillsmatesdb.message_receivers;