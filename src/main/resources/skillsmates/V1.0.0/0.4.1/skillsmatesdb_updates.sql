-- MariaDB dump 10.17  Distrib 10.4.13-MariaDB, for Win64 (AMD64)
--
-- Host: 127.0.0.1    Database: skillsmatesdb
-- ------------------------------------------------------
-- Server version	10.4.13-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `updates`
--

DROP TABLE IF EXISTS `updates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8mb4 */;
CREATE TABLE `updates` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `active` bit(1) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `deleted` bit(1) DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  `revision` bigint(20) DEFAULT NULL,
  `description` varchar(255) NOT NULL,
  `version` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_17s1w3f0sghnyowgy9rm9ji3g` (`id_server`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `updates`
--

LOCK TABLES `updates` WRITE;
/*!40000 ALTER TABLE `updates` DISABLE KEYS */;
INSERT INTO `updates` VALUES (2597,'264006164566700','\0','2021-01-18 10:05:29','\0',NULL,0,'Affichage de l\'alerte des nouvelles mises à jour','0.2.6'),(2598,'264006166085099','\0','2021-01-18 10:05:29','\0',NULL,0,'Mis a jour du batch des comptes online','0.2.6'),(2599,'264006167570500','\0','2021-01-18 10:05:29','\0',NULL,0,'Affichage du nombre de documents partagés','0.2.6'),(2600,'264006168696100','\0','2021-01-18 10:05:29','\0',NULL,0,'Refonte de la page de reception des messages instantanés','0.2.6'),(2601,'264006169824400','\0','2021-01-18 10:05:29','\0',NULL,0,'Résolution du bug sur le reception de l\'email d\'inscription','0.2.6'),(2602,'264006170950400','\0','2021-01-18 10:05:29','\0',NULL,0,'Modification du loader','0.2.6'),(2668,'83777333291600','\0','2021-01-30 07:45:25','\0',NULL,0,'Possiblité de commenter et liker un document dans la rubrique des documents partagés','0.2.7'),(2669,'83777334387800','\0','2021-01-30 07:45:25','\0',NULL,0,'Retirer la possiblité de supprimer un post si ce n\'est pas ton post','0.2.7'),(2670,'83777335534700','\0','2021-01-30 07:45:25','\0',NULL,0,'Envoi d\'email lors d\'un like, un commentaire, nouvel abonné','0.2.7'),(2671,'83777336763000','\0','2021-01-30 07:45:25','\0',NULL,0,'Affichage des notifications des abonnements','0.2.7'),(2672,'83777337823000','\0','2021-01-30 07:45:25','\0',NULL,0,'Mettre PROFIL en minuscule sur le dashboard','0.2.7'),(2673,'83777338945500','\0','2021-01-30 07:45:25','\0',NULL,0,'Inverser envoyeur et recepteur de message dans la rubrique messages instantanés','0.2.7'),(2674,'83777340064100','\0','2021-01-30 07:45:25','\0',NULL,0,'Changer \'Lycée professionel\' par \'Lycée Général et professionnel\' parmi les type d\'etablissement','0.2.7'),(2675,'83777341154600','\0','2021-01-30 07:45:25','\0',NULL,0,'Style sur les mots clés','0.2.7'),(2676,'83777342248200','\0','2021-01-30 07:45:25','\0',NULL,0,'Formatage de la biographie et de la description d\'une publication','0.2.7'),(2677,'83777343451800','\0','2021-01-30 07:45:25','\0',NULL,0,'Nombre d\'elements dans la rubrique de modification de profil','0.2.7'),(2761,'397839981853100','\0','2021-02-02 22:59:21','\0',NULL,0,'Desactiver un compte','0.3.1'),(2762,'397839983098200','\0','2021-02-02 22:59:21','\0',NULL,0,'Supprimer un compte','0.3.1'),(2763,'397839984267700','\0','2021-02-02 22:59:21','\0',NULL,0,'Mot de passe oublié','0.3.1'),(2764,'397839985326200','\0','2021-02-02 22:59:21','\0',NULL,0,'Fix like et commentaire','0.3.1'),(2765,'397839986312800','\0','2021-02-02 22:59:21','\0',NULL,0,'Email de notification (abonné, like, commentaire)','0.3.1'),(2766,'397839987418000','\0','2021-02-02 22:59:21','\0',NULL,0,'Fix email d\'inscription','0.3.1'),(2770,'40223494568200','\0','2021-02-08 19:04:08','\0',NULL,0,'ition profil - Affichage du nombre d\'elements','0.3.2'),(2771,'40223496024500','\0','2021-02-08 19:04:08','\0',NULL,0,'Epaisseur et soulignement des icones de la barre header','0.3.2'),(2772,'40223499687900','\0','2021-02-08 19:04:08','\0',NULL,0,'Scroll des columnes  independamment au tableau de board','0.3.2'),(2773,'40223501266300','\0','2021-02-08 19:04:08','\0',NULL,0,'Lien sur la photo de post','0.3.2'),(2774,'40223503514800','\0','2021-02-08 19:04:08','\0',NULL,0,'Mettre 4 elements dans parcours pro et formations','0.3.2'),(2793,'210815858427000','\0','2021-02-10 18:27:01','\0',NULL,0,'Edition profil - Affichage du nombre d\'elements','0.3.4'),(2794,'210815859822700','\0','2021-02-10 18:27:01','\0',NULL,0,'Epaisseur et soulignement des icones de la barre header','0.3.4'),(2795,'210815861105400','\0','2021-02-10 18:27:01','\0',NULL,0,'Fixer les colonnes laterales lors du scroll','0.3.4'),(2796,'210815862383900','\0','2021-02-10 18:27:01','\0',NULL,0,'Lien sur la photo de post','0.3.4'),(2797,'210815863640200','\0','2021-02-10 18:27:01','\0',NULL,0,'Mettre 4 elements dans parcours pro et formations','0.3.4'),(2798,'210815865199900','\0','2021-02-10 18:27:01','\0',NULL,0,'Affichage des notifications','0.3.4'),(2799,'210815866766600','\0','2021-02-10 18:27:01','\0',NULL,0,'Filtrage dans la partie mon reseau','0.3.4'),(5037,'375198917169400','','2021-02-24 18:37:21','\0',NULL,0,'Partage d\'une publication','0.4.1'),(5038,'375198918270200','','2021-02-24 18:37:21','\0',NULL,0,'Refonte de la page de creation d\'une publication','0.4.1'),(5039,'375198919442300','','2021-02-24 18:37:21','\0',NULL,0,'Ajout des textes informatifs dans les rubriques d\'edition de profil','0.4.1'),(5040,'375198920479600','','2021-02-24 18:37:21','\0',NULL,0,'Envoi des fichier multimedia via les messages instantanées','0.4.1'),(5041,'375198921660700','','2021-02-24 18:37:21','\0',NULL,0,'Mise à jour des formations dans l\'edition profile','0.4.1'),(5042,'376574198549200','','2021-02-24 19:00:16','\0',NULL,0,'Filter les documents par titre','0.4.1');
/*!40000 ALTER TABLE `updates` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-02-25 10:35:43
