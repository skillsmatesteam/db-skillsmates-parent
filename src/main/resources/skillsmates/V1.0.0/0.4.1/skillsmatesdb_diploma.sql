-- MariaDB dump 10.17  Distrib 10.4.13-MariaDB, for Win64 (AMD64)
--
-- Host: 127.0.0.1    Database: skillsmatesdb
-- ------------------------------------------------------
-- Server version	10.4.13-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `diploma`
--

DROP TABLE IF EXISTS `diploma`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8mb4 */;
CREATE TABLE `diploma` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `label` varchar(255) NOT NULL,
  `education` int(11) DEFAULT NULL,
  `specified` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_gi7a8g81d35pkdo1xabrrc294` (`id_server`),
  KEY `FKtc8s0lrn5ie925rom0xka5hp` (`education`),
  CONSTRAINT `FKtc8s0lrn5ie925rom0xka5hp` FOREIGN KEY (`education`) REFERENCES `education` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `diploma`
--

LOCK TABLES `diploma` WRITE;
/*!40000 ALTER TABLE `diploma` DISABLE KEYS */;
INSERT INTO `diploma` VALUES (1105,'78869941664100','Brevet Nationale des collèges','Brevet Nationale des collèges',438,0),(1106,'78869956146500','Baccalauréat Scientifique','Baccalauréat Scientifique',438,0),(1107,'78869959061000','Baccalauréat Sciences économique et sociales','Baccalauréat Sciences économique et sociales',438,0),(1108,'78869961473600','Baccalauréat littéraire','Baccalauréat littéraire',438,0),(1109,'78869964118200','Baccalauréat Sciences et technologies du management et de la gestion (STMG)','Baccalauréat Sciences et technologies du management et de la gestion (STMG)',438,0),(1110,'78869966879700','Baccalauréat sciences et technologies du design et des arts appliqués (STD2A)','Baccalauréat sciences et technologies du design et des arts appliqués (STD2A)',438,0),(1111,'78869969180200','Baccalauréat sciences et technologies de l\'industrie et du développement durable (STI2D)','Baccalauréat sciences et technologies de l\'industrie et du développement durable (STI2D)',438,0),(1112,'78869972356300','Baccalauréat sciences et technologies de laboratoire (STL)','Baccalauréat sciences et technologies de laboratoire (STL)',438,0),(1113,'78869975100500','Baccalauréat sciences et technologies de la santé et du social ST2S','Baccalauréat sciences et technologies de la santé et du social ST2S',438,0),(1114,'78869977864500','Baccalauréat sciences et technologies de l\'agronomie et du vivant (STAV)','Baccalauréat sciences et technologies de l\'agronomie et du vivant (STAV)',438,0),(1115,'78869980719700','Baccalauréat technique de la musique et de la danse (TMD)','Baccalauréat technique de la musique et de la danse (TMD)',438,0),(1116,'78869984143500','Baccalauréat sciences et technologies de l\'hôtellerie et de la restauration (STHR)','Baccalauréat sciences et technologies de l\'hôtellerie et de la restauration (STHR)',438,0),(1118,'78869990113600','CAP (Bac -1)','CAP (Bac -1)',438,0),(1119,'78869993327800','BEP (Bac -1)','BEP (Bac -1)',438,0),(1120,'78869996337600','Baccalauréat professionnel (à préciser manuellement)','Baccalauréat professionnel (à préciser manuellement)',438,1),(1121,'78869999481000','BTS (Bac +2)','BTS (Bac +2)',439,0),(1122,'78870002472500','DUT (BAC +2)','DUT (BAC +2)',439,0),(1123,'78870005318400','Licence Professionnelle (Bac +3)','Licence Professionnelle (Bac +3)',439,0),(1124,'78870007888100','Licence Générale (Bac+3)','Licence Générale (Bac+3)',439,0),(1125,'78870011010000','Maîtrise (Bac +4)','Maîtrise (Bac +4)',439,0),(1126,'78870015353500','Bachelor (Bac +4)','Bachelor (Bac +4)',439,0),(1127,'78870017894900','Master Professionnel (Bac +5)','Master Professionnel (Bac +5)',439,0),(1128,'78870020394000','Master Recherche (Bac +5)','Master Recherche (Bac +5)',439,0),(1129,'78870023428400','Magistère (Bac +5)','Magistère (Bac +5)',439,0),(1130,'78870026154400','Bac +6 (à préciser manuellement)','Bac +6 (à préciser manuellement)',439,0),(1131,'78870028624800','Bac +7 (à préciser manuellement)','Bac +7 (à préciser manuellement)',439,0),(1132,'78870030917300','Doctorat (Bac +8)','Doctorat (Bac +8)',439,0),(1134,'78870034829100','Docteur en Pharmacie (Bac+6)','Docteur en Pharmacie (Bac+6)',439,0),(1135,'78870037419600','Docteur en Pharmacie Spécialisé (Bac+9)','Docteur en Pharmacie Spécialisé (Bac+9)',439,0),(1136,'78870040082900','Docteur en Médecine (Bac +9)','Docteur en Médecine (Bac +9)',439,0),(1137,'78870042801900','Docteur en chirurgie dentaire Spécialisé (Bac+10)','Docteur en chirurgie dentaire Spécialisé (Bac+10)',439,0),(1138,'78870045169800','Docteur en médecine spécialisé (Bac +11)','Docteur en médecine spécialisé (Bac +11)',439,0),(1139,'78870047543400','Autres (à sélectionner de bac +1 à Bac +11 et à préciser)','Autres (à sélectionner de bac +1 à Bac +11 et à préciser)',439,1),(4991,'370957593135100','Autres type de diplôme (à préciser manuellement)','Autres type de diplôme (à préciser manuellement)',438,1),(4992,'370957598496500','Baccalauréat professionnel (précisez la spécialité manuellement)','Baccalauréat professionnel (précisez la spécialité manuellement)',438,1);
/*!40000 ALTER TABLE `diploma` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-02-25 10:35:43
