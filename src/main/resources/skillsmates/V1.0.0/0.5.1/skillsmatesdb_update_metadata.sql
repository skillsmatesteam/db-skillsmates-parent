ALTER TABLE skillsmatesdb.metadata ADD active bit(1) DEFAULT true;
ALTER TABLE skillsmatesdb.metadata ADD created_at datetime DEFAULT NOW();
ALTER TABLE skillsmatesdb.metadata ADD deleted bit(1) DEFAULT false;
ALTER TABLE skillsmatesdb.metadata ADD modified_at datetime DEFAULT NULL;
ALTER TABLE skillsmatesdb.metadata ADD revision bigint(20) DEFAULT 0;