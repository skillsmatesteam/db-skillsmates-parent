-- MariaDB dump 10.17  Distrib 10.4.13-MariaDB, for Win64 (AMD64)
--
-- Host: 127.0.0.1    Database: skillsmatesdb
-- ------------------------------------------------------
-- Server version	10.4.13-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `account`
--

DROP TABLE IF EXISTS `account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `active` bit(1) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `deleted` bit(1) DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  `revision` bigint(20) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `biography` varchar(2000) DEFAULT NULL,
  `birthdate` datetime DEFAULT NULL,
  `city` varchar(30) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `follower` int(11) DEFAULT 0,
  `following` int(11) DEFAULT 0,
  `lastname` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `phone_number` varchar(20) DEFAULT NULL,
  `profile_picture` varchar(255) DEFAULT NULL,
  `publication` int(11) DEFAULT 0,
  `country` int(11) DEFAULT NULL,
  `gender` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_iyeg6x6r8s959nhdt76efqc6u` (`id_server`),
  UNIQUE KEY `UK_q0uja26qgu1atulenwup9rxyr` (`email`),
  KEY `FKi7rut9n9vfl3ac9cxuegckab9` (`country`),
  KEY `FKge5lb1w9xvo78dg80k6s3ni27` (`gender`),
  KEY `FKny0dyt6qv6m9y7qlcbfdwms58` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account`
--

LOCK TABLES `account` WRITE;
/*!40000 ALTER TABLE `account` DISABLE KEYS */;
INSERT INTO `account` VALUES (1,'11308326636800','','2020-09-27 21:02:47','\0',NULL,0,NULL,'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.','2002-02-02 01:00:00','Yaounde',NULL,'belinda.mengue@yopmail.com','Belinda',1,1,'MENGUE','9e00068ca1fe3558a1e85c241fb05fcb6e4d0464effd36d1eb8a985817bb9df0839b1cbb1c71d86bda1b5e71ae1a29cf2c2bd6953314a6f1e776e02e908f268a',NULL,'avatar_3a23149a.jpg',0,48,3,5),(591,'67665637080700','','2020-09-28 12:41:54','\0',NULL,0,NULL,'Chimamanda Ngozi Adichie grew up in Nigeria.\n\nHer work has been translated into over thirty languages and has appeared in various publications, including The New Yorker, Granta, The O. Henry Prize Stories, the Financial Times, and Zoetrope. She is the author of the novels Purple Hibiscus, which won the Commonwealth Writers’ Prize and the Hurston/Wright Legacy Award; Half of a Yellow Sun, which won the Orange Prize and was a National Book Critics Circle Award Finalist and a New York Times Notable Book; and Americanah, which won the National Book Critics Circle Award and was named one of The New York Times Top Ten Best Books of 2013. Ms. Adichie is also the author of the story collection The Thing Around Your Neck.\n\nMs. Adichie has been invited to speak around the world. Her 2009 TED Talk, The Danger of A Single Story, is now one of the most-viewed TED Talks of all time. Her 2012 talk We Should All Be Feminists has a started a worldwide conversation about feminism, and was published as a book in 2014.\n\nHer most recent book, Dear Ijeawele, or a Feminist Manifesto in Fifteen Suggestions, was published in March 2017.\n\nA recipient of a MacArthur Foundation Fellowship, Ms. Adichie divides her time between the United States and Nigeria.\n\nFor a detailed bibliography, please see the independent “The Chimamanda Ngozi Adichie Website” maintained by Daria Tunca.','1977-08-15 02:00:00','Lagos',NULL,'chimamanda.ngozi@yopmail.com','Chimamanda',6,6,'NGOZI ADICHIE','f34847f857131ada50de3c9d944f849a57578df75704f637e58ec9a8f316e85ff9157d94f3e73ef948a19289170c42cf65c8f1d655767e65a6603fc4ce8c1523',NULL,'',0,169,3,6),(592,'67711260684600','','2020-09-28 12:42:40','\0',NULL,0,NULL,'Winnie Mandela was the controversial wife of Nelson Mandela who spent her life in varying governmental roles.\nWho Was Winnie Mandela?\nWinnie Mandela embarked on a career of social work that led to her involvement in activism. She married African National Congress leader Nelson Mandela in 1958, though he was imprisoned for much of their four decades of marriage. Winnie became president of the ANC Women\'s League in 1993, and the following year she was elected to Parliament. However, her accomplishments were also tainted by convictions for kidnapping and fraud. She passed away on April 2, 2018, in Johannesburg‚ South Africa. \n\nEarly Life and Career\nBorn Nomzamo Winifred Madikizela on September 26, 1936, in Bizana, a rural village in the Transkei district of South Africa, Winnie eventually moved to Johannesburg in 1953 to study at the Jan Hofmeyr School of Social Work. South Africa was under the system known as apartheid, where citizens of Indigenous African descent were subjected to a harsh caste system, while European descendants enjoyed much higher levels of wealth, health and social freedom.\n\nWinnie completed her studies and, though receiving a scholarship to study in America, decided instead to work as the first Black medical social worker at Baragwanath Hospital in Johannesburg. A dedicated professional, she came to learn via her fieldwork of the deplorable state that many of her patients lived in.','1936-09-26 01:00:00','Johannesburg',NULL,'wunnie.mandela@yopmail.com','Wunnie',6,6,'MANDELA','50b81b72ec10002df3de34d77be60573f130f4ae68416b0b2dd952fec4ace93e42e60f0434e85031afd7e610ce457175661eb58782779d8e0b58768c5af756f5',NULL,'',0,9,3,7);
/*!40000 ALTER TABLE `account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `activity_area`
--

DROP TABLE IF EXISTS `activity_area`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `activity_area` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `label` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_8kp9xgj172mu3tjwkq340if7x` (`id_server`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activity_area`
--

LOCK TABLES `activity_area` WRITE;
/*!40000 ALTER TABLE `activity_area` DISABLE KEYS */;
INSERT INTO `activity_area` VALUES (734,'106065088539600','Administratif','Administratif'),(735,'106065089547500','Aéronautique','Aéronautique'),(736,'106065090395600','Agriculture','Agriculture'),(737,'106065091150500','Agroalimentaire','Agroalimentaire'),(738,'106065091872100','Architecte – BTP – Urbanisme','Architecte – BTP – Urbanisme'),(739,'106065092578300','Armée – Défense','Armée – Défense'),(740,'106065093341900','Art','Art'),(741,'106065094167400','Artisanat','Artisanat'),(742,'106065095038900','Assurance','Assurance'),(743,'106065095871300','Audiovisuel – Cinéma','Audiovisuel – Cinéma'),(744,'106065096653100','Automobile','Automobile'),(745,'106065097484700','Banque-Finance','Banque-Finance'),(746,'106065098251600','Chimie – Biologie','Chimie – Biologie'),(747,'106065098972200','Commerce – Vente – Distribution','Commerce – Vente – Distribution'),(748,'106065099689500','Communication','Communication'),(749,'106065100837600','Comptabilité – Gestion','Comptabilité – Gestion'),(750,'106065101724600','Création','Création'),(751,'106065102459800','Culture','Culture'),(752,'106065103298000','Droit – Juridique – Justice','Droit – Juridique – Justice'),(753,'106065104314100','Économie','Économie'),(754,'106065105063400','Edition et Métier du livre','Edition et Métier du livre'),(755,'106065106826900','Énergie','Énergie'),(756,'106065107655500','Enseignement','Enseignement'),(757,'106065108479900','Environnement – Développement durable','Environnement – Développement durable'),(758,'106065109220300','Esthétique – Beauté – Coiffure','Esthétique – Beauté – Coiffure'),(759,'106065110059100','Évènementiel','Évènementiel'),(760,'106065110818700','Fonction public – Management public','Fonction public – Management public'),(761,'106065111533900','Hôtellerie – restauration','Hôtellerie – restauration'),(762,'106065112282900','Humanitaire','Humanitaire'),(763,'106065113051400','Immobilier','Immobilier'),(764,'106065113914800','Industrie','Industrie'),(765,'106065114743800','Informatique – Électronique – Numérique','Informatique – Électronique – Numérique'),(766,'106065115576900','Internet – Web','Internet – Web'),(767,'106065116389200','Jeux vidéo – Esport – Gaming','Jeux vidéo – Esport – Gaming'),(768,'106065117344500','Journalisme','Journalisme'),(769,'106065118153200','Luxe','Luxe'),(770,'106065119000100','Métiers Animaliers','Métiers Animaliers'),(771,'106065119745600','Mode – Textile','Mode – Textile'),(772,'106065120589700','Musique','Musique'),(773,'106065121370300','Paramédical','Paramédical'),(774,'106065122137500','Psychologie','Psychologie'),(775,'106065122957400','Publicité – Marketing','Publicité – Marketing'),(776,'106065124095200','Ressources Humaines','Ressources Humaines'),(777,'106065124988400','Santé','Santé'),(778,'106065125752800','Secrétariat – Assistance','Secrétariat – Assistance'),(779,'106065126460200','Sécurité','Sécurité'),(780,'106065127108700','Social','Social'),(781,'106065127833500','Sport','Sport'),(782,'106065128594800','Tourisme','Tourisme'),(783,'106065129387500','Transport - Logistique','Transport - Logistique'),(784,'106065130134200','Autres (à préciser manuellement)','Autres (à préciser manuellement)');
/*!40000 ALTER TABLE `activity_area` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `activity_sector`
--

DROP TABLE IF EXISTS `activity_sector`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `activity_sector` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `label` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_g3clcptstkdfkbdsa9s5pxatc` (`id_server`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activity_sector`
--

LOCK TABLES `activity_sector` WRITE;
/*!40000 ALTER TABLE `activity_sector` DISABLE KEYS */;
INSERT INTO `activity_sector` VALUES (409,'12373645608500','Administration, Public et Association','Administration, Public et Association'),(410,'12373646634500','Agriculture et Agroalimentaire','Agriculture et Agroalimentaire'),(411,'12373647658100','Artisanat','Artisanat'),(412,'12373648552900','Banque, Finance et Assurance','Banque, Finance et Assurance'),(413,'12373649724700','Automobile, aéronautiques et autres matériel de transport','Automobile, aéronautiques et autres matériel de transport'),(414,'12373650917700','BTP et Construction','BTP et Construction'),(415,'12373651712800','Commerce et Distribution','Commerce et Distribution'),(416,'12373652669300','Conseil et Gestion des entreprises','Conseil et Gestion des entreprises'),(417,'12373653478200','Droit et Juridique','Droit et Juridique'),(418,'12373654360500','Comptabilité et Audit','Comptabilité et Audit'),(419,'12373655417700','Eau, Energie et Minerais','Eau, Energie et Minerais'),(420,'12373656199300','Environnement et développement durable','Environnement et développement durable'),(421,'12373657087700','Formation académique, professionnel et divers','Formation académique, professionnel et divers'),(422,'12373657949900','Informatique et Technologie de l\'information','Informatique et Technologie de l\'information'),(423,'12373658755800','Industrie chimique, textile, Manufacturière et autres','Industrie chimique, textile, Manufacturière et autres'),(424,'12373659840700','Immobilier','Immobilier'),(425,'12373660694100','Marketing, communication et Médias','Marketing, communication et Médias'),(426,'12373661478400','Ressources Humaines','Ressources Humaines'),(427,'12373662461300','Restauration, Hôtellerie, et Loisirs','Restauration, Hôtellerie, et Loisirs'),(428,'12373663292200','Services divers aux entreprises','Services divers aux entreprises'),(429,'12373664095300','Santé et service à la personne et sociale','Santé et service à la personne et sociale'),(430,'12373665925100','Telecom','Telecom'),(431,'12373666892100','Transports et Logistique','Transports et Logistique'),(432,'12373667805100','Tourisme','Tourisme'),(433,'12373668822700','Autres (à préciser manuellement)','Autres (à préciser manuellement)');
/*!40000 ALTER TABLE `activity_sector` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `batch_job_execution`
--

DROP TABLE IF EXISTS `batch_job_execution`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `batch_job_execution` (
  `JOB_EXECUTION_ID` bigint(20) NOT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `JOB_INSTANCE_ID` bigint(20) NOT NULL,
  `CREATE_TIME` datetime NOT NULL,
  `START_TIME` datetime DEFAULT NULL,
  `END_TIME` datetime DEFAULT NULL,
  `STATUS` varchar(10) DEFAULT NULL,
  `EXIT_CODE` varchar(2500) DEFAULT NULL,
  `EXIT_MESSAGE` varchar(2500) DEFAULT NULL,
  `LAST_UPDATED` datetime DEFAULT NULL,
  `JOB_CONFIGURATION_LOCATION` varchar(2500) DEFAULT NULL,
  PRIMARY KEY (`JOB_EXECUTION_ID`),
  KEY `JOB_INST_EXEC_FK` (`JOB_INSTANCE_ID`),
  CONSTRAINT `JOB_INST_EXEC_FK` FOREIGN KEY (`JOB_INSTANCE_ID`) REFERENCES `batch_job_instance` (`JOB_INSTANCE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `batch_job_execution`
--

LOCK TABLES `batch_job_execution` WRITE;
/*!40000 ALTER TABLE `batch_job_execution` DISABLE KEYS */;
INSERT INTO `batch_job_execution` VALUES (1,2,1,'2020-09-27 21:06:07','2020-09-27 21:06:07','2020-09-27 21:06:08','COMPLETED','COMPLETED','','2020-09-27 21:06:08',NULL),(2,2,2,'2020-09-27 21:06:57','2020-09-27 21:06:57','2020-09-27 21:06:58','COMPLETED','COMPLETED','','2020-09-27 21:06:58',NULL),(3,2,3,'2020-09-27 21:07:48','2020-09-27 21:07:48','2020-09-27 21:07:49','COMPLETED','COMPLETED','','2020-09-27 21:07:49',NULL),(4,2,4,'2020-09-27 21:15:38','2020-09-27 21:15:38','2020-09-27 21:15:38','COMPLETED','COMPLETED','','2020-09-27 21:15:38',NULL),(5,2,5,'2020-09-27 21:16:19','2020-09-27 21:16:19','2020-09-27 21:16:20','COMPLETED','COMPLETED','','2020-09-27 21:16:20',NULL),(6,2,6,'2020-09-27 21:17:28','2020-09-27 21:17:28','2020-09-27 21:17:29','COMPLETED','COMPLETED','','2020-09-27 21:17:29',NULL),(7,2,7,'2020-09-27 21:20:32','2020-09-27 21:20:32','2020-09-27 21:20:32','COMPLETED','COMPLETED','','2020-09-27 21:20:32',NULL),(8,2,8,'2020-09-27 21:22:37','2020-09-27 21:22:37','2020-09-27 21:22:38','COMPLETED','COMPLETED','','2020-09-27 21:22:38',NULL),(9,2,9,'2020-09-27 21:25:39','2020-09-27 21:25:39','2020-09-27 21:25:39','COMPLETED','COMPLETED','','2020-09-27 21:25:39',NULL),(10,2,10,'2020-09-27 21:26:33','2020-09-27 21:26:33','2020-09-27 21:26:33','COMPLETED','COMPLETED','','2020-09-27 21:26:33',NULL),(11,2,11,'2020-09-27 21:27:58','2020-09-27 21:27:58','2020-09-27 21:27:59','COMPLETED','COMPLETED','','2020-09-27 21:27:59',NULL),(12,2,12,'2020-09-27 21:30:54','2020-09-27 21:30:54','2020-09-27 21:30:54','COMPLETED','COMPLETED','','2020-09-27 21:30:54',NULL),(13,2,13,'2020-09-27 21:56:40','2020-09-27 21:56:40','2020-09-27 21:56:40','COMPLETED','COMPLETED','','2020-09-27 21:56:40',NULL),(14,2,14,'2020-09-28 00:32:47','2020-09-28 00:32:47','2020-09-28 00:32:47','COMPLETED','COMPLETED','','2020-09-28 00:32:47',NULL),(15,2,15,'2020-09-28 00:33:45','2020-09-28 00:33:45','2020-09-28 00:33:45','COMPLETED','COMPLETED','','2020-09-28 00:33:45',NULL),(16,2,16,'2020-10-06 17:37:41','2020-10-06 17:37:41','2020-10-06 17:37:41','COMPLETED','COMPLETED','','2020-10-06 17:37:41',NULL),(17,2,17,'2020-10-06 17:38:46','2020-10-06 17:38:46','2020-10-06 17:38:46','COMPLETED','COMPLETED','','2020-10-06 17:38:46',NULL),(18,2,18,'2020-10-11 11:11:40','2020-10-11 11:11:40','2020-10-11 11:11:41','COMPLETED','COMPLETED','','2020-10-11 11:11:41',NULL),(19,2,19,'2020-10-11 15:06:45','2020-10-11 15:06:45','2020-10-11 15:06:45','COMPLETED','COMPLETED','','2020-10-11 15:06:45',NULL),(20,2,20,'2020-10-11 15:08:13','2020-10-11 15:08:13','2020-10-11 15:08:14','COMPLETED','COMPLETED','','2020-10-11 15:08:14',NULL),(21,2,21,'2020-10-11 15:35:23','2020-10-11 15:35:23','2020-10-11 15:35:23','COMPLETED','COMPLETED','','2020-10-11 15:35:23',NULL),(22,2,22,'2020-10-11 15:42:36','2020-10-11 15:42:36','2020-10-11 15:42:36','COMPLETED','COMPLETED','','2020-10-11 15:42:36',NULL),(23,2,23,'2020-10-11 15:52:55','2020-10-11 15:52:55','2020-10-11 15:52:55','COMPLETED','COMPLETED','','2020-10-11 15:52:55',NULL),(24,2,24,'2020-10-11 16:00:03','2020-10-11 16:00:03','2020-10-11 16:00:04','COMPLETED','COMPLETED','','2020-10-11 16:00:04',NULL),(25,2,25,'2020-10-11 16:38:20','2020-10-11 16:38:20','2020-10-11 16:38:21','COMPLETED','COMPLETED','','2020-10-11 16:38:21',NULL),(26,2,26,'2020-10-19 09:46:22','2020-10-19 09:46:22','2020-10-19 09:46:22','COMPLETED','COMPLETED','','2020-10-19 09:46:22',NULL),(27,2,27,'2020-10-19 10:07:49','2020-10-19 10:07:50','2020-10-19 10:07:51','COMPLETED','COMPLETED','','2020-10-19 10:07:51',NULL),(28,2,28,'2020-10-19 10:17:19','2020-10-19 10:17:19','2020-10-19 10:17:20','COMPLETED','COMPLETED','','2020-10-19 10:17:20',NULL);
/*!40000 ALTER TABLE `batch_job_execution` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `batch_job_execution_context`
--

DROP TABLE IF EXISTS `batch_job_execution_context`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `batch_job_execution_context` (
  `JOB_EXECUTION_ID` bigint(20) NOT NULL,
  `SHORT_CONTEXT` varchar(2500) NOT NULL,
  `SERIALIZED_CONTEXT` text DEFAULT NULL,
  PRIMARY KEY (`JOB_EXECUTION_ID`),
  CONSTRAINT `JOB_EXEC_CTX_FK` FOREIGN KEY (`JOB_EXECUTION_ID`) REFERENCES `batch_job_execution` (`JOB_EXECUTION_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `batch_job_execution_context`
--

LOCK TABLES `batch_job_execution_context` WRITE;
/*!40000 ALTER TABLE `batch_job_execution_context` DISABLE KEYS */;
INSERT INTO `batch_job_execution_context` VALUES (1,'{\"STATUS\":\"COMPLETED\",\"MODEL\":\"GENDER\"}',NULL),(2,'{\"STATUS\":\"COMPLETED\",\"MODEL\":\"PROFESSIONAL_STATUS\"}',NULL),(3,'{\"STATUS\":\"COMPLETED\",\"MODEL\":\"COUNTRY\"}',NULL),(4,'{\"STATUS\":\"COMPLETED\",\"MODEL\":\"SKILL_TYPE\"}',NULL),(5,'{\"STATUS\":\"COMPLETED\",\"MODEL\":\"DISCIPLINE\"}',NULL),(6,'{\"STATUS\":\"COMPLETED\",\"MODEL\":\"LEVEL\"}',NULL),(7,'{\"STATUS\":\"COMPLETED\",\"MODEL\":\"ACTIVITY_SECTOR\"}',NULL),(8,'{\"STATUS\":\"COMPLETED\",\"MODEL\":\"ACTIVITY_AREA\"}',NULL),(9,'{\"STATUS\":\"COMPLETED\",\"MODEL\":\"EDUCATION\"}',NULL),(10,'{\"STATUS\":\"COMPLETED\",\"MODEL\":\"ESTABLISHMENT_TYPE\"}',NULL),(11,'{\"STATUS\":\"COMPLETED\",\"MODEL\":\"FREQUENTED_CLASS\"}',NULL),(12,'{\"STATUS\":\"COMPLETED\",\"MODEL\":\"DIPLOMA\"}',NULL),(13,'{\"STATUS\":\"COMPLETED\",\"MODEL\":\"SPECIALTY\"}',NULL),(14,'{\"STATUS\":\"COMPLETED\",\"MODEL\":\"TEACHING_AREA\"}',NULL),(15,'{\"STATUS\":\"COMPLETED\",\"MODEL\":\"STUDY_LEVEL\"}',NULL),(16,'{\"STATUS\":\"COMPLETED\",\"MODEL\":\"MEDIA_TYPE\"}',NULL),(17,'{\"STATUS\":\"COMPLETED\",\"MODEL\":\"MEDIA_SUBTYPE\"}',NULL),(18,'{\"STATUS\":\"COMPLETED\",\"MODEL\":\"STUDY_LEVEL\"}',NULL),(19,'{\"STATUS\":\"COMPLETED\",\"MODEL\":\"ESTABLISHMENT_TYPE\"}',NULL),(20,'{\"STATUS\":\"COMPLETED\",\"MODEL\":\"ACTIVITY_AREA\"}',NULL),(21,'{\"STATUS\":\"COMPLETED\",\"MODEL\":\"STUDY_LEVEL\"}',NULL),(22,'{\"STATUS\":\"COMPLETED\",\"MODEL\":\"DIPLOMA\"}',NULL),(23,'{\"STATUS\":\"COMPLETED\",\"MODEL\":\"TEACHING_AREA\"}',NULL),(24,'{\"STATUS\":\"COMPLETED\",\"MODEL\":\"STUDY_LEVEL\"}',NULL),(25,'{\"STATUS\":\"COMPLETED\",\"MODEL\":\"TEACHING_AREA\"}',NULL),(26,'{\"STATUS\":\"COMPLETED\",\"MODEL\":\"LEVEL\"}',NULL),(27,'{\"STATUS\":\"COMPLETED\",\"MODEL\":\"DIPLOMA\"}',NULL),(28,'{\"STATUS\":\"COMPLETED\",\"MODEL\":\"MEDIA_SUBTYPE\"}',NULL);
/*!40000 ALTER TABLE `batch_job_execution_context` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `batch_job_execution_params`
--

DROP TABLE IF EXISTS `batch_job_execution_params`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `batch_job_execution_params` (
  `JOB_EXECUTION_ID` bigint(20) NOT NULL,
  `TYPE_CD` varchar(6) NOT NULL,
  `KEY_NAME` varchar(100) NOT NULL,
  `STRING_VAL` varchar(250) DEFAULT NULL,
  `DATE_VAL` datetime DEFAULT NULL,
  `LONG_VAL` bigint(20) DEFAULT NULL,
  `DOUBLE_VAL` double DEFAULT NULL,
  `IDENTIFYING` char(1) NOT NULL,
  KEY `JOB_EXEC_PARAMS_FK` (`JOB_EXECUTION_ID`),
  CONSTRAINT `JOB_EXEC_PARAMS_FK` FOREIGN KEY (`JOB_EXECUTION_ID`) REFERENCES `batch_job_execution` (`JOB_EXECUTION_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `batch_job_execution_params`
--

LOCK TABLES `batch_job_execution_params` WRITE;
/*!40000 ALTER TABLE `batch_job_execution_params` DISABLE KEYS */;
INSERT INTO `batch_job_execution_params` VALUES (1,'LONG','run.id','','1970-01-01 01:00:00',1,0,'Y'),(2,'LONG','run.id','','1970-01-01 01:00:00',2,0,'Y'),(3,'LONG','run.id','','1970-01-01 01:00:00',3,0,'Y'),(4,'LONG','run.id','','1970-01-01 01:00:00',4,0,'Y'),(5,'LONG','run.id','','1970-01-01 01:00:00',5,0,'Y'),(6,'LONG','run.id','','1970-01-01 01:00:00',6,0,'Y'),(7,'LONG','run.id','','1970-01-01 01:00:00',7,0,'Y'),(8,'LONG','run.id','','1970-01-01 01:00:00',8,0,'Y'),(9,'LONG','run.id','','1970-01-01 01:00:00',9,0,'Y'),(10,'LONG','run.id','','1970-01-01 01:00:00',10,0,'Y'),(11,'LONG','run.id','','1970-01-01 01:00:00',11,0,'Y'),(12,'LONG','run.id','','1970-01-01 01:00:00',12,0,'Y'),(13,'LONG','run.id','','1970-01-01 01:00:00',13,0,'Y'),(14,'LONG','run.id','','1970-01-01 01:00:00',14,0,'Y'),(15,'LONG','run.id','','1970-01-01 01:00:00',15,0,'Y'),(16,'LONG','run.id','','1970-01-01 01:00:00',16,0,'Y'),(17,'LONG','run.id','','1970-01-01 01:00:00',17,0,'Y'),(18,'LONG','run.id','','1970-01-01 01:00:00',18,0,'Y'),(19,'LONG','run.id','','1970-01-01 01:00:00',19,0,'Y'),(20,'LONG','run.id','','1970-01-01 01:00:00',20,0,'Y'),(21,'LONG','run.id','','1970-01-01 01:00:00',21,0,'Y'),(22,'LONG','run.id','','1970-01-01 01:00:00',22,0,'Y'),(23,'LONG','run.id','','1970-01-01 01:00:00',23,0,'Y'),(24,'LONG','run.id','','1970-01-01 01:00:00',24,0,'Y'),(25,'LONG','run.id','','1970-01-01 01:00:00',25,0,'Y'),(26,'LONG','run.id','','1970-01-01 01:00:00',26,0,'Y'),(27,'LONG','run.id','','1970-01-01 01:00:00',27,0,'Y'),(28,'LONG','run.id','','1970-01-01 01:00:00',28,0,'Y');
/*!40000 ALTER TABLE `batch_job_execution_params` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `batch_job_execution_seq`
--

DROP TABLE IF EXISTS `batch_job_execution_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `batch_job_execution_seq` (
  `ID` bigint(20) NOT NULL,
  `UNIQUE_KEY` char(1) NOT NULL,
  UNIQUE KEY `UNIQUE_KEY_UN` (`UNIQUE_KEY`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `batch_job_execution_seq`
--

LOCK TABLES `batch_job_execution_seq` WRITE;
/*!40000 ALTER TABLE `batch_job_execution_seq` DISABLE KEYS */;
INSERT INTO `batch_job_execution_seq` VALUES (28,'0');
/*!40000 ALTER TABLE `batch_job_execution_seq` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `batch_job_instance`
--

DROP TABLE IF EXISTS `batch_job_instance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `batch_job_instance` (
  `JOB_INSTANCE_ID` bigint(20) NOT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `JOB_NAME` varchar(100) NOT NULL,
  `JOB_KEY` varchar(32) NOT NULL,
  PRIMARY KEY (`JOB_INSTANCE_ID`),
  UNIQUE KEY `JOB_INST_UN` (`JOB_NAME`,`JOB_KEY`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `batch_job_instance`
--

LOCK TABLES `batch_job_instance` WRITE;
/*!40000 ALTER TABLE `batch_job_instance` DISABLE KEYS */;
INSERT INTO `batch_job_instance` VALUES (1,0,'populateObjectsJob','853d3449e311f40366811cbefb3d93d7'),(2,0,'populateObjectsJob','e070bff4379694c0210a51d9f6c6a564'),(3,0,'populateObjectsJob','a3364faf893276dea0caacefbf618db5'),(4,0,'populateObjectsJob','47c0a8118b74165a864b66d37c7b6cf5'),(5,0,'populateObjectsJob','ce148f5f9c2bf4dc9bd44a7a5f64204c'),(6,0,'populateObjectsJob','bd0034040292bc81e6ccac0e25d9a578'),(7,0,'populateObjectsJob','597815c7e4ab1092c1b25130aae725cb'),(8,0,'populateObjectsJob','f55a96b11012be4fcfb6cf005435182d'),(9,0,'populateObjectsJob','96a5ed9bac43e779455f3e71c0f64840'),(10,0,'populateObjectsJob','1aac4f3e74894b78fa3ce5d8a25e1ef0'),(11,0,'populateObjectsJob','604bbfc4c68cb1f903780c2853ad4801'),(12,0,'populateObjectsJob','556ebe34220b4032509f2581356ba47c'),(13,0,'populateObjectsJob','edc440efb5ddd2a3b2622f16a12bf105'),(14,0,'populateObjectsJob','f3d5e568c384ee72cba8bc6a51057fe4'),(15,0,'populateObjectsJob','378ef1ecb81cf9edac4ab119bdab9d9d'),(16,0,'populateObjectsJob','e073471cc312cadef424c3be7915c0af'),(17,0,'populateObjectsJob','46ba78a99abf1e2fba4a8861749d7572'),(18,0,'populateObjectsJob','b88d31b704adf9f94fe9d4ccff795708'),(19,0,'populateObjectsJob','64d4e6d635ee3ad949314224afce46c2'),(20,0,'populateObjectsJob','75c16c09800a944220a789de10278de0'),(21,0,'populateObjectsJob','1b759d32440acdcbf90da6919b5d16ad'),(22,0,'populateObjectsJob','1f995cec4b562af773a2e473c369f069'),(23,0,'populateObjectsJob','42106293a859255c2b210d04a51240ca'),(24,0,'populateObjectsJob','9799b3a84f4d6f15a5e8c11360e7387b'),(25,0,'populateObjectsJob','6eecb29840a845c35cfa9b2da21862f9'),(26,0,'populateObjectsJob','e465389b77512db6f30ed6a3b7a9682c'),(27,0,'populateObjectsJob','19be35489361f0d498838921e450c4cb'),(28,0,'populateObjectsJob','20744cb6ca7f8dc12940aa7fd8f89763');
/*!40000 ALTER TABLE `batch_job_instance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `batch_job_seq`
--

DROP TABLE IF EXISTS `batch_job_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `batch_job_seq` (
  `ID` bigint(20) NOT NULL,
  `UNIQUE_KEY` char(1) NOT NULL,
  UNIQUE KEY `UNIQUE_KEY_UN` (`UNIQUE_KEY`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `batch_job_seq`
--

LOCK TABLES `batch_job_seq` WRITE;
/*!40000 ALTER TABLE `batch_job_seq` DISABLE KEYS */;
INSERT INTO `batch_job_seq` VALUES (28,'0');
/*!40000 ALTER TABLE `batch_job_seq` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `batch_step_execution`
--

DROP TABLE IF EXISTS `batch_step_execution`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `batch_step_execution` (
  `STEP_EXECUTION_ID` bigint(20) NOT NULL,
  `VERSION` bigint(20) NOT NULL,
  `STEP_NAME` varchar(100) NOT NULL,
  `JOB_EXECUTION_ID` bigint(20) NOT NULL,
  `START_TIME` datetime NOT NULL,
  `END_TIME` datetime DEFAULT NULL,
  `STATUS` varchar(10) DEFAULT NULL,
  `COMMIT_COUNT` bigint(20) DEFAULT NULL,
  `READ_COUNT` bigint(20) DEFAULT NULL,
  `FILTER_COUNT` bigint(20) DEFAULT NULL,
  `WRITE_COUNT` bigint(20) DEFAULT NULL,
  `READ_SKIP_COUNT` bigint(20) DEFAULT NULL,
  `WRITE_SKIP_COUNT` bigint(20) DEFAULT NULL,
  `PROCESS_SKIP_COUNT` bigint(20) DEFAULT NULL,
  `ROLLBACK_COUNT` bigint(20) DEFAULT NULL,
  `EXIT_CODE` varchar(2500) DEFAULT NULL,
  `EXIT_MESSAGE` varchar(2500) DEFAULT NULL,
  `LAST_UPDATED` datetime DEFAULT NULL,
  PRIMARY KEY (`STEP_EXECUTION_ID`),
  KEY `JOB_EXEC_STEP_FK` (`JOB_EXECUTION_ID`),
  CONSTRAINT `JOB_EXEC_STEP_FK` FOREIGN KEY (`JOB_EXECUTION_ID`) REFERENCES `batch_job_execution` (`JOB_EXECUTION_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `batch_step_execution`
--

LOCK TABLES `batch_step_execution` WRITE;
/*!40000 ALTER TABLE `batch_step_execution` DISABLE KEYS */;
INSERT INTO `batch_step_execution` VALUES (1,3,'stepFileControl',1,'2020-09-27 21:06:07','2020-09-27 21:06:07','COMPLETED',1,0,0,0,0,0,0,0,'COMPLETED','','2020-09-27 21:06:07'),(2,3,'populateObjects',1,'2020-09-27 21:06:07','2020-09-27 21:06:08','COMPLETED',1,2,0,2,0,0,0,0,'COMPLETED','','2020-09-27 21:06:08'),(3,3,'stepFileControl',2,'2020-09-27 21:06:57','2020-09-27 21:06:57','COMPLETED',1,0,0,0,0,0,0,0,'COMPLETED','','2020-09-27 21:06:57'),(4,3,'populateObjects',2,'2020-09-27 21:06:57','2020-09-27 21:06:58','COMPLETED',1,3,0,3,0,0,0,0,'COMPLETED','','2020-09-27 21:06:58'),(5,3,'stepFileControl',3,'2020-09-27 21:07:48','2020-09-27 21:07:48','COMPLETED',1,0,0,0,0,0,0,0,'COMPLETED','','2020-09-27 21:07:48'),(6,5,'populateObjects',3,'2020-09-27 21:07:48','2020-09-27 21:07:49','COMPLETED',3,249,0,249,0,0,0,0,'COMPLETED','','2020-09-27 21:07:49'),(7,3,'stepFileControl',4,'2020-09-27 21:15:38','2020-09-27 21:15:38','COMPLETED',1,0,0,0,0,0,0,0,'COMPLETED','','2020-09-27 21:15:38'),(8,3,'populateObjects',4,'2020-09-27 21:15:38','2020-09-27 21:15:38','COMPLETED',1,2,0,2,0,0,0,0,'COMPLETED','','2020-09-27 21:15:38'),(9,3,'stepFileControl',5,'2020-09-27 21:16:19','2020-09-27 21:16:19','COMPLETED',1,0,0,0,0,0,0,0,'COMPLETED','','2020-09-27 21:16:19'),(10,4,'populateObjects',5,'2020-09-27 21:16:20','2020-09-27 21:16:20','COMPLETED',2,142,0,142,0,0,0,0,'COMPLETED','','2020-09-27 21:16:20'),(11,3,'stepFileControl',6,'2020-09-27 21:17:28','2020-09-27 21:17:28','COMPLETED',1,0,0,0,0,0,0,0,'COMPLETED','','2020-09-27 21:17:28'),(12,3,'populateObjects',6,'2020-09-27 21:17:28','2020-09-27 21:17:29','COMPLETED',1,4,0,4,0,0,0,0,'COMPLETED','','2020-09-27 21:17:29'),(13,3,'stepFileControl',7,'2020-09-27 21:20:32','2020-09-27 21:20:32','COMPLETED',1,0,0,0,0,0,0,0,'COMPLETED','','2020-09-27 21:20:32'),(14,3,'populateObjects',7,'2020-09-27 21:20:32','2020-09-27 21:20:32','COMPLETED',1,25,0,25,0,0,0,0,'COMPLETED','','2020-09-27 21:20:32'),(15,3,'stepFileControl',8,'2020-09-27 21:22:37','2020-09-27 21:22:37','COMPLETED',1,0,0,0,0,0,0,0,'COMPLETED','','2020-09-27 21:22:37'),(16,3,'populateObjects',8,'2020-09-27 21:22:37','2020-09-27 21:22:38','COMPLETED',1,2,0,2,0,0,0,0,'COMPLETED','','2020-09-27 21:22:38'),(17,3,'stepFileControl',9,'2020-09-27 21:25:39','2020-09-27 21:25:39','COMPLETED',1,0,0,0,0,0,0,0,'COMPLETED','','2020-09-27 21:25:39'),(18,3,'populateObjects',9,'2020-09-27 21:25:39','2020-09-27 21:25:39','COMPLETED',1,2,0,2,0,0,0,0,'COMPLETED','','2020-09-27 21:25:39'),(19,3,'stepFileControl',10,'2020-09-27 21:26:33','2020-09-27 21:26:33','COMPLETED',1,0,0,0,0,0,0,0,'COMPLETED','','2020-09-27 21:26:33'),(20,3,'populateObjects',10,'2020-09-27 21:26:33','2020-09-27 21:26:33','COMPLETED',1,13,0,13,0,0,0,0,'COMPLETED','','2020-09-27 21:26:33'),(21,3,'stepFileControl',11,'2020-09-27 21:27:58','2020-09-27 21:27:58','COMPLETED',1,0,0,0,0,0,0,0,'COMPLETED','','2020-09-27 21:27:58'),(22,3,'populateObjects',11,'2020-09-27 21:27:58','2020-09-27 21:27:59','COMPLETED',1,66,0,66,0,0,0,0,'COMPLETED','','2020-09-27 21:27:59'),(23,3,'stepFileControl',12,'2020-09-27 21:30:54','2020-09-27 21:30:54','COMPLETED',1,0,0,0,0,0,0,0,'COMPLETED','','2020-09-27 21:30:54'),(24,3,'populateObjects',12,'2020-09-27 21:30:54','2020-09-27 21:30:54','COMPLETED',1,34,0,34,0,0,0,0,'COMPLETED','','2020-09-27 21:30:54'),(25,3,'stepFileControl',13,'2020-09-27 21:56:40','2020-09-27 21:56:40','COMPLETED',1,0,0,0,0,0,0,0,'COMPLETED','','2020-09-27 21:56:40'),(26,3,'populateObjects',13,'2020-09-27 21:56:40','2020-09-27 21:56:40','COMPLETED',1,4,0,4,0,0,0,0,'COMPLETED','','2020-09-27 21:56:40'),(27,3,'stepFileControl',14,'2020-09-28 00:32:47','2020-09-28 00:32:47','COMPLETED',1,0,0,0,0,0,0,0,'COMPLETED','','2020-09-28 00:32:47'),(28,3,'populateObjects',14,'2020-09-28 00:32:47','2020-09-28 00:32:47','COMPLETED',1,4,0,4,0,0,0,0,'COMPLETED','','2020-09-28 00:32:47'),(29,3,'stepFileControl',15,'2020-09-28 00:33:45','2020-09-28 00:33:45','COMPLETED',1,0,0,0,0,0,0,0,'COMPLETED','','2020-09-28 00:33:45'),(30,3,'populateObjects',15,'2020-09-28 00:33:45','2020-09-28 00:33:45','COMPLETED',1,4,0,4,0,0,0,0,'COMPLETED','','2020-09-28 00:33:45'),(31,3,'stepFileControl',16,'2020-10-06 17:37:41','2020-10-06 17:37:41','COMPLETED',1,0,0,0,0,0,0,0,'COMPLETED','','2020-10-06 17:37:41'),(32,3,'populateObjects',16,'2020-10-06 17:37:41','2020-10-06 17:37:41','COMPLETED',1,6,0,6,0,0,0,0,'COMPLETED','','2020-10-06 17:37:41'),(33,3,'stepFileControl',17,'2020-10-06 17:38:46','2020-10-06 17:38:46','COMPLETED',1,0,0,0,0,0,0,0,'COMPLETED','','2020-10-06 17:38:46'),(34,3,'populateObjects',17,'2020-10-06 17:38:46','2020-10-06 17:38:46','COMPLETED',1,26,0,26,0,0,0,0,'COMPLETED','','2020-10-06 17:38:46'),(35,3,'stepFileControl',18,'2020-10-11 11:11:40','2020-10-11 11:11:40','COMPLETED',1,0,0,0,0,0,0,0,'COMPLETED','','2020-10-11 11:11:40'),(36,3,'populateObjects',18,'2020-10-11 11:11:40','2020-10-11 11:11:41','COMPLETED',1,66,0,66,0,0,0,0,'COMPLETED','','2020-10-11 11:11:41'),(37,3,'stepFileControl',19,'2020-10-11 15:06:45','2020-10-11 15:06:45','COMPLETED',1,0,0,0,0,0,0,0,'COMPLETED','','2020-10-11 15:06:45'),(38,3,'populateObjects',19,'2020-10-11 15:06:45','2020-10-11 15:06:45','COMPLETED',1,13,0,13,0,0,0,0,'COMPLETED','','2020-10-11 15:06:45'),(39,3,'stepFileControl',20,'2020-10-11 15:08:13','2020-10-11 15:08:13','COMPLETED',1,0,0,0,0,0,0,0,'COMPLETED','','2020-10-11 15:08:13'),(40,3,'populateObjects',20,'2020-10-11 15:08:13','2020-10-11 15:08:14','COMPLETED',1,51,0,51,0,0,0,0,'COMPLETED','','2020-10-11 15:08:14'),(41,3,'stepFileControl',21,'2020-10-11 15:35:23','2020-10-11 15:35:23','COMPLETED',1,0,0,0,0,0,0,0,'COMPLETED','','2020-10-11 15:35:23'),(42,3,'populateObjects',21,'2020-10-11 15:35:23','2020-10-11 15:35:23','COMPLETED',1,66,0,66,0,0,0,0,'COMPLETED','','2020-10-11 15:35:23'),(43,3,'stepFileControl',22,'2020-10-11 15:42:36','2020-10-11 15:42:36','COMPLETED',1,0,0,0,0,0,0,0,'COMPLETED','','2020-10-11 15:42:36'),(44,3,'populateObjects',22,'2020-10-11 15:42:36','2020-10-11 15:42:36','COMPLETED',1,34,0,34,0,0,0,0,'COMPLETED','','2020-10-11 15:42:36'),(45,3,'stepFileControl',23,'2020-10-11 15:52:55','2020-10-11 15:52:55','COMPLETED',1,0,0,0,0,0,0,0,'COMPLETED','','2020-10-11 15:52:55'),(46,3,'populateObjects',23,'2020-10-11 15:52:55','2020-10-11 15:52:55','COMPLETED',1,4,0,4,0,0,0,0,'COMPLETED','','2020-10-11 15:52:55'),(47,3,'stepFileControl',24,'2020-10-11 16:00:03','2020-10-11 16:00:03','COMPLETED',1,0,0,0,0,0,0,0,'COMPLETED','','2020-10-11 16:00:03'),(48,3,'populateObjects',24,'2020-10-11 16:00:03','2020-10-11 16:00:04','COMPLETED',1,50,0,50,0,0,0,0,'COMPLETED','','2020-10-11 16:00:04'),(49,3,'stepFileControl',25,'2020-10-11 16:38:20','2020-10-11 16:38:20','COMPLETED',1,0,0,0,0,0,0,0,'COMPLETED','','2020-10-11 16:38:20'),(50,4,'populateObjects',25,'2020-10-11 16:38:20','2020-10-11 16:38:21','COMPLETED',2,124,0,124,0,0,0,0,'COMPLETED','','2020-10-11 16:38:21'),(51,3,'stepFileControl',26,'2020-10-19 09:46:22','2020-10-19 09:46:22','COMPLETED',1,0,0,0,0,0,0,0,'COMPLETED','','2020-10-19 09:46:22'),(52,3,'populateObjects',26,'2020-10-19 09:46:22','2020-10-19 09:46:22','COMPLETED',1,4,0,4,0,0,0,0,'COMPLETED','','2020-10-19 09:46:22'),(53,3,'stepFileControl',27,'2020-10-19 10:07:50','2020-10-19 10:07:50','COMPLETED',1,0,0,0,0,0,0,0,'COMPLETED','','2020-10-19 10:07:50'),(54,3,'populateObjects',27,'2020-10-19 10:07:50','2020-10-19 10:07:51','COMPLETED',1,35,0,35,0,0,0,0,'COMPLETED','','2020-10-19 10:07:51'),(55,3,'stepFileControl',28,'2020-10-19 10:17:19','2020-10-19 10:17:19','COMPLETED',1,0,0,0,0,0,0,0,'COMPLETED','','2020-10-19 10:17:19'),(56,3,'populateObjects',28,'2020-10-19 10:17:19','2020-10-19 10:17:20','COMPLETED',1,27,0,27,0,0,0,0,'COMPLETED','','2020-10-19 10:17:20');
/*!40000 ALTER TABLE `batch_step_execution` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `batch_step_execution_context`
--

DROP TABLE IF EXISTS `batch_step_execution_context`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `batch_step_execution_context` (
  `STEP_EXECUTION_ID` bigint(20) NOT NULL,
  `SHORT_CONTEXT` varchar(2500) NOT NULL,
  `SERIALIZED_CONTEXT` text DEFAULT NULL,
  PRIMARY KEY (`STEP_EXECUTION_ID`),
  CONSTRAINT `STEP_EXEC_CTX_FK` FOREIGN KEY (`STEP_EXECUTION_ID`) REFERENCES `batch_step_execution` (`STEP_EXECUTION_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `batch_step_execution_context`
--

LOCK TABLES `batch_step_execution_context` WRITE;
/*!40000 ALTER TABLE `batch_step_execution_context` DISABLE KEYS */;
INSERT INTO `batch_step_execution_context` VALUES (1,'{\"batch.taskletType\":\"com.yamo.skillsmates.populate.tasklet.FileControlTasklet\",\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}',NULL),(2,'{\"batch.taskletType\":\"org.springframework.batch.core.step.item.ChunkOrientedTasklet\",\"FlatFileItemReader.read.count\":3,\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}',NULL),(3,'{\"batch.taskletType\":\"com.yamo.skillsmates.populate.tasklet.FileControlTasklet\",\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}',NULL),(4,'{\"batch.taskletType\":\"org.springframework.batch.core.step.item.ChunkOrientedTasklet\",\"FlatFileItemReader.read.count\":4,\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}',NULL),(5,'{\"batch.taskletType\":\"com.yamo.skillsmates.populate.tasklet.FileControlTasklet\",\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}',NULL),(6,'{\"batch.taskletType\":\"org.springframework.batch.core.step.item.ChunkOrientedTasklet\",\"FlatFileItemReader.read.count\":250,\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}',NULL),(7,'{\"batch.taskletType\":\"com.yamo.skillsmates.populate.tasklet.FileControlTasklet\",\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}',NULL),(8,'{\"batch.taskletType\":\"org.springframework.batch.core.step.item.ChunkOrientedTasklet\",\"FlatFileItemReader.read.count\":3,\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}',NULL),(9,'{\"batch.taskletType\":\"com.yamo.skillsmates.populate.tasklet.FileControlTasklet\",\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}',NULL),(10,'{\"batch.taskletType\":\"org.springframework.batch.core.step.item.ChunkOrientedTasklet\",\"FlatFileItemReader.read.count\":143,\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}',NULL),(11,'{\"batch.taskletType\":\"com.yamo.skillsmates.populate.tasklet.FileControlTasklet\",\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}',NULL),(12,'{\"batch.taskletType\":\"org.springframework.batch.core.step.item.ChunkOrientedTasklet\",\"FlatFileItemReader.read.count\":5,\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}',NULL),(13,'{\"batch.taskletType\":\"com.yamo.skillsmates.populate.tasklet.FileControlTasklet\",\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}',NULL),(14,'{\"batch.taskletType\":\"org.springframework.batch.core.step.item.ChunkOrientedTasklet\",\"FlatFileItemReader.read.count\":26,\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}',NULL),(15,'{\"batch.taskletType\":\"com.yamo.skillsmates.populate.tasklet.FileControlTasklet\",\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}',NULL),(16,'{\"batch.taskletType\":\"org.springframework.batch.core.step.item.ChunkOrientedTasklet\",\"FlatFileItemReader.read.count\":3,\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}',NULL),(17,'{\"batch.taskletType\":\"com.yamo.skillsmates.populate.tasklet.FileControlTasklet\",\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}',NULL),(18,'{\"batch.taskletType\":\"org.springframework.batch.core.step.item.ChunkOrientedTasklet\",\"FlatFileItemReader.read.count\":3,\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}',NULL),(19,'{\"batch.taskletType\":\"com.yamo.skillsmates.populate.tasklet.FileControlTasklet\",\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}',NULL),(20,'{\"batch.taskletType\":\"org.springframework.batch.core.step.item.ChunkOrientedTasklet\",\"FlatFileItemReader.read.count\":14,\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}',NULL),(21,'{\"batch.taskletType\":\"com.yamo.skillsmates.populate.tasklet.FileControlTasklet\",\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}',NULL),(22,'{\"batch.taskletType\":\"org.springframework.batch.core.step.item.ChunkOrientedTasklet\",\"FlatFileItemReader.read.count\":67,\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}',NULL),(23,'{\"batch.taskletType\":\"com.yamo.skillsmates.populate.tasklet.FileControlTasklet\",\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}',NULL),(24,'{\"batch.taskletType\":\"org.springframework.batch.core.step.item.ChunkOrientedTasklet\",\"FlatFileItemReader.read.count\":35,\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}',NULL),(25,'{\"batch.taskletType\":\"com.yamo.skillsmates.populate.tasklet.FileControlTasklet\",\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}',NULL),(26,'{\"batch.taskletType\":\"org.springframework.batch.core.step.item.ChunkOrientedTasklet\",\"FlatFileItemReader.read.count\":5,\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}',NULL),(27,'{\"batch.taskletType\":\"com.yamo.skillsmates.populate.tasklet.FileControlTasklet\",\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}',NULL),(28,'{\"batch.taskletType\":\"org.springframework.batch.core.step.item.ChunkOrientedTasklet\",\"FlatFileItemReader.read.count\":5,\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}',NULL),(29,'{\"batch.taskletType\":\"com.yamo.skillsmates.populate.tasklet.FileControlTasklet\",\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}',NULL),(30,'{\"batch.taskletType\":\"org.springframework.batch.core.step.item.ChunkOrientedTasklet\",\"FlatFileItemReader.read.count\":5,\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}',NULL),(31,'{\"batch.taskletType\":\"com.yamo.skillsmates.populate.tasklet.FileControlTasklet\",\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}',NULL),(32,'{\"batch.taskletType\":\"org.springframework.batch.core.step.item.ChunkOrientedTasklet\",\"FlatFileItemReader.read.count\":7,\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}',NULL),(33,'{\"batch.taskletType\":\"com.yamo.skillsmates.populate.tasklet.FileControlTasklet\",\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}',NULL),(34,'{\"batch.taskletType\":\"org.springframework.batch.core.step.item.ChunkOrientedTasklet\",\"FlatFileItemReader.read.count\":27,\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}',NULL),(35,'{\"batch.taskletType\":\"com.yamo.skillsmates.populate.tasklet.FileControlTasklet\",\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}',NULL),(36,'{\"batch.taskletType\":\"org.springframework.batch.core.step.item.ChunkOrientedTasklet\",\"FlatFileItemReader.read.count\":67,\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}',NULL),(37,'{\"batch.taskletType\":\"com.yamo.skillsmates.populate.tasklet.FileControlTasklet\",\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}',NULL),(38,'{\"batch.taskletType\":\"org.springframework.batch.core.step.item.ChunkOrientedTasklet\",\"FlatFileItemReader.read.count\":14,\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}',NULL),(39,'{\"batch.taskletType\":\"com.yamo.skillsmates.populate.tasklet.FileControlTasklet\",\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}',NULL),(40,'{\"batch.taskletType\":\"org.springframework.batch.core.step.item.ChunkOrientedTasklet\",\"FlatFileItemReader.read.count\":52,\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}',NULL),(41,'{\"batch.taskletType\":\"com.yamo.skillsmates.populate.tasklet.FileControlTasklet\",\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}',NULL),(42,'{\"batch.taskletType\":\"org.springframework.batch.core.step.item.ChunkOrientedTasklet\",\"FlatFileItemReader.read.count\":67,\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}',NULL),(43,'{\"batch.taskletType\":\"com.yamo.skillsmates.populate.tasklet.FileControlTasklet\",\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}',NULL),(44,'{\"batch.taskletType\":\"org.springframework.batch.core.step.item.ChunkOrientedTasklet\",\"FlatFileItemReader.read.count\":35,\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}',NULL),(45,'{\"batch.taskletType\":\"com.yamo.skillsmates.populate.tasklet.FileControlTasklet\",\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}',NULL),(46,'{\"batch.taskletType\":\"org.springframework.batch.core.step.item.ChunkOrientedTasklet\",\"FlatFileItemReader.read.count\":5,\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}',NULL),(47,'{\"batch.taskletType\":\"com.yamo.skillsmates.populate.tasklet.FileControlTasklet\",\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}',NULL),(48,'{\"batch.taskletType\":\"org.springframework.batch.core.step.item.ChunkOrientedTasklet\",\"FlatFileItemReader.read.count\":51,\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}',NULL),(49,'{\"batch.taskletType\":\"com.yamo.skillsmates.populate.tasklet.FileControlTasklet\",\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}',NULL),(50,'{\"batch.taskletType\":\"org.springframework.batch.core.step.item.ChunkOrientedTasklet\",\"FlatFileItemReader.read.count\":125,\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}',NULL),(51,'{\"batch.taskletType\":\"com.yamo.skillsmates.populate.tasklet.FileControlTasklet\",\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}',NULL),(52,'{\"batch.taskletType\":\"org.springframework.batch.core.step.item.ChunkOrientedTasklet\",\"FlatFileItemReader.read.count\":5,\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}',NULL),(53,'{\"batch.taskletType\":\"com.yamo.skillsmates.populate.tasklet.FileControlTasklet\",\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}',NULL),(54,'{\"batch.taskletType\":\"org.springframework.batch.core.step.item.ChunkOrientedTasklet\",\"FlatFileItemReader.read.count\":36,\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}',NULL),(55,'{\"batch.taskletType\":\"com.yamo.skillsmates.populate.tasklet.FileControlTasklet\",\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}',NULL),(56,'{\"batch.taskletType\":\"org.springframework.batch.core.step.item.ChunkOrientedTasklet\",\"FlatFileItemReader.read.count\":28,\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}',NULL);
/*!40000 ALTER TABLE `batch_step_execution_context` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `batch_step_execution_seq`
--

DROP TABLE IF EXISTS `batch_step_execution_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `batch_step_execution_seq` (
  `ID` bigint(20) NOT NULL,
  `UNIQUE_KEY` char(1) NOT NULL,
  UNIQUE KEY `UNIQUE_KEY_UN` (`UNIQUE_KEY`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `batch_step_execution_seq`
--

LOCK TABLES `batch_step_execution_seq` WRITE;
/*!40000 ALTER TABLE `batch_step_execution_seq` DISABLE KEYS */;
INSERT INTO `batch_step_execution_seq` VALUES (56,'0');
/*!40000 ALTER TABLE `batch_step_execution_seq` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comment`
--

DROP TABLE IF EXISTS `comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comment` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `active` bit(1) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `deleted` bit(1) DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  `revision` bigint(20) DEFAULT NULL,
  `body` varchar(255) NOT NULL,
  `account` int(11) DEFAULT NULL,
  `post` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_br19krw6o9vo598yx94h63wxd` (`id_server`),
  KEY `FKg4pifdpsrn19a8hubl96s2ncp` (`account`),
  KEY `FKomrdwc0ub3x7hvvlyu6htn8ti` (`post`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comment`
--

LOCK TABLES `comment` WRITE;
/*!40000 ALTER TABLE `comment` DISABLE KEYS */;
/*!40000 ALTER TABLE `comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `country`
--

DROP TABLE IF EXISTS `country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `country` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `label` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_bs66m8npqqbrydipm9k2ko187` (`id_server`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `country`
--

LOCK TABLES `country` WRITE;
/*!40000 ALTER TABLE `country` DISABLE KEYS */;
INSERT INTO `country` VALUES (8,'11609659966800','Afghanistan'),(9,'11609661120100','Afrique du sud'),(10,'11609661888700','Îles åland'),(11,'11609662594000','Albanie'),(12,'11609663338700','Algérie'),(13,'11609664117900','Allemagne'),(14,'11609664984200','Andorre'),(15,'11609665820000','Angola'),(16,'11609666623100','Anguilla'),(17,'11609667521000','Antarctique'),(18,'11609668436400','Antigua-et-barbuda'),(19,'11609669269600','Arabie saoudite'),(20,'11609670134100','Argentine'),(21,'11609670954400','Arménie'),(22,'11609671690100','Aruba'),(23,'11609672697800','Australie'),(24,'11609673518400','Autriche'),(25,'11609674260800','Azerbaïdjan'),(26,'11609675209000','Bahamas'),(27,'11609676013200','Bahreïn'),(28,'11609676937700','Bangladesh'),(29,'11609678788700','Barbade'),(30,'11609679488000','Biélorussie'),(31,'11609680165700','Belgique'),(32,'11609680925800','Belize'),(33,'11609681673800','Bénin'),(34,'11609682371100','Bermudes'),(35,'11609683043000','Bhoutan'),(36,'11609683767100','Bolivie'),(37,'11609684436800','Pays-bas caribéens'),(38,'11609685109100','Bosnie-herzégovine'),(39,'11609685838700','Botswana'),(40,'11609686502300','Île bouvet'),(41,'11609687205900','Brésil'),(42,'11609688027700','Brunei'),(43,'11609689087600','Bulgarie'),(44,'11609689979700','Burkina faso'),(45,'11609690725600','Burundi'),(46,'11609691428800','Îles caïmans'),(47,'11609692172700','Cambodge'),(48,'11609692905700','Cameroun'),(49,'11609694005700','Canada'),(50,'11609695260600','Cap-vert'),(51,'11609696127000','République centrafricaine'),(52,'11609696837500','Chili'),(53,'11609697526300','Chine'),(54,'11609698290600','Île christmas'),(55,'11609699184300','Chypre (pays)'),(56,'11609699970000','Îles cocos'),(57,'11609700788300','Colombie'),(58,'11609701490900','Comores (pays)'),(59,'11609702201800','République du congo'),(60,'11609702869200','République démocratique du congo'),(61,'11609703516400','Îles cook'),(62,'11609704167400','Corée du sud'),(63,'11609704943700','Corée du nord'),(64,'11609705746600','Costa rica'),(65,'11609706462900','Côte d\'ivoire'),(66,'11609707196500','Croatie'),(67,'11609708092000','Cuba'),(68,'11609708993700','Curaçao'),(69,'11609709837200','Danemark'),(70,'11609711079900','Djibouti'),(71,'11609712226000','République dominicaine'),(72,'11609713120500','Dominique'),(73,'11609714047100','Égypte'),(74,'11609714824500','Salvador'),(75,'11609715602200','Émirats arabes unis'),(76,'11609716312000','Équateur (pays)'),(77,'11609716999800','Érythrée'),(78,'11609717690500','Espagne'),(79,'11609718400700','Estonie'),(80,'11609719122900','États-unis'),(81,'11609719862000','Éthiopie'),(82,'11609720700900','Malouines'),(83,'11609721486600','Îles féroé'),(84,'11609722256600','Fidji'),(85,'11609723003000','Finlande'),(86,'11609723854600','France'),(87,'11609724741200','Gabon'),(88,'11609725496300','Gambie'),(89,'11609726301000','Géorgie (pays)'),(90,'11609727512800','Géorgie du sud-et-les îles sandwich du sud'),(91,'11609728382500','Ghana'),(92,'11609729282700','Gibraltar'),(93,'11609730190900','Grèce'),(94,'11609730895700','Grenade (pays)'),(95,'11609731719600','Groenland'),(96,'11609732460200','Guadeloupe'),(97,'11609733166400','Guam'),(98,'11609733810200','Guatemala'),(99,'11609734450000','Guernesey'),(100,'11609735187100','Guinée'),(101,'11609735920800','Guinée-bissau'),(102,'11609736641300','Guinée équatoriale'),(103,'11609737358100','Guyana'),(104,'11609738138300','Guyane'),(105,'11609738918400','Haïti'),(106,'11609739621700','Îles heard-et-macdonald'),(107,'11609740303200','Honduras'),(108,'11610051475100','Hong kong'),(109,'11610052191900','Hongrie'),(110,'11610052831600','Île de man'),(111,'11610053582300','Îles mineures éloignées des états-unis'),(112,'11610054289400','Îles vierges britanniques'),(113,'11610055228300','Îles vierges des états-unis'),(114,'11610056198900','Inde'),(115,'11610057193700','Indonésie'),(116,'11610058240000','Iran'),(117,'11610059087600','Irak'),(118,'11610060336300','Irlande (pays)'),(119,'11610061934100','Islande'),(120,'11610067169100','Israël'),(121,'11610068382100','Italie'),(122,'11610069522100','Jamaïque'),(123,'11610070712400','Japon'),(124,'11610071723800','Jersey'),(125,'11610072908000','Jordanie'),(126,'11610074294100','Kazakhstan'),(127,'11610075307300','Kenya'),(128,'11610076093000','Kirghizistan'),(129,'11610077966600','Kiribati'),(130,'11610079019100','Koweït'),(131,'11610079830800','Laos'),(132,'11610080676500','Lesotho'),(133,'11610081915400','Lettonie'),(134,'11610083178300','Liban'),(135,'11610085907800','Liberia'),(136,'11610087123000','Libye'),(137,'11610088113000','Liechtenstein'),(138,'11610089122600','Lituanie'),(139,'11610090043400','Luxembourg (pays)'),(140,'11610090850100','Macao'),(141,'11610091628500','Macédoine du nord'),(142,'11610092571200','Madagascar'),(143,'11610095200100','Malaisie'),(144,'11610096264000','Malawi'),(145,'11610097540600','Maldives'),(146,'11610098626800','Mali'),(147,'11610099417300','Malte'),(148,'11610100149200','Îles mariannes du nord'),(149,'11610100916300','Maroc'),(150,'11610101757200','Îles marshall (pays)'),(151,'11610102569400','Martinique'),(152,'11610103413900','Maurice (pays)'),(153,'11610104255300','Mauritanie'),(154,'11610105421500','Mayotte'),(155,'11610106527900','Mexique'),(156,'11610107504200','États fédérés de micronésie (pays)'),(157,'11610108660600','Moldavie'),(158,'11610109782800','Monaco'),(159,'11610112031500','Mongolie'),(160,'11610112778100','Monténégro'),(161,'11610113503800','Montserrat'),(162,'11610114277500','Mozambique'),(163,'11610115332600','Birmanie'),(164,'11610116391800','Namibie'),(165,'11610117391400','Nauru'),(166,'11610118541300','Népal'),(167,'11610119521100','Nicaragua'),(168,'11610120898100','Niger'),(169,'11610122187700','Nigeria'),(170,'11610123075500','Niue'),(171,'11610124333500','Île norfolk'),(172,'11610125333500','Norvège'),(173,'11610126332300','Nouvelle-calédonie'),(174,'11610129713800','Nouvelle-zélande'),(175,'11610130876100','Territoire britannique de l\'océan indien'),(176,'11610131863900','Oman'),(177,'11610132852700','Ouganda'),(178,'11610134430100','Ouzbékistan'),(179,'11610135441000','Pakistan'),(180,'11610136377300','Palaos'),(181,'11610137355200','Palestine'),(182,'11610138205300','Panama'),(183,'11610138968000','Papouasie-nouvelle-guinée'),(184,'11610139628700','Paraguay'),(185,'11610140245300','Pays-bas'),(186,'11610141082500','Pérou'),(187,'11610142213000','Philippines'),(188,'11610143874500','Îles pitcairn'),(189,'11610145832500','Pologne'),(190,'11610146907400','Polynésie française'),(191,'11610147959500','Porto rico'),(192,'11610149190300','Portugal'),(193,'11610150014200','Qatar'),(194,'11610150843200','La réunion'),(195,'11610151722000','Roumanie'),(196,'11610152532800','Royaume-uni'),(197,'11610153510400','Russie'),(198,'11610154737000','Rwanda'),(199,'11610156201600','République arabe sahraouie démocratique'),(200,'11610158172400','Saint-barthélemy'),(201,'11610159166400','Saint-christophe-et-niévès'),(202,'11610160765700','Saint-marin'),(203,'11610162144200','Saint-martin'),(204,'11610162972400','Saint-martin'),(205,'11610163777900','Saint-pierre-et-miquelon'),(206,'11610164536700','Saint-siège (état de la cité du vatican)'),(207,'11610165440700','Saint-vincent-et-les-grenadines'),(208,'11610353977200','Sainte-hélène; ascension et tristan da cunha'),(209,'11610354966100','Sainte-lucie'),(210,'11610355907900','Salomon'),(211,'11610357151300','Samoa'),(212,'11610358097600','Samoa américaines'),(213,'11610358771600','Sao tomé-et-principe'),(214,'11610359328500','Sénégal'),(215,'11610360417200','Serbie'),(216,'11610361430700','Seychelles'),(217,'11610362125700','Sierra leone'),(218,'11610362738500','Singapour'),(219,'11610363367300','Slovaquie'),(220,'11610364034200','Slovénie'),(221,'11610364926600','Somalie'),(222,'11610365805300','Soudan'),(223,'11610366823400','Soudan du sud'),(224,'11610367897100','Sri lanka'),(225,'11610368868200','Suède'),(226,'11610369584100','Suisse'),(227,'11610370135500','Suriname'),(228,'11610370707900','Svalbard et ile jan mayen'),(229,'11610371326200','Eswatini'),(230,'11610371977000','Syrie'),(231,'11610372633600','Tadjikistan'),(232,'11610373227400','Taïwan / (république de chine (taïwan))'),(233,'11610373856600','Tanzanie'),(234,'11610374493400','Tchad'),(235,'11610375359400','Tchéquie'),(236,'11610376171300','Terres australes et antarctiques françaises'),(237,'11610378115600','Thaïlande'),(238,'11610378889400','Timor oriental'),(239,'11610379575800','Togo'),(240,'11610380278300','Tokelau'),(241,'11610381046100','Tonga'),(242,'11610381781700','Trinité-et-tobago'),(243,'11610382508400','Tunisie'),(244,'11610383150700','Turkménistan'),(245,'11610383749200','Îles turques-et-caïques'),(246,'11610384362600','Turquie'),(247,'11610384984200','Tuvalu'),(248,'11610385592700','Ukraine'),(249,'11610386171900','Uruguay'),(250,'11610386754100','Vanuatu'),(251,'11610387398000','Venezuela'),(252,'11610388197600','Viêt nam'),(253,'11610389037500','Wallis-et-futuna'),(254,'11610389707400','Yémen'),(255,'11610390249100','Zambie'),(256,'11610390806900','Zimbabwe');
/*!40000 ALTER TABLE `country` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `degree_obtained`
--

DROP TABLE IF EXISTS `degree_obtained`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `degree_obtained` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `active` bit(1) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `deleted` bit(1) DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  `revision` bigint(20) DEFAULT NULL,
  `city` varchar(255) NOT NULL,
  `current_position` bit(1) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `establishment_name` varchar(255) NOT NULL,
  `specialty` varchar(255) DEFAULT NULL,
  `account` int(11) DEFAULT NULL,
  `end_date` datetime NOT NULL,
  `start_date` datetime NOT NULL,
  `establishment_type` int(11) DEFAULT NULL,
  `study_level` int(11) DEFAULT NULL,
  `teaching_area` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_it7wk6xlws8hdylq3dieky5ut` (`id_server`),
  KEY `FKkiktrv9qgmrh2sthbfx0hkd7u` (`establishment_type`),
  KEY `FK9ygsp9nh47o8enlr6gbxu3v1s` (`study_level`),
  KEY `FK75axn0e4e11xu6ny1kvaebh4g` (`teaching_area`),
  KEY `FK_kx2faddje0w3cixn027phkl7b` (`account`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `degree_obtained`
--

LOCK TABLES `degree_obtained` WRITE;
/*!40000 ALTER TABLE `degree_obtained` DISABLE KEYS */;
/*!40000 ALTER TABLE `degree_obtained` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `diploma`
--

DROP TABLE IF EXISTS `diploma`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `diploma` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `label` varchar(255) NOT NULL,
  `education` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_gi7a8g81d35pkdo1xabrrc294` (`id_server`),
  KEY `FKtc8s0lrn5ie925rom0xka5hp` (`education`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `diploma`
--

LOCK TABLES `diploma` WRITE;
/*!40000 ALTER TABLE `diploma` DISABLE KEYS */;
INSERT INTO `diploma` VALUES (1105,'78869941664100','Brevet Nationale des collèges','Brevet Nationale des collèges',438),(1106,'78869956146500','Baccalauréat Scientifique','Baccalauréat Scientifique',438),(1107,'78869959061000','Baccalauréat Sciences économique et sociales','Baccalauréat Sciences économique et sociales',438),(1108,'78869961473600','Baccalauréat littéraire','Baccalauréat littéraire',438),(1109,'78869964118200','Série Sciences et technologies du management et de la gestion (STMG)','Série Sciences et technologies du management et de la gestion (STMG)',438),(1110,'78869966879700','Le baccalauréat sciences et technologies du design et des arts appliqués (STD2A)','Le baccalauréat sciences et technologies du design et des arts appliqués (STD2A)',438),(1111,'78869969180200','Le baccalauréat sciences et technologies de l\'industrie et du développement durable (STI2D)','Le baccalauréat sciences et technologies de l\'industrie et du développement durable (STI2D)',438),(1112,'78869972356300','Le baccalauréat sciences et technologies de laboratoire (STL)','Le baccalauréat sciences et technologies de laboratoire (STL)',438),(1113,'78869975100500','Le baccalauréat sciences et technologies de la santé et du social ST2S','Le baccalauréat sciences et technologies de la santé et du social ST2S',438),(1114,'78869977864500','Baccalauréat sciences et technologies de l\'agronomie et du vivant (STAV)','Baccalauréat sciences et technologies de l\'agronomie et du vivant (STAV)',438),(1115,'78869980719700','Baccalauréat technique de la musique et de la danse (TMD)','Baccalauréat technique de la musique et de la danse (TMD)',438),(1116,'78869984143500','Baccalauréat sciences et technologies de l\'hôtellerie et de la restauration (STHR)','Baccalauréat sciences et technologies de l\'hôtellerie et de la restauration (STHR)',438),(1117,'78869987226300','Autres (à préciser manuellement)','Autres (à préciser manuellement)',438),(1118,'78869990113600','CAP (Bac -1)','CAP (Bac -1)',438),(1119,'78869993327800','BEP (Bac -1)','BEP (Bac -1)',438),(1120,'78869996337600','Baccalauréat professionnel (à préciser manuellement)','Baccalauréat professionnel (à préciser manuellement)',438),(1121,'78869999481000','BTS (Bac +2)','BTS (Bac +2)',439),(1122,'78870002472500','DUT (BAC +2)','DUT (BAC +2)',439),(1123,'78870005318400','Licence Professionnelle (Bac +3)','Licence Professionnelle (Bac +3)',439),(1124,'78870007888100','Licence Générale (Bac+3)','Licence Générale (Bac+3)',439),(1125,'78870011010000','Maîtrise (Bac +4)','Maîtrise (Bac +4)',439),(1126,'78870015353500','Bachelor (Bac +4)','Bachelor (Bac +4)',439),(1127,'78870017894900','Master Professionnel (Bac +5)','Master Professionnel (Bac +5)',439),(1128,'78870020394000','Master Recherche (Bac +5)','Master Recherche (Bac +5)',439),(1129,'78870023428400','Magistère (Bac +5)','Magistère (Bac +5)',439),(1130,'78870026154400','Bac +6 (à préciser manuellement)','Bac +6 (à préciser manuellement)',439),(1131,'78870028624800','Bac +7 (à préciser manuellement)','Bac +7 (à préciser manuellement)',439),(1132,'78870030917300','Doctorat (Bac +8)','Doctorat (Bac +8)',439),(1133,'78870032740900','Autres (à sélectionner de bac +1 à Bac +11 et à préciser)','Autres (à sélectionner de bac +1 à Bac +11 et à préciser)',439),(1134,'78870034829100','Docteur en Pharmacie (Bac+6)','Docteur en Pharmacie (Bac+6)',439),(1135,'78870037419600','Docteur en Pharmacie Spécialisé (Bac+9)','Docteur en Pharmacie Spécialisé (Bac+9)',439),(1136,'78870040082900','Docteur en Médecine (Bac +9)','Docteur en Médecine (Bac +9)',439),(1137,'78870042801900','Docteur en chirurgie dentaire Spécialisé (Bac+10)','Docteur en chirurgie dentaire Spécialisé (Bac+10)',439),(1138,'78870045169800','Docteur en médecine spécialisé (Bac +11)','Docteur en médecine spécialisé (Bac +11)',439),(1139,'78870047543400','Autres (à sélectionner de bac +1 à Bac +11 et à préciser)','Autres (à sélectionner de bac +1 à Bac +11 et à préciser)',439);
/*!40000 ALTER TABLE `diploma` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `discipline`
--

DROP TABLE IF EXISTS `discipline`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `discipline` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `label` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_5hj653mpeo2wjao494f7p1w2w` (`id_server`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `discipline`
--

LOCK TABLES `discipline` WRITE;
/*!40000 ALTER TABLE `discipline` DISABLE KEYS */;
/*!40000 ALTER TABLE `discipline` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `education`
--

DROP TABLE IF EXISTS `education`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `education` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `code` varchar(50) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `label` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_rxld3r7vdl36kn9gej7eu65b7` (`id_server`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `education`
--

LOCK TABLES `education` WRITE;
/*!40000 ALTER TABLE `education` DISABLE KEYS */;
INSERT INTO `education` VALUES (438,'12680588341800','SECONDARY','Enseignement secondaire (avant le baccalauréat)','Enseignement secondaire (avant le baccalauréat)'),(439,'12680589479800','HIGHER','Enseignement supérieur (après le baccalauréat)','Enseignement supérieur (après le baccalauréat)');
/*!40000 ALTER TABLE `education` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `establishment_type`
--

DROP TABLE IF EXISTS `establishment_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `establishment_type` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `label` varchar(255) NOT NULL,
  `education` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_8tc237adw2hrai8twqk5lu5kf` (`id_server`),
  KEY `FK49cpx0mji0tomjon7caxro4eg` (`education`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `establishment_type`
--

LOCK TABLES `establishment_type` WRITE;
/*!40000 ALTER TABLE `establishment_type` DISABLE KEYS */;
INSERT INTO `establishment_type` VALUES (721,'105976758462400','Collège','Collège',438),(722,'105976766287800','Lycée général et Technologie','Lycée général et Technologie',438),(723,'105976768321100','Lycée professionnel','Lycée professionnel',438),(724,'105976770256800','Centre de formation d\'apprentis','Centre de formation d\'apprentis',438),(725,'105976772225400','Autres établissements du secondaire','Autres établissements du secondaire',438),(726,'105976774834200','Université','Université',439),(727,'105976777300700','IUT - Institut Universitaire de Technologie','IUT - Institut Universitaire de Technologie',439),(728,'105976779869700','Grandes écoles','Grandes écoles',439),(729,'105976782026900','Lycée Professionnel','Lycée Professionnel',439),(730,'105976784376900','Ecoles et Instituts Specialisés','Ecoles et Instituts Specialisés',439),(731,'105976786614800','Ecoles supérieure d\'Art et d\'Arts appliqués','Ecoles supérieure d\'Art et d\'Arts appliqués',439),(732,'105976788804400','Ecoles Nationales Supérieur d\'Architecture','Ecoles Nationales Supérieur d\'Architecture',439),(733,'105976790993100','Autres établissements du Supérieur','Autres établissements du Supérieur',439);
/*!40000 ALTER TABLE `establishment_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gender`
--

DROP TABLE IF EXISTS `gender`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gender` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `label` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_pmsrnxn4kayxewyfw2vp1rht4` (`id_server`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gender`
--

LOCK TABLES `gender` WRITE;
/*!40000 ALTER TABLE `gender` DISABLE KEYS */;
INSERT INTO `gender` VALUES (3,'11508831360000','Femme'),(4,'11508832442600','Homme');
/*!40000 ALTER TABLE `gender` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hibernate_sequence`
--

DROP TABLE IF EXISTS `hibernate_sequence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hibernate_sequence`
--

LOCK TABLES `hibernate_sequence` WRITE;
/*!40000 ALTER TABLE `hibernate_sequence` DISABLE KEYS */;
INSERT INTO `hibernate_sequence` VALUES (1171),(1171),(1171),(1171),(1171),(1171),(1171),(1171),(1171),(1171),(1171),(1171),(1171),(1171),(1171),(1171),(1171),(1171),(1171),(1171),(1171),(1171),(1171);
/*!40000 ALTER TABLE `hibernate_sequence` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `higher_education`
--

DROP TABLE IF EXISTS `higher_education`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `higher_education` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `active` bit(1) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `deleted` bit(1) DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  `revision` bigint(20) DEFAULT NULL,
  `city` varchar(255) NOT NULL,
  `current_position` bit(1) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `establishment_name` varchar(255) NOT NULL,
  `specialty` varchar(255) DEFAULT NULL,
  `account` int(11) DEFAULT NULL,
  `establishment_type` int(11) DEFAULT NULL,
  `study_level` int(11) DEFAULT NULL,
  `targeted_diploma` int(11) DEFAULT NULL,
  `teaching_area` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_h3tahd3n1q4kp7v3xwxxueoud` (`id_server`),
  KEY `FKa908thtmqa8crsydwj32f1jjl` (`targeted_diploma`),
  KEY `FK8mst42dw0j0ejjp0qh831ty29` (`teaching_area`),
  KEY `FK_1owtti119f95r1vv9oc7xnbml` (`establishment_type`),
  KEY `FK_kiwsm8pa9yd00er0xivjymk20` (`study_level`),
  KEY `FK_th7r3t3j275wb1g1mmxkko829` (`account`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `higher_education`
--

LOCK TABLES `higher_education` WRITE;
/*!40000 ALTER TABLE `higher_education` DISABLE KEYS */;
/*!40000 ALTER TABLE `higher_education` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `infos_media_type`
--

DROP TABLE IF EXISTS `infos_media_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `infos_media_type` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `color` varchar(50) NOT NULL,
  `position` tinyint(4) NOT NULL,
  `url` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_666irr8e1spjsdlf4en8q0i2m` (`id_server`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `infos_media_type`
--

LOCK TABLES `infos_media_type` WRITE;
/*!40000 ALTER TABLE `infos_media_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `infos_media_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `level`
--

DROP TABLE IF EXISTS `level`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `level` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `description` varchar(50) DEFAULT NULL,
  `grade` int(11) NOT NULL,
  `icon` varchar(50) NOT NULL,
  `label` varchar(50) NOT NULL,
  `mastered` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_avbprobsmujuclg54t1crl3ip` (`id_server`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `level`
--

LOCK TABLES `level` WRITE;
/*!40000 ALTER TABLE `level` DISABLE KEYS */;
INSERT INTO `level` VALUES (1101,'77581904485800','Très bien',2,'green-arrow-plus.svg','Très bien',''),(1102,'77581905490800','Bien',1,'green-arrow.svg','Bien',''),(1103,'77581906341100','Notions de base',2,'orange-arrow.svg','Notions de base','\0'),(1104,'77581907171400','A approfondir',1,'red-arrow.svg','A approfondir','\0');
/*!40000 ALTER TABLE `level` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `likepost`
--

DROP TABLE IF EXISTS `likepost`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `likepost` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `account` int(11) DEFAULT NULL,
  `post` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_70dnwghwsc9oarv6u4511h6dc` (`id_server`),
  KEY `FKtayfrocv9dx7xaib7s0iq6m4v` (`account`),
  KEY `FKrpx8mhp3yv4x8k3dx8rlmkmsy` (`post`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `likepost`
--

LOCK TABLES `likepost` WRITE;
/*!40000 ALTER TABLE `likepost` DISABLE KEYS */;
/*!40000 ALTER TABLE `likepost` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `link`
--

DROP TABLE IF EXISTS `link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `link` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `active` bit(1) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `deleted` bit(1) DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  `revision` bigint(20) DEFAULT NULL,
  `link_type` varchar(255) DEFAULT NULL,
  `url` varchar(255) NOT NULL,
  `account` int(11) DEFAULT NULL,
  `link` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_muo7t66qy7bjxt2mpgk9u8doq` (`id_server`),
  KEY `FKgcicrmmae2kfvim5u85a5n8b` (`account`),
  KEY `FKbe6t25ltf7ig4hla2jqo437ss` (`link`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `link`
--

LOCK TABLES `link` WRITE;
/*!40000 ALTER TABLE `link` DISABLE KEYS */;
/*!40000 ALTER TABLE `link` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `media_subtype`
--

DROP TABLE IF EXISTS `media_subtype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `media_subtype` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `label` varchar(255) NOT NULL,
  `media_type` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_fpk6ncx36twffmkqpvpie81o` (`id_server`),
  KEY `FKgjun9jfynng9l72q9v8kkj6y9` (`media_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `media_subtype`
--

LOCK TABLES `media_subtype` WRITE;
/*!40000 ALTER TABLE `media_subtype` DISABLE KEYS */;
INSERT INTO `media_subtype` VALUES (1140,'79438914763500','Cours',599),(1141,'79438923761400','Exercices et corrections',599),(1142,'79438926090000','Sujets d\'examen et corrections',599),(1143,'79438928738600','Fiches et astuces de revision',599),(1144,'79438931131900','Mémoires, Exposés, présentation',599),(1145,'79438933346600','Diplômes et certifications',599),(1146,'79438935457100','Réalisations professionnelles',599),(1147,'79438937852400','Travaux de recherche et livres',599),(1148,'79438940296800','Autres',599),(1149,'79438942644500','Vidéo pédagogique / éducative',600),(1150,'79438945229700','Conférence',600),(1151,'79438947310500','Actualités et débats',600),(1152,'79438949374100','Autres',600),(1153,'79438951754800','Audio pédagogique / éducatif',601),(1154,'79438954034600','Conférence',601),(1155,'79438956484900','Actualités et débats',601),(1156,'79438958895000','Autres',601),(1157,'79438961586900','Article de presse',602),(1158,'79438964284500','Revue scientifique / pédagogique',602),(1159,'79438966661200','Site web',602),(1160,'79438968975300','Blog',602),(1161,'79438972588400','Questionnaires et jeux éducatifs',602),(1162,'79438974800500','Livres',602),(1163,'79438976984700','Autres',602),(1164,'79438979584600','Photos des publications',603),(1165,'79438981782600','Photos personnelles',603),(1166,'79438984308000','Autres',603);
/*!40000 ALTER TABLE `media_subtype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `media_type`
--

DROP TABLE IF EXISTS `media_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `media_type` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `label` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_at21ep413yoo6ja3927db4x94` (`id_server`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `media_type`
--

LOCK TABLES `media_type` WRITE;
/*!40000 ALTER TABLE `media_type` DISABLE KEYS */;
INSERT INTO `media_type` VALUES (598,'30836624139400','AVATAR'),(599,'30836625115500','DOCUMENT'),(600,'30836626055600','VIDEO'),(601,'30836626936100','AUDIO'),(602,'30836628127000','LINK'),(603,'30836629124600','PICTURE');
/*!40000 ALTER TABLE `media_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `multimedia`
--

DROP TABLE IF EXISTS `multimedia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `multimedia` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `active` bit(1) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `deleted` bit(1) DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  `revision` bigint(20) DEFAULT NULL,
  `checksum` varchar(255) DEFAULT NULL,
  `directory` varchar(255) DEFAULT NULL,
  `extension` varchar(255) DEFAULT NULL,
  `mime_type` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `account` int(11) DEFAULT NULL,
  `multimedia` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_fjsq1euaq26h44sxodq1duswj` (`id_server`),
  KEY `FKjp62ph2bdbrb6kpcxswhvhy85` (`account`),
  KEY `FK51vq8lev5q0usgnbgb3abbb9p` (`multimedia`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `multimedia`
--

LOCK TABLES `multimedia` WRITE;
/*!40000 ALTER TABLE `multimedia` DISABLE KEYS */;
/*!40000 ALTER TABLE `multimedia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `post`
--

DROP TABLE IF EXISTS `post`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `post` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `active` bit(1) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `deleted` bit(1) DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  `revision` bigint(20) DEFAULT NULL,
  `description` varchar(2000) DEFAULT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  `title` varchar(50) NOT NULL,
  `account` int(11) DEFAULT NULL,
  `media_subtype` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_hj56xjem2k1cs8nwm1nsh2xjx` (`id_server`),
  KEY `FKhldpsq033xe7e406jkt2ovuno` (`account`),
  KEY `FKchy449augo3lewkdmh8e9juh4` (`media_subtype`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `post`
--

LOCK TABLES `post` WRITE;
/*!40000 ALTER TABLE `post` DISABLE KEYS */;
/*!40000 ALTER TABLE `post` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `professional`
--

DROP TABLE IF EXISTS `professional`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `professional` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `active` bit(1) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `deleted` bit(1) DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  `revision` bigint(20) DEFAULT NULL,
  `city` varchar(255) NOT NULL,
  `current_position` bit(1) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `establishment_name` varchar(255) NOT NULL,
  `specialty` varchar(255) DEFAULT NULL,
  `account` int(11) DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `job_title` varchar(255) NOT NULL,
  `start_date` datetime NOT NULL,
  `activity_area` int(11) DEFAULT NULL,
  `activity_sector` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_jhtn3j550trbpd1t9gvk99ni4` (`id_server`),
  KEY `FKi5t5gwrrxhgyjedhvxcekbyxc` (`activity_area`),
  KEY `FKqn9wbhf8i3wxe4tfu4x2lo8pm` (`activity_sector`),
  KEY `FK_otn62bgvcnopnav5abccfgiu3` (`account`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `professional`
--

LOCK TABLES `professional` WRITE;
/*!40000 ALTER TABLE `professional` DISABLE KEYS */;
INSERT INTO `professional` VALUES (1169,'102452943742000','','2020-10-19 16:40:53','\0',NULL,0,'Yaounde','',NULL,'YAMO GROUP',NULL,1,NULL,'Developpeuse','2020-10-01 02:00:00',765,422);
/*!40000 ALTER TABLE `professional` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `professional_status`
--

DROP TABLE IF EXISTS `professional_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `professional_status` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `label` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_fc9uvppyqis203m9mqheyyvc6` (`id_server`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `professional_status`
--

LOCK TABLES `professional_status` WRITE;
/*!40000 ALTER TABLE `professional_status` DISABLE KEYS */;
INSERT INTO `professional_status` VALUES (5,'11559034729500','Etudiant'),(6,'11559035748200','Enseignant'),(7,'11559036679300','Professionnel');
/*!40000 ALTER TABLE `professional_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `secondary_education`
--

DROP TABLE IF EXISTS `secondary_education`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `secondary_education` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `active` bit(1) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `deleted` bit(1) DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  `revision` bigint(20) DEFAULT NULL,
  `city` varchar(255) NOT NULL,
  `current_position` bit(1) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `establishment_name` varchar(255) NOT NULL,
  `specialty` varchar(255) DEFAULT NULL,
  `account` int(11) DEFAULT NULL,
  `establishment_type` int(11) DEFAULT NULL,
  `study_level` int(11) DEFAULT NULL,
  `prepared_diploma` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_nhxvw6ieoh23429mwaq1qkmsj` (`id_server`),
  KEY `FKk3ptaihbdqfncqbwwou95xust` (`prepared_diploma`),
  KEY `FK_o2ypntl0oqre1t4gkekwfvtse` (`establishment_type`),
  KEY `FK_sbaqi5vxvcxexqg00nj4kubhc` (`study_level`),
  KEY `FK_h91sw2cbvc4og7hptcd4fw3ua` (`account`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `secondary_education`
--

LOCK TABLES `secondary_education` WRITE;
/*!40000 ALTER TABLE `secondary_education` DISABLE KEYS */;
INSERT INTO `secondary_education` VALUES (1170,'102718994503200','','2020-10-19 16:45:20','\0',NULL,0,'Yaounde','\0',NULL,'College la retraite','Lettre',1,721,896,1105);
/*!40000 ALTER TABLE `secondary_education` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `skill`
--

DROP TABLE IF EXISTS `skill`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `skill` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `active` bit(1) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `deleted` bit(1) DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  `revision` bigint(20) DEFAULT NULL,
  `discipline` varchar(255) DEFAULT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  `label` varchar(255) NOT NULL,
  `skill_mastered` bit(1) DEFAULT NULL,
  `account` int(11) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_mgwf4f5epvhru88djnobni1vl` (`id_server`),
  KEY `FK25xg7gyx32jtdiuek1fpp4xx9` (`account`),
  KEY `FKirhnfwdwcegn8fstgix349moy` (`level`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `skill`
--

LOCK TABLES `skill` WRITE;
/*!40000 ALTER TABLE `skill` DISABLE KEYS */;
INSERT INTO `skill` VALUES (1167,'81143587202000','','2020-10-19 10:45:44','\0',NULL,0,'Mathematiques','geometrie','Geomatrie plane','',1,1101),(1168,'102389425651300','','2020-10-19 16:39:50','\0',NULL,0,'Droit','affaires, droits','Droit des affaires','\0',1,1104);
/*!40000 ALTER TABLE `skill` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `study_level`
--

DROP TABLE IF EXISTS `study_level`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `study_level` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `label` varchar(255) NOT NULL,
  `education` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_jmdv843pnvonbtfhom3i794u1` (`id_server`),
  KEY `FK19p7f1vp5j6un4jr0u5svnvwp` (`education`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `study_level`
--

LOCK TABLES `study_level` WRITE;
/*!40000 ALTER TABLE `study_level` DISABLE KEYS */;
INSERT INTO `study_level` VALUES (894,'109175364431700','Sixième (6ème)','Sixième (6ème)',438),(895,'109175373156700','Cinquième (5ème)','Cinquième (5ème)',438),(896,'109175375387400','Quatrième (4ème)','Quatrième (4ème)',438),(897,'109175377541200','Troisième (3ème)','Troisième (3ème)',438),(898,'109175379615000','Première S (Scientifique)','Première S (Scientifique)',438),(899,'109175381826000','Première ES (Économique et social)','Première ES (Économique et social)',438),(900,'109175383832800','Première L (Littéraire)','Première L (Littéraire)',438),(901,'109175386356800','Terminal S (Scientifique)','Terminal S (Scientifique)',438),(902,'109175388462200','Terminal ES (Économique et social)','Terminal ES (Économique et social)',438),(903,'109175391086100','Terminal L (Littéraire)','Terminal L (Littéraire)',438),(904,'109175393329100','Première STMG (Sciences et technologies du management et de la gestion)','Première STMG (Sciences et technologies du management et de la gestion)',438),(905,'109175395738300','Première STD2A (Sciences et technologies du design et des arts appliqués)','Première STD2A (Sciences et technologies du design et des arts appliqués)',438),(906,'109175398259800','Première STI2D (sciences et technologies de l\'industrie et du développement durable)','Première STI2D (sciences et technologies de l\'industrie et du développement durable)',438),(907,'109175400254800','Première STL (sciences et technologies de laboratoire)','Première STL (sciences et technologies de laboratoire)',438),(908,'109175402496900','Première ST2S (sciences et technologies de la santé et du social)','Première ST2S (sciences et technologies de la santé et du social)',438),(909,'109175404587400','Première STAV (sciences et technologies de l\'agronomie et du vivant)','Première STAV (sciences et technologies de l\'agronomie et du vivant)',438),(910,'109175406961400','Première TMD (technique de la musique et de la danse)','Première TMD (technique de la musique et de la danse)',438),(911,'109175408990100','Première STHR (sciences et technologies de l\'hôtellerie et de la restauration)','Première STHR (sciences et technologies de l\'hôtellerie et de la restauration)',438),(912,'109175411165400','Terminal STMG (Sciences et technologies du management et de la gestion)','Terminal STMG (Sciences et technologies du management et de la gestion)',438),(913,'109175413303400','Terminal STD2A (Sciences et technologies du design et des arts appliqués)','Terminal STD2A (Sciences et technologies du design et des arts appliqués)',438),(914,'109175415608900','Terminal STI2D (sciences et technologies de l\'industrie et du développement durable)','Terminal STI2D (sciences et technologies de l\'industrie et du développement durable)',438),(915,'109175418957900','Terminal STL (sciences et technologies de laboratoire)','Terminal STL (sciences et technologies de laboratoire)',438),(916,'109175420935700','Terminal ST2S (sciences et technologies de la santé et du social)','Terminal ST2S (sciences et technologies de la santé et du social)',438),(917,'109175422980500','Terminal STAV (sciences et technologies de l\'agronomie et du vivant)','Terminal STAV (sciences et technologies de l\'agronomie et du vivant)',438),(918,'109175425133000','Terminal TMD (technique de la musique et de la danse)','Terminal TMD (technique de la musique et de la danse)',438),(919,'109175427187800','Terminal STHR (sciences et technologies de l\'hôtellerie et de la restauration)','Terminal STHR (sciences et technologies de l\'hôtellerie et de la restauration)',438),(920,'109175429011600','Seconde professionnelle CAP','Seconde professionnelle CAP',438),(921,'109175431024500','Première Professionnelle CAP','Première Professionnelle CAP',438),(922,'109175432849500','Seconde professionnelle BEP','Seconde professionnelle BEP',438),(923,'109175434977000','Première Professionnelle BEP','Première Professionnelle BEP',438),(924,'109175436954300','Seconde CAP','Seconde CAP',438),(925,'109175439062800','Première CAP','Première CAP',438),(926,'109175441318700','Autres établissements du secondaire','Autres établissements du secondaire',438),(927,'109175443426300','Licence 1 (Bac +1)','Licence 1 (Bac +1)',439),(928,'109175445325600','Licence 2 (Bac+2)','Licence 2 (Bac+2)',439),(929,'109175447236600','Licence 3 (Bac+3)','Licence 3 (Bac+3)',439),(930,'109175449100400','Master 1 (Bac+4)','Master 1 (Bac+4)',439),(931,'109175451088600','Master 2 (Bac+5)','Master 2 (Bac+5)',439),(932,'109175453159600','Doctorat 1 (Bac +6)','Doctorat 1 (Bac +6)',439),(933,'109175455316400','Doctorat 2 (Bac +7)','Doctorat 2 (Bac +7)',439),(934,'109175457545300','Doctorat 3 (Bac +8)','Doctorat 3 (Bac +8)',439),(935,'109175459700800','Première année (Bac +1)','Première année (Bac +1)',439),(936,'109175462112800','Deuxième année (Bac +2)','Deuxième année (Bac +2)',439),(937,'109175464066900','Classes préparatoires : Bac +1 à Bac +2','Classes préparatoires : Bac +1 à Bac +2',439),(938,'109175466067700','Bachelor (Bac+4)','Bachelor (Bac+4)',439),(939,'109175467869900','Première année BTS (Bac +1)','Première année BTS (Bac +1)',439),(940,'109175470371300','Deuxième année BTS (Bac +2)','Deuxième année BTS (Bac +2)',439),(941,'109175473998900','Première année IUT (Bac +1)','Première année IUT (Bac +1)',439),(942,'109175476685800','Deuxième année IUT (Bac +2)','Deuxième année IUT (Bac +2)',439),(943,'109175479128700','Autres à préciser manuellement','Autres à préciser manuellement',439);
/*!40000 ALTER TABLE `study_level` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teaching_area`
--

DROP TABLE IF EXISTS `teaching_area`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teaching_area` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `label` varchar(255) NOT NULL,
  `education` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_ox8ks35vdkc9nd83in6u23a1i` (`id_server`),
  KEY `FKr5km3wq5qmfmu05ttam0ojqu3` (`education`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teaching_area`
--

LOCK TABLES `teaching_area` WRITE;
/*!40000 ALTER TABLE `teaching_area` DISABLE KEYS */;
INSERT INTO `teaching_area` VALUES (945,'111472037396300','Droit privé et sciences criminelles','Droit privé et sciences criminelles',439),(946,'111472045568600','Droit public','Droit public',439),(947,'111472047525700','Histoire du droit et des institutions','Histoire du droit et des institutions',439),(948,'111472049572700','Science politique','Science politique',439),(949,'111472051998400','Sciences économiques','Sciences économiques',439),(950,'111472054248600','Sciences de gestion','Sciences de gestion',439),(951,'111472056345300','Sciences du langage : linguistique et phonétique générales','Sciences du langage : linguistique et phonétique générales',439),(952,'111472058813700','Langues et littératures anciennes','Langues et littératures anciennes',439),(953,'111472061024800','Langue et littérature françaises','Langue et littérature françaises',439),(954,'111472063142100','Littératures comparées','Littératures comparées',439),(955,'111472065232200','Langues et littératures anglaises et anglo-saxonnes','Langues et littératures anglaises et anglo-saxonnes',439),(956,'111472067274300','Langues et littératures germaniques et scandinaves','Langues et littératures germaniques et scandinaves',439),(957,'111472069765100','Langues et littératures romanes : espagnol, italien, portugais, autres langues romanes','Langues et littératures romanes : espagnol, italien, portugais, autres langues romanes',439),(958,'111472071790200','Langues et littératures arabes, chinoises, japonaises, hébraïques, d\'autres domaines linguistiques','Langues et littératures arabes, chinoises, japonaises, hébraïques, d\'autres domaines linguistiques',439),(959,'111472073900100','Psychologie et Ergonomie','Psychologie et Ergonomie',439),(960,'111472075955500','Philosophie','Philosophie',439),(961,'111472078106100','Architecture et Arts : plastiques, du spectacle, musique, musicologie, esthétique, sciences de l\'art','Architecture et Arts : plastiques, du spectacle, musique, musicologie, esthétique, sciences de l\'art',439),(962,'111472080147600','Sociologie, démographie','Sociologie, démographie',439),(963,'111472082150600','Anthropologie biologique, ethnologie, préhistoire','Anthropologie biologique, ethnologie, préhistoire',439),(964,'111472084336100','Histoire et civilisations : histoire et archéologie des mondes anciens et des mondes médiévaux ; de l\'art','Histoire et civilisations : histoire et archéologie des mondes anciens et des mondes médiévaux ; de l\'art',439),(965,'111472086678700','Histoire et civilisations : histoire des mondes modernes, histoire du monde contemporain ; de l\'art ; de la musique','Histoire et civilisations : histoire des mondes modernes, histoire du monde contemporain ; de l\'art ; de la musique',439),(966,'111472090014300','Géographie physique, humaine, économique et régionale','Géographie physique, humaine, économique et régionale',439),(967,'111472092079900','Aménagement de l\'espace, urbanisme','Aménagement de l\'espace, urbanisme',439),(968,'111472093893500','Sciences de l\'éducation','Sciences de l\'éducation',439),(969,'111472095980800','Sciences de l\'information et de la communication','Sciences de l\'information et de la communication',439),(970,'111472098092200','Épistémologie, histoire des sciences et des techniques','Épistémologie, histoire des sciences et des techniques',439),(971,'111472099954400','Cultures et langues régionales','Cultures et langues régionales',439),(972,'111472102274800','Sciences et techniques des activités physiques et sportives','Sciences et techniques des activités physiques et sportives',439),(973,'111472104290700','Théologie catholique','Théologie catholique',439),(974,'111472106393500','Théologie protestante','Théologie protestante',439),(975,'111472108529200','Mathématiques','Mathématiques',439),(976,'111472110839300','Mathématiques appliquées et applications des mathématiques','Mathématiques appliquées et applications des mathématiques',439),(977,'111472113002300','Informatique','Informatique',439),(978,'111472115143600','Milieux denses et matériaux','Milieux denses et matériaux',439),(979,'111472117031200','Constituants élémentaires','Constituants élémentaires',439),(980,'111472119212200','Milieux dilués et optique','Milieux dilués et optique',439),(981,'111472121490500','Chimie théorique, physique, analytique','Chimie théorique, physique, analytique',439),(982,'111472123689300','Chimie organique, inorganique, industrielle','Chimie organique, inorganique, industrielle',439),(983,'111472125710300','Chimie des matériaux','Chimie des matériaux',439),(984,'111472127729100','Astronomie, astrophysique','Astronomie, astrophysique',439),(985,'111472130027000','Structure et évolution de la Terre et des autres planètes','Structure et évolution de la Terre et des autres planètes',439),(986,'111472132233300','Terre solide : géodynamique des enveloppes supérieures, paléobiosphère','Terre solide : géodynamique des enveloppes supérieures, paléobiosphère',439),(987,'111472134838300','Météorologie, océanographie physique et physique de l\'environnement','Météorologie, océanographie physique et physique de l\'environnement',439),(988,'111472136931500','Mécanique, génie mécanique, génie civil','Mécanique, génie mécanique, génie civil',439),(989,'111472138936400','Génie informatique, automatique et traitement du signal','Génie informatique, automatique et traitement du signal',439),(990,'111472140731700','Énergétique, génie des procédés','Énergétique, génie des procédés',439),(991,'111472142670300','Génie Électrique, Électronique, optronique et systèmes','Génie Électrique, Électronique, optronique et systèmes',439),(992,'111472144536300','Biochimie et biologie moléculaire','Biochimie et biologie moléculaire',439),(993,'111472146417400','Biologie cellulaire','Biologie cellulaire',439),(994,'111472148293900','Physiologie','Physiologie',439),(995,'111472150350500','Biologie des populations et écologie','Biologie des populations et écologie',439),(996,'111472152605200','Biologie des organismes','Biologie des organismes',439),(997,'111472154518400','Neurosciences','Neurosciences',439),(998,'111472156521100','Sciences physico-chimiques et ingénierie appliquée à la santé','Sciences physico-chimiques et ingénierie appliquée à la santé',439),(999,'111472158395200','Sciences du médicament et des autres produits de santé','Sciences du médicament et des autres produits de santé',439),(1000,'111472160089000','Sciences biologiques, fondamentales et cliniques','Sciences biologiques, fondamentales et cliniques',439),(1001,'111472161949300','Sciences physico-chimiques et ingénierie appliquée à la santé','Sciences physico-chimiques et ingénierie appliquée à la santé',439),(1002,'111472163895300','Sciences du médicament et des autres produits de santé','Sciences du médicament et des autres produits de santé',439),(1003,'111472165707400','Sciences biologiques, fondamentales et cliniques','Sciences biologiques, fondamentales et cliniques',439),(1004,'111472168621300','Maïeutique','Maïeutique',439),(1005,'111472171201800','Sciences de la rééducation et de la réadaptation','Sciences de la rééducation et de la réadaptation',439),(1006,'111472173667200','Sciences infirmières','Sciences infirmières',439),(1007,'111472175606700','Anatomie','Anatomie',439),(1008,'111472178810400','Histologie, embryologie et cytogénétique','Histologie, embryologie et cytogénétique',439),(1009,'111472181456100','Anatomie et cytologie pathologiques','Anatomie et cytologie pathologiques',439),(1010,'111472183515100','Biophysique et médecine nucléaire','Biophysique et médecine nucléaire',439),(1011,'111472185652700','Radiologie et imagerie médicale','Radiologie et imagerie médicale',439),(1012,'111472187452200','Biochimie et biologie moléculaire','Biochimie et biologie moléculaire',439),(1013,'111472189311000','Physiologie','Physiologie',439),(1014,'111472190987000','Biologie cellulaire','Biologie cellulaire',439),(1015,'111472192611000','Nutrition','Nutrition',439),(1016,'111472194285500','Bactériologie - virologie ; hygiène hospitalière','Bactériologie - virologie ; hygiène hospitalière',439),(1017,'111472195889300','Parasitologie et mycologie','Parasitologie et mycologie',439),(1018,'111472197472800','Maladies infectieuses ; maladies tropicales','Maladies infectieuses ; maladies tropicales',439),(1019,'111472199273300','Épidémiologie, économie de la santé et prévention','Épidémiologie, économie de la santé et prévention',439),(1020,'111472201091800','Médecine et santé au travail','Médecine et santé au travail',439),(1021,'111472202893500','Médecine légale et droit de la santé','Médecine légale et droit de la santé',439),(1022,'111472204468800','Biostatistiques, informatique médicale et technologies de communication','Biostatistiques, informatique médicale et technologies de communication',439),(1023,'111472206062800','Épistémologie clinique','Épistémologie clinique',439),(1024,'111472207605700','Hématologie ; transfusion','Hématologie ; transfusion',439),(1025,'111472209220100','Cancérologie ; radiothérapie','Cancérologie ; radiothérapie',439),(1026,'111472210971400','Immunologie','Immunologie',439),(1027,'111472212697500','Génétique','Génétique',439),(1028,'111472214360700','Anesthésiologie - réanimation ; médecine d\'urgence','Anesthésiologie - réanimation ; médecine d\'urgence',439),(1029,'111472216319200','Réanimation ; médecine d\'urgence','Réanimation ; médecine d\'urgence',439),(1030,'111472219916900','Pharmacologie fondamentale ; pharmacologie clinique','Pharmacologie fondamentale ; pharmacologie clinique',439),(1031,'111472222161700','Thérapeutique','Thérapeutique',439),(1032,'111472224015100','Neurologie','Neurologie',439),(1033,'111472225981600','Neurochirurgie','Neurochirurgie',439),(1034,'111472227679200','Psychiatrie d\'adultes','Psychiatrie d\'adultes',439),(1035,'111472230036400','Pédopsychiatrie','Pédopsychiatrie',439),(1036,'111472232402200','Médecine physique et de réadaptation','Médecine physique et de réadaptation',439),(1037,'111472235108500','Rhumatologie','Rhumatologie',439),(1038,'111472237260200','Chirurgie orthopédique et traumatologique','Chirurgie orthopédique et traumatologique',439),(1039,'111472239348200','Dermato-vénéréologie','Dermato-vénéréologie',439),(1040,'111472241511300','Chirurgie plastique, reconstructrice et esthétique ; brûlologie','Chirurgie plastique, reconstructrice et esthétique ; brûlologie',439),(1041,'111472243327100','Pneumologie','Pneumologie',439),(1042,'111472245144800','Cardiologie','Cardiologie',439),(1043,'111472246918800','Chirurgie thoracique et cardiovasculaire','Chirurgie thoracique et cardiovasculaire',439),(1044,'111472248447400','Chirurgie vasculaire ; médecine vasculaire','Chirurgie vasculaire ; médecine vasculaire',439),(1045,'111472492393100','Gastroentérologie ; hépatologie','Gastroentérologie ; hépatologie',439),(1046,'111472494257300','Chirurgie digestive','Chirurgie digestive',439),(1047,'111472496009200','Néphrologie','Néphrologie',439),(1048,'111472497579500','Urologie','Urologie',439),(1049,'111472499021900','Médecine interne ; gériatrie et biologie du vieillissement, addictologie','Médecine interne ; gériatrie et biologie du vieillissement, addictologie',439),(1050,'111472500566900','Chirurgie générale','Chirurgie générale',439),(1051,'111472503461000','Médecine générale','Médecine générale',439),(1052,'111472505740700','Pédiatrie','Pédiatrie',439),(1053,'111472507755700','Chirurgie infantile','Chirurgie infantile',439),(1054,'111472509536600','Gynécologie-obstétrique ; gynécologie médicale','Gynécologie-obstétrique ; gynécologie médicale',439),(1055,'111472511036100','Endocrinologie, diabète et maladies métaboliques','Endocrinologie, diabète et maladies métaboliques',439),(1056,'111472512359800','Biologie et médecine du développement et de la reproduction','Biologie et médecine du développement et de la reproduction',439),(1057,'111472513838500','Oto-rhino-laryngologie','Oto-rhino-laryngologie',439),(1058,'111472515385500','Ophtalmologie','Ophtalmologie',439),(1059,'111472516974100','Chirurgie maxillo-faciale et stomatologie','Chirurgie maxillo-faciale et stomatologie',439),(1060,'111472520674500','Pédodontie','Pédodontie',439),(1061,'111472522927600','Orthopédie dento-faciale','Orthopédie dento-faciale',439),(1062,'111472524460600','Prévention, épidémiologie, économie de la santé, odontologie légale','Prévention, épidémiologie, économie de la santé, odontologie légale',439),(1063,'111472525999200','Parodontologie','Parodontologie',439),(1064,'111472527483900','Chirurgie buccale, pathologie et thérapeutique, anesthésiologie et réanimation','Chirurgie buccale, pathologie et thérapeutique, anesthésiologie et réanimation',439),(1065,'111472529276800','Sciences biologiques (biochimie, immunologie, histologie, embryologie, génétique, anatomie pathologique, bactériologie, pharmacologie)','Sciences biologiques (biochimie, immunologie, histologie, embryologie, génétique, anatomie pathologique, bactériologie, pharmacologie)',439),(1066,'111472531313300','Odontologie conservatrice, endodontie','Odontologie conservatrice, endodontie',439),(1067,'111472533125300','Prothèses','Prothèses',439),(1068,'111472535077300','Sciences anatomiques et physiologiques, occlusodontiques, biomatériaux, biophysique, radiologie','Sciences anatomiques et physiologiques, occlusodontiques, biomatériaux, biophysique, radiologie',439);
/*!40000 ALTER TABLE `teaching_area` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'skillsmatesdb'
--

--
-- Dumping routines for database 'skillsmatesdb'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-10-19 17:01:11
