--
-- update columns of account table
--

ALTER TABLE skillsmatesdb.account ADD current_establishment_name  varchar(255);
ALTER TABLE skillsmatesdb.account ADD current_job_title  varchar(255);