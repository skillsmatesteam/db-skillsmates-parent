LOCK TABLES `post` WRITE;

ALTER TABLE skillsmatesdb.post ADD media_subtype int(11);

ALTER TABLE skillsmatesdb.post
ADD CONSTRAINT FKt9gha83it3k5u801vlsm762pf
FOREIGN KEY (media_subtype) REFERENCES skillsmatesdb.media_subtype(id);

UNLOCK TABLES;