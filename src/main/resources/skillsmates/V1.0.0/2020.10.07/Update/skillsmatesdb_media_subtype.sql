-- MariaDB dump 10.17  Distrib 10.4.13-MariaDB, for Win64 (AMD64)
--
-- Host: 127.0.0.1    Database: skillsmatesdb
-- ------------------------------------------------------
-- Server version	10.4.13-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `media_subtype`
--

DROP TABLE IF EXISTS `media_subtype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `media_subtype` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `label` varchar(255) NOT NULL,
  `media_type` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_fpk6ncx36twffmkqpvpie81o` (`id_server`),
  KEY `FKgjun9jfynng9l72q9v8kkj6y9` (`media_type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `media_subtype`
--

LOCK TABLES `media_subtype` WRITE;
/*!40000 ALTER TABLE `media_subtype` DISABLE KEYS */;
INSERT INTO `media_subtype` VALUES (604,'30901732901700','Exercices et corrections',599),(605,'30901742129200','Sujets d\'examen et corrections',599),(606,'30901744286500','Fiches et astuces de revision',599),(607,'30901746306800','Mémoires, Exposés, présentation',599),(608,'30901748327700','Diplômes et certifications',599),(609,'30901750225700','Réalisations professionnelles',599),(610,'30901752252100','Travaux de recherche et livres',599),(611,'30901754571400','Autres',599),(612,'30901757168200','Vidéo pédagogique / éducative',600),(613,'30901759285000','Conférence',600),(614,'30901761595800','Actualités et débats',600),(615,'30901763526300','Autres',600),(616,'30901765701600','Audio pédagogique / éducatif',601),(617,'30901767644100','Conférence',601),(618,'30901769697600','Actualités et débats',601),(619,'30901772143300','Autres',601),(620,'30901774722200','Article de presse',602),(621,'30901777089600','Revue scientifique / pédagogique',602),(622,'30901779268100','Site web',602),(623,'30901781467600','Blog',602),(624,'30901783623700','Questionnaires et jeux éducatifs',602),(625,'30901787037900','Livres',602),(626,'30901789681900','Autres',602),(627,'30901792022600','Photos des publications',603),(628,'30901794100800','Photos personnelles',603),(629,'30901796234800','Autres',603);
/*!40000 ALTER TABLE `media_subtype` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-10-07 13:04:11
