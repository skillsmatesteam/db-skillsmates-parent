-- MariaDB dump 10.17  Distrib 10.4.13-MariaDB, for Win64 (AMD64)
--
-- Host: 127.0.0.1    Database: skillsmatesdb
-- ------------------------------------------------------
-- Server version	10.4.13-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `diploma`
--

DROP TABLE IF EXISTS `diploma`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `diploma` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `label` varchar(255) NOT NULL,
  `education` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_gi7a8g81d35pkdo1xabrrc294` (`id_server`),
  KEY `FKtc8s0lrn5ie925rom0xka5hp` (`education`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `diploma`
--

LOCK TABLES `diploma` WRITE;
/*!40000 ALTER TABLE `diploma` DISABLE KEYS */;
INSERT INTO `diploma` VALUES (519,'12995378753500','Baccalauréat Scientifique','Baccalauréat Scientifique',438),(520,'12995390650400','Baccalauréat Sciences économique et sociales','Baccalauréat Sciences économique et sociales',438),(521,'12995393583600','Baccalauréat littéraire','Baccalauréat littéraire',438),(522,'12995395992000','Série Sciences et technologies du management et de la gestion (STMG)','Série Sciences et technologies du management et de la gestion (STMG)',438),(523,'12995399289700','Le baccalauréat sciences et technologies du design et des arts appliqués (STD2A)','Le baccalauréat sciences et technologies du design et des arts appliqués (STD2A)',438),(524,'12995402050800','Le baccalauréat sciences et technologies de l\'industrie et du développement durable (STI2D)','Le baccalauréat sciences et technologies de l\'industrie et du développement durable (STI2D)',438),(525,'12995404896300','Le baccalauréat sciences et technologies de laboratoire (STL)','Le baccalauréat sciences et technologies de laboratoire (STL)',438),(526,'12995407741000','Le baccalauréat sciences et technologies de la santé et du social ST2S','Le baccalauréat sciences et technologies de la santé et du social ST2S',438),(527,'12995410892800','Baccalauréat sciences et technologies de l\'agronomie et du vivant (STAV)','Baccalauréat sciences et technologies de l\'agronomie et du vivant (STAV)',438),(528,'12995413883900','Baccalauréat technique de la musique et de la danse (TMD)','Baccalauréat technique de la musique et de la danse (TMD)',438),(529,'12995416450400','Baccalauréat sciences et technologies de l\'hôtellerie et de la restauration (STHR)','Baccalauréat sciences et technologies de l\'hôtellerie et de la restauration (STHR)',438),(530,'12995419445300','Autres (à préciser manuellement)','Autres (à préciser manuellement)',438),(531,'12995422176200','CAP (Bac -1)','CAP (Bac -1)',438),(532,'12995424668800','BEP (Bac -1)','BEP (Bac -1)',438),(533,'12995427406200','Baccalauréat professionnel (à préciser manuellement)','Baccalauréat professionnel (à préciser manuellement)',438),(534,'12995429865900','BTS (Bac +2)','BTS (Bac +2)',439),(535,'12995432877100','DUT (BAC +2)','DUT (BAC +2)',439),(536,'12995435257000','Licence Professionnelle (Bac +3)','Licence Professionnelle (Bac +3)',439),(537,'12995438172400','Licence Générale (Bac+3)','Licence Générale (Bac+3)',439),(538,'12995441159000','Maîtrise (Bac +4)','Maîtrise (Bac +4)',439),(539,'12995443871200','Bachelor (Bac +4)','Bachelor (Bac +4)',439),(540,'12995447398600','Master Professionnel (Bac +5)','Master Professionnel (Bac +5)',439),(541,'12995449780400','Master Recherche (Bac +5)','Master Recherche (Bac +5)',439),(542,'12995452129600','Magistère (Bac +5)','Magistère (Bac +5)',439),(543,'12995454975700','Bac +6 (à préciser manuellement)','Bac +6 (à préciser manuellement)',439),(544,'12995457561400','Bac +7 (à préciser manuellement)','Bac +7 (à préciser manuellement)',439),(545,'12995460555200','Doctorat (Bac +8)','Doctorat (Bac +8)',439),(546,'12995463250900','Autres (à sélectionner de bac +1 à Bac +11 et à préciser)','Autres (à sélectionner de bac +1 à Bac +11 et à préciser)',439),(547,'12995465311300','Docteur en Pharmacie (Bac+6)','Docteur en Pharmacie (Bac+6)',439),(548,'12995467610300','Docteur en Pharmacie Spécialisé (Bac+9)','Docteur en Pharmacie Spécialisé (Bac+9)',439),(549,'12995470602400','Docteur en Médecine (Bac +9)','Docteur en Médecine (Bac +9)',439),(550,'12995473370100','Docteur en chirurgie dentaire Spécialisé (Bac+10)','Docteur en chirurgie dentaire Spécialisé (Bac+10)',439),(551,'12995476107000','Docteur en médecine spécialisé (Bac +11)','Docteur en médecine spécialisé (Bac +11)',439),(552,'12995478501600','Autres (à sélectionner de bac +1 à Bac +11 et à préciser)','Autres (à sélectionner de bac +1 à Bac +11 et à préciser)',439);
/*!40000 ALTER TABLE `diploma` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-10-07 13:04:07
