-- MariaDB dump 10.17  Distrib 10.4.13-MariaDB, for Win64 (AMD64)
--
-- Host: 127.0.0.1    Database: skillsmatesdb
-- ------------------------------------------------------
-- Server version	10.4.13-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `batch_job_instance`
--

DROP TABLE IF EXISTS `batch_job_instance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `batch_job_instance` (
  `JOB_INSTANCE_ID` bigint(20) NOT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `JOB_NAME` varchar(100) NOT NULL,
  `JOB_KEY` varchar(32) NOT NULL,
  PRIMARY KEY (`JOB_INSTANCE_ID`),
  UNIQUE KEY `JOB_INST_UN` (`JOB_NAME`,`JOB_KEY`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `batch_job_instance`
--

LOCK TABLES `batch_job_instance` WRITE;
/*!40000 ALTER TABLE `batch_job_instance` DISABLE KEYS */;
INSERT INTO `batch_job_instance` VALUES (1,0,'populateObjectsJob','853d3449e311f40366811cbefb3d93d7'),(2,0,'populateObjectsJob','e070bff4379694c0210a51d9f6c6a564'),(3,0,'populateObjectsJob','a3364faf893276dea0caacefbf618db5'),(4,0,'populateObjectsJob','47c0a8118b74165a864b66d37c7b6cf5'),(5,0,'populateObjectsJob','ce148f5f9c2bf4dc9bd44a7a5f64204c'),(6,0,'populateObjectsJob','bd0034040292bc81e6ccac0e25d9a578'),(7,0,'populateObjectsJob','597815c7e4ab1092c1b25130aae725cb'),(8,0,'populateObjectsJob','f55a96b11012be4fcfb6cf005435182d'),(9,0,'populateObjectsJob','96a5ed9bac43e779455f3e71c0f64840'),(10,0,'populateObjectsJob','1aac4f3e74894b78fa3ce5d8a25e1ef0'),(11,0,'populateObjectsJob','604bbfc4c68cb1f903780c2853ad4801'),(12,0,'populateObjectsJob','556ebe34220b4032509f2581356ba47c'),(13,0,'populateObjectsJob','edc440efb5ddd2a3b2622f16a12bf105'),(14,0,'populateObjectsJob','f3d5e568c384ee72cba8bc6a51057fe4'),(15,0,'populateObjectsJob','378ef1ecb81cf9edac4ab119bdab9d9d'),(16,0,'populateObjectsJob','e073471cc312cadef424c3be7915c0af'),(17,0,'populateObjectsJob','46ba78a99abf1e2fba4a8861749d7572');
/*!40000 ALTER TABLE `batch_job_instance` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-10-07 13:04:05
