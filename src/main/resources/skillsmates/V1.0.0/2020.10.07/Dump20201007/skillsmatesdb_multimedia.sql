-- MariaDB dump 10.17  Distrib 10.4.13-MariaDB, for Win64 (AMD64)
--
-- Host: 127.0.0.1    Database: skillsmatesdb
-- ------------------------------------------------------
-- Server version	10.4.13-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `multimedia`
--

DROP TABLE IF EXISTS `multimedia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `multimedia` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `active` bit(1) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `deleted` bit(1) DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  `revision` bigint(20) DEFAULT NULL,
  `checksum` varchar(255) DEFAULT NULL,
  `directory` varchar(255) DEFAULT NULL,
  `extension` varchar(255) DEFAULT NULL,
  `mime_type` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `account` int(11) DEFAULT NULL,
  `multimedia` int(11) DEFAULT NULL,
  `media_subtype` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_fjsq1euaq26h44sxodq1duswj` (`id_server`),
  KEY `FKjp62ph2bdbrb6kpcxswhvhy85` (`account`),
  KEY `FKt9gha83it3k5u801vlsm762pf` (`media_subtype`),
  KEY `FK51vq8lev5q0usgnbgb3abbb9p` (`multimedia`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `multimedia`
--

LOCK TABLES `multimedia` WRITE;
/*!40000 ALTER TABLE `multimedia` DISABLE KEYS */;
INSERT INTO `multimedia` VALUES (2,'11404313335800','','2020-09-27 21:04:25','\0',NULL,0,'3a23149a','11308326636800','jpg','image/jpeg','mengue.jpg','AVATAR',1,NULL,NULL),(570,'62562111108800','','2020-09-28 11:17:01','\0',NULL,0,'dd755d77','11308326636800','jpg','image/jpeg','file_example_JPG_1MB.jpg','PICTURE',1,571,NULL),(572,'62941340136700','','2020-09-28 11:23:18','\0',NULL,0,'813886d2','11308326636800','mp3','audio/mpeg','file_example_MP3_2MG.mp3','AUDIO',1,573,NULL),(574,'63014018733100','','2020-09-28 11:25:02','\0',NULL,0,'9bbd7cab','11308326636800','mp4','video/mp4','file_example_MP4_1280_10MG.mp4','VIDEO',1,575,NULL),(578,'65575217778800','','2020-09-28 12:07:15','\0',NULL,0,'e5b86185','11308326636800','pdf','application/pdf','file-example_PDF_1MB.pdf','DOCUMENT',1,579,NULL),(586,'67381142006600','','2020-09-28 12:37:13','\0',NULL,0,'3722b84b','11308326636800','jpg','image/jpeg','books-768426_1920.jpg','PICTURE',1,587,NULL),(593,'67830247309800','','2020-09-28 12:44:41','\0',NULL,0,'b720cd51','67665637080700','jpg','image/jpeg','chimananda.jpg','AVATAR',591,NULL,NULL),(594,'68039010795900','','2020-09-28 12:48:09','\0',NULL,0,'b59bea21','67711260684600','jpg','image/jpeg','winnie-mandela.jpg','AVATAR',592,NULL,NULL),(595,'68191022171699','','2020-09-28 12:50:41','\0',NULL,0,'d10dc523','67606161628899','jpg','image/jpeg','nkrumah.jpg','AVATAR',590,NULL,NULL),(596,'68415711596099','','2020-09-28 12:54:26','\0',NULL,0,'c60f06cc','67542648596000','jpg','image/jpeg','ruben-um-nyobe.jpg','AVATAR',589,NULL,NULL),(597,'68441930469200','','2020-09-28 12:54:52','\0',NULL,0,'bbfcd0b8','67480294717000','jpg','image/jpeg','patrice_lumumba.jpg','AVATAR',588,NULL,NULL);
/*!40000 ALTER TABLE `multimedia` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-10-07 13:04:06
