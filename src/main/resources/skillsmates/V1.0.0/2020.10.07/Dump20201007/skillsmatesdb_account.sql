-- MariaDB dump 10.17  Distrib 10.4.13-MariaDB, for Win64 (AMD64)
--
-- Host: 127.0.0.1    Database: skillsmatesdb
-- ------------------------------------------------------
-- Server version	10.4.13-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `account`
--

DROP TABLE IF EXISTS `account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `active` bit(1) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `deleted` bit(1) DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  `revision` bigint(20) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `biography` varchar(2000) DEFAULT NULL,
  `birthdate` datetime DEFAULT NULL,
  `city` varchar(30) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `follower` int(11) DEFAULT 0,
  `following` int(11) DEFAULT 0,
  `lastname` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `phone_number` varchar(20) DEFAULT NULL,
  `profile_picture` varchar(255) DEFAULT NULL,
  `publication` int(11) DEFAULT 0,
  `country` int(11) DEFAULT NULL,
  `gender` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_iyeg6x6r8s959nhdt76efqc6u` (`id_server`),
  UNIQUE KEY `UK_q0uja26qgu1atulenwup9rxyr` (`email`),
  KEY `FKi7rut9n9vfl3ac9cxuegckab9` (`country`),
  KEY `FKge5lb1w9xvo78dg80k6s3ni27` (`gender`),
  KEY `FKny0dyt6qv6m9y7qlcbfdwms58` (`status`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account`
--

LOCK TABLES `account` WRITE;
/*!40000 ALTER TABLE `account` DISABLE KEYS */;
INSERT INTO `account` VALUES (1,'11308326636800','','2020-09-27 21:02:47','\0',NULL,0,NULL,'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.','2002-02-02 01:00:00','Yaounde',NULL,'belinda.mengue@yopmail.com','Belinda',1,1,'MENGUE','9e00068ca1fe3558a1e85c241fb05fcb6e4d0464effd36d1eb8a985817bb9df0839b1cbb1c71d86bda1b5e71ae1a29cf2c2bd6953314a6f1e776e02e908f268a',NULL,'avatar_3a23149a.jpg',0,48,3,5),(588,'67480294717000','','2020-09-28 12:38:49','\0',NULL,0,NULL,'Patrice Émery Lumumba est né à Onalua (territoire de Katako-Kombe au Sankuru, Congo belge, dans l\'actuelle République démocratique du Congo). Il fréquente l\'école catholique des missionnaires puis, élève brillant, une école protestante tenue par des Suédois. Jusqu’en 1954 (année de la fondation d\'un réseau d\'enseignement laïque et de la première université), la Belgique coloniale n’a que peu développé le système d’éducation, entièrement confié aux missions religieuses. L\'école ne donne qu’une éducation rudimentaire et vise plus à former des ouvriers que des clercs, mais Lumumba, autodidacte, se plonge dans des manuels d’histoire.\n\nIl travaille comme employé de bureau dans une société minière de la province du Sud-Kivu jusqu’en 1945, puis comme journaliste à Léopoldville (aujourd\'hui Kinshasa) et Stanleyville (Kisangani) employé de 2e classe à la poste, période pendant laquelle il écrit dans divers journaux.\n\nEn septembre 1954, il reçoit sa carte d\'« immatriculé », réservée par l\'administration belge à quelques éléments remarqués du pays (200 immatriculations sur les 13 millions d\'habitants de l\'époque).\n\nIl découvre, en travaillant pour la société minière, que les matières premières de son pays jouent un rôle capital dans l’économie mondiale, mais aussi que les sociétés multinationales ne font rien pour mêler des cadres congolais à la gestion de ces richesses. Il milite alors pour un Congo uni, se distinguant en cela des autres figures indépendantistes dont les partis constitués davantage sur des bases ethniques sont favorables au fédéralisme2. L\'historien congolais Isidore Ndaywel è Nziem précise : « Lumumba, à cause de son identité de Tetela, avait son électorat « naturel » dispersé dans l\'ensemble du pays, ce qui l\'obligeait à jouer une carte nationaliste unitaire »','1925-07-02 01:00:00','Kinshasa',NULL,'patrice.lumumba@yopmail.com','Patrice',6,6,'LUMUMBA','def34008ead8071b2c8efc7a24aa7d5c398e7afc9e7e31b701aaf2b9946e076650f5bd3185077f524d5eedce0469c72bfb70d833af07d9cc2f58ff1772d807b3',NULL,'AVATAR_bbfcd0b8.jpg',0,60,4,7),(589,'67542648596000','','2020-09-28 12:39:51','\0',NULL,0,NULL,'Ruben Um NyobèNote 1, surnommé « Mpodol » (« celui qui porte la parole des siens », en bassa)1, c\'est-à-dire le « porte-parole »2, est un dirigeant camerounais et première personnalité politique à revendiquer l\'indépendance de son pays, le Cameroun, en Afrique francophone et l\'unification des parties orientale (sous tutelle française) et occidentale (sous tutelle anglaise).\n\nD\'ethnie bassa, il est né le 10 avril 1913 à Eog Makon et est mort assassiné, alors qu\'il menait une rébellion armée, par l\'armée française le 13 septembre 1958 à Libelingoï, près de Boumnyébel (actuel département du Nyong-et-Kéllé, région du Centre). Um Nyobè est la figure de proue de la lutte pour l\'indépendance du Cameroun. Ses compagnons furent notamment Félix-Roland Moumié et Ernest Ouandié.\n\nJusque dans les années 1990, toute évocation de Ruben Um Nyobè était interdite3. La loi camerounaise n° 91/022 du 16 décembre 1991 le réhabilitera, celui-ci ayant « œuvré pour la naissance du sentiment national, l\'indépendance ou la construction du pays, le rayonnement de son histoire ou de sa culture4,5». Aux termes de l\'article 2 de la loi précitée, « la réhabilitation (...) a pour effet de dissiper tout préjugé négatif qui entourait toute référence à ces personnes, notamment en ce qui concerne leurs noms, biographies, effigies, portraits, la dénomination des rues, monuments ou édifices publics6 ».','1913-04-10 00:00:00','Eseka',NULL,'ruben.um_nyobe@yopmail.com','Ruben',6,6,'UM NYOBE','f1e3c3e3a0c92349eaae7a154401abf0aa33b623cff43a1ae059715aa70751b9ee82a2bbcf76f50af046db032c088f75933b5d416456571f560e32790619cb5d',NULL,'avatar_c60f06cc.jpg',0,48,4,7),(590,'67606161628899','','2020-09-28 12:40:55','\0',NULL,0,NULL,'Kwame Nkrumah’s father was a goldsmith and his mother a retail trader. Baptized a Roman Catholic, Nkrumah spent nine years at the Roman Catholic elementary school in nearby Half Assini. After graduation from Achimota College in 1930, he started his career as a teacher at Roman Catholic junior schools in Elmina and Axim and at a seminary.\n\nIncreasingly drawn to politics, Nkrumah decided to pursue further studies in the United States. He entered Lincoln University in Pennsylvania in 1935 and, after graduating in 1939, obtained master’s degrees from Lincoln and from the University of Pennsylvania. He studied the literature of socialism, notably Karl Marx and Vladimir Lenin, and of nationalism, especially Marcus Garvey, the Black American leader of the 1920s. Eventually, Nkrumah came to describe himself as a “nondenominational Christian and a Marxist socialist.” He also immersed himself in political work, reorganizing and becoming president of the African Students’ Organization of the United States and Canada. He left the United States in May 1945 and went to England, where he organized the 5th Pan-African Congress in Manchester.','1909-09-21 00:09:21','Accra',NULL,'kwame.nkumah@yopmail.com','Kwame',6,6,'NKRUMAH','37dfafc993b8d1cc4f579cddb41ca6b083b527b605e535e855e427ac1b9b6d0a0e42f899dcd4b8cfa6f35d133ac3c010f3f2ae868af3f8b1bd32dbb1812e52ed',NULL,'AVATAR_d10dc523.jpg',0,91,4,7),(591,'67665637080700','','2020-09-28 12:41:54','\0',NULL,0,NULL,'Chimamanda Ngozi Adichie grew up in Nigeria.\n\nHer work has been translated into over thirty languages and has appeared in various publications, including The New Yorker, Granta, The O. Henry Prize Stories, the Financial Times, and Zoetrope. She is the author of the novels Purple Hibiscus, which won the Commonwealth Writers’ Prize and the Hurston/Wright Legacy Award; Half of a Yellow Sun, which won the Orange Prize and was a National Book Critics Circle Award Finalist and a New York Times Notable Book; and Americanah, which won the National Book Critics Circle Award and was named one of The New York Times Top Ten Best Books of 2013. Ms. Adichie is also the author of the story collection The Thing Around Your Neck.\n\nMs. Adichie has been invited to speak around the world. Her 2009 TED Talk, The Danger of A Single Story, is now one of the most-viewed TED Talks of all time. Her 2012 talk We Should All Be Feminists has a started a worldwide conversation about feminism, and was published as a book in 2014.\n\nHer most recent book, Dear Ijeawele, or a Feminist Manifesto in Fifteen Suggestions, was published in March 2017.\n\nA recipient of a MacArthur Foundation Fellowship, Ms. Adichie divides her time between the United States and Nigeria.\n\nFor a detailed bibliography, please see the independent “The Chimamanda Ngozi Adichie Website” maintained by Daria Tunca.','1977-08-15 02:00:00','Lagos',NULL,'chimamanda.ngozi@yopmail.com','Chimamanda',6,6,'NGOZI ADICHIE','f34847f857131ada50de3c9d944f849a57578df75704f637e58ec9a8f316e85ff9157d94f3e73ef948a19289170c42cf65c8f1d655767e65a6603fc4ce8c1523',NULL,'AVATAR_b720cd51.jpg',0,169,3,6),(592,'67711260684600','','2020-09-28 12:42:40','\0',NULL,0,NULL,'Winnie Mandela was the controversial wife of Nelson Mandela who spent her life in varying governmental roles.\nWho Was Winnie Mandela?\nWinnie Mandela embarked on a career of social work that led to her involvement in activism. She married African National Congress leader Nelson Mandela in 1958, though he was imprisoned for much of their four decades of marriage. Winnie became president of the ANC Women\'s League in 1993, and the following year she was elected to Parliament. However, her accomplishments were also tainted by convictions for kidnapping and fraud. She passed away on April 2, 2018, in Johannesburg‚ South Africa. \n\nEarly Life and Career\nBorn Nomzamo Winifred Madikizela on September 26, 1936, in Bizana, a rural village in the Transkei district of South Africa, Winnie eventually moved to Johannesburg in 1953 to study at the Jan Hofmeyr School of Social Work. South Africa was under the system known as apartheid, where citizens of Indigenous African descent were subjected to a harsh caste system, while European descendants enjoyed much higher levels of wealth, health and social freedom.\n\nWinnie completed her studies and, though receiving a scholarship to study in America, decided instead to work as the first Black medical social worker at Baragwanath Hospital in Johannesburg. A dedicated professional, she came to learn via her fieldwork of the deplorable state that many of her patients lived in.','1936-09-26 01:00:00','Johannesburg',NULL,'wunnie.mandela@yopmail.com','Wunnie',6,6,'MANDELA','50b81b72ec10002df3de34d77be60573f130f4ae68416b0b2dd952fec4ace93e42e60f0434e85031afd7e610ce457175661eb58782779d8e0b58768c5af756f5',NULL,'AVATAR_b59bea21.jpg',0,9,3,7);
/*!40000 ALTER TABLE `account` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-10-07 13:04:09
