-- MariaDB dump 10.17  Distrib 10.4.13-MariaDB, for Win64 (AMD64)
--
-- Host: 127.0.0.1    Database: skillsmatesdb
-- ------------------------------------------------------
-- Server version	10.4.13-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `account`
--

DROP TABLE IF EXISTS `account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `active` bit(1) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `deleted` bit(1) DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  `revision` bigint(20) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `biography` varchar(2000) DEFAULT NULL,
  `birthdate` datetime DEFAULT NULL,
  `city` varchar(30) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `follower` int(11) DEFAULT 0,
  `following` int(11) DEFAULT 0,
  `lastname` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `phone_number` varchar(20) DEFAULT NULL,
  `profile_picture` varchar(255) DEFAULT NULL,
  `publication` int(11) DEFAULT 0,
  `country` int(11) DEFAULT NULL,
  `gender` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_iyeg6x6r8s959nhdt76efqc6u` (`id_server`),
  UNIQUE KEY `UK_q0uja26qgu1atulenwup9rxyr` (`email`),
  KEY `FKi7rut9n9vfl3ac9cxuegckab9` (`country`),
  KEY `FKge5lb1w9xvo78dg80k6s3ni27` (`gender`),
  KEY `FKny0dyt6qv6m9y7qlcbfdwms58` (`status`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account`
--

LOCK TABLES `account` WRITE;
/*!40000 ALTER TABLE `account` DISABLE KEYS */;
INSERT INTO `account` VALUES (1,'11308326636800','','2020-09-27 21:02:47','\0',NULL,0,NULL,'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.','2002-02-02 01:00:00','Yaounde',NULL,'belinda.mengue@yopmail.com','Belinda',1,1,'MENGUE','9e00068ca1fe3558a1e85c241fb05fcb6e4d0464effd36d1eb8a985817bb9df0839b1cbb1c71d86bda1b5e71ae1a29cf2c2bd6953314a6f1e776e02e908f268a',NULL,'avatar_3a23149a.jpg',0,48,3,5),(588,'67480294717000','','2020-09-28 12:38:49','\0',NULL,0,NULL,'Patrice Émery Lumumba est né à Onalua (territoire de Katako-Kombe au Sankuru, Congo belge, dans l\'actuelle République démocratique du Congo). Il fréquente l\'école catholique des missionnaires puis, élève brillant, une école protestante tenue par des Suédois. Jusqu’en 1954 (année de la fondation d\'un réseau d\'enseignement laïque et de la première université), la Belgique coloniale n’a que peu développé le système d’éducation, entièrement confié aux missions religieuses. L\'école ne donne qu’une éducation rudimentaire et vise plus à former des ouvriers que des clercs, mais Lumumba, autodidacte, se plonge dans des manuels d’histoire.\n\nIl travaille comme employé de bureau dans une société minière de la province du Sud-Kivu jusqu’en 1945, puis comme journaliste à Léopoldville (aujourd\'hui Kinshasa) et Stanleyville (Kisangani) employé de 2e classe à la poste, période pendant laquelle il écrit dans divers journaux.\n\nEn septembre 1954, il reçoit sa carte d\'« immatriculé », réservée par l\'administration belge à quelques éléments remarqués du pays (200 immatriculations sur les 13 millions d\'habitants de l\'époque).\n\nIl découvre, en travaillant pour la société minière, que les matières premières de son pays jouent un rôle capital dans l’économie mondiale, mais aussi que les sociétés multinationales ne font rien pour mêler des cadres congolais à la gestion de ces richesses. Il milite alors pour un Congo uni, se distinguant en cela des autres figures indépendantistes dont les partis constitués davantage sur des bases ethniques sont favorables au fédéralisme2. L\'historien congolais Isidore Ndaywel è Nziem précise : « Lumumba, à cause de son identité de Tetela, avait son électorat « naturel » dispersé dans l\'ensemble du pays, ce qui l\'obligeait à jouer une carte nationaliste unitaire »','1925-07-02 01:00:00','Kinshasa',NULL,'patrice.lumumba@yopmail.com','Patrice',6,6,'LUMUMBA','def34008ead8071b2c8efc7a24aa7d5c398e7afc9e7e31b701aaf2b9946e076650f5bd3185077f524d5eedce0469c72bfb70d833af07d9cc2f58ff1772d807b3',NULL,'AVATAR_bbfcd0b8.jpg',0,60,4,7),(589,'67542648596000','','2020-09-28 12:39:51','\0',NULL,0,NULL,'Ruben Um NyobèNote 1, surnommé « Mpodol » (« celui qui porte la parole des siens », en bassa)1, c\'est-à-dire le « porte-parole »2, est un dirigeant camerounais et première personnalité politique à revendiquer l\'indépendance de son pays, le Cameroun, en Afrique francophone et l\'unification des parties orientale (sous tutelle française) et occidentale (sous tutelle anglaise).\n\nD\'ethnie bassa, il est né le 10 avril 1913 à Eog Makon et est mort assassiné, alors qu\'il menait une rébellion armée, par l\'armée française le 13 septembre 1958 à Libelingoï, près de Boumnyébel (actuel département du Nyong-et-Kéllé, région du Centre). Um Nyobè est la figure de proue de la lutte pour l\'indépendance du Cameroun. Ses compagnons furent notamment Félix-Roland Moumié et Ernest Ouandié.\n\nJusque dans les années 1990, toute évocation de Ruben Um Nyobè était interdite3. La loi camerounaise n° 91/022 du 16 décembre 1991 le réhabilitera, celui-ci ayant « œuvré pour la naissance du sentiment national, l\'indépendance ou la construction du pays, le rayonnement de son histoire ou de sa culture4,5». Aux termes de l\'article 2 de la loi précitée, « la réhabilitation (...) a pour effet de dissiper tout préjugé négatif qui entourait toute référence à ces personnes, notamment en ce qui concerne leurs noms, biographies, effigies, portraits, la dénomination des rues, monuments ou édifices publics6 ».','1913-04-10 00:00:00','Eseka',NULL,'ruben.um_nyobe@yopmail.com','Ruben',6,6,'UM NYOBE','f1e3c3e3a0c92349eaae7a154401abf0aa33b623cff43a1ae059715aa70751b9ee82a2bbcf76f50af046db032c088f75933b5d416456571f560e32790619cb5d',NULL,'avatar_c60f06cc.jpg',0,48,4,7),(590,'67606161628899','','2020-09-28 12:40:55','\0',NULL,0,NULL,'Kwame Nkrumah’s father was a goldsmith and his mother a retail trader. Baptized a Roman Catholic, Nkrumah spent nine years at the Roman Catholic elementary school in nearby Half Assini. After graduation from Achimota College in 1930, he started his career as a teacher at Roman Catholic junior schools in Elmina and Axim and at a seminary.\n\nIncreasingly drawn to politics, Nkrumah decided to pursue further studies in the United States. He entered Lincoln University in Pennsylvania in 1935 and, after graduating in 1939, obtained master’s degrees from Lincoln and from the University of Pennsylvania. He studied the literature of socialism, notably Karl Marx and Vladimir Lenin, and of nationalism, especially Marcus Garvey, the Black American leader of the 1920s. Eventually, Nkrumah came to describe himself as a “nondenominational Christian and a Marxist socialist.” He also immersed himself in political work, reorganizing and becoming president of the African Students’ Organization of the United States and Canada. He left the United States in May 1945 and went to England, where he organized the 5th Pan-African Congress in Manchester.','1909-09-21 00:09:21','Accra',NULL,'kwame.nkumah@yopmail.com','Kwame',6,6,'NKRUMAH','37dfafc993b8d1cc4f579cddb41ca6b083b527b605e535e855e427ac1b9b6d0a0e42f899dcd4b8cfa6f35d133ac3c010f3f2ae868af3f8b1bd32dbb1812e52ed',NULL,'AVATAR_d10dc523.jpg',0,91,4,7),(591,'67665637080700','','2020-09-28 12:41:54','\0',NULL,0,NULL,'Chimamanda Ngozi Adichie grew up in Nigeria.\n\nHer work has been translated into over thirty languages and has appeared in various publications, including The New Yorker, Granta, The O. Henry Prize Stories, the Financial Times, and Zoetrope. She is the author of the novels Purple Hibiscus, which won the Commonwealth Writers’ Prize and the Hurston/Wright Legacy Award; Half of a Yellow Sun, which won the Orange Prize and was a National Book Critics Circle Award Finalist and a New York Times Notable Book; and Americanah, which won the National Book Critics Circle Award and was named one of The New York Times Top Ten Best Books of 2013. Ms. Adichie is also the author of the story collection The Thing Around Your Neck.\n\nMs. Adichie has been invited to speak around the world. Her 2009 TED Talk, The Danger of A Single Story, is now one of the most-viewed TED Talks of all time. Her 2012 talk We Should All Be Feminists has a started a worldwide conversation about feminism, and was published as a book in 2014.\n\nHer most recent book, Dear Ijeawele, or a Feminist Manifesto in Fifteen Suggestions, was published in March 2017.\n\nA recipient of a MacArthur Foundation Fellowship, Ms. Adichie divides her time between the United States and Nigeria.\n\nFor a detailed bibliography, please see the independent “The Chimamanda Ngozi Adichie Website” maintained by Daria Tunca.','1977-08-15 02:00:00','Lagos',NULL,'chimamanda.ngozi@yopmail.com','Chimamanda',6,6,'NGOZI ADICHIE','f34847f857131ada50de3c9d944f849a57578df75704f637e58ec9a8f316e85ff9157d94f3e73ef948a19289170c42cf65c8f1d655767e65a6603fc4ce8c1523',NULL,'AVATAR_b720cd51.jpg',0,169,3,6),(592,'67711260684600','','2020-09-28 12:42:40','\0',NULL,0,NULL,'Winnie Mandela was the controversial wife of Nelson Mandela who spent her life in varying governmental roles.\nWho Was Winnie Mandela?\nWinnie Mandela embarked on a career of social work that led to her involvement in activism. She married African National Congress leader Nelson Mandela in 1958, though he was imprisoned for much of their four decades of marriage. Winnie became president of the ANC Women\'s League in 1993, and the following year she was elected to Parliament. However, her accomplishments were also tainted by convictions for kidnapping and fraud. She passed away on April 2, 2018, in Johannesburg‚ South Africa. \n\nEarly Life and Career\nBorn Nomzamo Winifred Madikizela on September 26, 1936, in Bizana, a rural village in the Transkei district of South Africa, Winnie eventually moved to Johannesburg in 1953 to study at the Jan Hofmeyr School of Social Work. South Africa was under the system known as apartheid, where citizens of Indigenous African descent were subjected to a harsh caste system, while European descendants enjoyed much higher levels of wealth, health and social freedom.\n\nWinnie completed her studies and, though receiving a scholarship to study in America, decided instead to work as the first Black medical social worker at Baragwanath Hospital in Johannesburg. A dedicated professional, she came to learn via her fieldwork of the deplorable state that many of her patients lived in.','1936-09-26 01:00:00','Johannesburg',NULL,'wunnie.mandela@yopmail.com','Wunnie',6,6,'MANDELA','50b81b72ec10002df3de34d77be60573f130f4ae68416b0b2dd952fec4ace93e42e60f0434e85031afd7e610ce457175661eb58782779d8e0b58768c5af756f5',NULL,'AVATAR_b59bea21.jpg',0,9,3,7);
/*!40000 ALTER TABLE `account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `activity_area`
--

DROP TABLE IF EXISTS `activity_area`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `activity_area` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `label` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_8kp9xgj172mu3tjwkq340if7x` (`id_server`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activity_area`
--

LOCK TABLES `activity_area` WRITE;
/*!40000 ALTER TABLE `activity_area` DISABLE KEYS */;
INSERT INTO `activity_area` VALUES (434,'12499056161900','Activity area 1','Activity area 1'),(435,'12499057181700','Activity area 2','Activity area 2');
/*!40000 ALTER TABLE `activity_area` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `activity_sector`
--

DROP TABLE IF EXISTS `activity_sector`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `activity_sector` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `label` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_g3clcptstkdfkbdsa9s5pxatc` (`id_server`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activity_sector`
--

LOCK TABLES `activity_sector` WRITE;
/*!40000 ALTER TABLE `activity_sector` DISABLE KEYS */;
INSERT INTO `activity_sector` VALUES (409,'12373645608500','Administration, Public et Association','Administration, Public et Association'),(410,'12373646634500','Agriculture et Agroalimentaire','Agriculture et Agroalimentaire'),(411,'12373647658100','Artisanat','Artisanat'),(412,'12373648552900','Banque, Finance et Assurance','Banque, Finance et Assurance'),(413,'12373649724700','Automobile, aéronautiques et autres matériel de transport','Automobile, aéronautiques et autres matériel de transport'),(414,'12373650917700','BTP et Construction','BTP et Construction'),(415,'12373651712800','Commerce et Distribution','Commerce et Distribution'),(416,'12373652669300','Conseil et Gestion des entreprises','Conseil et Gestion des entreprises'),(417,'12373653478200','Droit et Juridique','Droit et Juridique'),(418,'12373654360500','Comptabilité et Audit','Comptabilité et Audit'),(419,'12373655417700','Eau, Energie et Minerais','Eau, Energie et Minerais'),(420,'12373656199300','Environnement et développement durable','Environnement et développement durable'),(421,'12373657087700','Formation académique, professionnel et divers','Formation académique, professionnel et divers'),(422,'12373657949900','Informatique et Technologie de l\'information','Informatique et Technologie de l\'information'),(423,'12373658755800','Industrie chimique, textile, Manufacturière et autres','Industrie chimique, textile, Manufacturière et autres'),(424,'12373659840700','Immobilier','Immobilier'),(425,'12373660694100','Marketing, communication et Médias','Marketing, communication et Médias'),(426,'12373661478400','Ressources Humaines','Ressources Humaines'),(427,'12373662461300','Restauration, Hôtellerie, et Loisirs','Restauration, Hôtellerie, et Loisirs'),(428,'12373663292200','Services divers aux entreprises','Services divers aux entreprises'),(429,'12373664095300','Santé et service à la personne et sociale','Santé et service à la personne et sociale'),(430,'12373665925100','Telecom','Telecom'),(431,'12373666892100','Transports et Logistique','Transports et Logistique'),(432,'12373667805100','Tourisme','Tourisme'),(433,'12373668822700','Autres (à préciser manuellement)','Autres (à préciser manuellement)');
/*!40000 ALTER TABLE `activity_sector` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `batch_job_execution`
--

DROP TABLE IF EXISTS `batch_job_execution`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `batch_job_execution` (
  `JOB_EXECUTION_ID` bigint(20) NOT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `JOB_INSTANCE_ID` bigint(20) NOT NULL,
  `CREATE_TIME` datetime NOT NULL,
  `START_TIME` datetime DEFAULT NULL,
  `END_TIME` datetime DEFAULT NULL,
  `STATUS` varchar(10) DEFAULT NULL,
  `EXIT_CODE` varchar(2500) DEFAULT NULL,
  `EXIT_MESSAGE` varchar(2500) DEFAULT NULL,
  `LAST_UPDATED` datetime DEFAULT NULL,
  `JOB_CONFIGURATION_LOCATION` varchar(2500) DEFAULT NULL,
  PRIMARY KEY (`JOB_EXECUTION_ID`),
  KEY `JOB_INST_EXEC_FK` (`JOB_INSTANCE_ID`),
  CONSTRAINT `JOB_INST_EXEC_FK` FOREIGN KEY (`JOB_INSTANCE_ID`) REFERENCES `batch_job_instance` (`JOB_INSTANCE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `batch_job_execution`
--

LOCK TABLES `batch_job_execution` WRITE;
/*!40000 ALTER TABLE `batch_job_execution` DISABLE KEYS */;
INSERT INTO `batch_job_execution` VALUES (1,2,1,'2020-09-27 21:06:07','2020-09-27 21:06:07','2020-09-27 21:06:08','COMPLETED','COMPLETED','','2020-09-27 21:06:08',NULL),(2,2,2,'2020-09-27 21:06:57','2020-09-27 21:06:57','2020-09-27 21:06:58','COMPLETED','COMPLETED','','2020-09-27 21:06:58',NULL),(3,2,3,'2020-09-27 21:07:48','2020-09-27 21:07:48','2020-09-27 21:07:49','COMPLETED','COMPLETED','','2020-09-27 21:07:49',NULL),(4,2,4,'2020-09-27 21:15:38','2020-09-27 21:15:38','2020-09-27 21:15:38','COMPLETED','COMPLETED','','2020-09-27 21:15:38',NULL),(5,2,5,'2020-09-27 21:16:19','2020-09-27 21:16:19','2020-09-27 21:16:20','COMPLETED','COMPLETED','','2020-09-27 21:16:20',NULL),(6,2,6,'2020-09-27 21:17:28','2020-09-27 21:17:28','2020-09-27 21:17:29','COMPLETED','COMPLETED','','2020-09-27 21:17:29',NULL),(7,2,7,'2020-09-27 21:20:32','2020-09-27 21:20:32','2020-09-27 21:20:32','COMPLETED','COMPLETED','','2020-09-27 21:20:32',NULL),(8,2,8,'2020-09-27 21:22:37','2020-09-27 21:22:37','2020-09-27 21:22:38','COMPLETED','COMPLETED','','2020-09-27 21:22:38',NULL),(9,2,9,'2020-09-27 21:25:39','2020-09-27 21:25:39','2020-09-27 21:25:39','COMPLETED','COMPLETED','','2020-09-27 21:25:39',NULL),(10,2,10,'2020-09-27 21:26:33','2020-09-27 21:26:33','2020-09-27 21:26:33','COMPLETED','COMPLETED','','2020-09-27 21:26:33',NULL),(11,2,11,'2020-09-27 21:27:58','2020-09-27 21:27:58','2020-09-27 21:27:59','COMPLETED','COMPLETED','','2020-09-27 21:27:59',NULL),(12,2,12,'2020-09-27 21:30:54','2020-09-27 21:30:54','2020-09-27 21:30:54','COMPLETED','COMPLETED','','2020-09-27 21:30:54',NULL),(13,2,13,'2020-09-27 21:56:40','2020-09-27 21:56:40','2020-09-27 21:56:40','COMPLETED','COMPLETED','','2020-09-27 21:56:40',NULL),(14,2,14,'2020-09-28 00:32:47','2020-09-28 00:32:47','2020-09-28 00:32:47','COMPLETED','COMPLETED','','2020-09-28 00:32:47',NULL),(15,2,15,'2020-09-28 00:33:45','2020-09-28 00:33:45','2020-09-28 00:33:45','COMPLETED','COMPLETED','','2020-09-28 00:33:45',NULL),(16,2,16,'2020-10-06 17:37:41','2020-10-06 17:37:41','2020-10-06 17:37:41','COMPLETED','COMPLETED','','2020-10-06 17:37:41',NULL),(17,2,17,'2020-10-06 17:38:46','2020-10-06 17:38:46','2020-10-06 17:38:46','COMPLETED','COMPLETED','','2020-10-06 17:38:46',NULL);
/*!40000 ALTER TABLE `batch_job_execution` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `batch_job_execution_context`
--

DROP TABLE IF EXISTS `batch_job_execution_context`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `batch_job_execution_context` (
  `JOB_EXECUTION_ID` bigint(20) NOT NULL,
  `SHORT_CONTEXT` varchar(2500) NOT NULL,
  `SERIALIZED_CONTEXT` text DEFAULT NULL,
  PRIMARY KEY (`JOB_EXECUTION_ID`),
  CONSTRAINT `JOB_EXEC_CTX_FK` FOREIGN KEY (`JOB_EXECUTION_ID`) REFERENCES `batch_job_execution` (`JOB_EXECUTION_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `batch_job_execution_context`
--

LOCK TABLES `batch_job_execution_context` WRITE;
/*!40000 ALTER TABLE `batch_job_execution_context` DISABLE KEYS */;
INSERT INTO `batch_job_execution_context` VALUES (1,'{\"STATUS\":\"COMPLETED\",\"MODEL\":\"GENDER\"}',NULL),(2,'{\"STATUS\":\"COMPLETED\",\"MODEL\":\"PROFESSIONAL_STATUS\"}',NULL),(3,'{\"STATUS\":\"COMPLETED\",\"MODEL\":\"COUNTRY\"}',NULL),(4,'{\"STATUS\":\"COMPLETED\",\"MODEL\":\"SKILL_TYPE\"}',NULL),(5,'{\"STATUS\":\"COMPLETED\",\"MODEL\":\"DISCIPLINE\"}',NULL),(6,'{\"STATUS\":\"COMPLETED\",\"MODEL\":\"LEVEL\"}',NULL),(7,'{\"STATUS\":\"COMPLETED\",\"MODEL\":\"ACTIVITY_SECTOR\"}',NULL),(8,'{\"STATUS\":\"COMPLETED\",\"MODEL\":\"ACTIVITY_AREA\"}',NULL),(9,'{\"STATUS\":\"COMPLETED\",\"MODEL\":\"EDUCATION\"}',NULL),(10,'{\"STATUS\":\"COMPLETED\",\"MODEL\":\"ESTABLISHMENT_TYPE\"}',NULL),(11,'{\"STATUS\":\"COMPLETED\",\"MODEL\":\"FREQUENTED_CLASS\"}',NULL),(12,'{\"STATUS\":\"COMPLETED\",\"MODEL\":\"DIPLOMA\"}',NULL),(13,'{\"STATUS\":\"COMPLETED\",\"MODEL\":\"SPECIALTY\"}',NULL),(14,'{\"STATUS\":\"COMPLETED\",\"MODEL\":\"TEACHING_AREA\"}',NULL),(15,'{\"STATUS\":\"COMPLETED\",\"MODEL\":\"STUDY_LEVEL\"}',NULL),(16,'{\"STATUS\":\"COMPLETED\",\"MODEL\":\"MEDIA_TYPE\"}',NULL),(17,'{\"STATUS\":\"COMPLETED\",\"MODEL\":\"MEDIA_SUBTYPE\"}',NULL);
/*!40000 ALTER TABLE `batch_job_execution_context` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `batch_job_execution_params`
--

DROP TABLE IF EXISTS `batch_job_execution_params`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `batch_job_execution_params` (
  `JOB_EXECUTION_ID` bigint(20) NOT NULL,
  `TYPE_CD` varchar(6) NOT NULL,
  `KEY_NAME` varchar(100) NOT NULL,
  `STRING_VAL` varchar(250) DEFAULT NULL,
  `DATE_VAL` datetime DEFAULT NULL,
  `LONG_VAL` bigint(20) DEFAULT NULL,
  `DOUBLE_VAL` double DEFAULT NULL,
  `IDENTIFYING` char(1) NOT NULL,
  KEY `JOB_EXEC_PARAMS_FK` (`JOB_EXECUTION_ID`),
  CONSTRAINT `JOB_EXEC_PARAMS_FK` FOREIGN KEY (`JOB_EXECUTION_ID`) REFERENCES `batch_job_execution` (`JOB_EXECUTION_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `batch_job_execution_params`
--

LOCK TABLES `batch_job_execution_params` WRITE;
/*!40000 ALTER TABLE `batch_job_execution_params` DISABLE KEYS */;
INSERT INTO `batch_job_execution_params` VALUES (1,'LONG','run.id','','1970-01-01 01:00:00',1,0,'Y'),(2,'LONG','run.id','','1970-01-01 01:00:00',2,0,'Y'),(3,'LONG','run.id','','1970-01-01 01:00:00',3,0,'Y'),(4,'LONG','run.id','','1970-01-01 01:00:00',4,0,'Y'),(5,'LONG','run.id','','1970-01-01 01:00:00',5,0,'Y'),(6,'LONG','run.id','','1970-01-01 01:00:00',6,0,'Y'),(7,'LONG','run.id','','1970-01-01 01:00:00',7,0,'Y'),(8,'LONG','run.id','','1970-01-01 01:00:00',8,0,'Y'),(9,'LONG','run.id','','1970-01-01 01:00:00',9,0,'Y'),(10,'LONG','run.id','','1970-01-01 01:00:00',10,0,'Y'),(11,'LONG','run.id','','1970-01-01 01:00:00',11,0,'Y'),(12,'LONG','run.id','','1970-01-01 01:00:00',12,0,'Y'),(13,'LONG','run.id','','1970-01-01 01:00:00',13,0,'Y'),(14,'LONG','run.id','','1970-01-01 01:00:00',14,0,'Y'),(15,'LONG','run.id','','1970-01-01 01:00:00',15,0,'Y'),(16,'LONG','run.id','','1970-01-01 01:00:00',16,0,'Y'),(17,'LONG','run.id','','1970-01-01 01:00:00',17,0,'Y');
/*!40000 ALTER TABLE `batch_job_execution_params` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `batch_job_execution_seq`
--

DROP TABLE IF EXISTS `batch_job_execution_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `batch_job_execution_seq` (
  `ID` bigint(20) NOT NULL,
  `UNIQUE_KEY` char(1) NOT NULL,
  UNIQUE KEY `UNIQUE_KEY_UN` (`UNIQUE_KEY`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `batch_job_execution_seq`
--

LOCK TABLES `batch_job_execution_seq` WRITE;
/*!40000 ALTER TABLE `batch_job_execution_seq` DISABLE KEYS */;
INSERT INTO `batch_job_execution_seq` VALUES (17,'0');
/*!40000 ALTER TABLE `batch_job_execution_seq` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `batch_job_instance`
--

DROP TABLE IF EXISTS `batch_job_instance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `batch_job_instance` (
  `JOB_INSTANCE_ID` bigint(20) NOT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `JOB_NAME` varchar(100) NOT NULL,
  `JOB_KEY` varchar(32) NOT NULL,
  PRIMARY KEY (`JOB_INSTANCE_ID`),
  UNIQUE KEY `JOB_INST_UN` (`JOB_NAME`,`JOB_KEY`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `batch_job_instance`
--

LOCK TABLES `batch_job_instance` WRITE;
/*!40000 ALTER TABLE `batch_job_instance` DISABLE KEYS */;
INSERT INTO `batch_job_instance` VALUES (1,0,'populateObjectsJob','853d3449e311f40366811cbefb3d93d7'),(2,0,'populateObjectsJob','e070bff4379694c0210a51d9f6c6a564'),(3,0,'populateObjectsJob','a3364faf893276dea0caacefbf618db5'),(4,0,'populateObjectsJob','47c0a8118b74165a864b66d37c7b6cf5'),(5,0,'populateObjectsJob','ce148f5f9c2bf4dc9bd44a7a5f64204c'),(6,0,'populateObjectsJob','bd0034040292bc81e6ccac0e25d9a578'),(7,0,'populateObjectsJob','597815c7e4ab1092c1b25130aae725cb'),(8,0,'populateObjectsJob','f55a96b11012be4fcfb6cf005435182d'),(9,0,'populateObjectsJob','96a5ed9bac43e779455f3e71c0f64840'),(10,0,'populateObjectsJob','1aac4f3e74894b78fa3ce5d8a25e1ef0'),(11,0,'populateObjectsJob','604bbfc4c68cb1f903780c2853ad4801'),(12,0,'populateObjectsJob','556ebe34220b4032509f2581356ba47c'),(13,0,'populateObjectsJob','edc440efb5ddd2a3b2622f16a12bf105'),(14,0,'populateObjectsJob','f3d5e568c384ee72cba8bc6a51057fe4'),(15,0,'populateObjectsJob','378ef1ecb81cf9edac4ab119bdab9d9d'),(16,0,'populateObjectsJob','e073471cc312cadef424c3be7915c0af'),(17,0,'populateObjectsJob','46ba78a99abf1e2fba4a8861749d7572');
/*!40000 ALTER TABLE `batch_job_instance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `batch_job_seq`
--

DROP TABLE IF EXISTS `batch_job_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `batch_job_seq` (
  `ID` bigint(20) NOT NULL,
  `UNIQUE_KEY` char(1) NOT NULL,
  UNIQUE KEY `UNIQUE_KEY_UN` (`UNIQUE_KEY`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `batch_job_seq`
--

LOCK TABLES `batch_job_seq` WRITE;
/*!40000 ALTER TABLE `batch_job_seq` DISABLE KEYS */;
INSERT INTO `batch_job_seq` VALUES (17,'0');
/*!40000 ALTER TABLE `batch_job_seq` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `batch_step_execution`
--

DROP TABLE IF EXISTS `batch_step_execution`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `batch_step_execution` (
  `STEP_EXECUTION_ID` bigint(20) NOT NULL,
  `VERSION` bigint(20) NOT NULL,
  `STEP_NAME` varchar(100) NOT NULL,
  `JOB_EXECUTION_ID` bigint(20) NOT NULL,
  `START_TIME` datetime NOT NULL,
  `END_TIME` datetime DEFAULT NULL,
  `STATUS` varchar(10) DEFAULT NULL,
  `COMMIT_COUNT` bigint(20) DEFAULT NULL,
  `READ_COUNT` bigint(20) DEFAULT NULL,
  `FILTER_COUNT` bigint(20) DEFAULT NULL,
  `WRITE_COUNT` bigint(20) DEFAULT NULL,
  `READ_SKIP_COUNT` bigint(20) DEFAULT NULL,
  `WRITE_SKIP_COUNT` bigint(20) DEFAULT NULL,
  `PROCESS_SKIP_COUNT` bigint(20) DEFAULT NULL,
  `ROLLBACK_COUNT` bigint(20) DEFAULT NULL,
  `EXIT_CODE` varchar(2500) DEFAULT NULL,
  `EXIT_MESSAGE` varchar(2500) DEFAULT NULL,
  `LAST_UPDATED` datetime DEFAULT NULL,
  PRIMARY KEY (`STEP_EXECUTION_ID`),
  KEY `JOB_EXEC_STEP_FK` (`JOB_EXECUTION_ID`),
  CONSTRAINT `JOB_EXEC_STEP_FK` FOREIGN KEY (`JOB_EXECUTION_ID`) REFERENCES `batch_job_execution` (`JOB_EXECUTION_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `batch_step_execution`
--

LOCK TABLES `batch_step_execution` WRITE;
/*!40000 ALTER TABLE `batch_step_execution` DISABLE KEYS */;
INSERT INTO `batch_step_execution` VALUES (1,3,'stepFileControl',1,'2020-09-27 21:06:07','2020-09-27 21:06:07','COMPLETED',1,0,0,0,0,0,0,0,'COMPLETED','','2020-09-27 21:06:07'),(2,3,'populateObjects',1,'2020-09-27 21:06:07','2020-09-27 21:06:08','COMPLETED',1,2,0,2,0,0,0,0,'COMPLETED','','2020-09-27 21:06:08'),(3,3,'stepFileControl',2,'2020-09-27 21:06:57','2020-09-27 21:06:57','COMPLETED',1,0,0,0,0,0,0,0,'COMPLETED','','2020-09-27 21:06:57'),(4,3,'populateObjects',2,'2020-09-27 21:06:57','2020-09-27 21:06:58','COMPLETED',1,3,0,3,0,0,0,0,'COMPLETED','','2020-09-27 21:06:58'),(5,3,'stepFileControl',3,'2020-09-27 21:07:48','2020-09-27 21:07:48','COMPLETED',1,0,0,0,0,0,0,0,'COMPLETED','','2020-09-27 21:07:48'),(6,5,'populateObjects',3,'2020-09-27 21:07:48','2020-09-27 21:07:49','COMPLETED',3,249,0,249,0,0,0,0,'COMPLETED','','2020-09-27 21:07:49'),(7,3,'stepFileControl',4,'2020-09-27 21:15:38','2020-09-27 21:15:38','COMPLETED',1,0,0,0,0,0,0,0,'COMPLETED','','2020-09-27 21:15:38'),(8,3,'populateObjects',4,'2020-09-27 21:15:38','2020-09-27 21:15:38','COMPLETED',1,2,0,2,0,0,0,0,'COMPLETED','','2020-09-27 21:15:38'),(9,3,'stepFileControl',5,'2020-09-27 21:16:19','2020-09-27 21:16:19','COMPLETED',1,0,0,0,0,0,0,0,'COMPLETED','','2020-09-27 21:16:19'),(10,4,'populateObjects',5,'2020-09-27 21:16:20','2020-09-27 21:16:20','COMPLETED',2,142,0,142,0,0,0,0,'COMPLETED','','2020-09-27 21:16:20'),(11,3,'stepFileControl',6,'2020-09-27 21:17:28','2020-09-27 21:17:28','COMPLETED',1,0,0,0,0,0,0,0,'COMPLETED','','2020-09-27 21:17:28'),(12,3,'populateObjects',6,'2020-09-27 21:17:28','2020-09-27 21:17:29','COMPLETED',1,4,0,4,0,0,0,0,'COMPLETED','','2020-09-27 21:17:29'),(13,3,'stepFileControl',7,'2020-09-27 21:20:32','2020-09-27 21:20:32','COMPLETED',1,0,0,0,0,0,0,0,'COMPLETED','','2020-09-27 21:20:32'),(14,3,'populateObjects',7,'2020-09-27 21:20:32','2020-09-27 21:20:32','COMPLETED',1,25,0,25,0,0,0,0,'COMPLETED','','2020-09-27 21:20:32'),(15,3,'stepFileControl',8,'2020-09-27 21:22:37','2020-09-27 21:22:37','COMPLETED',1,0,0,0,0,0,0,0,'COMPLETED','','2020-09-27 21:22:37'),(16,3,'populateObjects',8,'2020-09-27 21:22:37','2020-09-27 21:22:38','COMPLETED',1,2,0,2,0,0,0,0,'COMPLETED','','2020-09-27 21:22:38'),(17,3,'stepFileControl',9,'2020-09-27 21:25:39','2020-09-27 21:25:39','COMPLETED',1,0,0,0,0,0,0,0,'COMPLETED','','2020-09-27 21:25:39'),(18,3,'populateObjects',9,'2020-09-27 21:25:39','2020-09-27 21:25:39','COMPLETED',1,2,0,2,0,0,0,0,'COMPLETED','','2020-09-27 21:25:39'),(19,3,'stepFileControl',10,'2020-09-27 21:26:33','2020-09-27 21:26:33','COMPLETED',1,0,0,0,0,0,0,0,'COMPLETED','','2020-09-27 21:26:33'),(20,3,'populateObjects',10,'2020-09-27 21:26:33','2020-09-27 21:26:33','COMPLETED',1,13,0,13,0,0,0,0,'COMPLETED','','2020-09-27 21:26:33'),(21,3,'stepFileControl',11,'2020-09-27 21:27:58','2020-09-27 21:27:58','COMPLETED',1,0,0,0,0,0,0,0,'COMPLETED','','2020-09-27 21:27:58'),(22,3,'populateObjects',11,'2020-09-27 21:27:58','2020-09-27 21:27:59','COMPLETED',1,66,0,66,0,0,0,0,'COMPLETED','','2020-09-27 21:27:59'),(23,3,'stepFileControl',12,'2020-09-27 21:30:54','2020-09-27 21:30:54','COMPLETED',1,0,0,0,0,0,0,0,'COMPLETED','','2020-09-27 21:30:54'),(24,3,'populateObjects',12,'2020-09-27 21:30:54','2020-09-27 21:30:54','COMPLETED',1,34,0,34,0,0,0,0,'COMPLETED','','2020-09-27 21:30:54'),(25,3,'stepFileControl',13,'2020-09-27 21:56:40','2020-09-27 21:56:40','COMPLETED',1,0,0,0,0,0,0,0,'COMPLETED','','2020-09-27 21:56:40'),(26,3,'populateObjects',13,'2020-09-27 21:56:40','2020-09-27 21:56:40','COMPLETED',1,4,0,4,0,0,0,0,'COMPLETED','','2020-09-27 21:56:40'),(27,3,'stepFileControl',14,'2020-09-28 00:32:47','2020-09-28 00:32:47','COMPLETED',1,0,0,0,0,0,0,0,'COMPLETED','','2020-09-28 00:32:47'),(28,3,'populateObjects',14,'2020-09-28 00:32:47','2020-09-28 00:32:47','COMPLETED',1,4,0,4,0,0,0,0,'COMPLETED','','2020-09-28 00:32:47'),(29,3,'stepFileControl',15,'2020-09-28 00:33:45','2020-09-28 00:33:45','COMPLETED',1,0,0,0,0,0,0,0,'COMPLETED','','2020-09-28 00:33:45'),(30,3,'populateObjects',15,'2020-09-28 00:33:45','2020-09-28 00:33:45','COMPLETED',1,4,0,4,0,0,0,0,'COMPLETED','','2020-09-28 00:33:45'),(31,3,'stepFileControl',16,'2020-10-06 17:37:41','2020-10-06 17:37:41','COMPLETED',1,0,0,0,0,0,0,0,'COMPLETED','','2020-10-06 17:37:41'),(32,3,'populateObjects',16,'2020-10-06 17:37:41','2020-10-06 17:37:41','COMPLETED',1,6,0,6,0,0,0,0,'COMPLETED','','2020-10-06 17:37:41'),(33,3,'stepFileControl',17,'2020-10-06 17:38:46','2020-10-06 17:38:46','COMPLETED',1,0,0,0,0,0,0,0,'COMPLETED','','2020-10-06 17:38:46'),(34,3,'populateObjects',17,'2020-10-06 17:38:46','2020-10-06 17:38:46','COMPLETED',1,26,0,26,0,0,0,0,'COMPLETED','','2020-10-06 17:38:46');
/*!40000 ALTER TABLE `batch_step_execution` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `batch_step_execution_context`
--

DROP TABLE IF EXISTS `batch_step_execution_context`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `batch_step_execution_context` (
  `STEP_EXECUTION_ID` bigint(20) NOT NULL,
  `SHORT_CONTEXT` varchar(2500) NOT NULL,
  `SERIALIZED_CONTEXT` text DEFAULT NULL,
  PRIMARY KEY (`STEP_EXECUTION_ID`),
  CONSTRAINT `STEP_EXEC_CTX_FK` FOREIGN KEY (`STEP_EXECUTION_ID`) REFERENCES `batch_step_execution` (`STEP_EXECUTION_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `batch_step_execution_context`
--

LOCK TABLES `batch_step_execution_context` WRITE;
/*!40000 ALTER TABLE `batch_step_execution_context` DISABLE KEYS */;
INSERT INTO `batch_step_execution_context` VALUES (1,'{\"batch.taskletType\":\"com.yamo.skillsmates.populate.tasklet.FileControlTasklet\",\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}',NULL),(2,'{\"batch.taskletType\":\"org.springframework.batch.core.step.item.ChunkOrientedTasklet\",\"FlatFileItemReader.read.count\":3,\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}',NULL),(3,'{\"batch.taskletType\":\"com.yamo.skillsmates.populate.tasklet.FileControlTasklet\",\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}',NULL),(4,'{\"batch.taskletType\":\"org.springframework.batch.core.step.item.ChunkOrientedTasklet\",\"FlatFileItemReader.read.count\":4,\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}',NULL),(5,'{\"batch.taskletType\":\"com.yamo.skillsmates.populate.tasklet.FileControlTasklet\",\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}',NULL),(6,'{\"batch.taskletType\":\"org.springframework.batch.core.step.item.ChunkOrientedTasklet\",\"FlatFileItemReader.read.count\":250,\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}',NULL),(7,'{\"batch.taskletType\":\"com.yamo.skillsmates.populate.tasklet.FileControlTasklet\",\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}',NULL),(8,'{\"batch.taskletType\":\"org.springframework.batch.core.step.item.ChunkOrientedTasklet\",\"FlatFileItemReader.read.count\":3,\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}',NULL),(9,'{\"batch.taskletType\":\"com.yamo.skillsmates.populate.tasklet.FileControlTasklet\",\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}',NULL),(10,'{\"batch.taskletType\":\"org.springframework.batch.core.step.item.ChunkOrientedTasklet\",\"FlatFileItemReader.read.count\":143,\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}',NULL),(11,'{\"batch.taskletType\":\"com.yamo.skillsmates.populate.tasklet.FileControlTasklet\",\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}',NULL),(12,'{\"batch.taskletType\":\"org.springframework.batch.core.step.item.ChunkOrientedTasklet\",\"FlatFileItemReader.read.count\":5,\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}',NULL),(13,'{\"batch.taskletType\":\"com.yamo.skillsmates.populate.tasklet.FileControlTasklet\",\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}',NULL),(14,'{\"batch.taskletType\":\"org.springframework.batch.core.step.item.ChunkOrientedTasklet\",\"FlatFileItemReader.read.count\":26,\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}',NULL),(15,'{\"batch.taskletType\":\"com.yamo.skillsmates.populate.tasklet.FileControlTasklet\",\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}',NULL),(16,'{\"batch.taskletType\":\"org.springframework.batch.core.step.item.ChunkOrientedTasklet\",\"FlatFileItemReader.read.count\":3,\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}',NULL),(17,'{\"batch.taskletType\":\"com.yamo.skillsmates.populate.tasklet.FileControlTasklet\",\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}',NULL),(18,'{\"batch.taskletType\":\"org.springframework.batch.core.step.item.ChunkOrientedTasklet\",\"FlatFileItemReader.read.count\":3,\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}',NULL),(19,'{\"batch.taskletType\":\"com.yamo.skillsmates.populate.tasklet.FileControlTasklet\",\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}',NULL),(20,'{\"batch.taskletType\":\"org.springframework.batch.core.step.item.ChunkOrientedTasklet\",\"FlatFileItemReader.read.count\":14,\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}',NULL),(21,'{\"batch.taskletType\":\"com.yamo.skillsmates.populate.tasklet.FileControlTasklet\",\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}',NULL),(22,'{\"batch.taskletType\":\"org.springframework.batch.core.step.item.ChunkOrientedTasklet\",\"FlatFileItemReader.read.count\":67,\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}',NULL),(23,'{\"batch.taskletType\":\"com.yamo.skillsmates.populate.tasklet.FileControlTasklet\",\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}',NULL),(24,'{\"batch.taskletType\":\"org.springframework.batch.core.step.item.ChunkOrientedTasklet\",\"FlatFileItemReader.read.count\":35,\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}',NULL),(25,'{\"batch.taskletType\":\"com.yamo.skillsmates.populate.tasklet.FileControlTasklet\",\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}',NULL),(26,'{\"batch.taskletType\":\"org.springframework.batch.core.step.item.ChunkOrientedTasklet\",\"FlatFileItemReader.read.count\":5,\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}',NULL),(27,'{\"batch.taskletType\":\"com.yamo.skillsmates.populate.tasklet.FileControlTasklet\",\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}',NULL),(28,'{\"batch.taskletType\":\"org.springframework.batch.core.step.item.ChunkOrientedTasklet\",\"FlatFileItemReader.read.count\":5,\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}',NULL),(29,'{\"batch.taskletType\":\"com.yamo.skillsmates.populate.tasklet.FileControlTasklet\",\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}',NULL),(30,'{\"batch.taskletType\":\"org.springframework.batch.core.step.item.ChunkOrientedTasklet\",\"FlatFileItemReader.read.count\":5,\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}',NULL),(31,'{\"batch.taskletType\":\"com.yamo.skillsmates.populate.tasklet.FileControlTasklet\",\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}',NULL),(32,'{\"batch.taskletType\":\"org.springframework.batch.core.step.item.ChunkOrientedTasklet\",\"FlatFileItemReader.read.count\":7,\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}',NULL),(33,'{\"batch.taskletType\":\"com.yamo.skillsmates.populate.tasklet.FileControlTasklet\",\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}',NULL),(34,'{\"batch.taskletType\":\"org.springframework.batch.core.step.item.ChunkOrientedTasklet\",\"FlatFileItemReader.read.count\":27,\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}',NULL);
/*!40000 ALTER TABLE `batch_step_execution_context` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `batch_step_execution_seq`
--

DROP TABLE IF EXISTS `batch_step_execution_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `batch_step_execution_seq` (
  `ID` bigint(20) NOT NULL,
  `UNIQUE_KEY` char(1) NOT NULL,
  UNIQUE KEY `UNIQUE_KEY_UN` (`UNIQUE_KEY`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `batch_step_execution_seq`
--

LOCK TABLES `batch_step_execution_seq` WRITE;
/*!40000 ALTER TABLE `batch_step_execution_seq` DISABLE KEYS */;
INSERT INTO `batch_step_execution_seq` VALUES (34,'0');
/*!40000 ALTER TABLE `batch_step_execution_seq` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comment`
--

DROP TABLE IF EXISTS `comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comment` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `active` bit(1) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `deleted` bit(1) DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  `revision` bigint(20) DEFAULT NULL,
  `body` varchar(255) NOT NULL,
  `account` int(11) DEFAULT NULL,
  `post` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_br19krw6o9vo598yx94h63wxd` (`id_server`),
  KEY `FKg4pifdpsrn19a8hubl96s2ncp` (`account`),
  KEY `FKomrdwc0ub3x7hvvlyu6htn8ti` (`post`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comment`
--

LOCK TABLES `comment` WRITE;
/*!40000 ALTER TABLE `comment` DISABLE KEYS */;
/*!40000 ALTER TABLE `comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `country`
--

DROP TABLE IF EXISTS `country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `country` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `label` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_bs66m8npqqbrydipm9k2ko187` (`id_server`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `country`
--

LOCK TABLES `country` WRITE;
/*!40000 ALTER TABLE `country` DISABLE KEYS */;
INSERT INTO `country` VALUES (8,'11609659966800','Afghanistan'),(9,'11609661120100','Afrique du sud'),(10,'11609661888700','Îles åland'),(11,'11609662594000','Albanie'),(12,'11609663338700','Algérie'),(13,'11609664117900','Allemagne'),(14,'11609664984200','Andorre'),(15,'11609665820000','Angola'),(16,'11609666623100','Anguilla'),(17,'11609667521000','Antarctique'),(18,'11609668436400','Antigua-et-barbuda'),(19,'11609669269600','Arabie saoudite'),(20,'11609670134100','Argentine'),(21,'11609670954400','Arménie'),(22,'11609671690100','Aruba'),(23,'11609672697800','Australie'),(24,'11609673518400','Autriche'),(25,'11609674260800','Azerbaïdjan'),(26,'11609675209000','Bahamas'),(27,'11609676013200','Bahreïn'),(28,'11609676937700','Bangladesh'),(29,'11609678788700','Barbade'),(30,'11609679488000','Biélorussie'),(31,'11609680165700','Belgique'),(32,'11609680925800','Belize'),(33,'11609681673800','Bénin'),(34,'11609682371100','Bermudes'),(35,'11609683043000','Bhoutan'),(36,'11609683767100','Bolivie'),(37,'11609684436800','Pays-bas caribéens'),(38,'11609685109100','Bosnie-herzégovine'),(39,'11609685838700','Botswana'),(40,'11609686502300','Île bouvet'),(41,'11609687205900','Brésil'),(42,'11609688027700','Brunei'),(43,'11609689087600','Bulgarie'),(44,'11609689979700','Burkina faso'),(45,'11609690725600','Burundi'),(46,'11609691428800','Îles caïmans'),(47,'11609692172700','Cambodge'),(48,'11609692905700','Cameroun'),(49,'11609694005700','Canada'),(50,'11609695260600','Cap-vert'),(51,'11609696127000','République centrafricaine'),(52,'11609696837500','Chili'),(53,'11609697526300','Chine'),(54,'11609698290600','Île christmas'),(55,'11609699184300','Chypre (pays)'),(56,'11609699970000','Îles cocos'),(57,'11609700788300','Colombie'),(58,'11609701490900','Comores (pays)'),(59,'11609702201800','République du congo'),(60,'11609702869200','République démocratique du congo'),(61,'11609703516400','Îles cook'),(62,'11609704167400','Corée du sud'),(63,'11609704943700','Corée du nord'),(64,'11609705746600','Costa rica'),(65,'11609706462900','Côte d\'ivoire'),(66,'11609707196500','Croatie'),(67,'11609708092000','Cuba'),(68,'11609708993700','Curaçao'),(69,'11609709837200','Danemark'),(70,'11609711079900','Djibouti'),(71,'11609712226000','République dominicaine'),(72,'11609713120500','Dominique'),(73,'11609714047100','Égypte'),(74,'11609714824500','Salvador'),(75,'11609715602200','Émirats arabes unis'),(76,'11609716312000','Équateur (pays)'),(77,'11609716999800','Érythrée'),(78,'11609717690500','Espagne'),(79,'11609718400700','Estonie'),(80,'11609719122900','États-unis'),(81,'11609719862000','Éthiopie'),(82,'11609720700900','Malouines'),(83,'11609721486600','Îles féroé'),(84,'11609722256600','Fidji'),(85,'11609723003000','Finlande'),(86,'11609723854600','France'),(87,'11609724741200','Gabon'),(88,'11609725496300','Gambie'),(89,'11609726301000','Géorgie (pays)'),(90,'11609727512800','Géorgie du sud-et-les îles sandwich du sud'),(91,'11609728382500','Ghana'),(92,'11609729282700','Gibraltar'),(93,'11609730190900','Grèce'),(94,'11609730895700','Grenade (pays)'),(95,'11609731719600','Groenland'),(96,'11609732460200','Guadeloupe'),(97,'11609733166400','Guam'),(98,'11609733810200','Guatemala'),(99,'11609734450000','Guernesey'),(100,'11609735187100','Guinée'),(101,'11609735920800','Guinée-bissau'),(102,'11609736641300','Guinée équatoriale'),(103,'11609737358100','Guyana'),(104,'11609738138300','Guyane'),(105,'11609738918400','Haïti'),(106,'11609739621700','Îles heard-et-macdonald'),(107,'11609740303200','Honduras'),(108,'11610051475100','Hong kong'),(109,'11610052191900','Hongrie'),(110,'11610052831600','Île de man'),(111,'11610053582300','Îles mineures éloignées des états-unis'),(112,'11610054289400','Îles vierges britanniques'),(113,'11610055228300','Îles vierges des états-unis'),(114,'11610056198900','Inde'),(115,'11610057193700','Indonésie'),(116,'11610058240000','Iran'),(117,'11610059087600','Irak'),(118,'11610060336300','Irlande (pays)'),(119,'11610061934100','Islande'),(120,'11610067169100','Israël'),(121,'11610068382100','Italie'),(122,'11610069522100','Jamaïque'),(123,'11610070712400','Japon'),(124,'11610071723800','Jersey'),(125,'11610072908000','Jordanie'),(126,'11610074294100','Kazakhstan'),(127,'11610075307300','Kenya'),(128,'11610076093000','Kirghizistan'),(129,'11610077966600','Kiribati'),(130,'11610079019100','Koweït'),(131,'11610079830800','Laos'),(132,'11610080676500','Lesotho'),(133,'11610081915400','Lettonie'),(134,'11610083178300','Liban'),(135,'11610085907800','Liberia'),(136,'11610087123000','Libye'),(137,'11610088113000','Liechtenstein'),(138,'11610089122600','Lituanie'),(139,'11610090043400','Luxembourg (pays)'),(140,'11610090850100','Macao'),(141,'11610091628500','Macédoine du nord'),(142,'11610092571200','Madagascar'),(143,'11610095200100','Malaisie'),(144,'11610096264000','Malawi'),(145,'11610097540600','Maldives'),(146,'11610098626800','Mali'),(147,'11610099417300','Malte'),(148,'11610100149200','Îles mariannes du nord'),(149,'11610100916300','Maroc'),(150,'11610101757200','Îles marshall (pays)'),(151,'11610102569400','Martinique'),(152,'11610103413900','Maurice (pays)'),(153,'11610104255300','Mauritanie'),(154,'11610105421500','Mayotte'),(155,'11610106527900','Mexique'),(156,'11610107504200','États fédérés de micronésie (pays)'),(157,'11610108660600','Moldavie'),(158,'11610109782800','Monaco'),(159,'11610112031500','Mongolie'),(160,'11610112778100','Monténégro'),(161,'11610113503800','Montserrat'),(162,'11610114277500','Mozambique'),(163,'11610115332600','Birmanie'),(164,'11610116391800','Namibie'),(165,'11610117391400','Nauru'),(166,'11610118541300','Népal'),(167,'11610119521100','Nicaragua'),(168,'11610120898100','Niger'),(169,'11610122187700','Nigeria'),(170,'11610123075500','Niue'),(171,'11610124333500','Île norfolk'),(172,'11610125333500','Norvège'),(173,'11610126332300','Nouvelle-calédonie'),(174,'11610129713800','Nouvelle-zélande'),(175,'11610130876100','Territoire britannique de l\'océan indien'),(176,'11610131863900','Oman'),(177,'11610132852700','Ouganda'),(178,'11610134430100','Ouzbékistan'),(179,'11610135441000','Pakistan'),(180,'11610136377300','Palaos'),(181,'11610137355200','Palestine'),(182,'11610138205300','Panama'),(183,'11610138968000','Papouasie-nouvelle-guinée'),(184,'11610139628700','Paraguay'),(185,'11610140245300','Pays-bas'),(186,'11610141082500','Pérou'),(187,'11610142213000','Philippines'),(188,'11610143874500','Îles pitcairn'),(189,'11610145832500','Pologne'),(190,'11610146907400','Polynésie française'),(191,'11610147959500','Porto rico'),(192,'11610149190300','Portugal'),(193,'11610150014200','Qatar'),(194,'11610150843200','La réunion'),(195,'11610151722000','Roumanie'),(196,'11610152532800','Royaume-uni'),(197,'11610153510400','Russie'),(198,'11610154737000','Rwanda'),(199,'11610156201600','République arabe sahraouie démocratique'),(200,'11610158172400','Saint-barthélemy'),(201,'11610159166400','Saint-christophe-et-niévès'),(202,'11610160765700','Saint-marin'),(203,'11610162144200','Saint-martin'),(204,'11610162972400','Saint-martin'),(205,'11610163777900','Saint-pierre-et-miquelon'),(206,'11610164536700','Saint-siège (état de la cité du vatican)'),(207,'11610165440700','Saint-vincent-et-les-grenadines'),(208,'11610353977200','Sainte-hélène; ascension et tristan da cunha'),(209,'11610354966100','Sainte-lucie'),(210,'11610355907900','Salomon'),(211,'11610357151300','Samoa'),(212,'11610358097600','Samoa américaines'),(213,'11610358771600','Sao tomé-et-principe'),(214,'11610359328500','Sénégal'),(215,'11610360417200','Serbie'),(216,'11610361430700','Seychelles'),(217,'11610362125700','Sierra leone'),(218,'11610362738500','Singapour'),(219,'11610363367300','Slovaquie'),(220,'11610364034200','Slovénie'),(221,'11610364926600','Somalie'),(222,'11610365805300','Soudan'),(223,'11610366823400','Soudan du sud'),(224,'11610367897100','Sri lanka'),(225,'11610368868200','Suède'),(226,'11610369584100','Suisse'),(227,'11610370135500','Suriname'),(228,'11610370707900','Svalbard et ile jan mayen'),(229,'11610371326200','Eswatini'),(230,'11610371977000','Syrie'),(231,'11610372633600','Tadjikistan'),(232,'11610373227400','Taïwan / (république de chine (taïwan))'),(233,'11610373856600','Tanzanie'),(234,'11610374493400','Tchad'),(235,'11610375359400','Tchéquie'),(236,'11610376171300','Terres australes et antarctiques françaises'),(237,'11610378115600','Thaïlande'),(238,'11610378889400','Timor oriental'),(239,'11610379575800','Togo'),(240,'11610380278300','Tokelau'),(241,'11610381046100','Tonga'),(242,'11610381781700','Trinité-et-tobago'),(243,'11610382508400','Tunisie'),(244,'11610383150700','Turkménistan'),(245,'11610383749200','Îles turques-et-caïques'),(246,'11610384362600','Turquie'),(247,'11610384984200','Tuvalu'),(248,'11610385592700','Ukraine'),(249,'11610386171900','Uruguay'),(250,'11610386754100','Vanuatu'),(251,'11610387398000','Venezuela'),(252,'11610388197600','Viêt nam'),(253,'11610389037500','Wallis-et-futuna'),(254,'11610389707400','Yémen'),(255,'11610390249100','Zambie'),(256,'11610390806900','Zimbabwe');
/*!40000 ALTER TABLE `country` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `degree_obtained`
--

DROP TABLE IF EXISTS `degree_obtained`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `degree_obtained` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `active` bit(1) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `deleted` bit(1) DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  `revision` bigint(20) DEFAULT NULL,
  `city` varchar(50) NOT NULL,
  `current_position` bit(1) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `establishment_name` varchar(50) NOT NULL,
  `account` int(11) DEFAULT NULL,
  `end_date` datetime NOT NULL,
  `start_date` datetime NOT NULL,
  `establishment_type` int(11) DEFAULT NULL,
  `specialty` int(11) DEFAULT NULL,
  `study_level` int(11) DEFAULT NULL,
  `teaching_area` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_it7wk6xlws8hdylq3dieky5ut` (`id_server`),
  KEY `FKkiktrv9qgmrh2sthbfx0hkd7u` (`establishment_type`),
  KEY `FKock7rdq2hv8nc894ub5rr6srg` (`specialty`),
  KEY `FK9ygsp9nh47o8enlr6gbxu3v1s` (`study_level`),
  KEY `FK75axn0e4e11xu6ny1kvaebh4g` (`teaching_area`),
  KEY `FK_kx2faddje0w3cixn027phkl7b` (`account`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `degree_obtained`
--

LOCK TABLES `degree_obtained` WRITE;
/*!40000 ALTER TABLE `degree_obtained` DISABLE KEYS */;
INSERT INTO `degree_obtained` VALUES (568,'24038575733300','','2020-09-28 00:34:57','\0',NULL,0,'Buea','\0',NULL,'High hill ',1,'2020-09-30 02:00:00','2020-09-01 02:00:00',441,553,564,560),(569,'24347634827800','','2020-09-28 00:40:06','\0',NULL,0,'Limbe','\0','If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true ','Lycee classique',1,'2020-09-18 02:00:00','2020-09-01 02:00:00',441,554,565,561);
/*!40000 ALTER TABLE `degree_obtained` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `diploma`
--

DROP TABLE IF EXISTS `diploma`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `diploma` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `label` varchar(255) NOT NULL,
  `education` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_gi7a8g81d35pkdo1xabrrc294` (`id_server`),
  KEY `FKtc8s0lrn5ie925rom0xka5hp` (`education`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `diploma`
--

LOCK TABLES `diploma` WRITE;
/*!40000 ALTER TABLE `diploma` DISABLE KEYS */;
INSERT INTO `diploma` VALUES (519,'12995378753500','Baccalauréat Scientifique','Baccalauréat Scientifique',438),(520,'12995390650400','Baccalauréat Sciences économique et sociales','Baccalauréat Sciences économique et sociales',438),(521,'12995393583600','Baccalauréat littéraire','Baccalauréat littéraire',438),(522,'12995395992000','Série Sciences et technologies du management et de la gestion (STMG)','Série Sciences et technologies du management et de la gestion (STMG)',438),(523,'12995399289700','Le baccalauréat sciences et technologies du design et des arts appliqués (STD2A)','Le baccalauréat sciences et technologies du design et des arts appliqués (STD2A)',438),(524,'12995402050800','Le baccalauréat sciences et technologies de l\'industrie et du développement durable (STI2D)','Le baccalauréat sciences et technologies de l\'industrie et du développement durable (STI2D)',438),(525,'12995404896300','Le baccalauréat sciences et technologies de laboratoire (STL)','Le baccalauréat sciences et technologies de laboratoire (STL)',438),(526,'12995407741000','Le baccalauréat sciences et technologies de la santé et du social ST2S','Le baccalauréat sciences et technologies de la santé et du social ST2S',438),(527,'12995410892800','Baccalauréat sciences et technologies de l\'agronomie et du vivant (STAV)','Baccalauréat sciences et technologies de l\'agronomie et du vivant (STAV)',438),(528,'12995413883900','Baccalauréat technique de la musique et de la danse (TMD)','Baccalauréat technique de la musique et de la danse (TMD)',438),(529,'12995416450400','Baccalauréat sciences et technologies de l\'hôtellerie et de la restauration (STHR)','Baccalauréat sciences et technologies de l\'hôtellerie et de la restauration (STHR)',438),(530,'12995419445300','Autres (à préciser manuellement)','Autres (à préciser manuellement)',438),(531,'12995422176200','CAP (Bac -1)','CAP (Bac -1)',438),(532,'12995424668800','BEP (Bac -1)','BEP (Bac -1)',438),(533,'12995427406200','Baccalauréat professionnel (à préciser manuellement)','Baccalauréat professionnel (à préciser manuellement)',438),(534,'12995429865900','BTS (Bac +2)','BTS (Bac +2)',439),(535,'12995432877100','DUT (BAC +2)','DUT (BAC +2)',439),(536,'12995435257000','Licence Professionnelle (Bac +3)','Licence Professionnelle (Bac +3)',439),(537,'12995438172400','Licence Générale (Bac+3)','Licence Générale (Bac+3)',439),(538,'12995441159000','Maîtrise (Bac +4)','Maîtrise (Bac +4)',439),(539,'12995443871200','Bachelor (Bac +4)','Bachelor (Bac +4)',439),(540,'12995447398600','Master Professionnel (Bac +5)','Master Professionnel (Bac +5)',439),(541,'12995449780400','Master Recherche (Bac +5)','Master Recherche (Bac +5)',439),(542,'12995452129600','Magistère (Bac +5)','Magistère (Bac +5)',439),(543,'12995454975700','Bac +6 (à préciser manuellement)','Bac +6 (à préciser manuellement)',439),(544,'12995457561400','Bac +7 (à préciser manuellement)','Bac +7 (à préciser manuellement)',439),(545,'12995460555200','Doctorat (Bac +8)','Doctorat (Bac +8)',439),(546,'12995463250900','Autres (à sélectionner de bac +1 à Bac +11 et à préciser)','Autres (à sélectionner de bac +1 à Bac +11 et à préciser)',439),(547,'12995465311300','Docteur en Pharmacie (Bac+6)','Docteur en Pharmacie (Bac+6)',439),(548,'12995467610300','Docteur en Pharmacie Spécialisé (Bac+9)','Docteur en Pharmacie Spécialisé (Bac+9)',439),(549,'12995470602400','Docteur en Médecine (Bac +9)','Docteur en Médecine (Bac +9)',439),(550,'12995473370100','Docteur en chirurgie dentaire Spécialisé (Bac+10)','Docteur en chirurgie dentaire Spécialisé (Bac+10)',439),(551,'12995476107000','Docteur en médecine spécialisé (Bac +11)','Docteur en médecine spécialisé (Bac +11)',439),(552,'12995478501600','Autres (à sélectionner de bac +1 à Bac +11 et à préciser)','Autres (à sélectionner de bac +1 à Bac +11 et à préciser)',439);
/*!40000 ALTER TABLE `diploma` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `discipline`
--

DROP TABLE IF EXISTS `discipline`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `discipline` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `label` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_5hj653mpeo2wjao494f7p1w2w` (`id_server`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `discipline`
--

LOCK TABLES `discipline` WRITE;
/*!40000 ALTER TABLE `discipline` DISABLE KEYS */;
INSERT INTO `discipline` VALUES (259,'12121166818800','01 : Droit privé et sciences criminelles','01 : Droit privé et sciences criminelles'),(260,'12121167831700','02 : Droit public','02 : Droit public'),(261,'12121169001100','03 : Histoire du droit et des institutions','03 : Histoire du droit et des institutions'),(262,'12121169932500','04 : Science politique','04 : Science politique'),(263,'12121170703300','05 : Sciences économiques','05 : Sciences économiques'),(264,'12121171540100','06 : Sciences de gestion','06 : Sciences de gestion'),(265,'12121172362500','07 : Sciences du langage : linguistique et phonétique générales','07 : Sciences du langage : linguistique et phonétique générales'),(266,'12121173157800','08 : Langues et littératures anciennes','08 : Langues et littératures anciennes'),(267,'12121173925100','09 : Langue et littérature françaises','09 : Langue et littérature françaises'),(268,'12121174721800','10 : Littératures comparées','10 : Littératures comparées'),(269,'12121175556200','11 : Langues et littératures anglaises et anglo-saxonnes','11 : Langues et littératures anglaises et anglo-saxonnes'),(270,'12121176373000','12 : Langues et littératures germaniques et scandinaves','12 : Langues et littératures germaniques et scandinaves'),(271,'12121177191500','13 : Langues et littératures slaves','13 : Langues et littératures slaves'),(272,'12121178035900','14 : Langues et littératures romanes : espagnol, italien, portugais, autres langues romanes','14 : Langues et littératures romanes : espagnol, italien, portugais, autres langues romanes'),(273,'12121178904900','15 : Langues et littératures arabes, chinoises, japonaises, hébraïques, d\'autres domaines linguistiques','15 : Langues et littératures arabes, chinoises, japonaises, hébraïques, d\'autres domaines linguistiques'),(274,'12121179969800','16 : Psychologie et Ergonomie','16 : Psychologie et Ergonomie'),(275,'12121180933800','17 : Philosophie','17 : Philosophie'),(276,'12121181720800','18 : Architecture et Arts : plastiques, du spectacle, musique, musicologie, esthétique, sciences de l\'art','18 : Architecture et Arts : plastiques, du spectacle, musique, musicologie, esthétique, sciences de l\'art'),(277,'12121182618800','19 : Sociologie, démographie','19 : Sociologie, démographie'),(278,'12121183465200','20 : Anthropologie biologique, ethnologie, préhistoire','20 : Anthropologie biologique, ethnologie, préhistoire'),(279,'12121184317900','21 : Histoire et civilisations : histoire et archéologie des mondes anciens et des mondes médiévaux ; de l\'art','21 : Histoire et civilisations : histoire et archéologie des mondes anciens et des mondes médiévaux ; de l\'art'),(280,'12121186493600','22 : Histoire et civilisations : histoire des mondes modernes, histoire du monde contemporain ; de l\'art ; de la musique','22 : Histoire et civilisations : histoire des mondes modernes, histoire du monde contemporain ; de l\'art ; de la musique'),(281,'12121187259000','23 : Géographie physique, humaine, économique et régionale','23 : Géographie physique, humaine, économique et régionale'),(282,'12121187972300','24 : Aménagement de l\'espace, urbanisme','24 : Aménagement de l\'espace, urbanisme'),(283,'12121188812900','70 : Sciences de l\'éducation (voisine des disciplines 16, 19, 71, 74)','70 : Sciences de l\'éducation (voisine des disciplines 16, 19, 71, 74)'),(284,'12121189652500','71 : Sciences de l\'information et de la communication','71 : Sciences de l\'information et de la communication'),(285,'12121190436600','72 : Épistémologie, histoire des sciences et des techniques','72 : Épistémologie, histoire des sciences et des techniques'),(286,'12121191194000','73 : Cultures et langues régionales','73 : Cultures et langues régionales'),(287,'12121191982100','74 : Sciences et techniques des activités physiques et sportives','74 : Sciences et techniques des activités physiques et sportives'),(288,'12121192808700','76 : Théologie catholique','76 : Théologie catholique'),(289,'12121193669800','77 : Théologie protestante','77 : Théologie protestante'),(290,'12121194529200','25 : Mathématiques','25 : Mathématiques'),(291,'12121195277800','26 : Mathématiques appliquées et applications des mathématiques','26 : Mathématiques appliquées et applications des mathématiques'),(292,'12121196002100','27 : Informatique','27 : Informatique'),(293,'12121196809800','28 : Milieux denses et matériaux','28 : Milieux denses et matériaux'),(294,'12121197626200','29 : Constituants élémentaires','29 : Constituants élémentaires'),(295,'12121198570200','30 : Milieux dilués et optique','30 : Milieux dilués et optique'),(296,'12121199378200','31 : Chimie théorique, physique, analytique','31 : Chimie théorique, physique, analytique'),(297,'12121200090300','32 : Chimie organique, inorganique, industrielle','32 : Chimie organique, inorganique, industrielle'),(298,'12121201020000','33 : Chimie des matériaux','33 : Chimie des matériaux'),(299,'12121201969000','34 : Astronomie, astrophysique','34 : Astronomie, astrophysique'),(300,'12121203102500','35 : Structure et évolution de la Terre et des autres planètes','35 : Structure et évolution de la Terre et des autres planètes'),(301,'12121204324700','36 : Terre solide : géodynamique des enveloppes supérieures, paléobiosphère','36 : Terre solide : géodynamique des enveloppes supérieures, paléobiosphère'),(302,'12121205111400','37 : Météorologie, océanographie physique et physique de l\'environnement','37 : Météorologie, océanographie physique et physique de l\'environnement'),(303,'12121205879200','60 : Mécanique, génie mécanique, génie civil','60 : Mécanique, génie mécanique, génie civil'),(304,'12121206608800','61 : Génie informatique, automatique et traitement du signal','61 : Génie informatique, automatique et traitement du signal'),(305,'12121207385100','62 : Énergétique, génie des procédés','62 : Énergétique, génie des procédés'),(306,'12121208246500','63 : Génie Électrique, Électronique, optronique et systèmes','63 : Génie Électrique, Électronique, optronique et systèmes'),(307,'12121209272500','64 : Biochimie et biologie moléculaire','64 : Biochimie et biologie moléculaire'),(308,'12121210199200','65 : Biologie cellulaire','65 : Biologie cellulaire'),(309,'12121210974200','66 : Physiologie','66 : Physiologie'),(310,'12121211690900','67 : Biologie des populations et écologie','67 : Biologie des populations et écologie'),(311,'12121212439600','68 : Biologie des organismes','68 : Biologie des organismes'),(312,'12121213147100','69 : Neurosciences','69 : Neurosciences'),(313,'12121213832600','80 : Sciences physico-chimiques et ingénierie appliquée à la santé (ex-39)','80 : Sciences physico-chimiques et ingénierie appliquée à la santé (ex-39)'),(314,'12121214608200','81 : Sciences du médicament et des autres produits de santé (ex-40)','81 : Sciences du médicament et des autres produits de santé (ex-40)'),(315,'12121215341200','82 : Sciences biologiques, fondamentales et cliniques (ex-41)','82 : Sciences biologiques, fondamentales et cliniques (ex-41)'),(316,'12121216052900','85 : Sciences physico-chimiques et ingénierie appliquée à la santé (ex-39)','85 : Sciences physico-chimiques et ingénierie appliquée à la santé (ex-39)'),(317,'12121216809200','86 : Sciences du médicament et des autres produits de santé (ex-40)','86 : Sciences du médicament et des autres produits de santé (ex-40)'),(318,'12121217792200','87 : Sciences biologiques, fondamentales et cliniques (ex-41)','87 : Sciences biologiques, fondamentales et cliniques (ex-41)'),(319,'12121218824400','90 : Maïeutique','90 : Maïeutique'),(320,'12121219882200','91 : Sciences de la rééducation et de la réadaptation','91 : Sciences de la rééducation et de la réadaptation'),(321,'12121220859900','92 : Sciences infirmières','92 : Sciences infirmières'),(322,'12121222023100','42 : Morphologie et morphogenèse','42 : Morphologie et morphogenèse'),(323,'12121222936300','01 : Anatomie','01 : Anatomie'),(324,'12121223868700','02 : Histologie, embryologie et cytogénétique','02 : Histologie, embryologie et cytogénétique'),(325,'12121224657100','03 : Anatomie et cytologie pathologiques','03 : Anatomie et cytologie pathologiques'),(326,'12121225360600','43 : Biophysique et imagerie médicale','43 : Biophysique et imagerie médicale'),(327,'12121226057600','01 : Biophysique et médecine nucléaire','01 : Biophysique et médecine nucléaire'),(328,'12121226738200','02 : Radiologie et imagerie médicale)','02 : Radiologie et imagerie médicale)'),(329,'12121227463200','44 : Biochimie, biologie cellulaire et moléculaire, physiologie et nutrition','44 : Biochimie, biologie cellulaire et moléculaire, physiologie et nutrition'),(330,'12121228193700','01 : Biochimie et biologie moléculaire','01 : Biochimie et biologie moléculaire'),(331,'12121229026100','02 : Physiologie','02 : Physiologie'),(332,'12121229729700','03 : Biologie cellulaire','03 : Biologie cellulaire'),(333,'12121230541400','04 : Nutrition','04 : Nutrition'),(334,'12121231349700','45 : Microbiologie, maladies transmissibles et hygiène','45 : Microbiologie, maladies transmissibles et hygiène'),(335,'12121232059300','01 : Bactériologie - virologie ; hygiène hospitalière','01 : Bactériologie - virologie ; hygiène hospitalière'),(336,'12121232737400','02 : Parasitologie et mycologie','02 : Parasitologie et mycologie'),(337,'12121233571900','03 : Maladies infectieuses ; maladies tropicales','03 : Maladies infectieuses ; maladies tropicales'),(338,'12121234433800','46 : Santé publique, environnement et société','46 : Santé publique, environnement et société'),(339,'12121235259800','01 : Épidémiologie, économie de la santé et prévention','01 : Épidémiologie, économie de la santé et prévention'),(340,'12121236434800','02 : Médecine et santé au travail','02 : Médecine et santé au travail'),(341,'12121237381400','03 : Médecine légale et droit de la santé','03 : Médecine légale et droit de la santé'),(342,'12121238214800','04 : Biostatistiques, informatique médicale et technologies de communication','04 : Biostatistiques, informatique médicale et technologies de communication'),(343,'12121239216200','05 : Épistémologie clinique40','05 : Épistémologie clinique40'),(344,'12121240304000','47 : Cancérologie, génétique, hématologie, immunologie','47 : Cancérologie, génétique, hématologie, immunologie'),(345,'12121241141200','01 : Hématologie ; transfusion','01 : Hématologie ; transfusion'),(346,'12121241872300','02 : Cancérologie ; radiothérapie','02 : Cancérologie ; radiothérapie'),(347,'12121242626000','03 : Immunologie','03 : Immunologie'),(348,'12121243405000','04 : Génétique','04 : Génétique'),(349,'12121244094300','48 : Anesthésiologie, réanimation, médecine d\'urgence, pharmacologie et thérapeutique','48 : Anesthésiologie, réanimation, médecine d\'urgence, pharmacologie et thérapeutique'),(350,'12121244945000','01 : Anesthésiologie - réanimation ; médecine d\'urgence','01 : Anesthésiologie - réanimation ; médecine d\'urgence'),(351,'12121245802600','02 : Réanimation ; médecine d\'urgence','02 : Réanimation ; médecine d\'urgence'),(352,'12121246599400','03 : Pharmacologie fondamentale ; pharmacologie clinique','03 : Pharmacologie fondamentale ; pharmacologie clinique'),(353,'12121247320300','04 : Thérapeutique','04 : Thérapeutique'),(354,'12121248105100','49 : Pathologie nerveuse et musculaire, pathologie mentale, handicap et rééducation','49 : Pathologie nerveuse et musculaire, pathologie mentale, handicap et rééducation'),(355,'12121249000600','01 : Neurologie','01 : Neurologie'),(356,'12121249836600','02 : Neurochirurgie','02 : Neurochirurgie'),(357,'12121250541700','03 : Psychiatrie d\'adultes','03 : Psychiatrie d\'adultes'),(358,'12121251222300','04 : Pédopsychiatrie','04 : Pédopsychiatrie'),(359,'12121526419700','05 : Médecine physique et de réadaptation','05 : Médecine physique et de réadaptation'),(360,'12121527234000','50 : Pathologie ostéo-articulaire, dermatologie et chirurgie plastique','50 : Pathologie ostéo-articulaire, dermatologie et chirurgie plastique'),(361,'12121527970100','01 : Rhumatologie','01 : Rhumatologie'),(362,'12121528939500','02 : Chirurgie orthopédique et traumatologique','02 : Chirurgie orthopédique et traumatologique'),(363,'12121529686700','03 : Dermato-vénéréologie','03 : Dermato-vénéréologie'),(364,'12121530366700','04 : Chirurgie plastique, reconstructrice et esthétique ; brûlologie','04 : Chirurgie plastique, reconstructrice et esthétique ; brûlologie'),(365,'12121531175800','51 : Pathologie cardiorespiratoire et vasculaire','51 : Pathologie cardiorespiratoire et vasculaire'),(366,'12121531845900','01 : Pneumologie','01 : Pneumologie'),(367,'12121532547700','02 : Cardiologie','02 : Cardiologie'),(368,'12121533442900','03 : Chirurgie thoracique et cardiovasculaire','03 : Chirurgie thoracique et cardiovasculaire'),(369,'12121534272200','04 : Chirurgie vasculaire ; médecine vasculaire','04 : Chirurgie vasculaire ; médecine vasculaire'),(370,'12121535181400','52 : Maladies des appareils digestif et urinaire','52 : Maladies des appareils digestif et urinaire'),(371,'12121536231300','01 : Gastroentérologie ; hépatologie','01 : Gastroentérologie ; hépatologie'),(372,'12121537010300','02 : Chirurgie digestive','02 : Chirurgie digestive'),(373,'12121537789400','03 : Néphrologie','03 : Néphrologie'),(374,'12121538680600','04 : Urologie','04 : Urologie'),(375,'12121539545800','53 : Médecine interne, gériatrie, chirurgie générale et médecine générale','53 : Médecine interne, gériatrie, chirurgie générale et médecine générale'),(376,'12121540354700','01 : Médecine interne ; gériatrie et biologie du vieillissement, addictologie41','01 : Médecine interne ; gériatrie et biologie du vieillissement, addictologie41'),(377,'12121541056600','02 : Chirurgie générale','02 : Chirurgie générale'),(378,'12121541735100','03 : Médecine générale','03 : Médecine générale'),(379,'12121542414600','54 : Développement et pathologie de l\'enfant, gynécologie-obstétrique, endocrinologie et reproduction','54 : Développement et pathologie de l\'enfant, gynécologie-obstétrique, endocrinologie et reproduction'),(380,'12121543118500','01 : Pédiatrie','01 : Pédiatrie'),(381,'12121543974700','02 : Chirurgie infantile','02 : Chirurgie infantile'),(382,'12121544862300','03 : Gynécologie-obstétrique ; gynécologie médicale','03 : Gynécologie-obstétrique ; gynécologie médicale'),(383,'12121545610300','04 : Endocrinologie, diabète et maladies métaboliques','04 : Endocrinologie, diabète et maladies métaboliques'),(384,'12121546392700','05 : Biologie et médecine du développement et de la reproduction','05 : Biologie et médecine du développement et de la reproduction'),(385,'12121547249900','55 : Pathologie de la tête et du cou','55 : Pathologie de la tête et du cou'),(386,'12121549394000','01 : Oto-rhino-laryngologie','01 : Oto-rhino-laryngologie'),(387,'12121550305900','02 : Ophtalmologie','02 : Ophtalmologie'),(388,'12121551055700','03 : Chirurgie maxillo-faciale et stomatologie','03 : Chirurgie maxillo-faciale et stomatologie'),(389,'12121551829300','56 : Développement, croissance et prévention','56 : Développement, croissance et prévention'),(390,'12121552979800','01 : Pédodontie','01 : Pédodontie'),(391,'12121553724400','02 : Orthopédie dento-faciale','02 : Orthopédie dento-faciale'),(392,'12121554442000','03 : Prévention, épidémiologie, économie de la santé, odontologie légale','03 : Prévention, épidémiologie, économie de la santé, odontologie légale'),(393,'12121555194600','57 : Sciences biologiques, médecine et chirurgie buccales','57 : Sciences biologiques, médecine et chirurgie buccales'),(394,'12121555943700','01 : Parodontologie','01 : Parodontologie'),(395,'12121556629600','02 : Chirurgie buccale, pathologie et thérapeutique, anesthésiologie et réanimation','02 : Chirurgie buccale, pathologie et thérapeutique, anesthésiologie et réanimation'),(396,'12121557333700','03 : Sciences biologiques (biochimie, immunologie, histologie, embryologie, génétique, anatomie pathologique, bactériologie, pharmacologie)','03 : Sciences biologiques (biochimie, immunologie, histologie, embryologie, génétique, anatomie pathologique, bactériologie, pharmacologie)'),(397,'12121558053500','58 : Sciences physiques et physiologiques endodontiques et prothétiques','58 : Sciences physiques et physiologiques endodontiques et prothétiques'),(398,'12121558954400','01 : Odontologie conservatrice, endodontie','01 : Odontologie conservatrice, endodontie'),(399,'12121559687100','02 : Prothèses','02 : Prothèses'),(400,'12121560387800','03 : Sciences anatomiques et physiologiques, occlusodontiques, biomatériaux, biophysique, radiologie','03 : Sciences anatomiques et physiologiques, occlusodontiques, biomatériaux, biophysique, radiologie');
/*!40000 ALTER TABLE `discipline` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `education`
--

DROP TABLE IF EXISTS `education`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `education` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `code` varchar(50) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `label` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_rxld3r7vdl36kn9gej7eu65b7` (`id_server`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `education`
--

LOCK TABLES `education` WRITE;
/*!40000 ALTER TABLE `education` DISABLE KEYS */;
INSERT INTO `education` VALUES (438,'12680588341800','SECONDARY','Enseignement secondaire (avant le baccalauréat)','Enseignement secondaire (avant le baccalauréat)'),(439,'12680589479800','HIGHER','Enseignement supérieur (après le baccalauréat)','Enseignement supérieur (après le baccalauréat)');
/*!40000 ALTER TABLE `education` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `establishment_type`
--

DROP TABLE IF EXISTS `establishment_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `establishment_type` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `label` varchar(255) NOT NULL,
  `education` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_8tc237adw2hrai8twqk5lu5kf` (`id_server`),
  KEY `FK49cpx0mji0tomjon7caxro4eg` (`education`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `establishment_type`
--

LOCK TABLES `establishment_type` WRITE;
/*!40000 ALTER TABLE `establishment_type` DISABLE KEYS */;
INSERT INTO `establishment_type` VALUES (440,'12734489161300','Collège','Collège',438),(441,'12734496977100','Lycée général et Technologie','Lycée général et Technologie',438),(442,'12734498934700','Lycée professionnel','Lycée professionnel',438),(443,'12734501128400','Centre de formation d\'apprentis','Centre de formation d\'apprentis',438),(444,'12734502996200','Autres établissements du secondaire','Autres établissements du secondaire',438),(445,'12734505191500','Université','Université',439),(446,'12734507285900','IUT - Institut Universitaire de Technologie','IUT - Institut Universitaire de Technologie',439),(447,'12734509623500','Grandes écoles','Grandes écoles',439),(448,'12734511624800','Lycée Professionnel','Lycée Professionnel',439),(449,'12734513731000','Ecoles et Instituts Specialisés','Ecoles et Instituts Specialisés',439),(450,'12734515718000','Ecoles supérieure d\'Art et d\'Arts appliqués','Ecoles supérieure d\'Art et d\'Arts appliqués',439),(451,'12734518097800','Ecoles Nationales Supérieur d\'Architecture','Ecoles Nationales Supérieur d\'Architecture',439),(452,'12734520312300','Autres établissements du Supérieur','Autres établissements du Supérieur',439);
/*!40000 ALTER TABLE `establishment_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `frequented_class`
--

DROP TABLE IF EXISTS `frequented_class`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `frequented_class` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `label` varchar(255) NOT NULL,
  `education` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_reayxllx7vkm67nddnrjgyaxd` (`id_server`),
  KEY `FKa3m8swh05iskjbnxe1odk59ee` (`education`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `frequented_class`
--

LOCK TABLES `frequented_class` WRITE;
/*!40000 ALTER TABLE `frequented_class` DISABLE KEYS */;
INSERT INTO `frequented_class` VALUES (453,'12819533121100','Sixième (6ème)','Sixième (6ème)',438),(454,'12819540747300','Cinquième (5ème)','Cinquième (5ème)',438),(455,'12819542734300','Quatrième (4ème)','Quatrième (4ème)',438),(456,'12819544734700','Troisième (3ème)','Troisième (3ème)',438),(457,'12819546770300','Première S (Scientifique)','Première S (Scientifique)',438),(458,'12819549179600','Première ES (Économique et social)','Première ES (Économique et social)',438),(459,'12819551291200','Première L (Littéraire)','Première L (Littéraire)',438),(460,'12819553599500','Terminal S (Scientifique)','Terminal S (Scientifique)',438),(461,'12819555588200','Terminal ES (Économique et social)','Terminal ES (Économique et social)',438),(462,'12819557669100','Terminal L (Littéraire)','Terminal L (Littéraire)',438),(463,'12819559694500','Première STMG (Sciences et technologies du management et de la gestion)','Première STMG (Sciences et technologies du management et de la gestion)',438),(464,'12819561729900','Première STD2A (Sciences et technologies du design et des arts appliqués)','Première STD2A (Sciences et technologies du design et des arts appliqués)',438),(465,'12819563874500','Première STI2D (sciences et technologies de l\'industrie et du développement durable)','Première STI2D (sciences et technologies de l\'industrie et du développement durable)',438),(466,'12819565981800','Première STL (sciences et technologies de laboratoire)','Première STL (sciences et technologies de laboratoire)',438),(467,'12819567927900','Première ST2S (sciences et technologies de la santé et du social)','Première ST2S (sciences et technologies de la santé et du social)',438),(468,'12819570058300','Première STAV (sciences et technologies de l\'agronomie et du vivant)','Première STAV (sciences et technologies de l\'agronomie et du vivant)',438),(469,'12819572164300','Première TMD (technique de la musique et de la danse)','Première TMD (technique de la musique et de la danse)',438),(470,'12819574177200','Première STHR (sciences et technologies de l\'hôtellerie et de la restauration)','Première STHR (sciences et technologies de l\'hôtellerie et de la restauration)',438),(471,'12819582055800','Terminal STMG (Sciences et technologies du management et de la gestion)','Terminal STMG (Sciences et technologies du management et de la gestion)',438),(472,'12819584979600','Terminal STD2A (Sciences et technologies du design et des arts appliqués)','Terminal STD2A (Sciences et technologies du design et des arts appliqués)',438),(473,'12819587394800','Terminal STI2D (sciences et technologies de l\'industrie et du développement durable)','Terminal STI2D (sciences et technologies de l\'industrie et du développement durable)',438),(474,'12819591195500','Terminal STL (sciences et technologies de laboratoire)','Terminal STL (sciences et technologies de laboratoire)',438),(475,'12819593292300','Terminal ST2S (sciences et technologies de la santé et du social)','Terminal ST2S (sciences et technologies de la santé et du social)',438),(476,'12819595485400','Terminal STAV (sciences et technologies de l\'agronomie et du vivant)','Terminal STAV (sciences et technologies de l\'agronomie et du vivant)',438),(477,'12819597733200','Terminal TMD (technique de la musique et de la danse)','Terminal TMD (technique de la musique et de la danse)',438),(478,'12819600187300','Terminal STHR (sciences et technologies de l\'hôtellerie et de la restauration)','Terminal STHR (sciences et technologies de l\'hôtellerie et de la restauration)',438),(479,'12819602244800','Seconde professionnelle CAP','Seconde professionnelle CAP',438),(480,'12819604134200','Première Professionnelle CAP','Première Professionnelle CAP',438),(481,'12819605932900','Seconde professionnelle BEP','Seconde professionnelle BEP',438),(482,'12819608113000','Première Professionnelle BEP','Première Professionnelle BEP',438),(483,'12819610444900','Seconde CAP','Seconde CAP',438),(484,'12819612705700','Première CAP','Première CAP',438),(485,'12819615003700','Autres établissements du secondaire','Autres établissements du secondaire',438),(486,'12819617568700','Licence 1 (Bac +1)','Licence 1 (Bac +1)',439),(487,'12819619963600','Licence 2 (Bac+2)','Licence 2 (Bac+2)',439),(488,'12819622307900','Licence 3 (Bac+3)','Licence 3 (Bac+3)',439),(489,'12819624246600','Master 1 (Bac+4)','Master 1 (Bac+4)',439),(490,'12819626386700','Master 2 (Bac+5)','Master 2 (Bac+5)',439),(491,'12819628493700','Bac +6 (à préciser manuellement)','Bac +6 (à préciser manuellement)',439),(492,'12819630907400','Bac +7 (à préciser manuellement)','Bac +7 (à préciser manuellement)',439),(493,'12819633342600','Doctorat 1 (Bac +6)','Doctorat 1 (Bac +6)',439),(494,'12819635913400','Doctorat 2 (Bac +7)','Doctorat 2 (Bac +7)',439),(495,'12819638937600','Doctorat 3 (Bac +8)','Doctorat 3 (Bac +8)',439),(496,'12819641352700','Autres : Bac + 1 à Bac + 11','Autres : Bac + 1 à Bac + 11',439),(497,'12819643367100','Première année IUT (Bac +1)','Première année IUT (Bac +1)',439),(498,'12819645310400','Deuxième année IUT (Bac +2)','Deuxième année IUT (Bac +2)',439),(499,'12819647303100','Autres : Bac + 1 à Bac +2','Autres : Bac + 1 à Bac +2',439),(500,'12819649931300','Classes préparatoires : Bac +1 à Bac +2','Classes préparatoires : Bac +1 à Bac +2',439),(501,'12819652002700','Première année (Bac +1)','Première année (Bac +1)',439),(502,'12819654343300','Deuxième année (Bac +2)','Deuxième année (Bac +2)',439),(503,'12819656444300','Licence : Bac +3','Licence : Bac +3',439),(504,'12819658593800','Bachelor (Bac+4)','Bachelor (Bac+4)',439),(505,'12819660453600','Master 1 (Bac+4)','Master 1 (Bac+4)',439),(506,'12819662609600','Master 2 (Bac+5)','Master 2 (Bac+5)',439),(507,'12819664986000','Autres : Bac + 1 à Bac +7','Autres : Bac + 1 à Bac +7',439),(508,'12819666826500','Classes préparatoires : Bac +1 à Bac +2','Classes préparatoires : Bac +1 à Bac +2',439),(509,'12819668935500','Première année (Bac +1)','Première année (Bac +1)',439),(510,'12819671332400','Deuxième année (Bac +2)','Deuxième année (Bac +2)',439),(511,'12819673780100','Licence : Bac +3','Licence : Bac +3',439),(512,'12819676435600','Bachelor (Bac+4)','Bachelor (Bac+4)',439),(513,'12819679059300','Master 1 (Bac+4)','Master 1 (Bac+4)',439),(514,'12819681224400','Master 2 (Bac+5)','Master 2 (Bac+5)',439),(515,'12819684052900','Bac +1 à Bac+11 A préciser manuellement','Bac +1 à Bac+11 A préciser manuellement',439),(516,'12819689542000','Première année BTS (Bac +1)','Première année BTS (Bac +1)',439),(517,'12819692793000','Deuxième année BTS (Bac +2)','Deuxième année BTS (Bac +2)',439),(518,'12819695542400','Autres : Bac +1 à Bac+2','Autres : Bac +1 à Bac+2',439);
/*!40000 ALTER TABLE `frequented_class` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gender`
--

DROP TABLE IF EXISTS `gender`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gender` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `label` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_pmsrnxn4kayxewyfw2vp1rht4` (`id_server`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gender`
--

LOCK TABLES `gender` WRITE;
/*!40000 ALTER TABLE `gender` DISABLE KEYS */;
INSERT INTO `gender` VALUES (3,'11508831360000','Femme'),(4,'11508832442600','Homme');
/*!40000 ALTER TABLE `gender` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hibernate_sequence`
--

DROP TABLE IF EXISTS `hibernate_sequence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hibernate_sequence`
--

LOCK TABLES `hibernate_sequence` WRITE;
/*!40000 ALTER TABLE `hibernate_sequence` DISABLE KEYS */;
INSERT INTO `hibernate_sequence` VALUES (630),(630),(630),(630),(630),(630),(630),(630),(630),(630),(630),(630),(630),(630),(630),(630),(630),(630),(630),(630),(630),(630),(630);
/*!40000 ALTER TABLE `hibernate_sequence` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `higher_education`
--

DROP TABLE IF EXISTS `higher_education`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `higher_education` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `active` bit(1) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `deleted` bit(1) DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  `revision` bigint(20) DEFAULT NULL,
  `city` varchar(50) NOT NULL,
  `current_position` bit(1) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `establishment_name` varchar(50) NOT NULL,
  `account` int(11) DEFAULT NULL,
  `establishment_type` int(11) DEFAULT NULL,
  `prepared_diploma` int(11) DEFAULT NULL,
  `specialty` int(11) DEFAULT NULL,
  `study_level` int(11) DEFAULT NULL,
  `targeted_diploma` int(11) DEFAULT NULL,
  `teaching_area` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_h3tahd3n1q4kp7v3xwxxueoud` (`id_server`),
  KEY `FKsl3mgdgskuelgn0eh0xtguvs2` (`study_level`),
  KEY `FKa908thtmqa8crsydwj32f1jjl` (`targeted_diploma`),
  KEY `FK8mst42dw0j0ejjp0qh831ty29` (`teaching_area`),
  KEY `FK_1owtti119f95r1vv9oc7xnbml` (`establishment_type`),
  KEY `FK_ku013iked1p3yhnp1xgtdu5e8` (`prepared_diploma`),
  KEY `FK_9s32h9j11ilicwo4vpyrkedsg` (`specialty`),
  KEY `FK_th7r3t3j275wb1g1mmxkko829` (`account`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `higher_education`
--

LOCK TABLES `higher_education` WRITE;
/*!40000 ALTER TABLE `higher_education` DISABLE KEYS */;
/*!40000 ALTER TABLE `higher_education` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `infos_media_type`
--

DROP TABLE IF EXISTS `infos_media_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `infos_media_type` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `color` varchar(50) NOT NULL,
  `position` tinyint(4) NOT NULL,
  `url` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_666irr8e1spjsdlf4en8q0i2m` (`id_server`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `infos_media_type`
--

LOCK TABLES `infos_media_type` WRITE;
/*!40000 ALTER TABLE `infos_media_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `infos_media_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `level`
--

DROP TABLE IF EXISTS `level`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `level` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `description` varchar(50) DEFAULT NULL,
  `grade` int(11) NOT NULL,
  `icon` varchar(50) NOT NULL,
  `label` varchar(50) NOT NULL,
  `mastered` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_avbprobsmujuclg54t1crl3ip` (`id_server`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `level`
--

LOCK TABLES `level` WRITE;
/*!40000 ALTER TABLE `level` DISABLE KEYS */;
INSERT INTO `level` VALUES (401,'12190012024700','Très bien',2,'fa fa-check','Très bien',''),(402,'12190013111100','Bien',1,'fa fa-check','Bien',''),(403,'12190014062800','A approfondir',2,'fa fa-arrow-up','A approfondir','\0'),(404,'12190014923300','Notions de base',1,'fa fa-arrow-up','Notions de base','\0');
/*!40000 ALTER TABLE `level` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `link`
--

DROP TABLE IF EXISTS `link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `link` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `active` bit(1) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `deleted` bit(1) DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  `revision` bigint(20) DEFAULT NULL,
  `link_type` varchar(255) DEFAULT NULL,
  `url` varchar(255) NOT NULL,
  `account` int(11) DEFAULT NULL,
  `link` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_muo7t66qy7bjxt2mpgk9u8doq` (`id_server`),
  KEY `FKgcicrmmae2kfvim5u85a5n8b` (`account`),
  KEY `FKbe6t25ltf7ig4hla2jqo437ss` (`link`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `link`
--

LOCK TABLES `link` WRITE;
/*!40000 ALTER TABLE `link` DISABLE KEYS */;
INSERT INTO `link` VALUES (576,'65071765031700','','2020-09-28 11:58:41','\0',NULL,0,NULL,'https://www.google.com/search?q=angular+pipe+file+size+to+mb&oq=an&aqs=chrome.0.69i59l2j69i57j69i59j69i60l4.1144j0j7&sourceid=chrome&ie=UTF-8',1,577);
/*!40000 ALTER TABLE `link` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `media_subtype`
--

DROP TABLE IF EXISTS `media_subtype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `media_subtype` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `label` varchar(255) NOT NULL,
  `media_type` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_fpk6ncx36twffmkqpvpie81o` (`id_server`),
  KEY `FKgjun9jfynng9l72q9v8kkj6y9` (`media_type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `media_subtype`
--

LOCK TABLES `media_subtype` WRITE;
/*!40000 ALTER TABLE `media_subtype` DISABLE KEYS */;
INSERT INTO `media_subtype` VALUES (604,'30901732901700','Exercices et corrections',599),(605,'30901742129200','Sujets d\'examen et corrections',599),(606,'30901744286500','Fiches et astuces de revision',599),(607,'30901746306800','Mémoires, Exposés, présentation',599),(608,'30901748327700','Diplômes et certifications',599),(609,'30901750225700','Réalisations professionnelles',599),(610,'30901752252100','Travaux de recherche et livres',599),(611,'30901754571400','Autres',599),(612,'30901757168200','Vidéo pédagogique / éducative',600),(613,'30901759285000','Conférence',600),(614,'30901761595800','Actualités et débats',600),(615,'30901763526300','Autres',600),(616,'30901765701600','Audio pédagogique / éducatif',601),(617,'30901767644100','Conférence',601),(618,'30901769697600','Actualités et débats',601),(619,'30901772143300','Autres',601),(620,'30901774722200','Article de presse',602),(621,'30901777089600','Revue scientifique / pédagogique',602),(622,'30901779268100','Site web',602),(623,'30901781467600','Blog',602),(624,'30901783623700','Questionnaires et jeux éducatifs',602),(625,'30901787037900','Livres',602),(626,'30901789681900','Autres',602),(627,'30901792022600','Photos des publications',603),(628,'30901794100800','Photos personnelles',603),(629,'30901796234800','Autres',603);
/*!40000 ALTER TABLE `media_subtype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `media_type`
--

DROP TABLE IF EXISTS `media_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `media_type` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `label` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_at21ep413yoo6ja3927db4x94` (`id_server`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `media_type`
--

LOCK TABLES `media_type` WRITE;
/*!40000 ALTER TABLE `media_type` DISABLE KEYS */;
INSERT INTO `media_type` VALUES (598,'30836624139400','AVATAR'),(599,'30836625115500','DOCUMENT'),(600,'30836626055600','VIDEO'),(601,'30836626936100','AUDIO'),(602,'30836628127000','LINK'),(603,'30836629124600','PICTURE');
/*!40000 ALTER TABLE `media_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `multimedia`
--

DROP TABLE IF EXISTS `multimedia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `multimedia` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `active` bit(1) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `deleted` bit(1) DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  `revision` bigint(20) DEFAULT NULL,
  `checksum` varchar(255) DEFAULT NULL,
  `directory` varchar(255) DEFAULT NULL,
  `extension` varchar(255) DEFAULT NULL,
  `mime_type` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `account` int(11) DEFAULT NULL,
  `multimedia` int(11) DEFAULT NULL,
  `media_subtype` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_fjsq1euaq26h44sxodq1duswj` (`id_server`),
  KEY `FKjp62ph2bdbrb6kpcxswhvhy85` (`account`),
  KEY `FKt9gha83it3k5u801vlsm762pf` (`media_subtype`),
  KEY `FK51vq8lev5q0usgnbgb3abbb9p` (`multimedia`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `multimedia`
--

LOCK TABLES `multimedia` WRITE;
/*!40000 ALTER TABLE `multimedia` DISABLE KEYS */;
INSERT INTO `multimedia` VALUES (2,'11404313335800','','2020-09-27 21:04:25','\0',NULL,0,'3a23149a','11308326636800','jpg','image/jpeg','mengue.jpg','AVATAR',1,NULL,NULL),(570,'62562111108800','','2020-09-28 11:17:01','\0',NULL,0,'dd755d77','11308326636800','jpg','image/jpeg','file_example_JPG_1MB.jpg','PICTURE',1,571,NULL),(572,'62941340136700','','2020-09-28 11:23:18','\0',NULL,0,'813886d2','11308326636800','mp3','audio/mpeg','file_example_MP3_2MG.mp3','AUDIO',1,573,NULL),(574,'63014018733100','','2020-09-28 11:25:02','\0',NULL,0,'9bbd7cab','11308326636800','mp4','video/mp4','file_example_MP4_1280_10MG.mp4','VIDEO',1,575,NULL),(578,'65575217778800','','2020-09-28 12:07:15','\0',NULL,0,'e5b86185','11308326636800','pdf','application/pdf','file-example_PDF_1MB.pdf','DOCUMENT',1,579,NULL),(586,'67381142006600','','2020-09-28 12:37:13','\0',NULL,0,'3722b84b','11308326636800','jpg','image/jpeg','books-768426_1920.jpg','PICTURE',1,587,NULL),(593,'67830247309800','','2020-09-28 12:44:41','\0',NULL,0,'b720cd51','67665637080700','jpg','image/jpeg','chimananda.jpg','AVATAR',591,NULL,NULL),(594,'68039010795900','','2020-09-28 12:48:09','\0',NULL,0,'b59bea21','67711260684600','jpg','image/jpeg','winnie-mandela.jpg','AVATAR',592,NULL,NULL),(595,'68191022171699','','2020-09-28 12:50:41','\0',NULL,0,'d10dc523','67606161628899','jpg','image/jpeg','nkrumah.jpg','AVATAR',590,NULL,NULL),(596,'68415711596099','','2020-09-28 12:54:26','\0',NULL,0,'c60f06cc','67542648596000','jpg','image/jpeg','ruben-um-nyobe.jpg','AVATAR',589,NULL,NULL),(597,'68441930469200','','2020-09-28 12:54:52','\0',NULL,0,'bbfcd0b8','67480294717000','jpg','image/jpeg','patrice_lumumba.jpg','AVATAR',588,NULL,NULL);
/*!40000 ALTER TABLE `multimedia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `post`
--

DROP TABLE IF EXISTS `post`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `post` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `active` bit(1) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `deleted` bit(1) DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  `revision` bigint(20) DEFAULT NULL,
  `description` varchar(2000) DEFAULT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  `title` varchar(50) NOT NULL,
  `account` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_hj56xjem2k1cs8nwm1nsh2xjx` (`id_server`),
  KEY `FKhldpsq033xe7e406jkt2ovuno` (`account`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `post`
--

LOCK TABLES `post` WRITE;
/*!40000 ALTER TABLE `post` DISABLE KEYS */;
INSERT INTO `post` VALUES (571,'62571885784100','','2020-09-28 11:17:01','\0',NULL,0,'','','Laptop de bureau',1),(573,'62949459963500','','2020-09-28 11:23:18','\0',NULL,0,'','It is a long established fact that a reader will ','audio du cours de maths',1),(575,'63053622706100','','2020-09-28 11:25:02','\0',NULL,0,'maths fonctions','There are many variations of passages of Lorem ','video du cours',1),(577,'65071852667800','','2020-09-28 11:58:41','',NULL,0,NULL,NULL,'angular pipe',1),(579,'65585834052499','','2020-09-28 12:07:15','\0',NULL,0,'','','document de philo',1),(584,'67175883890300','','2020-09-28 12:33:45','\0',NULL,0,'Where does it come from?\nContrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.\n\nThe standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from \"de Finibus Bonorum et Malorum\" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.',NULL,'Where does it come from?\nContrary to popular belie',1),(585,'67296415370100','','2020-09-28 12:35:45','',NULL,0,'Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...',NULL,'Neque porro quisquam est qui dolorem ipsum quia do',1),(587,'67384577757199','','2020-09-28 12:37:13','\0',NULL,0,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam','','Lorem ipsum dolor sit amet, consectetur adipiscing',1);
/*!40000 ALTER TABLE `post` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `professional`
--

DROP TABLE IF EXISTS `professional`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `professional` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `active` bit(1) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `deleted` bit(1) DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  `revision` bigint(20) DEFAULT NULL,
  `city` varchar(50) NOT NULL,
  `current_position` bit(1) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `establishment_name` varchar(50) NOT NULL,
  `account` int(11) DEFAULT NULL,
  `end_date` datetime NOT NULL,
  `job_title` varchar(50) NOT NULL,
  `start_date` datetime NOT NULL,
  `activity_area` int(11) DEFAULT NULL,
  `activity_sector` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_jhtn3j550trbpd1t9gvk99ni4` (`id_server`),
  KEY `FKi5t5gwrrxhgyjedhvxcekbyxc` (`activity_area`),
  KEY `FKqn9wbhf8i3wxe4tfu4x2lo8pm` (`activity_sector`),
  KEY `FK_otn62bgvcnopnav5abccfgiu3` (`account`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `professional`
--

LOCK TABLES `professional` WRITE;
/*!40000 ALTER TABLE `professional` DISABLE KEYS */;
INSERT INTO `professional` VALUES (436,'12551106961700','','2020-09-27 21:23:30','\0',NULL,0,'Yaounde','',NULL,'YAMO GROUP',1,'2020-09-27 21:23:30','Comptable','2019-02-14 01:00:00',434,409),(437,'12614944611400','','2020-09-27 21:24:34','\0',NULL,0,'Yaounde','\0','The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from \"de Finibus Bonorum et Malorum\" by Cicero are also reproduced in their exact original form, accompanied by English','YAMO GROUP ',1,'2020-09-26 02:00:00','Developpeuse','2020-09-01 02:00:00',435,422);
/*!40000 ALTER TABLE `professional` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `professional_status`
--

DROP TABLE IF EXISTS `professional_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `professional_status` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `label` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_fc9uvppyqis203m9mqheyyvc6` (`id_server`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `professional_status`
--

LOCK TABLES `professional_status` WRITE;
/*!40000 ALTER TABLE `professional_status` DISABLE KEYS */;
INSERT INTO `professional_status` VALUES (5,'11559034729500','Etudiant'),(6,'11559035748200','Enseignant'),(7,'11559036679300','Professionnel');
/*!40000 ALTER TABLE `professional_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `secondary_education`
--

DROP TABLE IF EXISTS `secondary_education`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `secondary_education` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `active` bit(1) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `deleted` bit(1) DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  `revision` bigint(20) DEFAULT NULL,
  `city` varchar(50) NOT NULL,
  `current_position` bit(1) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `establishment_name` varchar(50) NOT NULL,
  `account` int(11) DEFAULT NULL,
  `establishment_type` int(11) DEFAULT NULL,
  `prepared_diploma` int(11) DEFAULT NULL,
  `specialty` int(11) DEFAULT NULL,
  `frequented_class` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_nhxvw6ieoh23429mwaq1qkmsj` (`id_server`),
  KEY `FK7lrguancul9nb4ef9cvkaye8i` (`frequented_class`),
  KEY `FK_o2ypntl0oqre1t4gkekwfvtse` (`establishment_type`),
  KEY `FK_7tuva3teovatjn6v1nrkjaxq8` (`prepared_diploma`),
  KEY `FK_3hwnb8bkop95ax5sbo14ydcw8` (`specialty`),
  KEY `FK_h91sw2cbvc4og7hptcd4fw3ua` (`account`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `secondary_education`
--

LOCK TABLES `secondary_education` WRITE;
/*!40000 ALTER TABLE `secondary_education` DISABLE KEYS */;
INSERT INTO `secondary_education` VALUES (558,'22348673299800','','2020-09-28 00:06:47','\0',NULL,0,'Yaounde','\0',NULL,'Mballa 2',1,440,519,553,468),(559,'22491744393800','','2020-09-28 00:09:10','\0',NULL,0,'Mbalmayo','\0','There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly','Lycee classique',1,441,529,554,463);
/*!40000 ALTER TABLE `secondary_education` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `skill`
--

DROP TABLE IF EXISTS `skill`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `skill` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `active` bit(1) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `deleted` bit(1) DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  `revision` bigint(20) DEFAULT NULL,
  `label` varchar(255) NOT NULL,
  `skill_mastered` bit(1) DEFAULT NULL,
  `account` int(11) DEFAULT NULL,
  `discipline` int(11) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `skill_type` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_mgwf4f5epvhru88djnobni1vl` (`id_server`),
  KEY `FK25xg7gyx32jtdiuek1fpp4xx9` (`account`),
  KEY `FKm9ado79oa3y3i2snum3hj6ryt` (`discipline`),
  KEY `FKirhnfwdwcegn8fstgix349moy` (`level`),
  KEY `FKey8ly3wbl7ms606i6i48bwjk1` (`skill_type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `skill`
--

LOCK TABLES `skill` WRITE;
/*!40000 ALTER TABLE `skill` DISABLE KEYS */;
INSERT INTO `skill` VALUES (405,'12221863937100','','2020-09-27 21:18:00','',NULL,0,'Droit des marchés','',1,260,402,257),(406,'12248342065700','','2020-09-27 21:18:27','',NULL,0,'Litterature slaves','\0',1,271,403,258),(407,'98411320695700','','2020-09-27 21:19:11','\0',NULL,0,'Droit des marchés et des particuliers','',1,268,401,257),(408,'98164086093000','','2020-09-27 21:19:28','\0',NULL,0,'Langues latines','\0',1,272,403,257);
/*!40000 ALTER TABLE `skill` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `skill_type`
--

DROP TABLE IF EXISTS `skill_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `skill_type` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `label` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_4le85m83df2y1yvuwopqwb317` (`id_server`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `skill_type`
--

LOCK TABLES `skill_type` WRITE;
/*!40000 ALTER TABLE `skill_type` DISABLE KEYS */;
INSERT INTO `skill_type` VALUES (257,'12079496841400','Skill type 1','Skill type 1'),(258,'12079497764600','Skill type 2','Skill type 2');
/*!40000 ALTER TABLE `skill_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `specialty`
--

DROP TABLE IF EXISTS `specialty`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `specialty` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `label` varchar(255) NOT NULL,
  `education` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_hvs3s263qsrdp83gsys83xg8t` (`id_server`),
  KEY `FKgvc0ovbufuhfn2t26by10cejb` (`education`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `specialty`
--

LOCK TABLES `specialty` WRITE;
/*!40000 ALTER TABLE `specialty` DISABLE KEYS */;
INSERT INTO `specialty` VALUES (553,'14541667659800','Spécialité 1','Spécialité 1',438),(554,'14541675832200','Spécialité 2','Spécialité 2',438),(555,'14541680065700','Spécialité 3','Spécialité 3',439),(556,'14541682473000','Spécialité 4','Spécialité 4',439);
/*!40000 ALTER TABLE `specialty` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `study_level`
--

DROP TABLE IF EXISTS `study_level`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `study_level` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `label` varchar(255) NOT NULL,
  `education` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_jmdv843pnvonbtfhom3i794u1` (`id_server`),
  KEY `FK19p7f1vp5j6un4jr0u5svnvwp` (`education`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `study_level`
--

LOCK TABLES `study_level` WRITE;
/*!40000 ALTER TABLE `study_level` DISABLE KEYS */;
INSERT INTO `study_level` VALUES (564,'23966803891100','Niveau d\'études 1','Niveau d\'études 1',438),(565,'23966812132500','Niveau d\'études 2','Niveau d\'études 2',438),(566,'23966814364400','Niveau d\'études 3','Niveau d\'études 3',439),(567,'23966816967100','Niveau d\'études 4','Niveau d\'études 4',439);
/*!40000 ALTER TABLE `study_level` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teaching_area`
--

DROP TABLE IF EXISTS `teaching_area`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teaching_area` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `label` varchar(255) NOT NULL,
  `education` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_ox8ks35vdkc9nd83in6u23a1i` (`id_server`),
  KEY `FKr5km3wq5qmfmu05ttam0ojqu3` (`education`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teaching_area`
--

LOCK TABLES `teaching_area` WRITE;
/*!40000 ALTER TABLE `teaching_area` DISABLE KEYS */;
INSERT INTO `teaching_area` VALUES (560,'23908525088100','Domaine d\'enseignement 1','Domaine d\'enseignement 1',438),(561,'23908533828500','Domaine d\'enseignement 2','Domaine d\'enseignement 2',438),(562,'23908536705600','Domaine d\'enseignement 3','Domaine d\'enseignement 3',439),(563,'23908538888300','Domaine d\'enseignement 4','Domaine d\'enseignement 4',439);
/*!40000 ALTER TABLE `teaching_area` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'skillsmatesdb'
--

--
-- Dumping routines for database 'skillsmatesdb'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-10-07 13:05:12
