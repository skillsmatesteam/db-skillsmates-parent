-- MariaDB dump 10.17  Distrib 10.4.13-MariaDB, for Win64 (AMD64)
--
-- Host: 127.0.0.1    Database: skillsmatesdb
-- ------------------------------------------------------
-- Server version	10.4.13-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `frequented_class`
--

DROP TABLE IF EXISTS `frequented_class`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `frequented_class` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `label` varchar(255) NOT NULL,
  `education` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_reayxllx7vkm67nddnrjgyaxd` (`id_server`),
  KEY `FKa3m8swh05iskjbnxe1odk59ee` (`education`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `frequented_class`
--

LOCK TABLES `frequented_class` WRITE;
/*!40000 ALTER TABLE `frequented_class` DISABLE KEYS */;
INSERT INTO `frequented_class` VALUES (453,'12819533121100','Sixième (6ème)','Sixième (6ème)',438),(454,'12819540747300','Cinquième (5ème)','Cinquième (5ème)',438),(455,'12819542734300','Quatrième (4ème)','Quatrième (4ème)',438),(456,'12819544734700','Troisième (3ème)','Troisième (3ème)',438),(457,'12819546770300','Première S (Scientifique)','Première S (Scientifique)',438),(458,'12819549179600','Première ES (Économique et social)','Première ES (Économique et social)',438),(459,'12819551291200','Première L (Littéraire)','Première L (Littéraire)',438),(460,'12819553599500','Terminal S (Scientifique)','Terminal S (Scientifique)',438),(461,'12819555588200','Terminal ES (Économique et social)','Terminal ES (Économique et social)',438),(462,'12819557669100','Terminal L (Littéraire)','Terminal L (Littéraire)',438),(463,'12819559694500','Première STMG (Sciences et technologies du management et de la gestion)','Première STMG (Sciences et technologies du management et de la gestion)',438),(464,'12819561729900','Première STD2A (Sciences et technologies du design et des arts appliqués)','Première STD2A (Sciences et technologies du design et des arts appliqués)',438),(465,'12819563874500','Première STI2D (sciences et technologies de l\'industrie et du développement durable)','Première STI2D (sciences et technologies de l\'industrie et du développement durable)',438),(466,'12819565981800','Première STL (sciences et technologies de laboratoire)','Première STL (sciences et technologies de laboratoire)',438),(467,'12819567927900','Première ST2S (sciences et technologies de la santé et du social)','Première ST2S (sciences et technologies de la santé et du social)',438),(468,'12819570058300','Première STAV (sciences et technologies de l\'agronomie et du vivant)','Première STAV (sciences et technologies de l\'agronomie et du vivant)',438),(469,'12819572164300','Première TMD (technique de la musique et de la danse)','Première TMD (technique de la musique et de la danse)',438),(470,'12819574177200','Première STHR (sciences et technologies de l\'hôtellerie et de la restauration)','Première STHR (sciences et technologies de l\'hôtellerie et de la restauration)',438),(471,'12819582055800','Terminal STMG (Sciences et technologies du management et de la gestion)','Terminal STMG (Sciences et technologies du management et de la gestion)',438),(472,'12819584979600','Terminal STD2A (Sciences et technologies du design et des arts appliqués)','Terminal STD2A (Sciences et technologies du design et des arts appliqués)',438),(473,'12819587394800','Terminal STI2D (sciences et technologies de l\'industrie et du développement durable)','Terminal STI2D (sciences et technologies de l\'industrie et du développement durable)',438),(474,'12819591195500','Terminal STL (sciences et technologies de laboratoire)','Terminal STL (sciences et technologies de laboratoire)',438),(475,'12819593292300','Terminal ST2S (sciences et technologies de la santé et du social)','Terminal ST2S (sciences et technologies de la santé et du social)',438),(476,'12819595485400','Terminal STAV (sciences et technologies de l\'agronomie et du vivant)','Terminal STAV (sciences et technologies de l\'agronomie et du vivant)',438),(477,'12819597733200','Terminal TMD (technique de la musique et de la danse)','Terminal TMD (technique de la musique et de la danse)',438),(478,'12819600187300','Terminal STHR (sciences et technologies de l\'hôtellerie et de la restauration)','Terminal STHR (sciences et technologies de l\'hôtellerie et de la restauration)',438),(479,'12819602244800','Seconde professionnelle CAP','Seconde professionnelle CAP',438),(480,'12819604134200','Première Professionnelle CAP','Première Professionnelle CAP',438),(481,'12819605932900','Seconde professionnelle BEP','Seconde professionnelle BEP',438),(482,'12819608113000','Première Professionnelle BEP','Première Professionnelle BEP',438),(483,'12819610444900','Seconde CAP','Seconde CAP',438),(484,'12819612705700','Première CAP','Première CAP',438),(485,'12819615003700','Autres établissements du secondaire','Autres établissements du secondaire',438),(486,'12819617568700','Licence 1 (Bac +1)','Licence 1 (Bac +1)',439),(487,'12819619963600','Licence 2 (Bac+2)','Licence 2 (Bac+2)',439),(488,'12819622307900','Licence 3 (Bac+3)','Licence 3 (Bac+3)',439),(489,'12819624246600','Master 1 (Bac+4)','Master 1 (Bac+4)',439),(490,'12819626386700','Master 2 (Bac+5)','Master 2 (Bac+5)',439),(491,'12819628493700','Bac +6 (à préciser manuellement)','Bac +6 (à préciser manuellement)',439),(492,'12819630907400','Bac +7 (à préciser manuellement)','Bac +7 (à préciser manuellement)',439),(493,'12819633342600','Doctorat 1 (Bac +6)','Doctorat 1 (Bac +6)',439),(494,'12819635913400','Doctorat 2 (Bac +7)','Doctorat 2 (Bac +7)',439),(495,'12819638937600','Doctorat 3 (Bac +8)','Doctorat 3 (Bac +8)',439),(496,'12819641352700','Autres : Bac + 1 à Bac + 11','Autres : Bac + 1 à Bac + 11',439),(497,'12819643367100','Première année IUT (Bac +1)','Première année IUT (Bac +1)',439),(498,'12819645310400','Deuxième année IUT (Bac +2)','Deuxième année IUT (Bac +2)',439),(499,'12819647303100','Autres : Bac + 1 à Bac +2','Autres : Bac + 1 à Bac +2',439),(500,'12819649931300','Classes préparatoires : Bac +1 à Bac +2','Classes préparatoires : Bac +1 à Bac +2',439),(501,'12819652002700','Première année (Bac +1)','Première année (Bac +1)',439),(502,'12819654343300','Deuxième année (Bac +2)','Deuxième année (Bac +2)',439),(503,'12819656444300','Licence : Bac +3','Licence : Bac +3',439),(504,'12819658593800','Bachelor (Bac+4)','Bachelor (Bac+4)',439),(505,'12819660453600','Master 1 (Bac+4)','Master 1 (Bac+4)',439),(506,'12819662609600','Master 2 (Bac+5)','Master 2 (Bac+5)',439),(507,'12819664986000','Autres : Bac + 1 à Bac +7','Autres : Bac + 1 à Bac +7',439),(508,'12819666826500','Classes préparatoires : Bac +1 à Bac +2','Classes préparatoires : Bac +1 à Bac +2',439),(509,'12819668935500','Première année (Bac +1)','Première année (Bac +1)',439),(510,'12819671332400','Deuxième année (Bac +2)','Deuxième année (Bac +2)',439),(511,'12819673780100','Licence : Bac +3','Licence : Bac +3',439),(512,'12819676435600','Bachelor (Bac+4)','Bachelor (Bac+4)',439),(513,'12819679059300','Master 1 (Bac+4)','Master 1 (Bac+4)',439),(514,'12819681224400','Master 2 (Bac+5)','Master 2 (Bac+5)',439),(515,'12819684052900','Bac +1 à Bac+11 A préciser manuellement','Bac +1 à Bac+11 A préciser manuellement',439),(516,'12819689542000','Première année BTS (Bac +1)','Première année BTS (Bac +1)',439),(517,'12819692793000','Deuxième année BTS (Bac +2)','Deuxième année BTS (Bac +2)',439),(518,'12819695542400','Autres : Bac +1 à Bac+2','Autres : Bac +1 à Bac+2',439);
/*!40000 ALTER TABLE `frequented_class` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-10-07 13:04:06
