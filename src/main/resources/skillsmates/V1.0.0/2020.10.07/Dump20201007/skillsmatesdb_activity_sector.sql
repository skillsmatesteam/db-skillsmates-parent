-- MariaDB dump 10.17  Distrib 10.4.13-MariaDB, for Win64 (AMD64)
--
-- Host: 127.0.0.1    Database: skillsmatesdb
-- ------------------------------------------------------
-- Server version	10.4.13-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `activity_sector`
--

DROP TABLE IF EXISTS `activity_sector`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `activity_sector` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `label` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_g3clcptstkdfkbdsa9s5pxatc` (`id_server`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activity_sector`
--

LOCK TABLES `activity_sector` WRITE;
/*!40000 ALTER TABLE `activity_sector` DISABLE KEYS */;
INSERT INTO `activity_sector` VALUES (409,'12373645608500','Administration, Public et Association','Administration, Public et Association'),(410,'12373646634500','Agriculture et Agroalimentaire','Agriculture et Agroalimentaire'),(411,'12373647658100','Artisanat','Artisanat'),(412,'12373648552900','Banque, Finance et Assurance','Banque, Finance et Assurance'),(413,'12373649724700','Automobile, aéronautiques et autres matériel de transport','Automobile, aéronautiques et autres matériel de transport'),(414,'12373650917700','BTP et Construction','BTP et Construction'),(415,'12373651712800','Commerce et Distribution','Commerce et Distribution'),(416,'12373652669300','Conseil et Gestion des entreprises','Conseil et Gestion des entreprises'),(417,'12373653478200','Droit et Juridique','Droit et Juridique'),(418,'12373654360500','Comptabilité et Audit','Comptabilité et Audit'),(419,'12373655417700','Eau, Energie et Minerais','Eau, Energie et Minerais'),(420,'12373656199300','Environnement et développement durable','Environnement et développement durable'),(421,'12373657087700','Formation académique, professionnel et divers','Formation académique, professionnel et divers'),(422,'12373657949900','Informatique et Technologie de l\'information','Informatique et Technologie de l\'information'),(423,'12373658755800','Industrie chimique, textile, Manufacturière et autres','Industrie chimique, textile, Manufacturière et autres'),(424,'12373659840700','Immobilier','Immobilier'),(425,'12373660694100','Marketing, communication et Médias','Marketing, communication et Médias'),(426,'12373661478400','Ressources Humaines','Ressources Humaines'),(427,'12373662461300','Restauration, Hôtellerie, et Loisirs','Restauration, Hôtellerie, et Loisirs'),(428,'12373663292200','Services divers aux entreprises','Services divers aux entreprises'),(429,'12373664095300','Santé et service à la personne et sociale','Santé et service à la personne et sociale'),(430,'12373665925100','Telecom','Telecom'),(431,'12373666892100','Transports et Logistique','Transports et Logistique'),(432,'12373667805100','Tourisme','Tourisme'),(433,'12373668822700','Autres (à préciser manuellement)','Autres (à préciser manuellement)');
/*!40000 ALTER TABLE `activity_sector` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-10-07 13:04:06
