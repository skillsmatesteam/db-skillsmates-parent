-- MariaDB dump 10.17  Distrib 10.4.13-MariaDB, for Win64 (AMD64)
--
-- Host: 127.0.0.1    Database: skillsmatesdb
-- ------------------------------------------------------
-- Server version	10.4.13-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `account`
--

DROP TABLE IF EXISTS `account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `active` bit(1) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `deleted` bit(1) DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  `revision` bigint(20) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `birthdate` datetime DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `firstname` varchar(50) NOT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `lastname` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `phone_number` varchar(20) DEFAULT NULL,
  `biography` varchar(150) DEFAULT NULL,
  `city` varchar(30) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `follower` int(11) DEFAULT 0,
  `following` int(11) DEFAULT 0,
  `profile_picture` varchar(255) DEFAULT NULL,
  `publication` int(11) DEFAULT 0,
  `status` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_iyeg6x6r8s959nhdt76efqc6u` (`id_server`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account`
--

LOCK TABLES `account` WRITE;
/*!40000 ALTER TABLE `account` DISABLE KEYS */;
INSERT INTO `account` VALUES (2,'1048742301590000','','2020-08-10 19:05:04','\0',NULL,0,NULL,NULL,NULL,NULL,'Marthe',NULL,'MOMO','e288bbfe8811268ec0a91262267e490c4ccb6cd5311cd13f73374772688e5d57a5870ac542a4f7167577fa88f1d4da528f126fff7aaacdee55b2e2011e9fabd2','0085335220',NULL,NULL,NULL,0,0,NULL,0,NULL),(20,'604859586427500','','2020-08-21 18:29:22','\0',NULL,0,NULL,NULL,NULL,'innocent.ntafor@gmail.com','Innocent',NULL,'NTAFOR','b9e4c748061f2c1962f8fee2055bdf92f3ecf4e89cc8e19afb992a8a9121e5b035add71568f5bb6801746ab543d605b55fd8bed141b8facf0ff60a9f3d7f2f52',NULL,NULL,NULL,NULL,0,0,NULL,0,NULL);
/*!40000 ALTER TABLE `account` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-08-24 15:00:14
