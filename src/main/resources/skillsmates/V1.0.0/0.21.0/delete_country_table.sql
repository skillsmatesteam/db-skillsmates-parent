--
-- delete country table
--

ALTER TABLE skillsmatesdb.account ADD country_code varchar(5);

UPDATE skillsmatesdb.account SET country_code = "CMR" WHERE country = 48;
UPDATE skillsmatesdb.account SET country_code = "NGA" WHERE country = 169;
UPDATE skillsmatesdb.account SET country_code = "ZAF" WHERE country = 9;
UPDATE skillsmatesdb.account SET country_code = "FRA" WHERE country = 86;
UPDATE skillsmatesdb.account SET country_code = "USA" WHERE country = 80;
UPDATE skillsmatesdb.account SET country_code = "BEL" WHERE country = 31;
UPDATE skillsmatesdb.account SET country_code = "CAN" WHERE country = 49;
UPDATE skillsmatesdb.account SET country_code = "CHE" WHERE country = 226;
UPDATE skillsmatesdb.account SET country_code = "PRT" WHERE country = 192;
UPDATE skillsmatesdb.account SET country_code = "BEN" WHERE country = 33;
UPDATE skillsmatesdb.account SET country_code = "CIV" WHERE country = 65;
UPDATE skillsmatesdb.account SET country_code = "COG" WHERE country = 59;

ALTER TABLE skillsmatesdb.account DROP FOREIGN KEY FKi7rut9n9vfl3ac9cxuegckab9;

ALTER TABLE skillsmatesdb.account DROP COLUMN country;

ALTER TABLE skillsmatesdb.account CHANGE COLUMN country_code country varchar(5);

DROP TABLE skillsmatesdb.country;