--
-- delete gender table
--

ALTER TABLE skillsmatesdb.account ADD gender_enum smallint;

SET SQL_SAFE_UPDATES = 0;
UPDATE skillsmatesdb.account SET gender_enum = 0 WHERE gender = 4;
UPDATE skillsmatesdb.account SET gender_enum = 1 WHERE gender = 3;
SET SQL_SAFE_UPDATES = 1;

ALTER TABLE skillsmatesdb.account DROP FOREIGN KEY FKge5lb1w9xvo78dg80k6s3ni27;

ALTER TABLE skillsmatesdb.account DROP COLUMN gender;

ALTER TABLE skillsmatesdb.account CHANGE COLUMN gender_enum gender smallint;

DROP TABLE skillsmatesdb.gender;