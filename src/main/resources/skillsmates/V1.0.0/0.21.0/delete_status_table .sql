--
-- delete status table
--

ALTER TABLE skillsmatesdb.account ADD status_enum  smallint;

UPDATE skillsmatesdb.account SET status_enum = 0 WHERE status = 5;
UPDATE skillsmatesdb.account SET status_enum = 1 WHERE status = 6;
UPDATE skillsmatesdb.account SET status_enum = 2 WHERE status = 7;

ALTER TABLE skillsmatesdb.account DROP FOREIGN KEY FKny0dyt6qv6m9y7qlcbfdwms58;

ALTER TABLE skillsmatesdb.account DROP COLUMN status;

ALTER TABLE skillsmatesdb.account CHANGE COLUMN status_enum status smallint;

DROP TABLE skillsmatesdb.professional_status;