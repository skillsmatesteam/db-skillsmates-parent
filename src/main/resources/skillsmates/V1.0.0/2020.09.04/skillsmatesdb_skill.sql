-- MariaDB dump 10.17  Distrib 10.4.13-MariaDB, for Win64 (AMD64)
--
-- Host: 127.0.0.1    Database: skillsmatesdb
-- ------------------------------------------------------
-- Server version	10.4.13-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `skill`
--

DROP TABLE IF EXISTS `skill`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `skill` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `active` bit(1) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `deleted` bit(1) DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  `revision` bigint(20) DEFAULT NULL,
  `skill_mastered` bit(1) DEFAULT NULL,
  `skill_name` varchar(50) NOT NULL,
  `id_account` int(11) DEFAULT NULL,
  `id_discipline` int(11) DEFAULT NULL,
  `id_level` int(11) DEFAULT NULL,
  `id_skill_type` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_mgwf4f5epvhru88djnobni1vl` (`id_server`),
  KEY `FKoga9thsge11ttuupeqp1djeme` (`id_account`),
  KEY `FKsbnax661o60es8shn4lxjlphx` (`id_discipline`),
  KEY `FKdep21erjtc4h8sjxm9b5117fu` (`id_level`),
  KEY `FK8doqcpmm41ahi05wuld1l7l16` (`id_skill_type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `skill`
--

LOCK TABLES `skill` WRITE;
/*!40000 ALTER TABLE `skill` DISABLE KEYS */;
INSERT INTO `skill` VALUES (37,'5918635871200','','2020-08-27 12:32:35','\0',NULL,0,'','competence 1',21,22,26,30),(38,'5980884562900','','2020-08-27 12:33:37','\0',NULL,0,'\0','competence 2',21,23,27,31),(63,'152493382254000','','2020-09-04 11:26:53','',NULL,0,'','Comptence1',62,22,26,30),(64,'152800512857400','','2020-09-04 11:32:00','',NULL,0,'','competence 2',62,23,27,31),(65,'155572439597800','','2020-09-04 12:18:12','',NULL,0,'','comptence 3',62,24,28,32),(66,'155720628473400','','2020-09-04 12:20:40','\0',NULL,0,'','comptence 4',62,25,29,33),(67,'155885398164800','','2020-09-04 12:23:25','',NULL,0,'\0','comptence1',62,22,27,30),(68,'156900040476200','','2020-09-04 12:40:20','\0',NULL,0,'\0','comptence 5',62,24,28,33),(69,'156999104340000','','2020-09-04 12:41:59','\0',NULL,0,'\0','competence 9',62,25,27,31),(78,'176484946834700','','2020-09-04 18:06:45','\0',NULL,0,'','comptence1',77,22,27,30),(79,'176631477015900','','2020-09-04 18:09:11','\0',NULL,0,'\0','comptence 4',77,23,29,31),(80,'176725195723500','','2020-09-04 18:10:45','\0',NULL,0,'\0','comptence 8',77,24,26,33),(81,'177836007805200','','2020-09-04 18:29:16','\0',NULL,0,'\0','comptence 8',77,25,28,31);
/*!40000 ALTER TABLE `skill` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-09-04 18:31:58
