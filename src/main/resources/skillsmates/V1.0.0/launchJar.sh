#!/bin/bash
#
# Cyrille MOFFO
#
# Script d'execution des jars skillsmates
#
nohup java -jar ms-skillsmates-accounts.jar > ms-skillsmates-accounts.log 2>&1 &
nohup java -jar ms-skillsmates-communications.jar > ms-skillsmates-communications.log 2>&1 &
nohup java -jar ms-skillsmates-media.jar > ms-skillsmates-media.log 2>&1 &
nohup java -jar ms-skillsmates-notifications.jar > ms-skillsmates-notifications.log 2>&1 &
nohup java -jar ms-skillsmates-pages.jar > ms-skillsmates-pages.log 2>&1 &
nohup java -jar ms-skillsmates-settings.jar > ms-skillsmates-settings.log 2>&1 &