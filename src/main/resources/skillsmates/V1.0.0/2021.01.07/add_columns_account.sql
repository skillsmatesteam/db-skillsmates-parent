 -- add fcm_registration_id in account
ALTER TABLE skillsmatesdb.account ADD COLUMN `fcm_registration_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;

 -- add message tables
---- run: skillsmatesdb_chat_room.sql
---- run: skillsmatesdb_message.sql
---- run: skillsmatesdb_message_receivers.sql
