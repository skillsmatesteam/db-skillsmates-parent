-- MariaDB dump 10.17  Distrib 10.4.13-MariaDB, for Win64 (AMD64)
--
-- Host: 127.0.0.1    Database: skillsmatesdb
-- ------------------------------------------------------
-- Server version	10.4.13-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `account`
--

DROP TABLE IF EXISTS `account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `active` bit(1) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `deleted` bit(1) DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  `revision` bigint(20) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `birthdate` datetime DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `phone_number` varchar(20) DEFAULT NULL,
  `city` varchar(30) DEFAULT NULL,
  `follower` int(11) DEFAULT 0,
  `following` int(11) DEFAULT 0,
  `profile_picture` varchar(255) DEFAULT NULL,
  `publication` int(11) DEFAULT 0,
  `biography` varchar(5000) DEFAULT NULL,
  `id_country` int(11) DEFAULT NULL,
  `id_gender` int(11) DEFAULT NULL,
  `id_status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_iyeg6x6r8s959nhdt76efqc6u` (`id_server`),
  KEY `FKk7k6inityad67q6d6dauvq7se` (`id_country`),
  KEY `FKimysbk21p49j16863efigckk2` (`id_gender`),
  KEY `FKef9wnnresqvo15w2wct5cb5dr` (`id_status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account`
--

LOCK TABLES `account` WRITE;
/*!40000 ALTER TABLE `account` DISABLE KEYS */;
INSERT INTO `account` VALUES (21,'205285312177400','','2020-08-24 18:35:43','\0',NULL,0,NULL,NULL,NULL,'innocent.ntafor@gmail.com','Innocent','NTAFOR','b9e4c748061f2c1962f8fee2055bdf92f3ecf4e89cc8e19afb992a8a9121e5b035add71568f5bb6801746ab543d605b55fd8bed141b8facf0ff60a9f3d7f2f52',NULL,NULL,0,0,NULL,0,NULL,NULL,NULL,NULL),(62,'94568172733300','','2020-09-03 19:21:40','\0',NULL,0,NULL,'2020-05-04 02:00:00',NULL,'belinda.mengue@yopmail.com','Belinda','MENGUE','9e00068ca1fe3558a1e85c241fb05fcb6e4d0464effd36d1eb8a985817bb9df0839b1cbb1c71d86bda1b5e71ae1a29cf2c2bd6953314a6f1e776e02e908f268a',NULL,'Lyon',0,0,NULL,0,'A quand remonte la dernière fois où vous avez mis à jour votre biographie Instagram ?\n\nParce que la présentation de votre profil est fondamentale pour susciter l’envie des utilisateurs de s’abonner à votre compte, proposer une biographie bien rédigée et attractive est plus important que jamais.\n\nDe la même manière que convertir les visiteurs de votre site Internet en clients, l’objectif de votre profil Instagram est de convertir des visiteurs en nouveaux followers. Cela signifie donc que vous devez vous assurer que votre biographie Instagram comprend tout ce qu’il faut pour faire une excellente première impression.\n\nJe vous expliquerai tout au long de cet article ce qu’il faut savoir pour réussir cet espace fondamental de votre profil, avec de nombreux exemples concrets pour que vous puissiez vous aussi reproduire cela pour votre marque.',107,106,109),(77,'176189985379000','','2020-09-04 18:01:50','\0',NULL,0,NULL,NULL,NULL,'marie.magne@yopmail.com','Marie','MAGNE','86002024197f55ff14d2f58d7230d6ff319944c3c8af2f82ecf14c6cc5e334aa215ac14b572a45f82a3416bd0f02c922744e661878f6b7a2d724b45c291345c0',NULL,NULL,0,0,NULL,0,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `activity_area`
--

DROP TABLE IF EXISTS `activity_area`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `activity_area` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `activity_area_name` varchar(50) NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_8kp9xgj172mu3tjwkq340if7x` (`id_server`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activity_area`
--

LOCK TABLES `activity_area` WRITE;
/*!40000 ALTER TABLE `activity_area` DISABLE KEYS */;
INSERT INTO `activity_area` VALUES (83,'260378271142800','Agroalimentaire',NULL),(84,'260378271179700','Banque / Assurance',NULL),(85,'260378271183300','Électronique / Électricité',NULL);
/*!40000 ALTER TABLE `activity_area` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `activity_sector`
--

DROP TABLE IF EXISTS `activity_sector`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `activity_sector` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `activity_sector_name` varchar(50) NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_g3clcptstkdfkbdsa9s5pxatc` (`id_server`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activity_sector`
--

LOCK TABLES `activity_sector` WRITE;
/*!40000 ALTER TABLE `activity_sector` DISABLE KEYS */;
INSERT INTO `activity_sector` VALUES (86,'260378372674400','Secteur primaire',NULL),(87,'260378372689000','Secteur secondaire',NULL),(88,'260378372692200','Secteur tertiaire',NULL);
/*!40000 ALTER TABLE `activity_sector` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `country`
--

DROP TABLE IF EXISTS `country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `country` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `label` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_bs66m8npqqbrydipm9k2ko187` (`id_server`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `country`
--

LOCK TABLES `country` WRITE;
/*!40000 ALTER TABLE `country` DISABLE KEYS */;
INSERT INTO `country` VALUES (107,'329192866685400','Cameroun'),(108,'329192866701800','France');
/*!40000 ALTER TABLE `country` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `degree_obtained`
--

DROP TABLE IF EXISTS `degree_obtained`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `degree_obtained` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `active` bit(1) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `deleted` bit(1) DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  `revision` bigint(20) DEFAULT NULL,
  `city` varchar(20) NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  `establishment_name` varchar(50) NOT NULL,
  `id_account` int(11) DEFAULT NULL,
  `end_date` datetime NOT NULL,
  `start_date` datetime NOT NULL,
  `id_establishment_type` int(11) DEFAULT NULL,
  `id_study_level` int(11) DEFAULT NULL,
  `id_teaching_area` int(11) DEFAULT NULL,
  `id_specialty` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_it7wk6xlws8hdylq3dieky5ut` (`id_server`),
  KEY `FKh8tevkh5lw7y07a54xo382yft` (`id_establishment_type`),
  KEY `FK60g5abwqrxsu6qaifnp34x4or` (`id_specialty`),
  KEY `FK6j2u3hgmm6idvdjwmmuv7v9w7` (`id_study_level`),
  KEY `FKbj5cgurncx5o2n02unxfjqd0p` (`id_teaching_area`),
  KEY `FK_nlfuly8amp0n6g71i9ewdrqbf` (`id_account`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `degree_obtained`
--

LOCK TABLES `degree_obtained` WRITE;
/*!40000 ALTER TABLE `degree_obtained` DISABLE KEYS */;
/*!40000 ALTER TABLE `degree_obtained` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `diploma_prepared`
--

DROP TABLE IF EXISTS `diploma_prepared`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `diploma_prepared` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  `diploma_name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_v13bf7n4217ytibxf74jq63r` (`id_server`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `diploma_prepared`
--

LOCK TABLES `diploma_prepared` WRITE;
/*!40000 ALTER TABLE `diploma_prepared` DISABLE KEYS */;
/*!40000 ALTER TABLE `diploma_prepared` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `discipline`
--

DROP TABLE IF EXISTS `discipline`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `discipline` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  `discipline_name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_5hj653mpeo2wjao494f7p1w2w` (`id_server`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `discipline`
--

LOCK TABLES `discipline` WRITE;
/*!40000 ALTER TABLE `discipline` DISABLE KEYS */;
INSERT INTO `discipline` VALUES (22,'358239611631600',NULL,'Discipline1'),(23,'358239611688000',NULL,'Discipline2'),(24,'358239611690800',NULL,'Discipline3'),(25,'358239611691800',NULL,'Discipline4');
/*!40000 ALTER TABLE `discipline` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `document`
--

DROP TABLE IF EXISTS `document`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `document` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `checksum` varchar(255) DEFAULT NULL,
  `directory` varchar(255) DEFAULT NULL,
  `extension` varchar(255) DEFAULT NULL,
  `mime_type` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `type_document` varchar(255) DEFAULT NULL,
  `account` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_pcgaqfgh097jgfpi35x7snhun` (`id_server`),
  KEY `FKh0s0i9sih76j4oxgyu604x66e` (`account`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `document`
--

LOCK TABLES `document` WRITE;
/*!40000 ALTER TABLE `document` DISABLE KEYS */;
/*!40000 ALTER TABLE `document` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `education`
--

DROP TABLE IF EXISTS `education`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `education` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  `education_name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_rxld3r7vdl36kn9gej7eu65b7` (`id_server`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `education`
--

LOCK TABLES `education` WRITE;
/*!40000 ALTER TABLE `education` DISABLE KEYS */;
INSERT INTO `education` VALUES (1,'358239611631600',NULL,'Enseignement Secondaire'),(2,'358239611688000',NULL,'Enseignement Supérieure');
/*!40000 ALTER TABLE `education` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `establishment_type`
--

DROP TABLE IF EXISTS `establishment_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `establishment_type` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  `id_education` int(11) DEFAULT NULL,
  `establishment_type_name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_8tc237adw2hrai8twqk5lu5kf` (`id_server`),
  KEY `FKoga9thsge11ttucveqp1djeme` (`id_education`),
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `establishment_type`
--

LOCK TABLES `establishment_type` WRITE;
/*!40000 ALTER TABLE `establishment_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `establishment_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `frequented_class`
--

DROP TABLE IF EXISTS `frequented_class`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `frequented_class` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `classe_name` varchar(50) NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_reayxllx7vkm67nddnrjgyaxd` (`id_server`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `frequented_class`
--

LOCK TABLES `frequented_class` WRITE;
/*!40000 ALTER TABLE `frequented_class` DISABLE KEYS */;
/*!40000 ALTER TABLE `frequented_class` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gender`
--

DROP TABLE IF EXISTS `gender`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gender` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `label` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_pmsrnxn4kayxewyfw2vp1rht4` (`id_server`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gender`
--

LOCK TABLES `gender` WRITE;
/*!40000 ALTER TABLE `gender` DISABLE KEYS */;
INSERT INTO `gender` VALUES (105,'329192731912900','Homme'),(106,'329192731963400','Femme');
/*!40000 ALTER TABLE `gender` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hibernate_sequence`
--

DROP TABLE IF EXISTS `hibernate_sequence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hibernate_sequence`
--

LOCK TABLES `hibernate_sequence` WRITE;
/*!40000 ALTER TABLE `hibernate_sequence` DISABLE KEYS */;
INSERT INTO `hibernate_sequence` VALUES (111);
/*!40000 ALTER TABLE `hibernate_sequence` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `higher_education`
--

DROP TABLE IF EXISTS `higher_education`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `higher_education` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `active` bit(1) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `deleted` bit(1) DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  `revision` bigint(20) DEFAULT NULL,
  `city` varchar(20) NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  `establishment_name` varchar(50) NOT NULL,
  `id_account` int(11) DEFAULT NULL,
  `id_diploma_prepared` int(11) DEFAULT NULL,
  `id_education` int(11) DEFAULT NULL,
  `id_specialty` int(11) DEFAULT NULL,
  `id_study_level` int(11) DEFAULT NULL,
  `id_teaching_area` int(11) DEFAULT NULL,
  `id_prepared_diploma` int(11) DEFAULT NULL,
  `id_targeted_diploma` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_h3tahd3n1q4kp7v3xwxxueoud` (`id_server`),
  KEY `FK_79myt3yhrkhffocup6uah90xo` (`id_diploma_prepared`),
  KEY `FKhd4e8amr37e3x0puao1tg2lu1` (`id_study_level`),
  KEY `FKje5l2mb8om7vvbj4x2m5jcfb0` (`id_targeted_diploma`),
  KEY `FK9i0wv6wfebjuucff6blyam81j` (`id_teaching_area`),
  KEY `FK_1k06flsqbfy9cxydgjyumnet6` (`id_education`),
  KEY `FK_akg9ohrpv4ohupmqdioji24ul` (`id_prepared_diploma`),
  KEY `FK_52smp5dw7dwkr13x1f8ykjuvw` (`id_specialty`),
  KEY `FK_92tdd3oy1bcxka7mtrgq1dwhc` (`id_account`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `higher_education`
--

LOCK TABLES `higher_education` WRITE;
/*!40000 ALTER TABLE `higher_education` DISABLE KEYS */;
/*!40000 ALTER TABLE `higher_education` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `level`
--

DROP TABLE IF EXISTS `level`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `level` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `description` varchar(50) DEFAULT NULL,
  `level_name` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_avbprobsmujuclg54t1crl3ip` (`id_server`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `level`
--

LOCK TABLES `level` WRITE;
/*!40000 ALTER TABLE `level` DISABLE KEYS */;
INSERT INTO `level` VALUES (26,'358239720342200',NULL,'Level1'),(27,'358239720363700',NULL,'Level2'),(28,'358239720365300',NULL,'Level3'),(29,'358239720366200',NULL,'Level4');
/*!40000 ALTER TABLE `level` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `marital_status`
--

DROP TABLE IF EXISTS `marital_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `marital_status` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `label` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_ssq0cgxl44530nckvv2deelra` (`id_server`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `marital_status`
--

LOCK TABLES `marital_status` WRITE;
/*!40000 ALTER TABLE `marital_status` DISABLE KEYS */;
INSERT INTO `marital_status` VALUES (109,'329192877272700','Célibatiare'),(110,'329192877290600','Marié');
/*!40000 ALTER TABLE `marital_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prepared_diploma`
--

DROP TABLE IF EXISTS `prepared_diploma`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prepared_diploma` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  `diploma_name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_dx5hu299bg8nxj0kg5b6qri9c` (`id_server`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prepared_diploma`
--

LOCK TABLES `prepared_diploma` WRITE;
/*!40000 ALTER TABLE `prepared_diploma` DISABLE KEYS */;
/*!40000 ALTER TABLE `prepared_diploma` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `professional`
--

DROP TABLE IF EXISTS `professional`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `professional` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `active` bit(1) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `deleted` bit(1) DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  `revision` bigint(20) DEFAULT NULL,
  `city` varchar(20) NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  `establishment_name` varchar(50) NOT NULL,
  `id_account` int(11) DEFAULT NULL,
  `end_date` datetime NOT NULL,
  `job_title` varchar(30) NOT NULL,
  `start_date` datetime NOT NULL,
  `id_activity_area` int(11) DEFAULT NULL,
  `id_activity_sector` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_jhtn3j550trbpd1t9gvk99ni4` (`id_server`),
  KEY `FKqqiq4iq4hjtu2w9xo76g8kbr5` (`id_activity_area`),
  KEY `FKrb9ipw4jpaovdyfl3b43ql3vw` (`id_activity_sector`),
  KEY `FK_d7boe4m4rripu794c5j4kl65y` (`id_account`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `professional`
--

LOCK TABLES `professional` WRITE;
/*!40000 ALTER TABLE `professional` DISABLE KEYS */;
INSERT INTO `professional` VALUES (89,'262136211683700','','2020-09-05 17:54:36','',NULL,0,'yaounde','awesome','yamo',62,'2020-09-06 02:00:00','informaticien','2020-09-01 02:00:00',84,88),(90,'262710190968700','','2020-09-05 18:03:40','',NULL,0,'yaounde','great','yamo',62,'2020-10-11 02:00:00','marketing','2020-08-04 02:00:00',84,87),(91,'265426228222800','','2020-09-05 18:48:56','\0',NULL,0,'Yaounde',NULL,'YAMO GROUP',62,'2020-09-06 02:00:00','Logistics','2020-09-05 02:00:00',83,87),(98,'266629868776400','','2020-09-05 19:09:00','\0',NULL,0,'Yaounde',NULL,'YAMO GROUP SARL',62,'2020-09-06 02:00:00','Agriculteur','2020-09-01 02:00:00',83,86);
/*!40000 ALTER TABLE `professional` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `secondary_education`
--

DROP TABLE IF EXISTS `secondary_education`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `secondary_education` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `active` bit(1) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `deleted` bit(1) DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  `revision` bigint(20) DEFAULT NULL,
  `city` varchar(20) NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  `establishment_name` varchar(50) NOT NULL,
  `id_account` int(11) DEFAULT NULL,
  `id_diploma_prepared` int(11) DEFAULT NULL,
  `id_education` int(11) DEFAULT NULL,
  `id_specialty` int(11) DEFAULT NULL,
  `id_class` int(11) DEFAULT NULL,
  `id_prepared_diploma` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_nhxvw6ieoh23429mwaq1qkmsj` (`id_server`),
  KEY `FK_9n7e4ordteykocejv11b5benh` (`id_diploma_prepared`),
  KEY `FK715eh87ujemtklbw433uc54r1` (`id_class`),
  KEY `FK_ndodnx6e9p6avx5t0txsaafb0` (`id_education`),
  KEY `FK_54lfigwbb69j4pvttb25bkxmq` (`id_prepared_diploma`),
  KEY `FK_fs1gip1u1fngy73cctmuf6bd7` (`id_specialty`),
  KEY `FK_a8adro1m7qqmtpeceunk8s1x0` (`id_account`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `secondary_education`
--

LOCK TABLES `secondary_education` WRITE;
/*!40000 ALTER TABLE `secondary_education` DISABLE KEYS */;
/*!40000 ALTER TABLE `secondary_education` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `skill`
--

DROP TABLE IF EXISTS `skill`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `skill` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `active` bit(1) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `deleted` bit(1) DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  `revision` bigint(20) DEFAULT NULL,
  `skill_mastered` bit(1) DEFAULT NULL,
  `skill_name` varchar(50) NOT NULL,
  `id_account` int(11) DEFAULT NULL,
  `id_discipline` int(11) DEFAULT NULL,
  `id_level` int(11) DEFAULT NULL,
  `id_skill_type` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_mgwf4f5epvhru88djnobni1vl` (`id_server`),
  KEY `FKoga9thsge11ttuupeqp1djeme` (`id_account`),
  KEY `FKsbnax661o60es8shn4lxjlphx` (`id_discipline`),
  KEY `FKdep21erjtc4h8sjxm9b5117fu` (`id_level`),
  KEY `FK8doqcpmm41ahi05wuld1l7l16` (`id_skill_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `skill`
--

LOCK TABLES `skill` WRITE;
/*!40000 ALTER TABLE `skill` DISABLE KEYS */;
INSERT INTO `skill` VALUES (37,'5918635871200','','2020-08-27 12:32:35','\0',NULL,0,'','competence 1',21,22,26,30),(38,'5980884562900','','2020-08-27 12:33:37','\0',NULL,0,'\0','competence 2',21,23,27,31),(63,'152493382254000','','2020-09-04 11:26:53','',NULL,0,'','Comptence1',62,22,26,30),(64,'152800512857400','','2020-09-04 11:32:00','',NULL,0,'','competence 2',62,23,27,31),(65,'155572439597800','','2020-09-04 12:18:12','',NULL,0,'','comptence 3',62,24,28,32),(66,'155720628473400','','2020-09-04 12:20:40','\0',NULL,0,'','comptence 4',62,25,29,33),(67,'155885398164800','','2020-09-04 12:23:25','',NULL,0,'\0','comptence1',62,22,27,30),(68,'156900040476200','','2020-09-04 12:40:20','\0',NULL,0,'\0','comptence 5',62,24,28,33),(69,'156999104340000','','2020-09-04 12:41:59','\0',NULL,0,'\0','competence 9',62,25,27,31),(78,'176484946834700','','2020-09-04 18:06:45','\0',NULL,0,'','comptence1',77,22,27,30),(79,'176631477015900','','2020-09-04 18:09:11','\0',NULL,0,'\0','comptence 4',77,23,29,31),(80,'176725195723500','','2020-09-04 18:10:45','\0',NULL,0,'\0','comptence 8',77,24,26,33),(81,'177836007805200','','2020-09-04 18:29:16','\0',NULL,0,'\0','comptence 8',77,25,28,31),(82,'243667489774900','','2020-09-05 12:46:16','\0',NULL,0,'\0','competence',62,23,29,31);
/*!40000 ALTER TABLE `skill` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `skill_type`
--

DROP TABLE IF EXISTS `skill_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `skill_type` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  `skill_type_name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_4le85m83df2y1yvuwopqwb317` (`id_server`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `skill_type`
--

LOCK TABLES `skill_type` WRITE;
/*!40000 ALTER TABLE `skill_type` DISABLE KEYS */;
INSERT INTO `skill_type` VALUES (30,'358239740353000',NULL,'SkillType1'),(31,'358239740380800',NULL,'SkillType2'),(32,'358239740383500',NULL,'SkillType3'),(33,'358239740385000',NULL,'SkillType4');
/*!40000 ALTER TABLE `skill_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `specialty`
--

DROP TABLE IF EXISTS `specialty`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `specialty` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  `specialty_name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_hvs3s263qsrdp83gsys83xg8t` (`id_server`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `specialty`
--

LOCK TABLES `specialty` WRITE;
/*!40000 ALTER TABLE `specialty` DISABLE KEYS */;
/*!40000 ALTER TABLE `specialty` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `study_level`
--

DROP TABLE IF EXISTS `study_level`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `study_level` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  `study_level_name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_jmdv843pnvonbtfhom3i794u1` (`id_server`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `study_level`
--

LOCK TABLES `study_level` WRITE;
/*!40000 ALTER TABLE `study_level` DISABLE KEYS */;
/*!40000 ALTER TABLE `study_level` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `targeted_diploma`
--

DROP TABLE IF EXISTS `targeted_diploma`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `targeted_diploma` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  `diploma_name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_ms5gp12rl98c57g1n27l5egek` (`id_server`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `targeted_diploma`
--

LOCK TABLES `targeted_diploma` WRITE;
/*!40000 ALTER TABLE `targeted_diploma` DISABLE KEYS */;
/*!40000 ALTER TABLE `targeted_diploma` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teaching_area`
--

DROP TABLE IF EXISTS `teaching_area`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teaching_area` (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  `teaching_area_name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_ox8ks35vdkc9nd83in6u23a1i` (`id_server`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teaching_area`
--

LOCK TABLES `teaching_area` WRITE;
/*!40000 ALTER TABLE `teaching_area` DISABLE KEYS */;
/*!40000 ALTER TABLE `teaching_area` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'skillsmatesdb'
--

--
-- Dumping routines for database 'skillsmatesdb'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-09-06 16:44:33
