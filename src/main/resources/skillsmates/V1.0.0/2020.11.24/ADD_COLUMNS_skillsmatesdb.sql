ALTER TABLE skillsmatesdb.activity_area ADD COLUMN `specified` tinyint(1) NULL DEFAULT 0;
ALTER TABLE skillsmatesdb.establishment_type ADD COLUMN `specified` tinyint(1) NULL DEFAULT 0;
ALTER TABLE skillsmatesdb.study_level ADD COLUMN `specified` tinyint(1) NULL DEFAULT 0;
ALTER TABLE skillsmatesdb.diploma ADD COLUMN `specified` tinyint(1) NULL DEFAULT 0;
ALTER TABLE skillsmatesdb.activity_sector ADD COLUMN `specified` tinyint(1) NULL DEFAULT 0;

ALTER TABLE skillsmatesdb.degree_obtained ADD COLUMN `another_diploma` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;
ALTER TABLE skillsmatesdb.degree_obtained ADD COLUMN `another_establishment_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;

ALTER TABLE skillsmatesdb.higher_education ADD COLUMN `another_targeted_diploma` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;
ALTER TABLE skillsmatesdb.higher_education ADD COLUMN `another_study_level` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;
ALTER TABLE skillsmatesdb.higher_education ADD COLUMN `another_establishment_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;

ALTER TABLE skillsmatesdb.professional ADD COLUMN `another_activity_area` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;
ALTER TABLE skillsmatesdb.professional ADD COLUMN `another_activity_sector` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;

ALTER TABLE skillsmatesdb.secondary_education ADD COLUMN `another_prepared_diploma` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;
ALTER TABLE skillsmatesdb.secondary_education ADD COLUMN `another_study_level` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;
ALTER TABLE skillsmatesdb.secondary_education ADD COLUMN `another_establishment_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;