/*
 Navicat Premium Data Transfer

 Source Server         : skillmates
 Source Server Type    : MariaDB
 Source Server Version : 100505
 Source Host           : localhost:3306
 Source Schema         : skillsmatesdb

 Target Server Type    : MariaDB
 Target Server Version : 100505
 File Encoding         : 65001

 Date: 03/12/2020 16:03:47
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for account
-- ----------------------------
DROP TABLE IF EXISTS `account`;
CREATE TABLE `account`  (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `active` bit(1) NULL DEFAULT NULL,
  `created_at` datetime(0) NOT NULL,
  `deleted` bit(1) NULL DEFAULT NULL,
  `modified_at` datetime(0) NULL DEFAULT NULL,
  `revision` bigint(20) NULL DEFAULT NULL,
  `address` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `biography` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `birthdate` datetime(0) NULL DEFAULT NULL,
  `city` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `email` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `firstname` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `follower` int(11) NULL DEFAULT 0,
  `following` int(11) NULL DEFAULT 0,
  `lastname` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `phone_number` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `profile_picture` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `publication` int(11) NULL DEFAULT 0,
  `country` int(11) NULL DEFAULT NULL,
  `gender` int(11) NULL DEFAULT NULL,
  `status` int(11) NULL DEFAULT NULL,
  `connected` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_iyeg6x6r8s959nhdt76efqc6u`(`id_server`) USING BTREE,
  UNIQUE INDEX `UK_q0uja26qgu1atulenwup9rxyr`(`email`) USING BTREE,
  INDEX `FKi7rut9n9vfl3ac9cxuegckab9`(`country`) USING BTREE,
  INDEX `FKge5lb1w9xvo78dg80k6s3ni27`(`gender`) USING BTREE,
  INDEX `FKny0dyt6qv6m9y7qlcbfdwms58`(`status`) USING BTREE,
  CONSTRAINT `FKge5lb1w9xvo78dg80k6s3ni27` FOREIGN KEY (`gender`) REFERENCES `gender` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FKi7rut9n9vfl3ac9cxuegckab9` FOREIGN KEY (`country`) REFERENCES `country` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FKny0dyt6qv6m9y7qlcbfdwms58` FOREIGN KEY (`status`) REFERENCES `professional_status` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of account
-- ----------------------------
INSERT INTO `account` VALUES (1, '11308326636800', b'1', '2020-09-27 21:02:47', b'0', NULL, 0, NULL, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\n\n\nContrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.', '2010-08-05 00:00:00', 'Yaounde', NULL, 'belinda.mengue@yopmail.com', 'Belinda', 6, 6, 'MENGUE', '9e00068ca1fe3558a1e85c241fb05fcb6e4d0464effd36d1eb8a985817bb9df0839b1cbb1c71d86bda1b5e71ae1a29cf2c2bd6953314a6f1e776e02e908f268a', NULL, 'avatar_3a23149a.jpg', 8, 48, 3, 5, 1);
INSERT INTO `account` VALUES (591, '67665637080700', b'1', '2020-09-28 12:41:54', b'0', NULL, 0, NULL, 'Chimamanda Ngozi Adichie grew up in Nigeria.\n\nHer work has been translated into over thirty languages and has appeared in various publications, including The New Yorker, Granta, The O. Henry Prize Stories, the Financial Times, and Zoetrope. She is the author of the novels Purple Hibiscus, which won the Commonwealth Writers’ Prize and the Hurston/Wright Legacy Award; Half of a Yellow Sun, which won the Orange Prize and was a National Book Critics Circle Award Finalist and a New York Times Notable Book; and Americanah, which won the National Book Critics Circle Award and was named one of The New York Times Top Ten Best Books of 2013. Ms. Adichie is also the author of the story collection The Thing Around Your Neck.\n\nMs. Adichie has been invited to speak around the world. Her 2009 TED Talk, The Danger of A Single Story, is now one of the most-viewed TED Talks of all time. Her 2012 talk We Should All Be Feminists has a started a worldwide conversation about feminism, and was published as a book in 2014.\n\nHer most recent book, Dear Ijeawele, or a Feminist Manifesto in Fifteen Suggestions, was published in March 2017.\n\nA recipient of a MacArthur Foundation Fellowship, Ms. Adichie divides her time between the United States and Nigeria.\n\nFor a detailed bibliography, please see the independent “The Chimamanda Ngozi Adichie Website” maintained by Daria Tunca.', '1977-08-15 02:00:00', 'Lagos', NULL, 'chimamanda.ngozi@yopmail.com', 'Chimamanda', 6, 6, 'NGOZI ADICHIE', 'f34847f857131ada50de3c9d944f849a57578df75704f637e58ec9a8f316e85ff9157d94f3e73ef948a19289170c42cf65c8f1d655767e65a6603fc4ce8c1523', NULL, 'AVATAR_b720cd51.jpg', 0, 169, 3, 6, 0);
INSERT INTO `account` VALUES (592, '67711260684600', b'1', '2020-09-28 12:42:40', b'0', NULL, 0, NULL, 'Winnie Mandela was the controversial wife of Nelson Mandela who spent her life in varying governmental roles.\nWho Was Winnie Mandela?\nWinnie Mandela embarked on a career of social work that led to her involvement in activism. She married African National Congress leader Nelson Mandela in 1958, though he was imprisoned for much of their four decades of marriage. Winnie became president of the ANC Women\'s League in 1993, and the following year she was elected to Parliament. However, her accomplishments were also tainted by convictions for kidnapping and fraud. She passed away on April 2, 2018, in Johannesburg‚ South Africa. \n\nEarly Life and Career\nBorn Nomzamo Winifred Madikizela on September 26, 1936, in Bizana, a rural village in the Transkei district of South Africa, Winnie eventually moved to Johannesburg in 1953 to study at the Jan Hofmeyr School of Social Work. South Africa was under the system known as apartheid, where citizens of Indigenous African descent were subjected to a harsh caste system, while European descendants enjoyed much higher levels of wealth, health and social freedom.\n\nWinnie completed her studies and, though receiving a scholarship to study in America, decided instead to work as the first Black medical social worker at Baragwanath Hospital in Johannesburg. A dedicated professional, she came to learn via her fieldwork of the deplorable state that many of her patients lived in.', '1936-09-26 01:00:00', 'Johannesburg', NULL, 'wunnie.mandela@yopmail.com', 'Wunnie', 6, 6, 'MANDELA', '50b81b72ec10002df3de34d77be60573f130f4ae68416b0b2dd952fec4ace93e42e60f0434e85031afd7e610ce457175661eb58782779d8e0b58768c5af756f5', NULL, '', 0, 9, 3, 7, 0);
INSERT INTO `account` VALUES (1171, '162523995070100', b'1', '2020-10-20 09:21:55', b'0', NULL, 0, NULL, NULL, NULL, NULL, NULL, 'chinua.achebe@yopmail.com', 'Chinua', 6, 6, 'ACHEBE', 'a0ae4f30ba298dd007a1778ba85f1fbab17f274bed233262e37e7caf8d3c45efe5f4b213ce9e403dffff2638394a9578815c8a8c030ae4ce85501c78216f0a32', NULL, NULL, 0, NULL, NULL, NULL, 0);
INSERT INTO `account` VALUES (1445, '436473988760115', b'1', '2020-11-03 12:18:10', b'0', NULL, 0, NULL, 'Je suis un homme assez cool', '1987-08-14 00:00:00', 'Douala', NULL, 'atchiegang@gmail.com', 'Alain', 6, 6, 'TCHIEGANG', '21208acd628f24941e6c5960fbd7e125aa6d4adefc46864f9d751f07a5dacd43b3b5267d653d88ef20a97ac2c3d4a51eafa9afbf22d15c7edb0a80db4f7bd432', NULL, 'avatar_58bf2faa.jpg', 14, 48, 4, 7, 1);
INSERT INTO `account` VALUES (1447, '436719833572637', b'1', '2020-11-03 12:22:15', b'0', NULL, 0, NULL, 'Je suis une jeune femme assez dynamique', '1990-05-11 00:00:00', 'Yaoundé', NULL, 'alexia.mekie@gmail.com', 'Alexia', 6, 6, 'MEKIE', '89d99f76df4d8640cd7a0ee6611499a5fa5bc942439945a48cd8b2863fb305834f98993d9d85f6ea2c5d864f18eb8f04c37e23e60e1c7c50a6ce881e79119d7d', NULL, 'AVATAR_f61b46a.jpg', 0, 48, 3, 5, 1);

-- ----------------------------
-- Table structure for activity_area
-- ----------------------------
DROP TABLE IF EXISTS `activity_area`;
CREATE TABLE `activity_area`  (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `label` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `specified` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_8kp9xgj172mu3tjwkq340if7x`(`id_server`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of activity_area
-- ----------------------------
INSERT INTO `activity_area` VALUES (734, '106065088539600', 'Administratif', 'Administratif', 0);
INSERT INTO `activity_area` VALUES (735, '106065089547500', 'Aéronautique', 'Aéronautique', 0);
INSERT INTO `activity_area` VALUES (736, '106065090395600', 'Agriculture', 'Agriculture', 0);
INSERT INTO `activity_area` VALUES (737, '106065091150500', 'Agroalimentaire', 'Agroalimentaire', 0);
INSERT INTO `activity_area` VALUES (738, '106065091872100', 'Architecte – BTP – Urbanisme', 'Architecte – BTP – Urbanisme', 0);
INSERT INTO `activity_area` VALUES (739, '106065092578300', 'Armée – Défense', 'Armée – Défense', 0);
INSERT INTO `activity_area` VALUES (740, '106065093341900', 'Art', 'Art', 0);
INSERT INTO `activity_area` VALUES (741, '106065094167400', 'Artisanat', 'Artisanat', 0);
INSERT INTO `activity_area` VALUES (742, '106065095038900', 'Assurance', 'Assurance', 0);
INSERT INTO `activity_area` VALUES (743, '106065095871300', 'Audiovisuel – Cinéma', 'Audiovisuel – Cinéma', 0);
INSERT INTO `activity_area` VALUES (744, '106065096653100', 'Automobile', 'Automobile', 0);
INSERT INTO `activity_area` VALUES (745, '106065097484700', 'Banque-Finance', 'Banque-Finance', 0);
INSERT INTO `activity_area` VALUES (746, '106065098251600', 'Chimie – Biologie', 'Chimie – Biologie', 0);
INSERT INTO `activity_area` VALUES (747, '106065098972200', 'Commerce – Vente – Distribution', 'Commerce – Vente – Distribution', 0);
INSERT INTO `activity_area` VALUES (748, '106065099689500', 'Communication', 'Communication', 0);
INSERT INTO `activity_area` VALUES (749, '106065100837600', 'Comptabilité – Gestion', 'Comptabilité – Gestion', 0);
INSERT INTO `activity_area` VALUES (750, '106065101724600', 'Création', 'Création', 0);
INSERT INTO `activity_area` VALUES (751, '106065102459800', 'Culture', 'Culture', 0);
INSERT INTO `activity_area` VALUES (752, '106065103298000', 'Droit – Juridique – Justice', 'Droit – Juridique – Justice', 0);
INSERT INTO `activity_area` VALUES (753, '106065104314100', 'Économie', 'Économie', 0);
INSERT INTO `activity_area` VALUES (754, '106065105063400', 'Edition et Métier du livre', 'Edition et Métier du livre', 0);
INSERT INTO `activity_area` VALUES (755, '106065106826900', 'Énergie', 'Énergie', 0);
INSERT INTO `activity_area` VALUES (756, '106065107655500', 'Enseignement', 'Enseignement', 0);
INSERT INTO `activity_area` VALUES (757, '106065108479900', 'Environnement – Développement durable', 'Environnement – Développement durable', 0);
INSERT INTO `activity_area` VALUES (758, '106065109220300', 'Esthétique – Beauté – Coiffure', 'Esthétique – Beauté – Coiffure', 0);
INSERT INTO `activity_area` VALUES (759, '106065110059100', 'Évènementiel', 'Évènementiel', 0);
INSERT INTO `activity_area` VALUES (760, '106065110818700', 'Fonction public – Management public', 'Fonction public – Management public', 0);
INSERT INTO `activity_area` VALUES (761, '106065111533900', 'Hôtellerie – restauration', 'Hôtellerie – restauration', 0);
INSERT INTO `activity_area` VALUES (762, '106065112282900', 'Humanitaire', 'Humanitaire', 0);
INSERT INTO `activity_area` VALUES (763, '106065113051400', 'Immobilier', 'Immobilier', 0);
INSERT INTO `activity_area` VALUES (764, '106065113914800', 'Industrie', 'Industrie', 0);
INSERT INTO `activity_area` VALUES (765, '106065114743800', 'Informatique – Électronique – Numérique', 'Informatique – Électronique – Numérique', 0);
INSERT INTO `activity_area` VALUES (766, '106065115576900', 'Internet – Web', 'Internet – Web', 0);
INSERT INTO `activity_area` VALUES (767, '106065116389200', 'Jeux vidéo – Esport – Gaming', 'Jeux vidéo – Esport – Gaming', 0);
INSERT INTO `activity_area` VALUES (768, '106065117344500', 'Journalisme', 'Journalisme', 0);
INSERT INTO `activity_area` VALUES (769, '106065118153200', 'Luxe', 'Luxe', 0);
INSERT INTO `activity_area` VALUES (770, '106065119000100', 'Métiers Animaliers', 'Métiers Animaliers', 0);
INSERT INTO `activity_area` VALUES (771, '106065119745600', 'Mode – Textile', 'Mode – Textile', 0);
INSERT INTO `activity_area` VALUES (772, '106065120589700', 'Musique', 'Musique', 0);
INSERT INTO `activity_area` VALUES (773, '106065121370300', 'Paramédical', 'Paramédical', 0);
INSERT INTO `activity_area` VALUES (774, '106065122137500', 'Psychologie', 'Psychologie', 0);
INSERT INTO `activity_area` VALUES (775, '106065122957400', 'Publicité – Marketing', 'Publicité – Marketing', 0);
INSERT INTO `activity_area` VALUES (776, '106065124095200', 'Ressources Humaines', 'Ressources Humaines', 0);
INSERT INTO `activity_area` VALUES (777, '106065124988400', 'Santé', 'Santé', 0);
INSERT INTO `activity_area` VALUES (778, '106065125752800', 'Secrétariat – Assistance', 'Secrétariat – Assistance', 0);
INSERT INTO `activity_area` VALUES (779, '106065126460200', 'Sécurité', 'Sécurité', 0);
INSERT INTO `activity_area` VALUES (780, '106065127108700', 'Social', 'Social', 0);
INSERT INTO `activity_area` VALUES (781, '106065127833500', 'Sport', 'Sport', 0);
INSERT INTO `activity_area` VALUES (782, '106065128594800', 'Tourisme', 'Tourisme', 0);
INSERT INTO `activity_area` VALUES (783, '106065129387500', 'Transport - Logistique', 'Transport - Logistique', 0);
INSERT INTO `activity_area` VALUES (784, '106065130134200', 'Autres (à préciser manuellement)', 'Autres (à préciser manuellement)', 1);

-- ----------------------------
-- Table structure for activity_sector
-- ----------------------------
DROP TABLE IF EXISTS `activity_sector`;
CREATE TABLE `activity_sector`  (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `label` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `specified` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_g3clcptstkdfkbdsa9s5pxatc`(`id_server`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of activity_sector
-- ----------------------------
INSERT INTO `activity_sector` VALUES (1367, '93177516710500', 'Administration – Établissements publics', 'Administration – Établissements publics', 0);
INSERT INTO `activity_sector` VALUES (1368, '93177517634600', 'Agriculture – Agroalimentaire', 'Agriculture – Agroalimentaire', 0);
INSERT INTO `activity_sector` VALUES (1369, '93177519180200', 'Architecture – Urbanisme', 'Architecture – Urbanisme', 0);
INSERT INTO `activity_sector` VALUES (1370, '93177520130300', 'Art', 'Art', 0);
INSERT INTO `activity_sector` VALUES (1371, '93177520934200', 'Artisanat', 'Artisanat', 0);
INSERT INTO `activity_sector` VALUES (1372, '93177521755800', 'Association – Humanitaire', 'Association – Humanitaire', 0);
INSERT INTO `activity_sector` VALUES (1373, '93177522513300', 'Audiovisuel – cinéma', 'Audiovisuel – cinéma', 0);
INSERT INTO `activity_sector` VALUES (1374, '93177523349700', 'Banque – Finance – Assurance', 'Banque – Finance – Assurance', 0);
INSERT INTO `activity_sector` VALUES (1375, '93177524216100', 'Automobile – Aéronautiques – Autres Matériel de Transport', 'Automobile – Aéronautiques – Autres Matériel de Transport', 0);
INSERT INTO `activity_sector` VALUES (1376, '93177525107800', 'BTP – Construction', 'BTP – Construction', 0);
INSERT INTO `activity_sector` VALUES (1377, '93177525874100', 'Commerce – Vente – Distribution – E-commerce', 'Commerce – Vente – Distribution – E-commerce', 0);
INSERT INTO `activity_sector` VALUES (1378, '93177526660500', 'Conseil – Gestion des entreprises', 'Conseil – Gestion des entreprises', 0);
INSERT INTO `activity_sector` VALUES (1379, '93177527474900', 'Chimie – Biologie', 'Chimie – Biologie', 0);
INSERT INTO `activity_sector` VALUES (1380, '93177528240100', 'Comptabilité – Audit – Gestion', 'Comptabilité – Audit – Gestion', 0);
INSERT INTO `activity_sector` VALUES (1381, '93177529010400', 'Culture', 'Culture', 0);
INSERT INTO `activity_sector` VALUES (1382, '93177530058200', 'Droit – Justice', 'Droit – Justice', 0);
INSERT INTO `activity_sector` VALUES (1383, '93177530946500', 'Eau – Énergie – Minerais', 'Eau – Énergie – Minerais', 0);
INSERT INTO `activity_sector` VALUES (1384, '93177531799400', 'Edition – Métiers du Livre', 'Edition – Métiers du Livre', 0);
INSERT INTO `activity_sector` VALUES (1385, '93177532721300', 'Environnement – Développement durable', 'Environnement – Développement durable', 0);
INSERT INTO `activity_sector` VALUES (1386, '93177533612800', 'Enseignement – Éducation – Formation', 'Enseignement – Éducation – Formation', 0);
INSERT INTO `activity_sector` VALUES (1387, '93177534494600', 'Esthétique – Beauté – Coiffure', 'Esthétique – Beauté – Coiffure', 0);
INSERT INTO `activity_sector` VALUES (1388, '93177536878900', 'Évènementiel', 'Évènementiel', 0);
INSERT INTO `activity_sector` VALUES (1389, '93177537749600', 'Informatique – Électronique – Numérique', 'Informatique – Électronique – Numérique', 0);
INSERT INTO `activity_sector` VALUES (1390, '93177538666600', 'Industrie – Manufactures', 'Industrie – Manufactures', 0);
INSERT INTO `activity_sector` VALUES (1391, '93177539555200', 'Immobilier', 'Immobilier', 0);
INSERT INTO `activity_sector` VALUES (1392, '93177540431200', 'Internet', 'Internet', 0);
INSERT INTO `activity_sector` VALUES (1393, '93177541241900', 'Marketing – Communication', 'Marketing – Communication', 0);
INSERT INTO `activity_sector` VALUES (1394, '93177542019000', 'Métiers animaliers', 'Métiers animaliers', 0);
INSERT INTO `activity_sector` VALUES (1395, '93177542835400', 'Mode – Textile', 'Mode – Textile', 0);
INSERT INTO `activity_sector` VALUES (1396, '93177543552600', 'Ressources Humaines', 'Ressources Humaines', 0);
INSERT INTO `activity_sector` VALUES (1397, '93177544289000', 'Restauration – Hôtellerie', 'Restauration – Hôtellerie', 0);
INSERT INTO `activity_sector` VALUES (1398, '93177545117100', 'Loisirs', 'Loisirs', 0);
INSERT INTO `activity_sector` VALUES (1399, '93177545941300', 'Luxe', 'Luxe', 0);
INSERT INTO `activity_sector` VALUES (1400, '93177546786600', 'Services divers aux entreprises', 'Services divers aux entreprises', 0);
INSERT INTO `activity_sector` VALUES (1401, '93177547651900', 'Santé – Service à la personne', 'Santé – Service à la personne', 0);
INSERT INTO `activity_sector` VALUES (1402, '93177548486200', 'Sociale', 'Sociale', 0);
INSERT INTO `activity_sector` VALUES (1403, '93177549364700', 'Sport', 'Sport', 0);
INSERT INTO `activity_sector` VALUES (1404, '93177550156700', 'Télécommunication', 'Télécommunication', 0);
INSERT INTO `activity_sector` VALUES (1405, '93177550927100', 'Transports – Logistique', 'Transports – Logistique', 0);
INSERT INTO `activity_sector` VALUES (1406, '93177552251600', 'Tourisme', 'Tourisme', 0);
INSERT INTO `activity_sector` VALUES (1407, '93177553156700', 'Autres (à préciser manuellement)', 'Autres (à préciser manuellement)', 1);

-- ----------------------------
-- Table structure for batch_job_execution
-- ----------------------------
DROP TABLE IF EXISTS `batch_job_execution`;
CREATE TABLE `batch_job_execution`  (
  `JOB_EXECUTION_ID` bigint(20) NOT NULL,
  `VERSION` bigint(20) NULL DEFAULT NULL,
  `JOB_INSTANCE_ID` bigint(20) NOT NULL,
  `CREATE_TIME` datetime(0) NOT NULL,
  `START_TIME` datetime(0) NULL DEFAULT NULL,
  `END_TIME` datetime(0) NULL DEFAULT NULL,
  `STATUS` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `EXIT_CODE` varchar(2500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `EXIT_MESSAGE` varchar(2500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `LAST_UPDATED` datetime(0) NULL DEFAULT NULL,
  `JOB_CONFIGURATION_LOCATION` varchar(2500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`JOB_EXECUTION_ID`) USING BTREE,
  INDEX `JOB_INST_EXEC_FK`(`JOB_INSTANCE_ID`) USING BTREE,
  CONSTRAINT `JOB_INST_EXEC_FK` FOREIGN KEY (`JOB_INSTANCE_ID`) REFERENCES `batch_job_instance` (`JOB_INSTANCE_ID`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of batch_job_execution
-- ----------------------------
INSERT INTO `batch_job_execution` VALUES (1, 2, 1, '2020-09-27 21:06:07', '2020-09-27 21:06:07', '2020-09-27 21:06:08', 'COMPLETED', 'COMPLETED', '', '2020-09-27 21:06:08', NULL);
INSERT INTO `batch_job_execution` VALUES (2, 2, 2, '2020-09-27 21:06:57', '2020-09-27 21:06:57', '2020-09-27 21:06:58', 'COMPLETED', 'COMPLETED', '', '2020-09-27 21:06:58', NULL);
INSERT INTO `batch_job_execution` VALUES (3, 2, 3, '2020-09-27 21:07:48', '2020-09-27 21:07:48', '2020-09-27 21:07:49', 'COMPLETED', 'COMPLETED', '', '2020-09-27 21:07:49', NULL);
INSERT INTO `batch_job_execution` VALUES (4, 2, 4, '2020-09-27 21:15:38', '2020-09-27 21:15:38', '2020-09-27 21:15:38', 'COMPLETED', 'COMPLETED', '', '2020-09-27 21:15:38', NULL);
INSERT INTO `batch_job_execution` VALUES (5, 2, 5, '2020-09-27 21:16:19', '2020-09-27 21:16:19', '2020-09-27 21:16:20', 'COMPLETED', 'COMPLETED', '', '2020-09-27 21:16:20', NULL);
INSERT INTO `batch_job_execution` VALUES (6, 2, 6, '2020-09-27 21:17:28', '2020-09-27 21:17:28', '2020-09-27 21:17:29', 'COMPLETED', 'COMPLETED', '', '2020-09-27 21:17:29', NULL);
INSERT INTO `batch_job_execution` VALUES (7, 2, 7, '2020-09-27 21:20:32', '2020-09-27 21:20:32', '2020-09-27 21:20:32', 'COMPLETED', 'COMPLETED', '', '2020-09-27 21:20:32', NULL);
INSERT INTO `batch_job_execution` VALUES (8, 2, 8, '2020-09-27 21:22:37', '2020-09-27 21:22:37', '2020-09-27 21:22:38', 'COMPLETED', 'COMPLETED', '', '2020-09-27 21:22:38', NULL);
INSERT INTO `batch_job_execution` VALUES (9, 2, 9, '2020-09-27 21:25:39', '2020-09-27 21:25:39', '2020-09-27 21:25:39', 'COMPLETED', 'COMPLETED', '', '2020-09-27 21:25:39', NULL);
INSERT INTO `batch_job_execution` VALUES (10, 2, 10, '2020-09-27 21:26:33', '2020-09-27 21:26:33', '2020-09-27 21:26:33', 'COMPLETED', 'COMPLETED', '', '2020-09-27 21:26:33', NULL);
INSERT INTO `batch_job_execution` VALUES (11, 2, 11, '2020-09-27 21:27:58', '2020-09-27 21:27:58', '2020-09-27 21:27:59', 'COMPLETED', 'COMPLETED', '', '2020-09-27 21:27:59', NULL);
INSERT INTO `batch_job_execution` VALUES (12, 2, 12, '2020-09-27 21:30:54', '2020-09-27 21:30:54', '2020-09-27 21:30:54', 'COMPLETED', 'COMPLETED', '', '2020-09-27 21:30:54', NULL);
INSERT INTO `batch_job_execution` VALUES (13, 2, 13, '2020-09-27 21:56:40', '2020-09-27 21:56:40', '2020-09-27 21:56:40', 'COMPLETED', 'COMPLETED', '', '2020-09-27 21:56:40', NULL);
INSERT INTO `batch_job_execution` VALUES (14, 2, 14, '2020-09-28 00:32:47', '2020-09-28 00:32:47', '2020-09-28 00:32:47', 'COMPLETED', 'COMPLETED', '', '2020-09-28 00:32:47', NULL);
INSERT INTO `batch_job_execution` VALUES (15, 2, 15, '2020-09-28 00:33:45', '2020-09-28 00:33:45', '2020-09-28 00:33:45', 'COMPLETED', 'COMPLETED', '', '2020-09-28 00:33:45', NULL);
INSERT INTO `batch_job_execution` VALUES (16, 2, 16, '2020-10-06 17:37:41', '2020-10-06 17:37:41', '2020-10-06 17:37:41', 'COMPLETED', 'COMPLETED', '', '2020-10-06 17:37:41', NULL);
INSERT INTO `batch_job_execution` VALUES (17, 2, 17, '2020-10-06 17:38:46', '2020-10-06 17:38:46', '2020-10-06 17:38:46', 'COMPLETED', 'COMPLETED', '', '2020-10-06 17:38:46', NULL);
INSERT INTO `batch_job_execution` VALUES (18, 2, 18, '2020-10-11 11:11:40', '2020-10-11 11:11:40', '2020-10-11 11:11:41', 'COMPLETED', 'COMPLETED', '', '2020-10-11 11:11:41', NULL);
INSERT INTO `batch_job_execution` VALUES (19, 2, 19, '2020-10-11 15:06:45', '2020-10-11 15:06:45', '2020-10-11 15:06:45', 'COMPLETED', 'COMPLETED', '', '2020-10-11 15:06:45', NULL);
INSERT INTO `batch_job_execution` VALUES (20, 2, 20, '2020-10-11 15:08:13', '2020-10-11 15:08:13', '2020-10-11 15:08:14', 'COMPLETED', 'COMPLETED', '', '2020-10-11 15:08:14', NULL);
INSERT INTO `batch_job_execution` VALUES (21, 2, 21, '2020-10-11 15:35:23', '2020-10-11 15:35:23', '2020-10-11 15:35:23', 'COMPLETED', 'COMPLETED', '', '2020-10-11 15:35:23', NULL);
INSERT INTO `batch_job_execution` VALUES (22, 2, 22, '2020-10-11 15:42:36', '2020-10-11 15:42:36', '2020-10-11 15:42:36', 'COMPLETED', 'COMPLETED', '', '2020-10-11 15:42:36', NULL);
INSERT INTO `batch_job_execution` VALUES (23, 2, 23, '2020-10-11 15:52:55', '2020-10-11 15:52:55', '2020-10-11 15:52:55', 'COMPLETED', 'COMPLETED', '', '2020-10-11 15:52:55', NULL);
INSERT INTO `batch_job_execution` VALUES (24, 2, 24, '2020-10-11 16:00:03', '2020-10-11 16:00:03', '2020-10-11 16:00:04', 'COMPLETED', 'COMPLETED', '', '2020-10-11 16:00:04', NULL);
INSERT INTO `batch_job_execution` VALUES (25, 2, 25, '2020-10-11 16:38:20', '2020-10-11 16:38:20', '2020-10-11 16:38:21', 'COMPLETED', 'COMPLETED', '', '2020-10-11 16:38:21', NULL);
INSERT INTO `batch_job_execution` VALUES (26, 2, 26, '2020-10-19 09:46:22', '2020-10-19 09:46:22', '2020-10-19 09:46:22', 'COMPLETED', 'COMPLETED', '', '2020-10-19 09:46:22', NULL);
INSERT INTO `batch_job_execution` VALUES (27, 2, 27, '2020-10-19 10:07:49', '2020-10-19 10:07:50', '2020-10-19 10:07:51', 'COMPLETED', 'COMPLETED', '', '2020-10-19 10:07:51', NULL);
INSERT INTO `batch_job_execution` VALUES (28, 2, 28, '2020-10-19 10:17:19', '2020-10-19 10:17:19', '2020-10-19 10:17:20', 'COMPLETED', 'COMPLETED', '', '2020-10-19 10:17:20', NULL);
INSERT INTO `batch_job_execution` VALUES (29, 2, 29, '2020-10-21 15:28:55', '2020-10-21 15:28:55', '2020-10-21 15:31:59', 'COMPLETED', 'COMPLETED', '', '2020-10-21 15:31:59', NULL);
INSERT INTO `batch_job_execution` VALUES (30, 2, 30, '2020-10-26 10:10:34', '2020-10-26 10:10:34', '2020-10-26 10:10:34', 'COMPLETED', 'COMPLETED', '', '2020-10-26 10:10:34', NULL);

-- ----------------------------
-- Table structure for batch_job_execution_context
-- ----------------------------
DROP TABLE IF EXISTS `batch_job_execution_context`;
CREATE TABLE `batch_job_execution_context`  (
  `JOB_EXECUTION_ID` bigint(20) NOT NULL,
  `SHORT_CONTEXT` varchar(2500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `SERIALIZED_CONTEXT` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`JOB_EXECUTION_ID`) USING BTREE,
  CONSTRAINT `JOB_EXEC_CTX_FK` FOREIGN KEY (`JOB_EXECUTION_ID`) REFERENCES `batch_job_execution` (`JOB_EXECUTION_ID`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of batch_job_execution_context
-- ----------------------------
INSERT INTO `batch_job_execution_context` VALUES (1, '{\"STATUS\":\"COMPLETED\",\"MODEL\":\"GENDER\"}', NULL);
INSERT INTO `batch_job_execution_context` VALUES (2, '{\"STATUS\":\"COMPLETED\",\"MODEL\":\"PROFESSIONAL_STATUS\"}', NULL);
INSERT INTO `batch_job_execution_context` VALUES (3, '{\"STATUS\":\"COMPLETED\",\"MODEL\":\"COUNTRY\"}', NULL);
INSERT INTO `batch_job_execution_context` VALUES (4, '{\"STATUS\":\"COMPLETED\",\"MODEL\":\"SKILL_TYPE\"}', NULL);
INSERT INTO `batch_job_execution_context` VALUES (5, '{\"STATUS\":\"COMPLETED\",\"MODEL\":\"DISCIPLINE\"}', NULL);
INSERT INTO `batch_job_execution_context` VALUES (6, '{\"STATUS\":\"COMPLETED\",\"MODEL\":\"LEVEL\"}', NULL);
INSERT INTO `batch_job_execution_context` VALUES (7, '{\"STATUS\":\"COMPLETED\",\"MODEL\":\"ACTIVITY_SECTOR\"}', NULL);
INSERT INTO `batch_job_execution_context` VALUES (8, '{\"STATUS\":\"COMPLETED\",\"MODEL\":\"ACTIVITY_AREA\"}', NULL);
INSERT INTO `batch_job_execution_context` VALUES (9, '{\"STATUS\":\"COMPLETED\",\"MODEL\":\"EDUCATION\"}', NULL);
INSERT INTO `batch_job_execution_context` VALUES (10, '{\"STATUS\":\"COMPLETED\",\"MODEL\":\"ESTABLISHMENT_TYPE\"}', NULL);
INSERT INTO `batch_job_execution_context` VALUES (11, '{\"STATUS\":\"COMPLETED\",\"MODEL\":\"FREQUENTED_CLASS\"}', NULL);
INSERT INTO `batch_job_execution_context` VALUES (12, '{\"STATUS\":\"COMPLETED\",\"MODEL\":\"DIPLOMA\"}', NULL);
INSERT INTO `batch_job_execution_context` VALUES (13, '{\"STATUS\":\"COMPLETED\",\"MODEL\":\"SPECIALTY\"}', NULL);
INSERT INTO `batch_job_execution_context` VALUES (14, '{\"STATUS\":\"COMPLETED\",\"MODEL\":\"TEACHING_AREA\"}', NULL);
INSERT INTO `batch_job_execution_context` VALUES (15, '{\"STATUS\":\"COMPLETED\",\"MODEL\":\"STUDY_LEVEL\"}', NULL);
INSERT INTO `batch_job_execution_context` VALUES (16, '{\"STATUS\":\"COMPLETED\",\"MODEL\":\"MEDIA_TYPE\"}', NULL);
INSERT INTO `batch_job_execution_context` VALUES (17, '{\"STATUS\":\"COMPLETED\",\"MODEL\":\"MEDIA_SUBTYPE\"}', NULL);
INSERT INTO `batch_job_execution_context` VALUES (18, '{\"STATUS\":\"COMPLETED\",\"MODEL\":\"STUDY_LEVEL\"}', NULL);
INSERT INTO `batch_job_execution_context` VALUES (19, '{\"STATUS\":\"COMPLETED\",\"MODEL\":\"ESTABLISHMENT_TYPE\"}', NULL);
INSERT INTO `batch_job_execution_context` VALUES (20, '{\"STATUS\":\"COMPLETED\",\"MODEL\":\"ACTIVITY_AREA\"}', NULL);
INSERT INTO `batch_job_execution_context` VALUES (21, '{\"STATUS\":\"COMPLETED\",\"MODEL\":\"STUDY_LEVEL\"}', NULL);
INSERT INTO `batch_job_execution_context` VALUES (22, '{\"STATUS\":\"COMPLETED\",\"MODEL\":\"DIPLOMA\"}', NULL);
INSERT INTO `batch_job_execution_context` VALUES (23, '{\"STATUS\":\"COMPLETED\",\"MODEL\":\"TEACHING_AREA\"}', NULL);
INSERT INTO `batch_job_execution_context` VALUES (24, '{\"STATUS\":\"COMPLETED\",\"MODEL\":\"STUDY_LEVEL\"}', NULL);
INSERT INTO `batch_job_execution_context` VALUES (25, '{\"STATUS\":\"COMPLETED\",\"MODEL\":\"TEACHING_AREA\"}', NULL);
INSERT INTO `batch_job_execution_context` VALUES (26, '{\"STATUS\":\"COMPLETED\",\"MODEL\":\"LEVEL\"}', NULL);
INSERT INTO `batch_job_execution_context` VALUES (27, '{\"STATUS\":\"COMPLETED\",\"MODEL\":\"DIPLOMA\"}', NULL);
INSERT INTO `batch_job_execution_context` VALUES (28, '{\"STATUS\":\"COMPLETED\",\"MODEL\":\"MEDIA_SUBTYPE\"}', NULL);
INSERT INTO `batch_job_execution_context` VALUES (29, '{\"STATUS\":\"COMPLETED\",\"MODEL\":\"TEACHING_AREA\"}', NULL);
INSERT INTO `batch_job_execution_context` VALUES (30, '{\"STATUS\":\"COMPLETED\",\"MODEL\":\"ACTIVITY_SECTOR\"}', NULL);

-- ----------------------------
-- Table structure for batch_job_execution_params
-- ----------------------------
DROP TABLE IF EXISTS `batch_job_execution_params`;
CREATE TABLE `batch_job_execution_params`  (
  `JOB_EXECUTION_ID` bigint(20) NOT NULL,
  `TYPE_CD` varchar(6) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `KEY_NAME` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `STRING_VAL` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `DATE_VAL` datetime(0) NULL DEFAULT NULL,
  `LONG_VAL` bigint(20) NULL DEFAULT NULL,
  `DOUBLE_VAL` double NULL DEFAULT NULL,
  `IDENTIFYING` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  INDEX `JOB_EXEC_PARAMS_FK`(`JOB_EXECUTION_ID`) USING BTREE,
  CONSTRAINT `JOB_EXEC_PARAMS_FK` FOREIGN KEY (`JOB_EXECUTION_ID`) REFERENCES `batch_job_execution` (`JOB_EXECUTION_ID`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of batch_job_execution_params
-- ----------------------------
INSERT INTO `batch_job_execution_params` VALUES (1, 'LONG', 'run.id', '', '1970-01-01 01:00:00', 1, 0, 'Y');
INSERT INTO `batch_job_execution_params` VALUES (2, 'LONG', 'run.id', '', '1970-01-01 01:00:00', 2, 0, 'Y');
INSERT INTO `batch_job_execution_params` VALUES (3, 'LONG', 'run.id', '', '1970-01-01 01:00:00', 3, 0, 'Y');
INSERT INTO `batch_job_execution_params` VALUES (4, 'LONG', 'run.id', '', '1970-01-01 01:00:00', 4, 0, 'Y');
INSERT INTO `batch_job_execution_params` VALUES (5, 'LONG', 'run.id', '', '1970-01-01 01:00:00', 5, 0, 'Y');
INSERT INTO `batch_job_execution_params` VALUES (6, 'LONG', 'run.id', '', '1970-01-01 01:00:00', 6, 0, 'Y');
INSERT INTO `batch_job_execution_params` VALUES (7, 'LONG', 'run.id', '', '1970-01-01 01:00:00', 7, 0, 'Y');
INSERT INTO `batch_job_execution_params` VALUES (8, 'LONG', 'run.id', '', '1970-01-01 01:00:00', 8, 0, 'Y');
INSERT INTO `batch_job_execution_params` VALUES (9, 'LONG', 'run.id', '', '1970-01-01 01:00:00', 9, 0, 'Y');
INSERT INTO `batch_job_execution_params` VALUES (10, 'LONG', 'run.id', '', '1970-01-01 01:00:00', 10, 0, 'Y');
INSERT INTO `batch_job_execution_params` VALUES (11, 'LONG', 'run.id', '', '1970-01-01 01:00:00', 11, 0, 'Y');
INSERT INTO `batch_job_execution_params` VALUES (12, 'LONG', 'run.id', '', '1970-01-01 01:00:00', 12, 0, 'Y');
INSERT INTO `batch_job_execution_params` VALUES (13, 'LONG', 'run.id', '', '1970-01-01 01:00:00', 13, 0, 'Y');
INSERT INTO `batch_job_execution_params` VALUES (14, 'LONG', 'run.id', '', '1970-01-01 01:00:00', 14, 0, 'Y');
INSERT INTO `batch_job_execution_params` VALUES (15, 'LONG', 'run.id', '', '1970-01-01 01:00:00', 15, 0, 'Y');
INSERT INTO `batch_job_execution_params` VALUES (16, 'LONG', 'run.id', '', '1970-01-01 01:00:00', 16, 0, 'Y');
INSERT INTO `batch_job_execution_params` VALUES (17, 'LONG', 'run.id', '', '1970-01-01 01:00:00', 17, 0, 'Y');
INSERT INTO `batch_job_execution_params` VALUES (18, 'LONG', 'run.id', '', '1970-01-01 01:00:00', 18, 0, 'Y');
INSERT INTO `batch_job_execution_params` VALUES (19, 'LONG', 'run.id', '', '1970-01-01 01:00:00', 19, 0, 'Y');
INSERT INTO `batch_job_execution_params` VALUES (20, 'LONG', 'run.id', '', '1970-01-01 01:00:00', 20, 0, 'Y');
INSERT INTO `batch_job_execution_params` VALUES (21, 'LONG', 'run.id', '', '1970-01-01 01:00:00', 21, 0, 'Y');
INSERT INTO `batch_job_execution_params` VALUES (22, 'LONG', 'run.id', '', '1970-01-01 01:00:00', 22, 0, 'Y');
INSERT INTO `batch_job_execution_params` VALUES (23, 'LONG', 'run.id', '', '1970-01-01 01:00:00', 23, 0, 'Y');
INSERT INTO `batch_job_execution_params` VALUES (24, 'LONG', 'run.id', '', '1970-01-01 01:00:00', 24, 0, 'Y');
INSERT INTO `batch_job_execution_params` VALUES (25, 'LONG', 'run.id', '', '1970-01-01 01:00:00', 25, 0, 'Y');
INSERT INTO `batch_job_execution_params` VALUES (26, 'LONG', 'run.id', '', '1970-01-01 01:00:00', 26, 0, 'Y');
INSERT INTO `batch_job_execution_params` VALUES (27, 'LONG', 'run.id', '', '1970-01-01 01:00:00', 27, 0, 'Y');
INSERT INTO `batch_job_execution_params` VALUES (28, 'LONG', 'run.id', '', '1970-01-01 01:00:00', 28, 0, 'Y');
INSERT INTO `batch_job_execution_params` VALUES (29, 'LONG', 'run.id', '', '1970-01-01 01:00:00', 29, 0, 'Y');
INSERT INTO `batch_job_execution_params` VALUES (30, 'LONG', 'run.id', '', '1970-01-01 01:00:00', 30, 0, 'Y');

-- ----------------------------
-- Table structure for batch_job_execution_seq
-- ----------------------------
DROP TABLE IF EXISTS `batch_job_execution_seq`;
CREATE TABLE `batch_job_execution_seq`  (
  `ID` bigint(20) NOT NULL,
  `UNIQUE_KEY` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  UNIQUE INDEX `UNIQUE_KEY_UN`(`UNIQUE_KEY`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of batch_job_execution_seq
-- ----------------------------
INSERT INTO `batch_job_execution_seq` VALUES (30, '0');

-- ----------------------------
-- Table structure for batch_job_instance
-- ----------------------------
DROP TABLE IF EXISTS `batch_job_instance`;
CREATE TABLE `batch_job_instance`  (
  `JOB_INSTANCE_ID` bigint(20) NOT NULL,
  `VERSION` bigint(20) NULL DEFAULT NULL,
  `JOB_NAME` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `JOB_KEY` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`JOB_INSTANCE_ID`) USING BTREE,
  UNIQUE INDEX `JOB_INST_UN`(`JOB_NAME`, `JOB_KEY`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of batch_job_instance
-- ----------------------------
INSERT INTO `batch_job_instance` VALUES (1, 0, 'populateObjectsJob', '853d3449e311f40366811cbefb3d93d7');
INSERT INTO `batch_job_instance` VALUES (2, 0, 'populateObjectsJob', 'e070bff4379694c0210a51d9f6c6a564');
INSERT INTO `batch_job_instance` VALUES (3, 0, 'populateObjectsJob', 'a3364faf893276dea0caacefbf618db5');
INSERT INTO `batch_job_instance` VALUES (4, 0, 'populateObjectsJob', '47c0a8118b74165a864b66d37c7b6cf5');
INSERT INTO `batch_job_instance` VALUES (5, 0, 'populateObjectsJob', 'ce148f5f9c2bf4dc9bd44a7a5f64204c');
INSERT INTO `batch_job_instance` VALUES (6, 0, 'populateObjectsJob', 'bd0034040292bc81e6ccac0e25d9a578');
INSERT INTO `batch_job_instance` VALUES (7, 0, 'populateObjectsJob', '597815c7e4ab1092c1b25130aae725cb');
INSERT INTO `batch_job_instance` VALUES (8, 0, 'populateObjectsJob', 'f55a96b11012be4fcfb6cf005435182d');
INSERT INTO `batch_job_instance` VALUES (9, 0, 'populateObjectsJob', '96a5ed9bac43e779455f3e71c0f64840');
INSERT INTO `batch_job_instance` VALUES (10, 0, 'populateObjectsJob', '1aac4f3e74894b78fa3ce5d8a25e1ef0');
INSERT INTO `batch_job_instance` VALUES (11, 0, 'populateObjectsJob', '604bbfc4c68cb1f903780c2853ad4801');
INSERT INTO `batch_job_instance` VALUES (12, 0, 'populateObjectsJob', '556ebe34220b4032509f2581356ba47c');
INSERT INTO `batch_job_instance` VALUES (13, 0, 'populateObjectsJob', 'edc440efb5ddd2a3b2622f16a12bf105');
INSERT INTO `batch_job_instance` VALUES (14, 0, 'populateObjectsJob', 'f3d5e568c384ee72cba8bc6a51057fe4');
INSERT INTO `batch_job_instance` VALUES (15, 0, 'populateObjectsJob', '378ef1ecb81cf9edac4ab119bdab9d9d');
INSERT INTO `batch_job_instance` VALUES (16, 0, 'populateObjectsJob', 'e073471cc312cadef424c3be7915c0af');
INSERT INTO `batch_job_instance` VALUES (17, 0, 'populateObjectsJob', '46ba78a99abf1e2fba4a8861749d7572');
INSERT INTO `batch_job_instance` VALUES (18, 0, 'populateObjectsJob', 'b88d31b704adf9f94fe9d4ccff795708');
INSERT INTO `batch_job_instance` VALUES (19, 0, 'populateObjectsJob', '64d4e6d635ee3ad949314224afce46c2');
INSERT INTO `batch_job_instance` VALUES (20, 0, 'populateObjectsJob', '75c16c09800a944220a789de10278de0');
INSERT INTO `batch_job_instance` VALUES (21, 0, 'populateObjectsJob', '1b759d32440acdcbf90da6919b5d16ad');
INSERT INTO `batch_job_instance` VALUES (22, 0, 'populateObjectsJob', '1f995cec4b562af773a2e473c369f069');
INSERT INTO `batch_job_instance` VALUES (23, 0, 'populateObjectsJob', '42106293a859255c2b210d04a51240ca');
INSERT INTO `batch_job_instance` VALUES (24, 0, 'populateObjectsJob', '9799b3a84f4d6f15a5e8c11360e7387b');
INSERT INTO `batch_job_instance` VALUES (25, 0, 'populateObjectsJob', '6eecb29840a845c35cfa9b2da21862f9');
INSERT INTO `batch_job_instance` VALUES (26, 0, 'populateObjectsJob', 'e465389b77512db6f30ed6a3b7a9682c');
INSERT INTO `batch_job_instance` VALUES (27, 0, 'populateObjectsJob', '19be35489361f0d498838921e450c4cb');
INSERT INTO `batch_job_instance` VALUES (28, 0, 'populateObjectsJob', '20744cb6ca7f8dc12940aa7fd8f89763');
INSERT INTO `batch_job_instance` VALUES (29, 0, 'populateObjectsJob', 'a3bcf78496166aaf18ec0c14120074d6');
INSERT INTO `batch_job_instance` VALUES (30, 0, 'populateObjectsJob', '890787fe3f8be2564cf02f516acdac28');

-- ----------------------------
-- Table structure for batch_job_seq
-- ----------------------------
DROP TABLE IF EXISTS `batch_job_seq`;
CREATE TABLE `batch_job_seq`  (
  `ID` bigint(20) NOT NULL,
  `UNIQUE_KEY` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  UNIQUE INDEX `UNIQUE_KEY_UN`(`UNIQUE_KEY`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of batch_job_seq
-- ----------------------------
INSERT INTO `batch_job_seq` VALUES (30, '0');

-- ----------------------------
-- Table structure for batch_step_execution
-- ----------------------------
DROP TABLE IF EXISTS `batch_step_execution`;
CREATE TABLE `batch_step_execution`  (
  `STEP_EXECUTION_ID` bigint(20) NOT NULL,
  `VERSION` bigint(20) NOT NULL,
  `STEP_NAME` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `JOB_EXECUTION_ID` bigint(20) NOT NULL,
  `START_TIME` datetime(0) NOT NULL,
  `END_TIME` datetime(0) NULL DEFAULT NULL,
  `STATUS` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `COMMIT_COUNT` bigint(20) NULL DEFAULT NULL,
  `READ_COUNT` bigint(20) NULL DEFAULT NULL,
  `FILTER_COUNT` bigint(20) NULL DEFAULT NULL,
  `WRITE_COUNT` bigint(20) NULL DEFAULT NULL,
  `READ_SKIP_COUNT` bigint(20) NULL DEFAULT NULL,
  `WRITE_SKIP_COUNT` bigint(20) NULL DEFAULT NULL,
  `PROCESS_SKIP_COUNT` bigint(20) NULL DEFAULT NULL,
  `ROLLBACK_COUNT` bigint(20) NULL DEFAULT NULL,
  `EXIT_CODE` varchar(2500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `EXIT_MESSAGE` varchar(2500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `LAST_UPDATED` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`STEP_EXECUTION_ID`) USING BTREE,
  INDEX `JOB_EXEC_STEP_FK`(`JOB_EXECUTION_ID`) USING BTREE,
  CONSTRAINT `JOB_EXEC_STEP_FK` FOREIGN KEY (`JOB_EXECUTION_ID`) REFERENCES `batch_job_execution` (`JOB_EXECUTION_ID`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of batch_step_execution
-- ----------------------------
INSERT INTO `batch_step_execution` VALUES (1, 3, 'stepFileControl', 1, '2020-09-27 21:06:07', '2020-09-27 21:06:07', 'COMPLETED', 1, 0, 0, 0, 0, 0, 0, 0, 'COMPLETED', '', '2020-09-27 21:06:07');
INSERT INTO `batch_step_execution` VALUES (2, 3, 'populateObjects', 1, '2020-09-27 21:06:07', '2020-09-27 21:06:08', 'COMPLETED', 1, 2, 0, 2, 0, 0, 0, 0, 'COMPLETED', '', '2020-09-27 21:06:08');
INSERT INTO `batch_step_execution` VALUES (3, 3, 'stepFileControl', 2, '2020-09-27 21:06:57', '2020-09-27 21:06:57', 'COMPLETED', 1, 0, 0, 0, 0, 0, 0, 0, 'COMPLETED', '', '2020-09-27 21:06:57');
INSERT INTO `batch_step_execution` VALUES (4, 3, 'populateObjects', 2, '2020-09-27 21:06:57', '2020-09-27 21:06:58', 'COMPLETED', 1, 3, 0, 3, 0, 0, 0, 0, 'COMPLETED', '', '2020-09-27 21:06:58');
INSERT INTO `batch_step_execution` VALUES (5, 3, 'stepFileControl', 3, '2020-09-27 21:07:48', '2020-09-27 21:07:48', 'COMPLETED', 1, 0, 0, 0, 0, 0, 0, 0, 'COMPLETED', '', '2020-09-27 21:07:48');
INSERT INTO `batch_step_execution` VALUES (6, 5, 'populateObjects', 3, '2020-09-27 21:07:48', '2020-09-27 21:07:49', 'COMPLETED', 3, 249, 0, 249, 0, 0, 0, 0, 'COMPLETED', '', '2020-09-27 21:07:49');
INSERT INTO `batch_step_execution` VALUES (7, 3, 'stepFileControl', 4, '2020-09-27 21:15:38', '2020-09-27 21:15:38', 'COMPLETED', 1, 0, 0, 0, 0, 0, 0, 0, 'COMPLETED', '', '2020-09-27 21:15:38');
INSERT INTO `batch_step_execution` VALUES (8, 3, 'populateObjects', 4, '2020-09-27 21:15:38', '2020-09-27 21:15:38', 'COMPLETED', 1, 2, 0, 2, 0, 0, 0, 0, 'COMPLETED', '', '2020-09-27 21:15:38');
INSERT INTO `batch_step_execution` VALUES (9, 3, 'stepFileControl', 5, '2020-09-27 21:16:19', '2020-09-27 21:16:19', 'COMPLETED', 1, 0, 0, 0, 0, 0, 0, 0, 'COMPLETED', '', '2020-09-27 21:16:19');
INSERT INTO `batch_step_execution` VALUES (10, 4, 'populateObjects', 5, '2020-09-27 21:16:20', '2020-09-27 21:16:20', 'COMPLETED', 2, 142, 0, 142, 0, 0, 0, 0, 'COMPLETED', '', '2020-09-27 21:16:20');
INSERT INTO `batch_step_execution` VALUES (11, 3, 'stepFileControl', 6, '2020-09-27 21:17:28', '2020-09-27 21:17:28', 'COMPLETED', 1, 0, 0, 0, 0, 0, 0, 0, 'COMPLETED', '', '2020-09-27 21:17:28');
INSERT INTO `batch_step_execution` VALUES (12, 3, 'populateObjects', 6, '2020-09-27 21:17:28', '2020-09-27 21:17:29', 'COMPLETED', 1, 4, 0, 4, 0, 0, 0, 0, 'COMPLETED', '', '2020-09-27 21:17:29');
INSERT INTO `batch_step_execution` VALUES (13, 3, 'stepFileControl', 7, '2020-09-27 21:20:32', '2020-09-27 21:20:32', 'COMPLETED', 1, 0, 0, 0, 0, 0, 0, 0, 'COMPLETED', '', '2020-09-27 21:20:32');
INSERT INTO `batch_step_execution` VALUES (14, 3, 'populateObjects', 7, '2020-09-27 21:20:32', '2020-09-27 21:20:32', 'COMPLETED', 1, 25, 0, 25, 0, 0, 0, 0, 'COMPLETED', '', '2020-09-27 21:20:32');
INSERT INTO `batch_step_execution` VALUES (15, 3, 'stepFileControl', 8, '2020-09-27 21:22:37', '2020-09-27 21:22:37', 'COMPLETED', 1, 0, 0, 0, 0, 0, 0, 0, 'COMPLETED', '', '2020-09-27 21:22:37');
INSERT INTO `batch_step_execution` VALUES (16, 3, 'populateObjects', 8, '2020-09-27 21:22:37', '2020-09-27 21:22:38', 'COMPLETED', 1, 2, 0, 2, 0, 0, 0, 0, 'COMPLETED', '', '2020-09-27 21:22:38');
INSERT INTO `batch_step_execution` VALUES (17, 3, 'stepFileControl', 9, '2020-09-27 21:25:39', '2020-09-27 21:25:39', 'COMPLETED', 1, 0, 0, 0, 0, 0, 0, 0, 'COMPLETED', '', '2020-09-27 21:25:39');
INSERT INTO `batch_step_execution` VALUES (18, 3, 'populateObjects', 9, '2020-09-27 21:25:39', '2020-09-27 21:25:39', 'COMPLETED', 1, 2, 0, 2, 0, 0, 0, 0, 'COMPLETED', '', '2020-09-27 21:25:39');
INSERT INTO `batch_step_execution` VALUES (19, 3, 'stepFileControl', 10, '2020-09-27 21:26:33', '2020-09-27 21:26:33', 'COMPLETED', 1, 0, 0, 0, 0, 0, 0, 0, 'COMPLETED', '', '2020-09-27 21:26:33');
INSERT INTO `batch_step_execution` VALUES (20, 3, 'populateObjects', 10, '2020-09-27 21:26:33', '2020-09-27 21:26:33', 'COMPLETED', 1, 13, 0, 13, 0, 0, 0, 0, 'COMPLETED', '', '2020-09-27 21:26:33');
INSERT INTO `batch_step_execution` VALUES (21, 3, 'stepFileControl', 11, '2020-09-27 21:27:58', '2020-09-27 21:27:58', 'COMPLETED', 1, 0, 0, 0, 0, 0, 0, 0, 'COMPLETED', '', '2020-09-27 21:27:58');
INSERT INTO `batch_step_execution` VALUES (22, 3, 'populateObjects', 11, '2020-09-27 21:27:58', '2020-09-27 21:27:59', 'COMPLETED', 1, 66, 0, 66, 0, 0, 0, 0, 'COMPLETED', '', '2020-09-27 21:27:59');
INSERT INTO `batch_step_execution` VALUES (23, 3, 'stepFileControl', 12, '2020-09-27 21:30:54', '2020-09-27 21:30:54', 'COMPLETED', 1, 0, 0, 0, 0, 0, 0, 0, 'COMPLETED', '', '2020-09-27 21:30:54');
INSERT INTO `batch_step_execution` VALUES (24, 3, 'populateObjects', 12, '2020-09-27 21:30:54', '2020-09-27 21:30:54', 'COMPLETED', 1, 34, 0, 34, 0, 0, 0, 0, 'COMPLETED', '', '2020-09-27 21:30:54');
INSERT INTO `batch_step_execution` VALUES (25, 3, 'stepFileControl', 13, '2020-09-27 21:56:40', '2020-09-27 21:56:40', 'COMPLETED', 1, 0, 0, 0, 0, 0, 0, 0, 'COMPLETED', '', '2020-09-27 21:56:40');
INSERT INTO `batch_step_execution` VALUES (26, 3, 'populateObjects', 13, '2020-09-27 21:56:40', '2020-09-27 21:56:40', 'COMPLETED', 1, 4, 0, 4, 0, 0, 0, 0, 'COMPLETED', '', '2020-09-27 21:56:40');
INSERT INTO `batch_step_execution` VALUES (27, 3, 'stepFileControl', 14, '2020-09-28 00:32:47', '2020-09-28 00:32:47', 'COMPLETED', 1, 0, 0, 0, 0, 0, 0, 0, 'COMPLETED', '', '2020-09-28 00:32:47');
INSERT INTO `batch_step_execution` VALUES (28, 3, 'populateObjects', 14, '2020-09-28 00:32:47', '2020-09-28 00:32:47', 'COMPLETED', 1, 4, 0, 4, 0, 0, 0, 0, 'COMPLETED', '', '2020-09-28 00:32:47');
INSERT INTO `batch_step_execution` VALUES (29, 3, 'stepFileControl', 15, '2020-09-28 00:33:45', '2020-09-28 00:33:45', 'COMPLETED', 1, 0, 0, 0, 0, 0, 0, 0, 'COMPLETED', '', '2020-09-28 00:33:45');
INSERT INTO `batch_step_execution` VALUES (30, 3, 'populateObjects', 15, '2020-09-28 00:33:45', '2020-09-28 00:33:45', 'COMPLETED', 1, 4, 0, 4, 0, 0, 0, 0, 'COMPLETED', '', '2020-09-28 00:33:45');
INSERT INTO `batch_step_execution` VALUES (31, 3, 'stepFileControl', 16, '2020-10-06 17:37:41', '2020-10-06 17:37:41', 'COMPLETED', 1, 0, 0, 0, 0, 0, 0, 0, 'COMPLETED', '', '2020-10-06 17:37:41');
INSERT INTO `batch_step_execution` VALUES (32, 3, 'populateObjects', 16, '2020-10-06 17:37:41', '2020-10-06 17:37:41', 'COMPLETED', 1, 6, 0, 6, 0, 0, 0, 0, 'COMPLETED', '', '2020-10-06 17:37:41');
INSERT INTO `batch_step_execution` VALUES (33, 3, 'stepFileControl', 17, '2020-10-06 17:38:46', '2020-10-06 17:38:46', 'COMPLETED', 1, 0, 0, 0, 0, 0, 0, 0, 'COMPLETED', '', '2020-10-06 17:38:46');
INSERT INTO `batch_step_execution` VALUES (34, 3, 'populateObjects', 17, '2020-10-06 17:38:46', '2020-10-06 17:38:46', 'COMPLETED', 1, 26, 0, 26, 0, 0, 0, 0, 'COMPLETED', '', '2020-10-06 17:38:46');
INSERT INTO `batch_step_execution` VALUES (35, 3, 'stepFileControl', 18, '2020-10-11 11:11:40', '2020-10-11 11:11:40', 'COMPLETED', 1, 0, 0, 0, 0, 0, 0, 0, 'COMPLETED', '', '2020-10-11 11:11:40');
INSERT INTO `batch_step_execution` VALUES (36, 3, 'populateObjects', 18, '2020-10-11 11:11:40', '2020-10-11 11:11:41', 'COMPLETED', 1, 66, 0, 66, 0, 0, 0, 0, 'COMPLETED', '', '2020-10-11 11:11:41');
INSERT INTO `batch_step_execution` VALUES (37, 3, 'stepFileControl', 19, '2020-10-11 15:06:45', '2020-10-11 15:06:45', 'COMPLETED', 1, 0, 0, 0, 0, 0, 0, 0, 'COMPLETED', '', '2020-10-11 15:06:45');
INSERT INTO `batch_step_execution` VALUES (38, 3, 'populateObjects', 19, '2020-10-11 15:06:45', '2020-10-11 15:06:45', 'COMPLETED', 1, 13, 0, 13, 0, 0, 0, 0, 'COMPLETED', '', '2020-10-11 15:06:45');
INSERT INTO `batch_step_execution` VALUES (39, 3, 'stepFileControl', 20, '2020-10-11 15:08:13', '2020-10-11 15:08:13', 'COMPLETED', 1, 0, 0, 0, 0, 0, 0, 0, 'COMPLETED', '', '2020-10-11 15:08:13');
INSERT INTO `batch_step_execution` VALUES (40, 3, 'populateObjects', 20, '2020-10-11 15:08:13', '2020-10-11 15:08:14', 'COMPLETED', 1, 51, 0, 51, 0, 0, 0, 0, 'COMPLETED', '', '2020-10-11 15:08:14');
INSERT INTO `batch_step_execution` VALUES (41, 3, 'stepFileControl', 21, '2020-10-11 15:35:23', '2020-10-11 15:35:23', 'COMPLETED', 1, 0, 0, 0, 0, 0, 0, 0, 'COMPLETED', '', '2020-10-11 15:35:23');
INSERT INTO `batch_step_execution` VALUES (42, 3, 'populateObjects', 21, '2020-10-11 15:35:23', '2020-10-11 15:35:23', 'COMPLETED', 1, 66, 0, 66, 0, 0, 0, 0, 'COMPLETED', '', '2020-10-11 15:35:23');
INSERT INTO `batch_step_execution` VALUES (43, 3, 'stepFileControl', 22, '2020-10-11 15:42:36', '2020-10-11 15:42:36', 'COMPLETED', 1, 0, 0, 0, 0, 0, 0, 0, 'COMPLETED', '', '2020-10-11 15:42:36');
INSERT INTO `batch_step_execution` VALUES (44, 3, 'populateObjects', 22, '2020-10-11 15:42:36', '2020-10-11 15:42:36', 'COMPLETED', 1, 34, 0, 34, 0, 0, 0, 0, 'COMPLETED', '', '2020-10-11 15:42:36');
INSERT INTO `batch_step_execution` VALUES (45, 3, 'stepFileControl', 23, '2020-10-11 15:52:55', '2020-10-11 15:52:55', 'COMPLETED', 1, 0, 0, 0, 0, 0, 0, 0, 'COMPLETED', '', '2020-10-11 15:52:55');
INSERT INTO `batch_step_execution` VALUES (46, 3, 'populateObjects', 23, '2020-10-11 15:52:55', '2020-10-11 15:52:55', 'COMPLETED', 1, 4, 0, 4, 0, 0, 0, 0, 'COMPLETED', '', '2020-10-11 15:52:55');
INSERT INTO `batch_step_execution` VALUES (47, 3, 'stepFileControl', 24, '2020-10-11 16:00:03', '2020-10-11 16:00:03', 'COMPLETED', 1, 0, 0, 0, 0, 0, 0, 0, 'COMPLETED', '', '2020-10-11 16:00:03');
INSERT INTO `batch_step_execution` VALUES (48, 3, 'populateObjects', 24, '2020-10-11 16:00:03', '2020-10-11 16:00:04', 'COMPLETED', 1, 50, 0, 50, 0, 0, 0, 0, 'COMPLETED', '', '2020-10-11 16:00:04');
INSERT INTO `batch_step_execution` VALUES (49, 3, 'stepFileControl', 25, '2020-10-11 16:38:20', '2020-10-11 16:38:20', 'COMPLETED', 1, 0, 0, 0, 0, 0, 0, 0, 'COMPLETED', '', '2020-10-11 16:38:20');
INSERT INTO `batch_step_execution` VALUES (50, 4, 'populateObjects', 25, '2020-10-11 16:38:20', '2020-10-11 16:38:21', 'COMPLETED', 2, 124, 0, 124, 0, 0, 0, 0, 'COMPLETED', '', '2020-10-11 16:38:21');
INSERT INTO `batch_step_execution` VALUES (51, 3, 'stepFileControl', 26, '2020-10-19 09:46:22', '2020-10-19 09:46:22', 'COMPLETED', 1, 0, 0, 0, 0, 0, 0, 0, 'COMPLETED', '', '2020-10-19 09:46:22');
INSERT INTO `batch_step_execution` VALUES (52, 3, 'populateObjects', 26, '2020-10-19 09:46:22', '2020-10-19 09:46:22', 'COMPLETED', 1, 4, 0, 4, 0, 0, 0, 0, 'COMPLETED', '', '2020-10-19 09:46:22');
INSERT INTO `batch_step_execution` VALUES (53, 3, 'stepFileControl', 27, '2020-10-19 10:07:50', '2020-10-19 10:07:50', 'COMPLETED', 1, 0, 0, 0, 0, 0, 0, 0, 'COMPLETED', '', '2020-10-19 10:07:50');
INSERT INTO `batch_step_execution` VALUES (54, 3, 'populateObjects', 27, '2020-10-19 10:07:50', '2020-10-19 10:07:51', 'COMPLETED', 1, 35, 0, 35, 0, 0, 0, 0, 'COMPLETED', '', '2020-10-19 10:07:51');
INSERT INTO `batch_step_execution` VALUES (55, 3, 'stepFileControl', 28, '2020-10-19 10:17:19', '2020-10-19 10:17:19', 'COMPLETED', 1, 0, 0, 0, 0, 0, 0, 0, 'COMPLETED', '', '2020-10-19 10:17:19');
INSERT INTO `batch_step_execution` VALUES (56, 3, 'populateObjects', 28, '2020-10-19 10:17:19', '2020-10-19 10:17:20', 'COMPLETED', 1, 27, 0, 27, 0, 0, 0, 0, 'COMPLETED', '', '2020-10-19 10:17:20');
INSERT INTO `batch_step_execution` VALUES (57, 3, 'stepFileControl', 29, '2020-10-21 15:28:55', '2020-10-21 15:28:55', 'COMPLETED', 1, 0, 0, 0, 0, 0, 0, 0, 'COMPLETED', '', '2020-10-21 15:28:55');
INSERT INTO `batch_step_execution` VALUES (58, 4, 'populateObjects', 29, '2020-10-21 15:28:55', '2020-10-21 15:31:59', 'COMPLETED', 2, 124, 0, 124, 0, 0, 0, 0, 'COMPLETED', '', '2020-10-21 15:31:59');
INSERT INTO `batch_step_execution` VALUES (59, 3, 'stepFileControl', 30, '2020-10-26 10:10:34', '2020-10-26 10:10:34', 'COMPLETED', 1, 0, 0, 0, 0, 0, 0, 0, 'COMPLETED', '', '2020-10-26 10:10:34');
INSERT INTO `batch_step_execution` VALUES (60, 3, 'populateObjects', 30, '2020-10-26 10:10:34', '2020-10-26 10:10:34', 'COMPLETED', 1, 41, 0, 41, 0, 0, 0, 0, 'COMPLETED', '', '2020-10-26 10:10:34');

-- ----------------------------
-- Table structure for batch_step_execution_context
-- ----------------------------
DROP TABLE IF EXISTS `batch_step_execution_context`;
CREATE TABLE `batch_step_execution_context`  (
  `STEP_EXECUTION_ID` bigint(20) NOT NULL,
  `SHORT_CONTEXT` varchar(2500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `SERIALIZED_CONTEXT` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`STEP_EXECUTION_ID`) USING BTREE,
  CONSTRAINT `STEP_EXEC_CTX_FK` FOREIGN KEY (`STEP_EXECUTION_ID`) REFERENCES `batch_step_execution` (`STEP_EXECUTION_ID`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of batch_step_execution_context
-- ----------------------------
INSERT INTO `batch_step_execution_context` VALUES (1, '{\"batch.taskletType\":\"com.yamo.skillsmates.populate.tasklet.FileControlTasklet\",\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}', NULL);
INSERT INTO `batch_step_execution_context` VALUES (2, '{\"batch.taskletType\":\"org.springframework.batch.core.step.item.ChunkOrientedTasklet\",\"FlatFileItemReader.read.count\":3,\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}', NULL);
INSERT INTO `batch_step_execution_context` VALUES (3, '{\"batch.taskletType\":\"com.yamo.skillsmates.populate.tasklet.FileControlTasklet\",\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}', NULL);
INSERT INTO `batch_step_execution_context` VALUES (4, '{\"batch.taskletType\":\"org.springframework.batch.core.step.item.ChunkOrientedTasklet\",\"FlatFileItemReader.read.count\":4,\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}', NULL);
INSERT INTO `batch_step_execution_context` VALUES (5, '{\"batch.taskletType\":\"com.yamo.skillsmates.populate.tasklet.FileControlTasklet\",\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}', NULL);
INSERT INTO `batch_step_execution_context` VALUES (6, '{\"batch.taskletType\":\"org.springframework.batch.core.step.item.ChunkOrientedTasklet\",\"FlatFileItemReader.read.count\":250,\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}', NULL);
INSERT INTO `batch_step_execution_context` VALUES (7, '{\"batch.taskletType\":\"com.yamo.skillsmates.populate.tasklet.FileControlTasklet\",\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}', NULL);
INSERT INTO `batch_step_execution_context` VALUES (8, '{\"batch.taskletType\":\"org.springframework.batch.core.step.item.ChunkOrientedTasklet\",\"FlatFileItemReader.read.count\":3,\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}', NULL);
INSERT INTO `batch_step_execution_context` VALUES (9, '{\"batch.taskletType\":\"com.yamo.skillsmates.populate.tasklet.FileControlTasklet\",\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}', NULL);
INSERT INTO `batch_step_execution_context` VALUES (10, '{\"batch.taskletType\":\"org.springframework.batch.core.step.item.ChunkOrientedTasklet\",\"FlatFileItemReader.read.count\":143,\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}', NULL);
INSERT INTO `batch_step_execution_context` VALUES (11, '{\"batch.taskletType\":\"com.yamo.skillsmates.populate.tasklet.FileControlTasklet\",\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}', NULL);
INSERT INTO `batch_step_execution_context` VALUES (12, '{\"batch.taskletType\":\"org.springframework.batch.core.step.item.ChunkOrientedTasklet\",\"FlatFileItemReader.read.count\":5,\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}', NULL);
INSERT INTO `batch_step_execution_context` VALUES (13, '{\"batch.taskletType\":\"com.yamo.skillsmates.populate.tasklet.FileControlTasklet\",\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}', NULL);
INSERT INTO `batch_step_execution_context` VALUES (14, '{\"batch.taskletType\":\"org.springframework.batch.core.step.item.ChunkOrientedTasklet\",\"FlatFileItemReader.read.count\":26,\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}', NULL);
INSERT INTO `batch_step_execution_context` VALUES (15, '{\"batch.taskletType\":\"com.yamo.skillsmates.populate.tasklet.FileControlTasklet\",\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}', NULL);
INSERT INTO `batch_step_execution_context` VALUES (16, '{\"batch.taskletType\":\"org.springframework.batch.core.step.item.ChunkOrientedTasklet\",\"FlatFileItemReader.read.count\":3,\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}', NULL);
INSERT INTO `batch_step_execution_context` VALUES (17, '{\"batch.taskletType\":\"com.yamo.skillsmates.populate.tasklet.FileControlTasklet\",\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}', NULL);
INSERT INTO `batch_step_execution_context` VALUES (18, '{\"batch.taskletType\":\"org.springframework.batch.core.step.item.ChunkOrientedTasklet\",\"FlatFileItemReader.read.count\":3,\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}', NULL);
INSERT INTO `batch_step_execution_context` VALUES (19, '{\"batch.taskletType\":\"com.yamo.skillsmates.populate.tasklet.FileControlTasklet\",\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}', NULL);
INSERT INTO `batch_step_execution_context` VALUES (20, '{\"batch.taskletType\":\"org.springframework.batch.core.step.item.ChunkOrientedTasklet\",\"FlatFileItemReader.read.count\":14,\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}', NULL);
INSERT INTO `batch_step_execution_context` VALUES (21, '{\"batch.taskletType\":\"com.yamo.skillsmates.populate.tasklet.FileControlTasklet\",\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}', NULL);
INSERT INTO `batch_step_execution_context` VALUES (22, '{\"batch.taskletType\":\"org.springframework.batch.core.step.item.ChunkOrientedTasklet\",\"FlatFileItemReader.read.count\":67,\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}', NULL);
INSERT INTO `batch_step_execution_context` VALUES (23, '{\"batch.taskletType\":\"com.yamo.skillsmates.populate.tasklet.FileControlTasklet\",\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}', NULL);
INSERT INTO `batch_step_execution_context` VALUES (24, '{\"batch.taskletType\":\"org.springframework.batch.core.step.item.ChunkOrientedTasklet\",\"FlatFileItemReader.read.count\":35,\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}', NULL);
INSERT INTO `batch_step_execution_context` VALUES (25, '{\"batch.taskletType\":\"com.yamo.skillsmates.populate.tasklet.FileControlTasklet\",\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}', NULL);
INSERT INTO `batch_step_execution_context` VALUES (26, '{\"batch.taskletType\":\"org.springframework.batch.core.step.item.ChunkOrientedTasklet\",\"FlatFileItemReader.read.count\":5,\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}', NULL);
INSERT INTO `batch_step_execution_context` VALUES (27, '{\"batch.taskletType\":\"com.yamo.skillsmates.populate.tasklet.FileControlTasklet\",\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}', NULL);
INSERT INTO `batch_step_execution_context` VALUES (28, '{\"batch.taskletType\":\"org.springframework.batch.core.step.item.ChunkOrientedTasklet\",\"FlatFileItemReader.read.count\":5,\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}', NULL);
INSERT INTO `batch_step_execution_context` VALUES (29, '{\"batch.taskletType\":\"com.yamo.skillsmates.populate.tasklet.FileControlTasklet\",\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}', NULL);
INSERT INTO `batch_step_execution_context` VALUES (30, '{\"batch.taskletType\":\"org.springframework.batch.core.step.item.ChunkOrientedTasklet\",\"FlatFileItemReader.read.count\":5,\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}', NULL);
INSERT INTO `batch_step_execution_context` VALUES (31, '{\"batch.taskletType\":\"com.yamo.skillsmates.populate.tasklet.FileControlTasklet\",\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}', NULL);
INSERT INTO `batch_step_execution_context` VALUES (32, '{\"batch.taskletType\":\"org.springframework.batch.core.step.item.ChunkOrientedTasklet\",\"FlatFileItemReader.read.count\":7,\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}', NULL);
INSERT INTO `batch_step_execution_context` VALUES (33, '{\"batch.taskletType\":\"com.yamo.skillsmates.populate.tasklet.FileControlTasklet\",\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}', NULL);
INSERT INTO `batch_step_execution_context` VALUES (34, '{\"batch.taskletType\":\"org.springframework.batch.core.step.item.ChunkOrientedTasklet\",\"FlatFileItemReader.read.count\":27,\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}', NULL);
INSERT INTO `batch_step_execution_context` VALUES (35, '{\"batch.taskletType\":\"com.yamo.skillsmates.populate.tasklet.FileControlTasklet\",\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}', NULL);
INSERT INTO `batch_step_execution_context` VALUES (36, '{\"batch.taskletType\":\"org.springframework.batch.core.step.item.ChunkOrientedTasklet\",\"FlatFileItemReader.read.count\":67,\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}', NULL);
INSERT INTO `batch_step_execution_context` VALUES (37, '{\"batch.taskletType\":\"com.yamo.skillsmates.populate.tasklet.FileControlTasklet\",\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}', NULL);
INSERT INTO `batch_step_execution_context` VALUES (38, '{\"batch.taskletType\":\"org.springframework.batch.core.step.item.ChunkOrientedTasklet\",\"FlatFileItemReader.read.count\":14,\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}', NULL);
INSERT INTO `batch_step_execution_context` VALUES (39, '{\"batch.taskletType\":\"com.yamo.skillsmates.populate.tasklet.FileControlTasklet\",\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}', NULL);
INSERT INTO `batch_step_execution_context` VALUES (40, '{\"batch.taskletType\":\"org.springframework.batch.core.step.item.ChunkOrientedTasklet\",\"FlatFileItemReader.read.count\":52,\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}', NULL);
INSERT INTO `batch_step_execution_context` VALUES (41, '{\"batch.taskletType\":\"com.yamo.skillsmates.populate.tasklet.FileControlTasklet\",\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}', NULL);
INSERT INTO `batch_step_execution_context` VALUES (42, '{\"batch.taskletType\":\"org.springframework.batch.core.step.item.ChunkOrientedTasklet\",\"FlatFileItemReader.read.count\":67,\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}', NULL);
INSERT INTO `batch_step_execution_context` VALUES (43, '{\"batch.taskletType\":\"com.yamo.skillsmates.populate.tasklet.FileControlTasklet\",\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}', NULL);
INSERT INTO `batch_step_execution_context` VALUES (44, '{\"batch.taskletType\":\"org.springframework.batch.core.step.item.ChunkOrientedTasklet\",\"FlatFileItemReader.read.count\":35,\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}', NULL);
INSERT INTO `batch_step_execution_context` VALUES (45, '{\"batch.taskletType\":\"com.yamo.skillsmates.populate.tasklet.FileControlTasklet\",\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}', NULL);
INSERT INTO `batch_step_execution_context` VALUES (46, '{\"batch.taskletType\":\"org.springframework.batch.core.step.item.ChunkOrientedTasklet\",\"FlatFileItemReader.read.count\":5,\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}', NULL);
INSERT INTO `batch_step_execution_context` VALUES (47, '{\"batch.taskletType\":\"com.yamo.skillsmates.populate.tasklet.FileControlTasklet\",\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}', NULL);
INSERT INTO `batch_step_execution_context` VALUES (48, '{\"batch.taskletType\":\"org.springframework.batch.core.step.item.ChunkOrientedTasklet\",\"FlatFileItemReader.read.count\":51,\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}', NULL);
INSERT INTO `batch_step_execution_context` VALUES (49, '{\"batch.taskletType\":\"com.yamo.skillsmates.populate.tasklet.FileControlTasklet\",\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}', NULL);
INSERT INTO `batch_step_execution_context` VALUES (50, '{\"batch.taskletType\":\"org.springframework.batch.core.step.item.ChunkOrientedTasklet\",\"FlatFileItemReader.read.count\":125,\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}', NULL);
INSERT INTO `batch_step_execution_context` VALUES (51, '{\"batch.taskletType\":\"com.yamo.skillsmates.populate.tasklet.FileControlTasklet\",\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}', NULL);
INSERT INTO `batch_step_execution_context` VALUES (52, '{\"batch.taskletType\":\"org.springframework.batch.core.step.item.ChunkOrientedTasklet\",\"FlatFileItemReader.read.count\":5,\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}', NULL);
INSERT INTO `batch_step_execution_context` VALUES (53, '{\"batch.taskletType\":\"com.yamo.skillsmates.populate.tasklet.FileControlTasklet\",\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}', NULL);
INSERT INTO `batch_step_execution_context` VALUES (54, '{\"batch.taskletType\":\"org.springframework.batch.core.step.item.ChunkOrientedTasklet\",\"FlatFileItemReader.read.count\":36,\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}', NULL);
INSERT INTO `batch_step_execution_context` VALUES (55, '{\"batch.taskletType\":\"com.yamo.skillsmates.populate.tasklet.FileControlTasklet\",\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}', NULL);
INSERT INTO `batch_step_execution_context` VALUES (56, '{\"batch.taskletType\":\"org.springframework.batch.core.step.item.ChunkOrientedTasklet\",\"FlatFileItemReader.read.count\":28,\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}', NULL);
INSERT INTO `batch_step_execution_context` VALUES (57, '{\"batch.taskletType\":\"com.yamo.skillsmates.populate.tasklet.FileControlTasklet\",\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}', NULL);
INSERT INTO `batch_step_execution_context` VALUES (58, '{\"batch.taskletType\":\"org.springframework.batch.core.step.item.ChunkOrientedTasklet\",\"FlatFileItemReader.read.count\":125,\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}', NULL);
INSERT INTO `batch_step_execution_context` VALUES (59, '{\"batch.taskletType\":\"com.yamo.skillsmates.populate.tasklet.FileControlTasklet\",\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}', NULL);
INSERT INTO `batch_step_execution_context` VALUES (60, '{\"batch.taskletType\":\"org.springframework.batch.core.step.item.ChunkOrientedTasklet\",\"FlatFileItemReader.read.count\":42,\"batch.stepType\":\"org.springframework.batch.core.step.tasklet.TaskletStep\"}', NULL);

-- ----------------------------
-- Table structure for batch_step_execution_seq
-- ----------------------------
DROP TABLE IF EXISTS `batch_step_execution_seq`;
CREATE TABLE `batch_step_execution_seq`  (
  `ID` bigint(20) NOT NULL,
  `UNIQUE_KEY` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  UNIQUE INDEX `UNIQUE_KEY_UN`(`UNIQUE_KEY`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of batch_step_execution_seq
-- ----------------------------
INSERT INTO `batch_step_execution_seq` VALUES (60, '0');

-- ----------------------------
-- Table structure for comment
-- ----------------------------
DROP TABLE IF EXISTS `comment`;
CREATE TABLE `comment`  (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `active` bit(1) NULL DEFAULT NULL,
  `created_at` datetime(0) NOT NULL,
  `deleted` bit(1) NULL DEFAULT NULL,
  `modified_at` datetime(0) NULL DEFAULT NULL,
  `revision` bigint(20) NULL DEFAULT NULL,
  `body` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `account` int(11) NULL DEFAULT NULL,
  `post` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_br19krw6o9vo598yx94h63wxd`(`id_server`) USING BTREE,
  INDEX `FKg4pifdpsrn19a8hubl96s2ncp`(`account`) USING BTREE,
  INDEX `FKomrdwc0ub3x7hvvlyu6htn8ti`(`post`) USING BTREE,
  CONSTRAINT `FKg4pifdpsrn19a8hubl96s2ncp` FOREIGN KEY (`account`) REFERENCES `account` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FKomrdwc0ub3x7hvvlyu6htn8ti` FOREIGN KEY (`post`) REFERENCES `post` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of comment
-- ----------------------------
INSERT INTO `comment` VALUES (1356, '241779346659800', b'1', '2020-10-24 13:57:01', b'0', NULL, 0, 'good document', 1, 1173);
INSERT INTO `comment` VALUES (1357, '241861771736100', b'1', '2020-10-24 13:58:24', b'0', NULL, 0, 'derivees des fonctions usuelles', 1, 1173);
INSERT INTO `comment` VALUES (1366, '91041163267900', b'1', '2020-10-26 09:34:58', b'0', NULL, 0, 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one ', 1, 1365);
INSERT INTO `comment` VALUES (1422, '433820495091600', b'1', '2020-11-01 10:09:45', b'0', NULL, 0, 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable.', 1, 1363);
INSERT INTO `comment` VALUES (1443, '435412363987500', b'1', '2020-11-01 10:36:17', b'1', NULL, 0, ' If you are going to use a passage of Lorem Ipsum', 1, 1365);
INSERT INTO `comment` VALUES (1444, '439883089144200', b'1', '2020-11-01 11:50:48', b'0', NULL, 0, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua', 1, 1365);
INSERT INTO `comment` VALUES (1474, '12446111711020', b'1', '2020-11-04 13:19:38', b'0', NULL, 0, 'cool', 1445, 1472);
INSERT INTO `comment` VALUES (1476, '12492626650144', b'1', '2020-11-04 13:20:23', b'0', NULL, 0, 'cool', 1445, 1175);
INSERT INTO `comment` VALUES (1480, '12925783887868', b'1', '2020-11-04 13:27:37', b'0', NULL, 0, 'cool', 1445, 1470);
INSERT INTO `comment` VALUES (1491, '26981083756599', b'1', '2020-11-07 16:07:51', b'1', NULL, 0, 'Très belle', 1445, 1486);
INSERT INTO `comment` VALUES (1492, '26984866837673', b'1', '2020-11-07 16:07:54', b'1', NULL, 0, 'Très belle', 1445, 1486);
INSERT INTO `comment` VALUES (1494, '26990098741424', b'1', '2020-11-07 16:07:59', b'1', NULL, 0, 'Très belle', 1445, 1486);
INSERT INTO `comment` VALUES (1495, '27019492513848', b'1', '2020-11-07 16:08:28', b'0', NULL, 0, 'Très belle', 1445, 1486);
INSERT INTO `comment` VALUES (1496, '27026287277715', b'1', '2020-11-07 16:08:35', b'1', NULL, 0, 'je kiff', 1445, 1486);
INSERT INTO `comment` VALUES (1498, '27110782325131', b'1', '2020-11-07 16:10:00', b'0', NULL, 0, 'belle nature', 1445, 1482);
INSERT INTO `comment` VALUES (1506, '1336491262144', b'1', '2020-11-11 10:15:48', b'0', NULL, 0, 'cool', 1445, 1500);
INSERT INTO `comment` VALUES (1522, '176050828174521', b'1', '2020-11-18 11:06:55', b'0', NULL, 0, 'Test reussi', 1445, 1518);
INSERT INTO `comment` VALUES (1524, '176867068561750', b'1', '2020-11-18 11:20:32', b'1', NULL, 0, 'test', 1445, 1518);
INSERT INTO `comment` VALUES (1525, '176895814826786', b'1', '2020-11-18 11:21:00', b'0', NULL, 0, 'ok', 1445, 1520);
INSERT INTO `comment` VALUES (1531, '178201986279394', b'1', '2020-11-18 11:42:46', b'1', NULL, 0, 'cool', 1445, 1529);

-- ----------------------------
-- Table structure for country
-- ----------------------------
DROP TABLE IF EXISTS `country`;
CREATE TABLE `country`  (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `label` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_bs66m8npqqbrydipm9k2ko187`(`id_server`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of country
-- ----------------------------
INSERT INTO `country` VALUES (8, '11609659966800', 'Afghanistan');
INSERT INTO `country` VALUES (9, '11609661120100', 'Afrique du sud');
INSERT INTO `country` VALUES (10, '11609661888700', 'Îles åland');
INSERT INTO `country` VALUES (11, '11609662594000', 'Albanie');
INSERT INTO `country` VALUES (12, '11609663338700', 'Algérie');
INSERT INTO `country` VALUES (13, '11609664117900', 'Allemagne');
INSERT INTO `country` VALUES (14, '11609664984200', 'Andorre');
INSERT INTO `country` VALUES (15, '11609665820000', 'Angola');
INSERT INTO `country` VALUES (16, '11609666623100', 'Anguilla');
INSERT INTO `country` VALUES (17, '11609667521000', 'Antarctique');
INSERT INTO `country` VALUES (18, '11609668436400', 'Antigua-et-barbuda');
INSERT INTO `country` VALUES (19, '11609669269600', 'Arabie saoudite');
INSERT INTO `country` VALUES (20, '11609670134100', 'Argentine');
INSERT INTO `country` VALUES (21, '11609670954400', 'Arménie');
INSERT INTO `country` VALUES (22, '11609671690100', 'Aruba');
INSERT INTO `country` VALUES (23, '11609672697800', 'Australie');
INSERT INTO `country` VALUES (24, '11609673518400', 'Autriche');
INSERT INTO `country` VALUES (25, '11609674260800', 'Azerbaïdjan');
INSERT INTO `country` VALUES (26, '11609675209000', 'Bahamas');
INSERT INTO `country` VALUES (27, '11609676013200', 'Bahreïn');
INSERT INTO `country` VALUES (28, '11609676937700', 'Bangladesh');
INSERT INTO `country` VALUES (29, '11609678788700', 'Barbade');
INSERT INTO `country` VALUES (30, '11609679488000', 'Biélorussie');
INSERT INTO `country` VALUES (31, '11609680165700', 'Belgique');
INSERT INTO `country` VALUES (32, '11609680925800', 'Belize');
INSERT INTO `country` VALUES (33, '11609681673800', 'Bénin');
INSERT INTO `country` VALUES (34, '11609682371100', 'Bermudes');
INSERT INTO `country` VALUES (35, '11609683043000', 'Bhoutan');
INSERT INTO `country` VALUES (36, '11609683767100', 'Bolivie');
INSERT INTO `country` VALUES (37, '11609684436800', 'Pays-bas caribéens');
INSERT INTO `country` VALUES (38, '11609685109100', 'Bosnie-herzégovine');
INSERT INTO `country` VALUES (39, '11609685838700', 'Botswana');
INSERT INTO `country` VALUES (40, '11609686502300', 'Île bouvet');
INSERT INTO `country` VALUES (41, '11609687205900', 'Brésil');
INSERT INTO `country` VALUES (42, '11609688027700', 'Brunei');
INSERT INTO `country` VALUES (43, '11609689087600', 'Bulgarie');
INSERT INTO `country` VALUES (44, '11609689979700', 'Burkina faso');
INSERT INTO `country` VALUES (45, '11609690725600', 'Burundi');
INSERT INTO `country` VALUES (46, '11609691428800', 'Îles caïmans');
INSERT INTO `country` VALUES (47, '11609692172700', 'Cambodge');
INSERT INTO `country` VALUES (48, '11609692905700', 'Cameroun');
INSERT INTO `country` VALUES (49, '11609694005700', 'Canada');
INSERT INTO `country` VALUES (50, '11609695260600', 'Cap-vert');
INSERT INTO `country` VALUES (51, '11609696127000', 'République centrafricaine');
INSERT INTO `country` VALUES (52, '11609696837500', 'Chili');
INSERT INTO `country` VALUES (53, '11609697526300', 'Chine');
INSERT INTO `country` VALUES (54, '11609698290600', 'Île christmas');
INSERT INTO `country` VALUES (55, '11609699184300', 'Chypre (pays)');
INSERT INTO `country` VALUES (56, '11609699970000', 'Îles cocos');
INSERT INTO `country` VALUES (57, '11609700788300', 'Colombie');
INSERT INTO `country` VALUES (58, '11609701490900', 'Comores (pays)');
INSERT INTO `country` VALUES (59, '11609702201800', 'République du congo');
INSERT INTO `country` VALUES (60, '11609702869200', 'République démocratique du congo');
INSERT INTO `country` VALUES (61, '11609703516400', 'Îles cook');
INSERT INTO `country` VALUES (62, '11609704167400', 'Corée du sud');
INSERT INTO `country` VALUES (63, '11609704943700', 'Corée du nord');
INSERT INTO `country` VALUES (64, '11609705746600', 'Costa rica');
INSERT INTO `country` VALUES (65, '11609706462900', 'Côte d\'ivoire');
INSERT INTO `country` VALUES (66, '11609707196500', 'Croatie');
INSERT INTO `country` VALUES (67, '11609708092000', 'Cuba');
INSERT INTO `country` VALUES (68, '11609708993700', 'Curaçao');
INSERT INTO `country` VALUES (69, '11609709837200', 'Danemark');
INSERT INTO `country` VALUES (70, '11609711079900', 'Djibouti');
INSERT INTO `country` VALUES (71, '11609712226000', 'République dominicaine');
INSERT INTO `country` VALUES (72, '11609713120500', 'Dominique');
INSERT INTO `country` VALUES (73, '11609714047100', 'Égypte');
INSERT INTO `country` VALUES (74, '11609714824500', 'Salvador');
INSERT INTO `country` VALUES (75, '11609715602200', 'Émirats arabes unis');
INSERT INTO `country` VALUES (76, '11609716312000', 'Équateur (pays)');
INSERT INTO `country` VALUES (77, '11609716999800', 'Érythrée');
INSERT INTO `country` VALUES (78, '11609717690500', 'Espagne');
INSERT INTO `country` VALUES (79, '11609718400700', 'Estonie');
INSERT INTO `country` VALUES (80, '11609719122900', 'États-unis');
INSERT INTO `country` VALUES (81, '11609719862000', 'Éthiopie');
INSERT INTO `country` VALUES (82, '11609720700900', 'Malouines');
INSERT INTO `country` VALUES (83, '11609721486600', 'Îles féroé');
INSERT INTO `country` VALUES (84, '11609722256600', 'Fidji');
INSERT INTO `country` VALUES (85, '11609723003000', 'Finlande');
INSERT INTO `country` VALUES (86, '11609723854600', 'France');
INSERT INTO `country` VALUES (87, '11609724741200', 'Gabon');
INSERT INTO `country` VALUES (88, '11609725496300', 'Gambie');
INSERT INTO `country` VALUES (89, '11609726301000', 'Géorgie (pays)');
INSERT INTO `country` VALUES (90, '11609727512800', 'Géorgie du sud-et-les îles sandwich du sud');
INSERT INTO `country` VALUES (91, '11609728382500', 'Ghana');
INSERT INTO `country` VALUES (92, '11609729282700', 'Gibraltar');
INSERT INTO `country` VALUES (93, '11609730190900', 'Grèce');
INSERT INTO `country` VALUES (94, '11609730895700', 'Grenade (pays)');
INSERT INTO `country` VALUES (95, '11609731719600', 'Groenland');
INSERT INTO `country` VALUES (96, '11609732460200', 'Guadeloupe');
INSERT INTO `country` VALUES (97, '11609733166400', 'Guam');
INSERT INTO `country` VALUES (98, '11609733810200', 'Guatemala');
INSERT INTO `country` VALUES (99, '11609734450000', 'Guernesey');
INSERT INTO `country` VALUES (100, '11609735187100', 'Guinée');
INSERT INTO `country` VALUES (101, '11609735920800', 'Guinée-bissau');
INSERT INTO `country` VALUES (102, '11609736641300', 'Guinée équatoriale');
INSERT INTO `country` VALUES (103, '11609737358100', 'Guyana');
INSERT INTO `country` VALUES (104, '11609738138300', 'Guyane');
INSERT INTO `country` VALUES (105, '11609738918400', 'Haïti');
INSERT INTO `country` VALUES (106, '11609739621700', 'Îles heard-et-macdonald');
INSERT INTO `country` VALUES (107, '11609740303200', 'Honduras');
INSERT INTO `country` VALUES (108, '11610051475100', 'Hong kong');
INSERT INTO `country` VALUES (109, '11610052191900', 'Hongrie');
INSERT INTO `country` VALUES (110, '11610052831600', 'Île de man');
INSERT INTO `country` VALUES (111, '11610053582300', 'Îles mineures éloignées des états-unis');
INSERT INTO `country` VALUES (112, '11610054289400', 'Îles vierges britanniques');
INSERT INTO `country` VALUES (113, '11610055228300', 'Îles vierges des états-unis');
INSERT INTO `country` VALUES (114, '11610056198900', 'Inde');
INSERT INTO `country` VALUES (115, '11610057193700', 'Indonésie');
INSERT INTO `country` VALUES (116, '11610058240000', 'Iran');
INSERT INTO `country` VALUES (117, '11610059087600', 'Irak');
INSERT INTO `country` VALUES (118, '11610060336300', 'Irlande (pays)');
INSERT INTO `country` VALUES (119, '11610061934100', 'Islande');
INSERT INTO `country` VALUES (120, '11610067169100', 'Israël');
INSERT INTO `country` VALUES (121, '11610068382100', 'Italie');
INSERT INTO `country` VALUES (122, '11610069522100', 'Jamaïque');
INSERT INTO `country` VALUES (123, '11610070712400', 'Japon');
INSERT INTO `country` VALUES (124, '11610071723800', 'Jersey');
INSERT INTO `country` VALUES (125, '11610072908000', 'Jordanie');
INSERT INTO `country` VALUES (126, '11610074294100', 'Kazakhstan');
INSERT INTO `country` VALUES (127, '11610075307300', 'Kenya');
INSERT INTO `country` VALUES (128, '11610076093000', 'Kirghizistan');
INSERT INTO `country` VALUES (129, '11610077966600', 'Kiribati');
INSERT INTO `country` VALUES (130, '11610079019100', 'Koweït');
INSERT INTO `country` VALUES (131, '11610079830800', 'Laos');
INSERT INTO `country` VALUES (132, '11610080676500', 'Lesotho');
INSERT INTO `country` VALUES (133, '11610081915400', 'Lettonie');
INSERT INTO `country` VALUES (134, '11610083178300', 'Liban');
INSERT INTO `country` VALUES (135, '11610085907800', 'Liberia');
INSERT INTO `country` VALUES (136, '11610087123000', 'Libye');
INSERT INTO `country` VALUES (137, '11610088113000', 'Liechtenstein');
INSERT INTO `country` VALUES (138, '11610089122600', 'Lituanie');
INSERT INTO `country` VALUES (139, '11610090043400', 'Luxembourg (pays)');
INSERT INTO `country` VALUES (140, '11610090850100', 'Macao');
INSERT INTO `country` VALUES (141, '11610091628500', 'Macédoine du nord');
INSERT INTO `country` VALUES (142, '11610092571200', 'Madagascar');
INSERT INTO `country` VALUES (143, '11610095200100', 'Malaisie');
INSERT INTO `country` VALUES (144, '11610096264000', 'Malawi');
INSERT INTO `country` VALUES (145, '11610097540600', 'Maldives');
INSERT INTO `country` VALUES (146, '11610098626800', 'Mali');
INSERT INTO `country` VALUES (147, '11610099417300', 'Malte');
INSERT INTO `country` VALUES (148, '11610100149200', 'Îles mariannes du nord');
INSERT INTO `country` VALUES (149, '11610100916300', 'Maroc');
INSERT INTO `country` VALUES (150, '11610101757200', 'Îles marshall (pays)');
INSERT INTO `country` VALUES (151, '11610102569400', 'Martinique');
INSERT INTO `country` VALUES (152, '11610103413900', 'Maurice (pays)');
INSERT INTO `country` VALUES (153, '11610104255300', 'Mauritanie');
INSERT INTO `country` VALUES (154, '11610105421500', 'Mayotte');
INSERT INTO `country` VALUES (155, '11610106527900', 'Mexique');
INSERT INTO `country` VALUES (156, '11610107504200', 'États fédérés de micronésie (pays)');
INSERT INTO `country` VALUES (157, '11610108660600', 'Moldavie');
INSERT INTO `country` VALUES (158, '11610109782800', 'Monaco');
INSERT INTO `country` VALUES (159, '11610112031500', 'Mongolie');
INSERT INTO `country` VALUES (160, '11610112778100', 'Monténégro');
INSERT INTO `country` VALUES (161, '11610113503800', 'Montserrat');
INSERT INTO `country` VALUES (162, '11610114277500', 'Mozambique');
INSERT INTO `country` VALUES (163, '11610115332600', 'Birmanie');
INSERT INTO `country` VALUES (164, '11610116391800', 'Namibie');
INSERT INTO `country` VALUES (165, '11610117391400', 'Nauru');
INSERT INTO `country` VALUES (166, '11610118541300', 'Népal');
INSERT INTO `country` VALUES (167, '11610119521100', 'Nicaragua');
INSERT INTO `country` VALUES (168, '11610120898100', 'Niger');
INSERT INTO `country` VALUES (169, '11610122187700', 'Nigeria');
INSERT INTO `country` VALUES (170, '11610123075500', 'Niue');
INSERT INTO `country` VALUES (171, '11610124333500', 'Île norfolk');
INSERT INTO `country` VALUES (172, '11610125333500', 'Norvège');
INSERT INTO `country` VALUES (173, '11610126332300', 'Nouvelle-calédonie');
INSERT INTO `country` VALUES (174, '11610129713800', 'Nouvelle-zélande');
INSERT INTO `country` VALUES (175, '11610130876100', 'Territoire britannique de l\'océan indien');
INSERT INTO `country` VALUES (176, '11610131863900', 'Oman');
INSERT INTO `country` VALUES (177, '11610132852700', 'Ouganda');
INSERT INTO `country` VALUES (178, '11610134430100', 'Ouzbékistan');
INSERT INTO `country` VALUES (179, '11610135441000', 'Pakistan');
INSERT INTO `country` VALUES (180, '11610136377300', 'Palaos');
INSERT INTO `country` VALUES (181, '11610137355200', 'Palestine');
INSERT INTO `country` VALUES (182, '11610138205300', 'Panama');
INSERT INTO `country` VALUES (183, '11610138968000', 'Papouasie-nouvelle-guinée');
INSERT INTO `country` VALUES (184, '11610139628700', 'Paraguay');
INSERT INTO `country` VALUES (185, '11610140245300', 'Pays-bas');
INSERT INTO `country` VALUES (186, '11610141082500', 'Pérou');
INSERT INTO `country` VALUES (187, '11610142213000', 'Philippines');
INSERT INTO `country` VALUES (188, '11610143874500', 'Îles pitcairn');
INSERT INTO `country` VALUES (189, '11610145832500', 'Pologne');
INSERT INTO `country` VALUES (190, '11610146907400', 'Polynésie française');
INSERT INTO `country` VALUES (191, '11610147959500', 'Porto rico');
INSERT INTO `country` VALUES (192, '11610149190300', 'Portugal');
INSERT INTO `country` VALUES (193, '11610150014200', 'Qatar');
INSERT INTO `country` VALUES (194, '11610150843200', 'La réunion');
INSERT INTO `country` VALUES (195, '11610151722000', 'Roumanie');
INSERT INTO `country` VALUES (196, '11610152532800', 'Royaume-uni');
INSERT INTO `country` VALUES (197, '11610153510400', 'Russie');
INSERT INTO `country` VALUES (198, '11610154737000', 'Rwanda');
INSERT INTO `country` VALUES (199, '11610156201600', 'République arabe sahraouie démocratique');
INSERT INTO `country` VALUES (200, '11610158172400', 'Saint-barthélemy');
INSERT INTO `country` VALUES (201, '11610159166400', 'Saint-christophe-et-niévès');
INSERT INTO `country` VALUES (202, '11610160765700', 'Saint-marin');
INSERT INTO `country` VALUES (203, '11610162144200', 'Saint-martin');
INSERT INTO `country` VALUES (204, '11610162972400', 'Saint-martin');
INSERT INTO `country` VALUES (205, '11610163777900', 'Saint-pierre-et-miquelon');
INSERT INTO `country` VALUES (206, '11610164536700', 'Saint-siège (état de la cité du vatican)');
INSERT INTO `country` VALUES (207, '11610165440700', 'Saint-vincent-et-les-grenadines');
INSERT INTO `country` VALUES (208, '11610353977200', 'Sainte-hélène; ascension et tristan da cunha');
INSERT INTO `country` VALUES (209, '11610354966100', 'Sainte-lucie');
INSERT INTO `country` VALUES (210, '11610355907900', 'Salomon');
INSERT INTO `country` VALUES (211, '11610357151300', 'Samoa');
INSERT INTO `country` VALUES (212, '11610358097600', 'Samoa américaines');
INSERT INTO `country` VALUES (213, '11610358771600', 'Sao tomé-et-principe');
INSERT INTO `country` VALUES (214, '11610359328500', 'Sénégal');
INSERT INTO `country` VALUES (215, '11610360417200', 'Serbie');
INSERT INTO `country` VALUES (216, '11610361430700', 'Seychelles');
INSERT INTO `country` VALUES (217, '11610362125700', 'Sierra leone');
INSERT INTO `country` VALUES (218, '11610362738500', 'Singapour');
INSERT INTO `country` VALUES (219, '11610363367300', 'Slovaquie');
INSERT INTO `country` VALUES (220, '11610364034200', 'Slovénie');
INSERT INTO `country` VALUES (221, '11610364926600', 'Somalie');
INSERT INTO `country` VALUES (222, '11610365805300', 'Soudan');
INSERT INTO `country` VALUES (223, '11610366823400', 'Soudan du sud');
INSERT INTO `country` VALUES (224, '11610367897100', 'Sri lanka');
INSERT INTO `country` VALUES (225, '11610368868200', 'Suède');
INSERT INTO `country` VALUES (226, '11610369584100', 'Suisse');
INSERT INTO `country` VALUES (227, '11610370135500', 'Suriname');
INSERT INTO `country` VALUES (228, '11610370707900', 'Svalbard et ile jan mayen');
INSERT INTO `country` VALUES (229, '11610371326200', 'Eswatini');
INSERT INTO `country` VALUES (230, '11610371977000', 'Syrie');
INSERT INTO `country` VALUES (231, '11610372633600', 'Tadjikistan');
INSERT INTO `country` VALUES (232, '11610373227400', 'Taïwan / (république de chine (taïwan))');
INSERT INTO `country` VALUES (233, '11610373856600', 'Tanzanie');
INSERT INTO `country` VALUES (234, '11610374493400', 'Tchad');
INSERT INTO `country` VALUES (235, '11610375359400', 'Tchéquie');
INSERT INTO `country` VALUES (236, '11610376171300', 'Terres australes et antarctiques françaises');
INSERT INTO `country` VALUES (237, '11610378115600', 'Thaïlande');
INSERT INTO `country` VALUES (238, '11610378889400', 'Timor oriental');
INSERT INTO `country` VALUES (239, '11610379575800', 'Togo');
INSERT INTO `country` VALUES (240, '11610380278300', 'Tokelau');
INSERT INTO `country` VALUES (241, '11610381046100', 'Tonga');
INSERT INTO `country` VALUES (242, '11610381781700', 'Trinité-et-tobago');
INSERT INTO `country` VALUES (243, '11610382508400', 'Tunisie');
INSERT INTO `country` VALUES (244, '11610383150700', 'Turkménistan');
INSERT INTO `country` VALUES (245, '11610383749200', 'Îles turques-et-caïques');
INSERT INTO `country` VALUES (246, '11610384362600', 'Turquie');
INSERT INTO `country` VALUES (247, '11610384984200', 'Tuvalu');
INSERT INTO `country` VALUES (248, '11610385592700', 'Ukraine');
INSERT INTO `country` VALUES (249, '11610386171900', 'Uruguay');
INSERT INTO `country` VALUES (250, '11610386754100', 'Vanuatu');
INSERT INTO `country` VALUES (251, '11610387398000', 'Venezuela');
INSERT INTO `country` VALUES (252, '11610388197600', 'Viêt nam');
INSERT INTO `country` VALUES (253, '11610389037500', 'Wallis-et-futuna');
INSERT INTO `country` VALUES (254, '11610389707400', 'Yémen');
INSERT INTO `country` VALUES (255, '11610390249100', 'Zambie');
INSERT INTO `country` VALUES (256, '11610390806900', 'Zimbabwe');

-- ----------------------------
-- Table structure for degree_obtained
-- ----------------------------
DROP TABLE IF EXISTS `degree_obtained`;
CREATE TABLE `degree_obtained`  (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `active` bit(1) NULL DEFAULT NULL,
  `created_at` datetime(0) NOT NULL,
  `deleted` bit(1) NULL DEFAULT NULL,
  `modified_at` datetime(0) NULL DEFAULT NULL,
  `revision` bigint(20) NULL DEFAULT NULL,
  `city` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `current_position` bit(1) NULL DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `establishment_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `specialty` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `account` int(11) NULL DEFAULT NULL,
  `end_date` datetime(0) NOT NULL,
  `start_date` datetime(0) NOT NULL,
  `diploma` int(11) NULL DEFAULT NULL,
  `establishment_type` int(11) NULL DEFAULT NULL,
  `teaching_area` int(11) NULL DEFAULT NULL,
  `another_diploma` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `another_establishment_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_it7wk6xlws8hdylq3dieky5ut`(`id_server`) USING BTREE,
  INDEX `FKfk1i6e7r5qsqj3w741eluxj48`(`diploma`) USING BTREE,
  INDEX `FKkiktrv9qgmrh2sthbfx0hkd7u`(`establishment_type`) USING BTREE,
  INDEX `FK75axn0e4e11xu6ny1kvaebh4g`(`teaching_area`) USING BTREE,
  INDEX `FK_kx2faddje0w3cixn027phkl7b`(`account`) USING BTREE,
  CONSTRAINT `FK75axn0e4e11xu6ny1kvaebh4g` FOREIGN KEY (`teaching_area`) REFERENCES `teaching_area` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_kx2faddje0w3cixn027phkl7b` FOREIGN KEY (`account`) REFERENCES `account` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FKfk1i6e7r5qsqj3w741eluxj48` FOREIGN KEY (`diploma`) REFERENCES `diploma` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FKkiktrv9qgmrh2sthbfx0hkd7u` FOREIGN KEY (`establishment_type`) REFERENCES `establishment_type` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of degree_obtained
-- ----------------------------
INSERT INTO `degree_obtained` VALUES (1346, '59038194195200', b'1', '2020-10-22 11:11:43', b'0', NULL, 0, 'Yaounde', b'0', 'brevet ', 'College la retraite', 'Langue francaise', 1, '2020-10-31 01:00:00', '2020-10-01 02:00:00', 1105, 721, 1220, NULL, NULL);
INSERT INTO `degree_obtained` VALUES (1348, '79658667104600', b'1', '2020-10-22 16:55:23', b'0', NULL, 0, 'Yaounde', b'0', 'EII', 'Université de yaounde', 'Informatique industrielle', 1, '2020-10-31 01:00:00', '2020-10-01 02:00:00', 1128, 726, 1244, NULL, NULL);
INSERT INTO `degree_obtained` VALUES (1361, '269590073226700', b'1', '2020-10-24 21:40:42', b'0', NULL, 0, 'Yaounde', b'0', NULL, 'College adventiste', 'langue', 1, '2020-10-31 01:00:00', '2020-10-01 02:00:00', 1105, 721, NULL, NULL, NULL);
INSERT INTO `degree_obtained` VALUES (1415, '380928748529600', b'1', '2020-10-31 19:28:22', b'0', NULL, 0, 'Rennes', b'0', NULL, 'Lycee colbert', 'Droit', 1, '2020-11-26 00:00:00', '2020-11-03 00:00:00', 1121, 729, NULL, NULL, NULL);
INSERT INTO `degree_obtained` VALUES (1450, '437281106428289', b'1', '2020-11-03 12:31:36', b'0', NULL, 0, 'Yaoundé', b'0', 'Baccalauréat général', 'Lycée de nkoléton', 'Série D', 1447, '2013-09-05 00:00:00', '2013-09-05 00:00:00', 1108, 722, NULL, NULL, NULL);
INSERT INTO `degree_obtained` VALUES (1457, '438563087159652', b'1', '2020-11-03 12:49:23', b'0', NULL, 0, 'Yaoundé', b'0', 'Génie Industriel', 'Ecole nationale supérieure polytechnique', 'Génie Industriel', 1445, '2008-09-01 00:00:00', '2005-09-05 00:00:00', 1127, 728, NULL, NULL, NULL);
INSERT INTO `degree_obtained` VALUES (1458, '438505010477449', b'1', '2020-11-03 12:52:00', b'0', NULL, 0, 'Yaoundé', b'0', 'Maths, Physiques et Chimie', 'Ecole nationale supérieure polytechnique', 'MSP', 1445, '2005-09-05 00:00:00', '2003-09-01 00:00:00', 1122, 728, NULL, NULL, NULL);
INSERT INTO `degree_obtained` VALUES (1508, '181529562156985', b'1', '2020-11-13 12:18:10', b'0', NULL, 0, 'Yaoundé', b'0', 'Général', 'Lycée de mballa 2', 'Général', 1445, '2003-05-30 00:00:00', '2002-09-02 00:00:00', 1117, 725, NULL, 'BAC C', NULL);
INSERT INTO `degree_obtained` VALUES (1509, '189441664991072', b'1', '2020-11-13 12:24:57', b'0', NULL, 0, 'Soa 2', b'0', 'Général', 'Soa', 'Piscine', 1445, '2020-11-13 00:00:00', '2020-11-09 00:00:00', 1131, 731, NULL, 'DEA', NULL);
INSERT INTO `degree_obtained` VALUES (1516, '17252203117362', b'1', '2020-11-16 14:59:03', b'0', NULL, 0, 'Yaoundé', b'0', 'Physique atomique', 'Ngoakélé', 'Physique', 1447, '2020-11-13 00:00:00', '2020-11-04 00:00:00', 1131, 725, NULL, 'MASTER 3', 'Facultés de Sciences');

-- ----------------------------
-- Table structure for diploma
-- ----------------------------
DROP TABLE IF EXISTS `diploma`;
CREATE TABLE `diploma`  (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `label` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `education` int(11) NULL DEFAULT NULL,
  `specified` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_gi7a8g81d35pkdo1xabrrc294`(`id_server`) USING BTREE,
  INDEX `FKtc8s0lrn5ie925rom0xka5hp`(`education`) USING BTREE,
  CONSTRAINT `FKtc8s0lrn5ie925rom0xka5hp` FOREIGN KEY (`education`) REFERENCES `education` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of diploma
-- ----------------------------
INSERT INTO `diploma` VALUES (1105, '78869941664100', 'Brevet Nationale des collèges', 'Brevet Nationale des collèges', 438, 0);
INSERT INTO `diploma` VALUES (1106, '78869956146500', 'Baccalauréat Scientifique', 'Baccalauréat Scientifique', 438, 0);
INSERT INTO `diploma` VALUES (1107, '78869959061000', 'Baccalauréat Sciences économique et sociales', 'Baccalauréat Sciences économique et sociales', 438, 0);
INSERT INTO `diploma` VALUES (1108, '78869961473600', 'Baccalauréat littéraire', 'Baccalauréat littéraire', 438, 0);
INSERT INTO `diploma` VALUES (1109, '78869964118200', 'Série Sciences et technologies du management et de la gestion (STMG)', 'Série Sciences et technologies du management et de la gestion (STMG)', 438, 0);
INSERT INTO `diploma` VALUES (1110, '78869966879700', 'Le baccalauréat sciences et technologies du design et des arts appliqués (STD2A)', 'Le baccalauréat sciences et technologies du design et des arts appliqués (STD2A)', 438, 0);
INSERT INTO `diploma` VALUES (1111, '78869969180200', 'Le baccalauréat sciences et technologies de l\'industrie et du développement durable (STI2D)', 'Le baccalauréat sciences et technologies de l\'industrie et du développement durable (STI2D)', 438, 0);
INSERT INTO `diploma` VALUES (1112, '78869972356300', 'Le baccalauréat sciences et technologies de laboratoire (STL)', 'Le baccalauréat sciences et technologies de laboratoire (STL)', 438, 0);
INSERT INTO `diploma` VALUES (1113, '78869975100500', 'Le baccalauréat sciences et technologies de la santé et du social ST2S', 'Le baccalauréat sciences et technologies de la santé et du social ST2S', 438, 0);
INSERT INTO `diploma` VALUES (1114, '78869977864500', 'Baccalauréat sciences et technologies de l\'agronomie et du vivant (STAV)', 'Baccalauréat sciences et technologies de l\'agronomie et du vivant (STAV)', 438, 0);
INSERT INTO `diploma` VALUES (1115, '78869980719700', 'Baccalauréat technique de la musique et de la danse (TMD)', 'Baccalauréat technique de la musique et de la danse (TMD)', 438, 0);
INSERT INTO `diploma` VALUES (1116, '78869984143500', 'Baccalauréat sciences et technologies de l\'hôtellerie et de la restauration (STHR)', 'Baccalauréat sciences et technologies de l\'hôtellerie et de la restauration (STHR)', 438, 0);
INSERT INTO `diploma` VALUES (1117, '78869987226300', 'Autres (à préciser manuellement)', 'Autres (à préciser manuellement)', 438, 1);
INSERT INTO `diploma` VALUES (1118, '78869990113600', 'CAP (Bac -1)', 'CAP (Bac -1)', 438, 0);
INSERT INTO `diploma` VALUES (1119, '78869993327800', 'BEP (Bac -1)', 'BEP (Bac -1)', 438, 0);
INSERT INTO `diploma` VALUES (1120, '78869996337600', 'Baccalauréat professionnel (à préciser manuellement)', 'Baccalauréat professionnel (à préciser manuellement)', 438, 1);
INSERT INTO `diploma` VALUES (1121, '78869999481000', 'BTS (Bac +2)', 'BTS (Bac +2)', 439, 0);
INSERT INTO `diploma` VALUES (1122, '78870002472500', 'DUT (BAC +2)', 'DUT (BAC +2)', 439, 0);
INSERT INTO `diploma` VALUES (1123, '78870005318400', 'Licence Professionnelle (Bac +3)', 'Licence Professionnelle (Bac +3)', 439, 0);
INSERT INTO `diploma` VALUES (1124, '78870007888100', 'Licence Générale (Bac+3)', 'Licence Générale (Bac+3)', 439, 0);
INSERT INTO `diploma` VALUES (1125, '78870011010000', 'Maîtrise (Bac +4)', 'Maîtrise (Bac +4)', 439, 0);
INSERT INTO `diploma` VALUES (1126, '78870015353500', 'Bachelor (Bac +4)', 'Bachelor (Bac +4)', 439, 0);
INSERT INTO `diploma` VALUES (1127, '78870017894900', 'Master Professionnel (Bac +5)', 'Master Professionnel (Bac +5)', 439, 0);
INSERT INTO `diploma` VALUES (1128, '78870020394000', 'Master Recherche (Bac +5)', 'Master Recherche (Bac +5)', 439, 0);
INSERT INTO `diploma` VALUES (1129, '78870023428400', 'Magistère (Bac +5)', 'Magistère (Bac +5)', 439, 0);
INSERT INTO `diploma` VALUES (1130, '78870026154400', 'Bac +6 (à préciser manuellement)', 'Bac +6 (à préciser manuellement)', 439, 1);
INSERT INTO `diploma` VALUES (1131, '78870028624800', 'Bac +7 (à préciser manuellement)', 'Bac +7 (à préciser manuellement)', 439, 1);
INSERT INTO `diploma` VALUES (1132, '78870030917300', 'Doctorat (Bac +8)', 'Doctorat (Bac +8)', 439, 0);
INSERT INTO `diploma` VALUES (1133, '78870032740900', 'Autres (à sélectionner de bac +1 à Bac +11 et à préciser)', 'Autres (à sélectionner de bac +1 à Bac +11 et à préciser)', 439, 1);
INSERT INTO `diploma` VALUES (1134, '78870034829100', 'Docteur en Pharmacie (Bac+6)', 'Docteur en Pharmacie (Bac+6)', 439, 0);
INSERT INTO `diploma` VALUES (1135, '78870037419600', 'Docteur en Pharmacie Spécialisé (Bac+9)', 'Docteur en Pharmacie Spécialisé (Bac+9)', 439, 0);
INSERT INTO `diploma` VALUES (1136, '78870040082900', 'Docteur en Médecine (Bac +9)', 'Docteur en Médecine (Bac +9)', 439, 0);
INSERT INTO `diploma` VALUES (1137, '78870042801900', 'Docteur en chirurgie dentaire Spécialisé (Bac+10)', 'Docteur en chirurgie dentaire Spécialisé (Bac+10)', 439, 0);
INSERT INTO `diploma` VALUES (1138, '78870045169800', 'Docteur en médecine spécialisé (Bac +11)', 'Docteur en médecine spécialisé (Bac +11)', 439, 0);
INSERT INTO `diploma` VALUES (1139, '78870047543400', 'Autres (à sélectionner de bac +1 à Bac +11 et à préciser)', 'Autres (à sélectionner de bac +1 à Bac +11 et à préciser)', 439, 1);

-- ----------------------------
-- Table structure for discipline
-- ----------------------------
DROP TABLE IF EXISTS `discipline`;
CREATE TABLE `discipline`  (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `label` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_5hj653mpeo2wjao494f7p1w2w`(`id_server`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of discipline
-- ----------------------------

-- ----------------------------
-- Table structure for education
-- ----------------------------
DROP TABLE IF EXISTS `education`;
CREATE TABLE `education`  (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `code` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `label` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_rxld3r7vdl36kn9gej7eu65b7`(`id_server`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of education
-- ----------------------------
INSERT INTO `education` VALUES (438, '12680588341800', 'SECONDARY', 'Enseignement secondaire (avant le baccalauréat)', 'Enseignement secondaire (avant le baccalauréat)');
INSERT INTO `education` VALUES (439, '12680589479800', 'HIGHER', 'Enseignement supérieur (après le baccalauréat)', 'Enseignement supérieur (après le baccalauréat)');

-- ----------------------------
-- Table structure for establishment_type
-- ----------------------------
DROP TABLE IF EXISTS `establishment_type`;
CREATE TABLE `establishment_type`  (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `label` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `education` int(11) NULL DEFAULT NULL,
  `specified` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_8tc237adw2hrai8twqk5lu5kf`(`id_server`) USING BTREE,
  INDEX `FK49cpx0mji0tomjon7caxro4eg`(`education`) USING BTREE,
  CONSTRAINT `FK49cpx0mji0tomjon7caxro4eg` FOREIGN KEY (`education`) REFERENCES `education` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of establishment_type
-- ----------------------------
INSERT INTO `establishment_type` VALUES (721, '105976758462400', 'Collège', 'Collège', 438, 0);
INSERT INTO `establishment_type` VALUES (722, '105976766287800', 'Lycée général et Technologie', 'Lycée général et Technologie', 438, 0);
INSERT INTO `establishment_type` VALUES (723, '105976768321100', 'Lycée professionnel', 'Lycée professionnel', 438, 0);
INSERT INTO `establishment_type` VALUES (724, '105976770256800', 'Centre de formation d\'apprentis', 'Centre de formation d\'apprentis', 438, 0);
INSERT INTO `establishment_type` VALUES (725, '105976772225400', 'Autres établissements du secondaire', 'Autres établissements du secondaire', 438, 1);
INSERT INTO `establishment_type` VALUES (726, '105976774834200', 'Université', 'Université', 439, 0);
INSERT INTO `establishment_type` VALUES (727, '105976777300700', 'IUT - Institut Universitaire de Technologie', 'IUT - Institut Universitaire de Technologie', 439, 0);
INSERT INTO `establishment_type` VALUES (728, '105976779869700', 'Grandes écoles', 'Grandes écoles', 439, 0);
INSERT INTO `establishment_type` VALUES (729, '105976782026900', 'Lycée Professionnel', 'Lycée Professionnel', 439, 0);
INSERT INTO `establishment_type` VALUES (730, '105976784376900', 'Ecoles et Instituts Specialisés', 'Ecoles et Instituts Specialisés', 439, 0);
INSERT INTO `establishment_type` VALUES (731, '105976786614800', 'Ecoles supérieure d\'Art et d\'Arts appliqués', 'Ecoles supérieure d\'Art et d\'Arts appliqués', 439, 0);
INSERT INTO `establishment_type` VALUES (732, '105976788804400', 'Ecoles Nationales Supérieur d\'Architecture', 'Ecoles Nationales Supérieur d\'Architecture', 439, 0);
INSERT INTO `establishment_type` VALUES (733, '105976790993100', 'Autres établissements du Supérieur', 'Autres établissements du Supérieur', 439, 1);

-- ----------------------------
-- Table structure for follower
-- ----------------------------
DROP TABLE IF EXISTS `follower`;
CREATE TABLE `follower`  (
  `account_id` int(11) NOT NULL,
  `follower_id` int(11) NOT NULL,
  PRIMARY KEY (`account_id`, `follower_id`) USING BTREE,
  INDEX `FK1ychsfw8wfahvaewkb8oiowii`(`follower_id`) USING BTREE,
  CONSTRAINT `FK1ychsfw8wfahvaewkb8oiowii` FOREIGN KEY (`follower_id`) REFERENCES `account` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FKtblw5bbu6gynu89dd9e39vr68` FOREIGN KEY (`account_id`) REFERENCES `account` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of follower
-- ----------------------------

-- ----------------------------
-- Table structure for gender
-- ----------------------------
DROP TABLE IF EXISTS `gender`;
CREATE TABLE `gender`  (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `label` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_pmsrnxn4kayxewyfw2vp1rht4`(`id_server`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gender
-- ----------------------------
INSERT INTO `gender` VALUES (3, '11508831360000', 'Femme');
INSERT INTO `gender` VALUES (4, '11508832442600', 'Homme');

-- ----------------------------
-- Table structure for hibernate_sequence
-- ----------------------------
DROP TABLE IF EXISTS `hibernate_sequence`;
CREATE TABLE `hibernate_sequence`  (
  `next_val` bigint(20) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of hibernate_sequence
-- ----------------------------
INSERT INTO `hibernate_sequence` VALUES (1540);
INSERT INTO `hibernate_sequence` VALUES (1540);
INSERT INTO `hibernate_sequence` VALUES (1540);
INSERT INTO `hibernate_sequence` VALUES (1540);
INSERT INTO `hibernate_sequence` VALUES (1540);
INSERT INTO `hibernate_sequence` VALUES (1540);
INSERT INTO `hibernate_sequence` VALUES (1540);
INSERT INTO `hibernate_sequence` VALUES (1540);
INSERT INTO `hibernate_sequence` VALUES (1540);
INSERT INTO `hibernate_sequence` VALUES (1540);
INSERT INTO `hibernate_sequence` VALUES (1540);
INSERT INTO `hibernate_sequence` VALUES (1540);
INSERT INTO `hibernate_sequence` VALUES (1540);
INSERT INTO `hibernate_sequence` VALUES (1540);
INSERT INTO `hibernate_sequence` VALUES (1540);
INSERT INTO `hibernate_sequence` VALUES (1540);
INSERT INTO `hibernate_sequence` VALUES (1540);
INSERT INTO `hibernate_sequence` VALUES (1540);
INSERT INTO `hibernate_sequence` VALUES (1540);
INSERT INTO `hibernate_sequence` VALUES (1540);
INSERT INTO `hibernate_sequence` VALUES (1540);
INSERT INTO `hibernate_sequence` VALUES (1540);
INSERT INTO `hibernate_sequence` VALUES (1540);

-- ----------------------------
-- Table structure for higher_education
-- ----------------------------
DROP TABLE IF EXISTS `higher_education`;
CREATE TABLE `higher_education`  (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `active` bit(1) NULL DEFAULT NULL,
  `created_at` datetime(0) NOT NULL,
  `deleted` bit(1) NULL DEFAULT NULL,
  `modified_at` datetime(0) NULL DEFAULT NULL,
  `revision` bigint(20) NULL DEFAULT NULL,
  `city` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `current_position` bit(1) NULL DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `establishment_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `specialty` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `account` int(11) NULL DEFAULT NULL,
  `establishment_type` int(11) NULL DEFAULT NULL,
  `study_level` int(11) NULL DEFAULT NULL,
  `targeted_diploma` int(11) NULL DEFAULT NULL,
  `teaching_area` int(11) NULL DEFAULT NULL,
  `another_targeted_diploma` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `another_study_level` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `another_establishment_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_h3tahd3n1q4kp7v3xwxxueoud`(`id_server`) USING BTREE,
  INDEX `FKa908thtmqa8crsydwj32f1jjl`(`targeted_diploma`) USING BTREE,
  INDEX `FK8mst42dw0j0ejjp0qh831ty29`(`teaching_area`) USING BTREE,
  INDEX `FK_1owtti119f95r1vv9oc7xnbml`(`establishment_type`) USING BTREE,
  INDEX `FK_kiwsm8pa9yd00er0xivjymk20`(`study_level`) USING BTREE,
  INDEX `FK_th7r3t3j275wb1g1mmxkko829`(`account`) USING BTREE,
  CONSTRAINT `FK8mst42dw0j0ejjp0qh831ty29` FOREIGN KEY (`teaching_area`) REFERENCES `teaching_area` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_1owtti119f95r1vv9oc7xnbml` FOREIGN KEY (`establishment_type`) REFERENCES `establishment_type` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_kiwsm8pa9yd00er0xivjymk20` FOREIGN KEY (`study_level`) REFERENCES `study_level` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_th7r3t3j275wb1g1mmxkko829` FOREIGN KEY (`account`) REFERENCES `account` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FKa908thtmqa8crsydwj32f1jjl` FOREIGN KEY (`targeted_diploma`) REFERENCES `diploma` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of higher_education
-- ----------------------------
INSERT INTO `higher_education` VALUES (1344, '82683325908600', b'1', '2020-10-21 16:59:54', b'0', NULL, 0, 'Yaoundé ', b'0', '\"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia volupt', 'Université de yaoundé 1', 'Droit des affaires', 1, 726, 927, 1128, 1213, NULL, NULL, NULL);
INSERT INTO `higher_education` VALUES (1413, '357016282024100', b'1', '2020-10-31 12:49:50', b'0', NULL, 0, 'Maroua', b'0', NULL, 'Uit de maroua', 'Droit commun', 1, 727, 928, 1123, 1213, NULL, NULL, NULL);
INSERT INTO `higher_education` VALUES (1414, '361223704863300', b'1', '2020-10-31 13:59:57', b'0', NULL, 0, 'Bamenda', b'0', NULL, 'Université de bamenda', 'Mathematiques de l\'economie', 1, 726, 932, 1132, 1243, NULL, NULL, NULL);
INSERT INTO `higher_education` VALUES (1451, '437400932928463', b'1', '2020-11-03 12:33:36', b'1', NULL, 0, 'Yaoundé ii', b'0', 'Banque & Finances', 'Soa', 'Banque & Finances', 1447, 726, 931, 1127, 1216, NULL, NULL, NULL);
INSERT INTO `higher_education` VALUES (1459, '438656643749101', b'1', '2020-11-03 12:54:32', b'1', NULL, 0, 'Yaoundé', b'0', 'Mastère spécialisé', 'Ecole centrale paris', 'Systèmes Embarqués', 1445, 728, 932, 1130, 1258, NULL, NULL, NULL);
INSERT INTO `higher_education` VALUES (1511, '187737300812057', b'1', '2020-11-13 13:59:13', b'1', NULL, 0, 'Soa', b'0', 'Test', 'Issam', 'Natation', 1445, 733, 943, 1131, 1213, NULL, NULL, NULL);
INSERT INTO `higher_education` VALUES (1512, '191329315092405', b'1', '2020-11-13 14:31:03', b'0', NULL, 0, 'Yaoundé', b'0', 'Tot ou rien', 'Polytech', 'Course', 1445, 733, 943, 1130, 1216, 'Master++', 'Je pars chez Mbom', NULL);
INSERT INTO `higher_education` VALUES (1514, '5363117506536', b'1', '2020-11-16 11:43:02', b'1', NULL, 0, 'Yaoundé', b'0', 'MSP', 'Ensp', 'Physiques', 1447, 733, 927, 1139, 1246, 'Bacc+2', NULL, 'Ecole Polytechnque');

-- ----------------------------
-- Table structure for infos_media_type
-- ----------------------------
DROP TABLE IF EXISTS `infos_media_type`;
CREATE TABLE `infos_media_type`  (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `color` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `position` tinyint(4) NOT NULL,
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_666irr8e1spjsdlf4en8q0i2m`(`id_server`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of infos_media_type
-- ----------------------------

-- ----------------------------
-- Table structure for level
-- ----------------------------
DROP TABLE IF EXISTS `level`;
CREATE TABLE `level`  (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `description` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `grade` int(11) NOT NULL,
  `icon` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `label` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `mastered` bit(1) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_avbprobsmujuclg54t1crl3ip`(`id_server`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of level
-- ----------------------------
INSERT INTO `level` VALUES (1101, '77581904485800', 'Très bien', 2, 'green-arrow-plus.svg', 'Très bien', b'1');
INSERT INTO `level` VALUES (1102, '77581905490800', 'Bien', 1, 'green-arrow.svg', 'Bien', b'1');
INSERT INTO `level` VALUES (1103, '77581906341100', 'Notions de base', -2, 'orange-arrow.svg', 'Notions de base', b'0');
INSERT INTO `level` VALUES (1104, '77581907171400', 'A approfondir', -1, 'red-arrow.svg', 'A approfondir', b'0');

-- ----------------------------
-- Table structure for like_post
-- ----------------------------
DROP TABLE IF EXISTS `like_post`;
CREATE TABLE `like_post`  (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `account` int(11) NULL DEFAULT NULL,
  `post` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_q3fmv0m2w7d8ntnmn3hv3wo37`(`id_server`) USING BTREE,
  INDEX `FKn3fih30t2357eiijmd0etsrl9`(`account`) USING BTREE,
  INDEX `FKrasyhkem81o3qsxvm2t8do4t4`(`post`) USING BTREE,
  CONSTRAINT `FKn3fih30t2357eiijmd0etsrl9` FOREIGN KEY (`account`) REFERENCES `account` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FKrasyhkem81o3qsxvm2t8do4t4` FOREIGN KEY (`post`) REFERENCES `post` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of like_post
-- ----------------------------
INSERT INTO `like_post` VALUES (1416, '390045627875000', 1, 1365);
INSERT INTO `like_post` VALUES (1418, '390495784813100', 1, 1181);
INSERT INTO `like_post` VALUES (1421, '433782626595800', 1, 1173);
INSERT INTO `like_post` VALUES (1473, '12437470402564', 1445, 1472);
INSERT INTO `like_post` VALUES (1475, '12487099702678', 1445, 1175);
INSERT INTO `like_post` VALUES (1479, '12920651883291', 1445, 1470);
INSERT INTO `like_post` VALUES (1497, '27104894196307', 1445, 1482);
INSERT INTO `like_post` VALUES (1501, '284473247252635', 1445, 1500);
INSERT INTO `like_post` VALUES (1502, '284479090952432', 1445, 1490);
INSERT INTO `like_post` VALUES (1505, '284501635224241', 1445, 1486);
INSERT INTO `like_post` VALUES (1523, '176057271346590', 1445, 1520);
INSERT INTO `like_post` VALUES (1530, '178197148893029', 1445, 1529);

-- ----------------------------
-- Table structure for link
-- ----------------------------
DROP TABLE IF EXISTS `link`;
CREATE TABLE `link`  (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `active` bit(1) NULL DEFAULT NULL,
  `created_at` datetime(0) NOT NULL,
  `deleted` bit(1) NULL DEFAULT NULL,
  `modified_at` datetime(0) NULL DEFAULT NULL,
  `revision` bigint(20) NULL DEFAULT NULL,
  `link_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `account` int(11) NULL DEFAULT NULL,
  `link` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_muo7t66qy7bjxt2mpgk9u8doq`(`id_server`) USING BTREE,
  INDEX `FKgcicrmmae2kfvim5u85a5n8b`(`account`) USING BTREE,
  INDEX `FKbe6t25ltf7ig4hla2jqo437ss`(`link`) USING BTREE,
  CONSTRAINT `FKbe6t25ltf7ig4hla2jqo437ss` FOREIGN KEY (`link`) REFERENCES `post` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FKgcicrmmae2kfvim5u85a5n8b` FOREIGN KEY (`account`) REFERENCES `account` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of link
-- ----------------------------
INSERT INTO `link` VALUES (1477, '12914164517952', b'1', '2020-11-04 13:27:25', b'0', NULL, 0, NULL, 'https://www.bbc.com/news/election/us2020', 1445, 1478);

-- ----------------------------
-- Table structure for media_subtype
-- ----------------------------
DROP TABLE IF EXISTS `media_subtype`;
CREATE TABLE `media_subtype`  (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `label` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `media_type` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_fpk6ncx36twffmkqpvpie81o`(`id_server`) USING BTREE,
  INDEX `FKgjun9jfynng9l72q9v8kkj6y9`(`media_type`) USING BTREE,
  CONSTRAINT `FKgjun9jfynng9l72q9v8kkj6y9` FOREIGN KEY (`media_type`) REFERENCES `media_type` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of media_subtype
-- ----------------------------
INSERT INTO `media_subtype` VALUES (1140, '79438914763500', 'Cours', 599);
INSERT INTO `media_subtype` VALUES (1141, '79438923761400', 'Exercices et corrections', 599);
INSERT INTO `media_subtype` VALUES (1142, '79438926090000', 'Sujets d\'examen et corrections', 599);
INSERT INTO `media_subtype` VALUES (1143, '79438928738600', 'Fiches et astuces de revision', 599);
INSERT INTO `media_subtype` VALUES (1144, '79438931131900', 'Mémoires, Exposés, présentation', 599);
INSERT INTO `media_subtype` VALUES (1145, '79438933346600', 'Diplômes et certifications', 599);
INSERT INTO `media_subtype` VALUES (1146, '79438935457100', 'Réalisations professionnelles', 599);
INSERT INTO `media_subtype` VALUES (1147, '79438937852400', 'Travaux de recherche et livres', 599);
INSERT INTO `media_subtype` VALUES (1149, '79438942644500', 'Vidéo pédagogique / éducative', 600);
INSERT INTO `media_subtype` VALUES (1150, '79438945229700', 'Conférence', 600);
INSERT INTO `media_subtype` VALUES (1151, '79438947310500', 'Actualités et débats', 600);
INSERT INTO `media_subtype` VALUES (1152, '79438949374100', 'Autres', 600);
INSERT INTO `media_subtype` VALUES (1153, '79438951754800', 'Audio pédagogique / éducatif', 601);
INSERT INTO `media_subtype` VALUES (1154, '79438954034600', 'Conférence', 601);
INSERT INTO `media_subtype` VALUES (1155, '79438956484900', 'Actualités et débats', 601);
INSERT INTO `media_subtype` VALUES (1156, '79438958895000', 'Autres', 601);
INSERT INTO `media_subtype` VALUES (1157, '79438961586900', 'Article de presse', 602);
INSERT INTO `media_subtype` VALUES (1158, '79438964284500', 'Revue scientifique / pédagogique', 602);
INSERT INTO `media_subtype` VALUES (1159, '79438966661200', 'Site web', 602);
INSERT INTO `media_subtype` VALUES (1160, '79438968975300', 'Blog', 602);
INSERT INTO `media_subtype` VALUES (1161, '79438972588400', 'Questionnaires et jeux éducatifs', 602);
INSERT INTO `media_subtype` VALUES (1162, '79438974800500', 'Livres', 602);
INSERT INTO `media_subtype` VALUES (1163, '79438976984700', 'Autres', 602);
INSERT INTO `media_subtype` VALUES (1164, '79438979584600', 'Photos des publications', 603);
INSERT INTO `media_subtype` VALUES (1165, '79438981782600', 'Photos personnelles', 603);
INSERT INTO `media_subtype` VALUES (1166, '79438984308000', 'Autres', 603);

-- ----------------------------
-- Table structure for media_type
-- ----------------------------
DROP TABLE IF EXISTS `media_type`;
CREATE TABLE `media_type`  (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `label` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_at21ep413yoo6ja3927db4x94`(`id_server`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of media_type
-- ----------------------------
INSERT INTO `media_type` VALUES (598, '30836624139400', 'AVATAR');
INSERT INTO `media_type` VALUES (599, '30836625115500', 'DOCUMENT');
INSERT INTO `media_type` VALUES (600, '30836626055600', 'VIDEO');
INSERT INTO `media_type` VALUES (601, '30836626936100', 'AUDIO');
INSERT INTO `media_type` VALUES (602, '30836628127000', 'LINK');
INSERT INTO `media_type` VALUES (603, '30836629124600', 'PICTURE');

-- ----------------------------
-- Table structure for multimedia
-- ----------------------------
DROP TABLE IF EXISTS `multimedia`;
CREATE TABLE `multimedia`  (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `active` bit(1) NULL DEFAULT NULL,
  `created_at` datetime(0) NOT NULL,
  `deleted` bit(1) NULL DEFAULT NULL,
  `modified_at` datetime(0) NULL DEFAULT NULL,
  `revision` bigint(20) NULL DEFAULT NULL,
  `checksum` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `directory` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `extension` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `mime_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `account` int(11) NULL DEFAULT NULL,
  `multimedia` int(11) NULL DEFAULT NULL,
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_fjsq1euaq26h44sxodq1duswj`(`id_server`) USING BTREE,
  INDEX `FKjp62ph2bdbrb6kpcxswhvhy85`(`account`) USING BTREE,
  INDEX `FK51vq8lev5q0usgnbgb3abbb9p`(`multimedia`) USING BTREE,
  CONSTRAINT `FK51vq8lev5q0usgnbgb3abbb9p` FOREIGN KEY (`multimedia`) REFERENCES `post` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FKjp62ph2bdbrb6kpcxswhvhy85` FOREIGN KEY (`account`) REFERENCES `account` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of multimedia
-- ----------------------------
INSERT INTO `multimedia` VALUES (1172, '178699539194100', b'1', '2020-10-20 13:51:41', b'0', NULL, 0, 'e5b86185', '11308326636800', 'pdf', 'application/pdf', 'file-example_PDF_1MB.pdf', 'DOCUMENT', 1, 1173, NULL);
INSERT INTO `multimedia` VALUES (1174, '179242460482000', b'1', '2020-10-20 14:00:43', b'0', NULL, 0, 'e74aba3e', '11308326636800', 'mp4', 'video/mp4', 'file_example_MP4_480_1_5MG.mp4', 'VIDEO', 1, 1175, NULL);
INSERT INTO `multimedia` VALUES (1176, '180675976102000', b'1', '2020-10-20 14:24:31', b'0', NULL, 0, '5e42c48', '11308326636800', 'mp3', 'audio/mpeg', 'file_example_MP3_700KB.mp3', 'AUDIO', 1, 1177, NULL);
INSERT INTO `multimedia` VALUES (1178, '180804399521300', b'1', '2020-10-20 14:26:42', b'0', NULL, 0, 'fb1384b3', '11308326636800', 'mp3', 'audio/mpeg', 'file_example_MP3_1MG.mp3', 'AUDIO', 1, 1179, NULL);
INSERT INTO `multimedia` VALUES (1180, '181289669089500', b'1', '2020-10-20 14:34:46', b'0', NULL, 0, '813886d2', '11308326636800', 'mp3', 'audio/mpeg', 'file_example_MP3_2MG.mp3', 'AUDIO', 1, 1181, NULL);
INSERT INTO `multimedia` VALUES (1358, '241968449359500', b'1', '2020-10-24 14:00:13', b'0', NULL, 0, 'b720cd51', '67665637080700', 'jpg', 'image/jpeg', 'chimananda.jpg', 'AVATAR', 591, NULL, NULL);
INSERT INTO `multimedia` VALUES (1362, '270532752900000', b'1', '2020-10-24 21:56:19', b'0', NULL, 0, '3722b84b', '11308326636800', 'jpg', 'image/jpeg', 'books-768426_1920.jpg', 'PICTURE', 1, 1363, NULL);
INSERT INTO `multimedia` VALUES (1364, '271780017042700', b'1', '2020-10-24 22:17:06', b'0', NULL, 0, 'dd755d77', '11308326636800', 'jpg', 'image/jpeg', 'file_example_JPG_1MB.jpg', 'PICTURE', 1, 1365, NULL);
INSERT INTO `multimedia` VALUES (1419, '431604505677500', b'1', '2020-11-01 09:32:52', b'0', NULL, 0, '5bf528fd', '11308326636800', 'jpg', 'image/jpeg', 'book-419589_1920.jpg', 'PICTURE', 1, 1420, NULL);
INSERT INTO `multimedia` VALUES (1449, '437045731645217', b'1', '2020-11-03 12:27:44', b'0', NULL, 0, 'f61b46a', '436719833572637', 'jpg', 'image/jpeg', 'alexia.jpg', 'AVATAR', 1447, NULL, NULL);
INSERT INTO `multimedia` VALUES (1468, '7626207198512', b'1', '2020-11-04 11:59:28', b'0', NULL, 0, '58bf2faa', '436473988760115', 'jpg', 'image/jpeg', 'alain.jpg', 'AVATAR', 1445, NULL, NULL);
INSERT INTO `multimedia` VALUES (1469, '12243392957962', b'1', '2020-11-04 13:16:37', b'1', NULL, 0, '28b0a451', '436473988760115', 'pdf', 'application/pdf', 'file-sample_150kB.pdf', 'DOCUMENT', 1445, 1470, NULL);
INSERT INTO `multimedia` VALUES (1471, '12356299025484', b'1', '2020-11-04 13:18:52', b'0', NULL, 0, 'fb1384b3', '436473988760115', 'mp3', 'audio/mpeg', 'file_example_MP3_1MG.mp3', 'AUDIO', 1445, 1472, NULL);
INSERT INTO `multimedia` VALUES (1481, '13000758991416', b'1', '2020-11-04 13:28:57', b'0', NULL, 0, '37ef122f', '436473988760115', 'jpg', 'image/jpeg', 'file_example_JPG_100kB.jpg', 'PICTURE', 1445, 1482, NULL);
INSERT INTO `multimedia` VALUES (1483, '197651305285887', b'1', '2020-11-06 16:46:02', b'0', NULL, 0, '58bf2faa', '436473988760115', 'jpg', 'image/jpeg', 'alain.jpg', 'PICTURE', 1445, 1484, NULL);
INSERT INTO `multimedia` VALUES (1485, '197716936552841', b'1', '2020-11-06 16:46:58', b'0', NULL, 0, 'f61b46a', '436473988760115', 'jpg', 'image/jpeg', 'alexia.jpg', 'PICTURE', 1445, 1486, NULL);
INSERT INTO `multimedia` VALUES (1487, '197758746443248', b'1', '2020-11-06 16:47:40', b'0', NULL, 0, '58bf2faa', '436473988760115', 'jpg', 'image/jpeg', 'alain.jpg', 'PICTURE', 1445, 1488, NULL);
INSERT INTO `multimedia` VALUES (1489, '197821342700826', b'1', '2020-11-06 16:48:43', b'0', NULL, 0, '58bf2faa', '436473988760115', 'jpg', 'image/jpeg', 'alain.jpg', 'PICTURE', 1445, 1490, NULL);
INSERT INTO `multimedia` VALUES (1499, '36771058222430', b'1', '2020-11-07 18:51:11', b'0', NULL, 0, 'e74aba3e', '436473988760115', 'mp4', 'video/mp4', 'file_example_MP4_480_1_5MG.mp4', 'VIDEO', 1445, 1500, NULL);
INSERT INTO `multimedia` VALUES (1517, '173877553393258', b'1', '2020-11-18 10:30:42', b'0', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1445, 1518, 'https://www.youtube.com/watch?v=jH8EoFCnYI8');
INSERT INTO `multimedia` VALUES (1519, '175964586216044', b'1', '2020-11-18 11:05:30', b'0', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1445, 1520, 'https://www.youtube.com/watch?v=QjjkStofi3Q');
INSERT INTO `multimedia` VALUES (1526, '177931397088272', b'1', '2020-11-18 11:38:16', b'1', NULL, 0, '5be65c9c', '436473988760115', 'pdf', 'application/pdf', 'Methodes_de_factorisation.pdf', 'DOCUMENT', 1445, 1527, NULL);
INSERT INTO `multimedia` VALUES (1528, '178112561224925', b'1', '2020-11-18 11:41:18', b'0', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1445, 1529, 'https://www.youtube.com/watch?v=nUMcJijJctE');
INSERT INTO `multimedia` VALUES (1532, '266497538594854', b'1', '2020-11-19 12:14:05', b'0', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1445, 1533, 'https://www.youtube.com/watch?v=wbLj2nUusSI');
INSERT INTO `multimedia` VALUES (1534, '266562510514570', b'1', '2020-11-19 12:15:09', b'0', NULL, 0, 'fb1384b3', '436473988760115', 'mp3', 'audio/mpeg', 'file_example_MP3_1MG.mp3', 'AUDIO', 1445, 1535, NULL);
INSERT INTO `multimedia` VALUES (1538, '12836221779942', b'1', '2020-11-30 13:04:13', b'0', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1445, 1539, 'https://www.youtube.com/watch?v=Ce2_k0LaE7E');

-- ----------------------------
-- Table structure for post
-- ----------------------------
DROP TABLE IF EXISTS `post`;
CREATE TABLE `post`  (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `active` bit(1) NULL DEFAULT NULL,
  `created_at` datetime(0) NOT NULL,
  `deleted` bit(1) NULL DEFAULT NULL,
  `modified_at` datetime(0) NULL DEFAULT NULL,
  `revision` bigint(20) NULL DEFAULT NULL,
  `description` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `keywords` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `account` int(11) NULL DEFAULT NULL,
  `media_subtype` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_hj56xjem2k1cs8nwm1nsh2xjx`(`id_server`) USING BTREE,
  INDEX `FKhldpsq033xe7e406jkt2ovuno`(`account`) USING BTREE,
  INDEX `FKchy449augo3lewkdmh8e9juh4`(`media_subtype`) USING BTREE,
  CONSTRAINT `FKchy449augo3lewkdmh8e9juh4` FOREIGN KEY (`media_subtype`) REFERENCES `media_subtype` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FKhldpsq033xe7e406jkt2ovuno` FOREIGN KEY (`account`) REFERENCES `account` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of post
-- ----------------------------
INSERT INTO `post` VALUES (1173, '178709546258700', b'1', '2020-10-20 13:51:41', b'0', NULL, 0, 'derivé', '', 'maths', 1, 1140);
INSERT INTO `post` VALUES (1175, '179251435389500', b'1', '2020-10-20 14:00:43', b'0', NULL, 0, '', '', 'l\'ecole à la maison', 1, 1149);
INSERT INTO `post` VALUES (1177, '180679908552000', b'1', '2020-10-20 14:24:31', b'0', NULL, 0, 'philo socrate', '', 'podcast pholi', 1, 1153);
INSERT INTO `post` VALUES (1179, '180810695554000', b'1', '2020-10-20 14:26:42', b'0', NULL, 0, 'climat', '', 'accord paris climat', 1, 1154);
INSERT INTO `post` VALUES (1181, '181294660992200', b'1', '2020-10-20 14:34:46', b'0', NULL, 0, 'francais', '', 'podcast francais', 1, 1153);
INSERT INTO `post` VALUES (1363, '270536501453300', b'1', '2020-10-24 21:56:19', b'0', NULL, 0, 'livre maths', 'maths', 'bibliotheque', 1, 1164);
INSERT INTO `post` VALUES (1365, '271784204074900', b'1', '2020-10-24 22:17:06', b'0', NULL, 0, '', '', 'laptop', 1, 1165);
INSERT INTO `post` VALUES (1420, '431607981545300', b'1', '2020-11-01 09:32:53', b'0', NULL, 0, '', '', 'Livre', 1, 1164);
INSERT INTO `post` VALUES (1470, '12266299699803', b'1', '2020-11-04 13:16:38', b'1', NULL, 0, 'Mathématiques', 'Les racines carrés', 'Racine carré', 1445, 1141);
INSERT INTO `post` VALUES (1472, '12401732885457', b'1', '2020-11-04 13:18:53', b'0', NULL, 0, 'Programmation', 'Java débutants', 'Java', 1445, 1154);
INSERT INTO `post` VALUES (1478, '12914371528639', b'1', '2020-11-04 13:27:25', b'0', NULL, 0, 'USA', 'Elections présidentielles', 'Elections USA', 1445, 1157);
INSERT INTO `post` VALUES (1482, '13006588567379', b'1', '2020-11-04 13:28:57', b'0', NULL, 0, 'Paysage', 'Test', 'Nature', 1445, 1164);
INSERT INTO `post` VALUES (1484, '197662561840898', b'1', '2020-11-06 16:46:03', b'0', NULL, 0, 'Cool', 'desc', 'Moi', 1445, 1165);
INSERT INTO `post` VALUES (1486, '197719045232674', b'1', '2020-11-06 16:46:59', b'0', NULL, 0, 'Maths', 'cool', 'Alexia', 1445, 1164);
INSERT INTO `post` VALUES (1488, '197760854659777', b'1', '2020-11-06 16:47:40', b'0', NULL, 0, 'Info', 'Vraiment cool', 'Moi', 1445, 1164);
INSERT INTO `post` VALUES (1490, '197823486879310', b'1', '2020-11-06 16:48:43', b'0', NULL, 0, 'Rap', 'La fouine', 'Rap', 1445, 1164);
INSERT INTO `post` VALUES (1500, '36782560839940', b'1', '2020-11-07 18:51:12', b'0', NULL, 0, 'Animations', 'Jeux', 'Naruto', 1445, 1154);
INSERT INTO `post` VALUES (1518, '173878097755079', b'1', '2020-11-18 10:30:43', b'0', NULL, 0, 'Informations', NULL, 'Journal Equinoxe', 1445, 1166);
INSERT INTO `post` VALUES (1520, '175966019639688', b'1', '2020-11-18 11:05:31', b'0', NULL, 0, 'Films', 'test', 'Dead pool 2', 1445, 1155);
INSERT INTO `post` VALUES (1527, '177931465046196', b'1', '2020-11-18 11:38:16', b'1', NULL, 0, 'Maths', 'test', 'Factorisation', 1445, 1140);
INSERT INTO `post` VALUES (1529, '178114103838801', b'1', '2020-11-18 11:41:19', b'0', NULL, 0, 'Films', 'test', 'spiderman', 1445, 1166);
INSERT INTO `post` VALUES (1533, '266499372713966', b'1', '2020-11-19 12:14:06', b'0', NULL, 0, 'Live', NULL, 'Facebook', 1445, 1154);
INSERT INTO `post` VALUES (1535, '266562587714481', b'1', '2020-11-19 12:15:09', b'0', NULL, 0, 'test', '', 'Test', 1445, 1154);
INSERT INTO `post` VALUES (1536, '271950196381117', b'1', '2020-11-19 13:44:57', b'1', NULL, 0, 'Comédie', NULL, 'Les Tyoristes : convocation', 1445, 1149);
INSERT INTO `post` VALUES (1537, '272285352692596', b'1', '2020-11-19 13:50:32', b'1', NULL, 0, 'test', NULL, 'test', 1445, 1155);
INSERT INTO `post` VALUES (1539, '12839657438144', b'1', '2020-11-30 13:04:14', b'0', NULL, 0, 'sm', NULL, 'Rihanna', 1445, 1154);

-- ----------------------------
-- Table structure for professional
-- ----------------------------
DROP TABLE IF EXISTS `professional`;
CREATE TABLE `professional`  (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `active` bit(1) NULL DEFAULT NULL,
  `created_at` datetime(0) NOT NULL,
  `deleted` bit(1) NULL DEFAULT NULL,
  `modified_at` datetime(0) NULL DEFAULT NULL,
  `revision` bigint(20) NULL DEFAULT NULL,
  `city` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `current_position` bit(1) NULL DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `establishment_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `specialty` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `account` int(11) NULL DEFAULT NULL,
  `end_date` datetime(0) NULL DEFAULT NULL,
  `job_title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `start_date` datetime(0) NOT NULL,
  `activity_area` int(11) NULL DEFAULT NULL,
  `activity_sector` int(11) NULL DEFAULT NULL,
  `another_activity_area` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `another_activity_sector` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_jhtn3j550trbpd1t9gvk99ni4`(`id_server`) USING BTREE,
  INDEX `FKi5t5gwrrxhgyjedhvxcekbyxc`(`activity_area`) USING BTREE,
  INDEX `FKqn9wbhf8i3wxe4tfu4x2lo8pm`(`activity_sector`) USING BTREE,
  INDEX `FK_otn62bgvcnopnav5abccfgiu3`(`account`) USING BTREE,
  CONSTRAINT `FK_otn62bgvcnopnav5abccfgiu3` FOREIGN KEY (`account`) REFERENCES `account` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FKi5t5gwrrxhgyjedhvxcekbyxc` FOREIGN KEY (`activity_area`) REFERENCES `activity_area` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FKqn9wbhf8i3wxe4tfu4x2lo8pm` FOREIGN KEY (`activity_sector`) REFERENCES `activity_sector` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of professional
-- ----------------------------
INSERT INTO `professional` VALUES (1409, '118488529919300', b'1', '2020-10-28 18:34:54', b'0', NULL, 0, 'Yaounde', b'1', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using', 'YAMO GROUP', NULL, 1, NULL, 'Developpeuse', '2020-10-01 02:00:00', 765, 1389, NULL, NULL);
INSERT INTO `professional` VALUES (1410, '286603711065500', b'1', '2020-10-30 16:46:35', b'0', NULL, 0, 'Yaounde', b'0', 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one ', 'YAMO GROUP', NULL, 1, '2020-10-30 16:46:34', 'Conseillere', '2020-10-30 00:00:00', 745, 1374, NULL, NULL);
INSERT INTO `professional` VALUES (1411, '298262098879200', b'1', '2020-10-30 20:30:46', b'1', NULL, 0, 'Douala', b'0', 'yyyyyyyyyyyyyyysqhdcbusvuycbzuedhvuiesrhbfuinebfuszeufbzuefuebufiherufgezrbfijhzeuirbfhzerjifbguebruhgbzeibgfeubrfgnzeurhbgzerbfgiuzneirbghzebrjginerigbizernfgbezrbugbhizerhfgzeirguzeringhizerbguvzehbnrigbeirbgizeribhigebzrigbzbiehznvizbevbifvngegibzurdfi', 'NEOVA', NULL, 1, '2020-10-31 00:00:00', 'Cameraman', '2020-10-01 00:00:00', 743, 1373, NULL, NULL);
INSERT INTO `professional` VALUES (1412, '298345226626200', b'1', '2020-10-30 20:32:09', b'0', NULL, 0, 'Douala', b'0', NULL, 'NEOVA', NULL, 1, '2020-10-31 00:00:00', 'Cameraman', '2020-10-01 00:00:00', 743, 1373, NULL, NULL);
INSERT INTO `professional` VALUES (1452, '15279040274625', b'1', '2020-11-03 12:35:20', b'0', NULL, 0, 'Yaoundé', b'0', 'Gestion des clients', 'Express union', NULL, 1447, '2018-11-21 00:00:00', 'Stagiaire', '2017-11-30 00:00:00', 784, 1407, 'Leelcash', 'LeelCash-box');
INSERT INTO `professional` VALUES (1453, '437622831627594', b'1', '2020-11-03 12:37:18', b'0', NULL, 0, 'Yaoundé', b'1', 'Suivi clientele', 'Snh', NULL, 1447, '2020-11-03 12:37:18', 'Suivi clientèle', '2018-11-07 00:00:00', 753, 1383, NULL, NULL);
INSERT INTO `professional` VALUES (1464, '439130839490782', b'1', '2020-11-03 13:02:27', b'0', NULL, 0, 'Douala', b'0', 'Automaticien', 'Sylène cameroun', NULL, 1445, '2010-08-31 00:00:00', 'Automaticien', '2008-09-01 00:00:00', 765, 1390, NULL, NULL);
INSERT INTO `professional` VALUES (1465, '439210443091942', b'1', '2020-11-03 13:03:46', b'0', NULL, 0, 'Yaoundé', b'0', 'Développeur Informatique', 'Ufi paiement solutions', NULL, 1445, '2020-01-31 00:00:00', 'Développeur', '2018-09-03 00:00:00', 745, 1374, NULL, NULL);
INSERT INTO `professional` VALUES (1466, '175245012473043', b'1', '2020-11-03 13:05:13', b'0', NULL, 0, 'Yaoundé', b'1', 'Développeur backend & frontend', 'Yamo group', NULL, 1445, '2020-11-13 10:33:24', 'Développeur', '2020-08-12 00:00:00', 784, 1407, 'Espace', 'Astronomie');
INSERT INTO `professional` VALUES (1507, '183857902349913', b'1', '2020-11-13 10:45:37', b'0', NULL, 0, 'Yaoundé', b'0', 'Développement', 'Amla cameroun', NULL, 1445, '2014-12-18 00:00:00', 'Ingénieur développeur', '2014-03-04 00:00:00', 780, 1378, 'Ventes et Détails', 'Informatique - Pétrole');

-- ----------------------------
-- Table structure for professional_status
-- ----------------------------
DROP TABLE IF EXISTS `professional_status`;
CREATE TABLE `professional_status`  (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `label` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_fc9uvppyqis203m9mqheyyvc6`(`id_server`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of professional_status
-- ----------------------------
INSERT INTO `professional_status` VALUES (5, '11559034729500', 'Etudiant');
INSERT INTO `professional_status` VALUES (6, '11559035748200', 'Enseignant');
INSERT INTO `professional_status` VALUES (7, '11559036679300', 'Professionnel');

-- ----------------------------
-- Table structure for secondary_education
-- ----------------------------
DROP TABLE IF EXISTS `secondary_education`;
CREATE TABLE `secondary_education`  (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `active` bit(1) NULL DEFAULT NULL,
  `created_at` datetime(0) NOT NULL,
  `deleted` bit(1) NULL DEFAULT NULL,
  `modified_at` datetime(0) NULL DEFAULT NULL,
  `revision` bigint(20) NULL DEFAULT NULL,
  `city` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `current_position` bit(1) NULL DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `establishment_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `specialty` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `account` int(11) NULL DEFAULT NULL,
  `establishment_type` int(11) NULL DEFAULT NULL,
  `study_level` int(11) NULL DEFAULT NULL,
  `prepared_diploma` int(11) NULL DEFAULT NULL,
  `another_prepared_diploma` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `another_study_level` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `another_establishment_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_nhxvw6ieoh23429mwaq1qkmsj`(`id_server`) USING BTREE,
  INDEX `FKk3ptaihbdqfncqbwwou95xust`(`prepared_diploma`) USING BTREE,
  INDEX `FK_o2ypntl0oqre1t4gkekwfvtse`(`establishment_type`) USING BTREE,
  INDEX `FK_sbaqi5vxvcxexqg00nj4kubhc`(`study_level`) USING BTREE,
  INDEX `FK_h91sw2cbvc4og7hptcd4fw3ua`(`account`) USING BTREE,
  CONSTRAINT `FK_h91sw2cbvc4og7hptcd4fw3ua` FOREIGN KEY (`account`) REFERENCES `account` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_o2ypntl0oqre1t4gkekwfvtse` FOREIGN KEY (`establishment_type`) REFERENCES `establishment_type` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_sbaqi5vxvcxexqg00nj4kubhc` FOREIGN KEY (`study_level`) REFERENCES `study_level` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FKk3ptaihbdqfncqbwwou95xust` FOREIGN KEY (`prepared_diploma`) REFERENCES `diploma` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of secondary_education
-- ----------------------------
INSERT INTO `secondary_education` VALUES (1347, '62295837715400', b'1', '2020-10-22 12:06:00', b'0', NULL, 0, 'Yaounde', b'0', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco', 'College la retraite', 'Langue francaise', 1, 721, 897, 1105, NULL, NULL, NULL);
INSERT INTO `secondary_education` VALUES (1349, '83820687325200', b'1', '2020-10-22 18:04:45', b'0', NULL, 0, 'Yaounde', b'0', 'But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human', 'Lycee de mimboman', 'Langues anciennes', 1, 722, 903, 1108, NULL, NULL, NULL);
INSERT INTO `secondary_education` VALUES (1510, '188927834076753', b'1', '2020-11-13 12:50:54', b'0', NULL, 0, 'Yaoundé', b'0', 'Attention', 'Cetic nsam', 'Menuserie', 1445, 724, 921, 1120, 'Konguep', NULL, NULL);
INSERT INTO `secondary_education` VALUES (1513, '2305857489358', b'1', '2020-11-16 10:52:05', b'0', NULL, 0, 'Douala', b'0', 'Coiffure & Esthétique', 'Yamo group', 'Mode Fashion', 1447, 724, 903, 1117, 'BTS', NULL, NULL);
INSERT INTO `secondary_education` VALUES (1515, '7741409818731', b'1', '2020-11-16 12:01:41', b'0', NULL, 0, 'Dschang', b'0', 'Test', 'Fokou', 'Maths', 1447, 721, 921, 1112, NULL, 'Manyan', 'Test');

-- ----------------------------
-- Table structure for skill
-- ----------------------------
DROP TABLE IF EXISTS `skill`;
CREATE TABLE `skill`  (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `active` bit(1) NULL DEFAULT NULL,
  `created_at` datetime(0) NOT NULL,
  `deleted` bit(1) NULL DEFAULT NULL,
  `modified_at` datetime(0) NULL DEFAULT NULL,
  `revision` bigint(20) NULL DEFAULT NULL,
  `discipline` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `keywords` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `label` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `skill_mastered` bit(1) NULL DEFAULT NULL,
  `account` int(11) NULL DEFAULT NULL,
  `level` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_mgwf4f5epvhru88djnobni1vl`(`id_server`) USING BTREE,
  INDEX `FK25xg7gyx32jtdiuek1fpp4xx9`(`account`) USING BTREE,
  INDEX `FKirhnfwdwcegn8fstgix349moy`(`level`) USING BTREE,
  CONSTRAINT `FK25xg7gyx32jtdiuek1fpp4xx9` FOREIGN KEY (`account`) REFERENCES `account` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FKirhnfwdwcegn8fstgix349moy` FOREIGN KEY (`level`) REFERENCES `level` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of skill
-- ----------------------------
INSERT INTO `skill` VALUES (1167, '149408030282400', b'1', '2020-10-19 10:45:44', b'0', NULL, 0, 'Mathematiques', 'geometrie, trigonometrie', 'Geomatrie plane', b'1', 1, 1101);
INSERT INTO `skill` VALUES (1168, '149457454461700', b'1', '2020-10-19 16:39:50', b'0', NULL, 0, 'Droit', 'affaires, droits, public', 'Droit des affaires', b'0', 1, 1104);
INSERT INTO `skill` VALUES (1352, '149424253203700', b'1', '2020-10-23 12:14:36', b'0', NULL, 0, 'histoire de l\'Afrique', 'le peuple Peul et Foulbé', 'histoire', b'1', 1, 1102);
INSERT INTO `skill` VALUES (1353, '149443534129100', b'1', '2020-10-23 12:15:26', b'0', NULL, 0, 'politique de la Chine', 'chine, politique, la soie', 'Geopolitique', b'0', 1, 1103);
INSERT INTO `skill` VALUES (1354, '149629805131000', b'1', '2020-10-23 12:21:22', b'0', NULL, 0, 'philosophie de Freud', 'freud, psychologie', 'philosophie', b'1', 1, 1102);
INSERT INTO `skill` VALUES (1355, '149691006136700', b'1', '2020-10-23 12:22:23', b'0', NULL, 0, 'psychologie des enfants', 'enfants, psychologie', 'Psychologie', b'0', 1, 1103);
INSERT INTO `skill` VALUES (1454, '437682615095188', b'1', '2020-11-03 12:38:18', b'0', NULL, 0, 'Economie', 'Economie général', 'Micro économie', b'1', 1447, 1101);
INSERT INTO `skill` VALUES (1455, '437758660097391', b'1', '2020-11-03 12:39:34', b'0', NULL, 0, 'Mathématiques', 'Cosinus & Sinus', 'Trigonométrie', b'0', 1447, 1104);
INSERT INTO `skill` VALUES (1456, '437896098203362', b'1', '2020-11-03 12:41:51', b'0', NULL, 0, 'Mathématiques', 'la dérivée', 'Fonctions', b'0', 1447, 1103);
INSERT INTO `skill` VALUES (1460, '438725114592927', b'1', '2020-11-03 12:55:40', b'0', NULL, 0, 'Programmation', 'Language Java', 'Java', b'1', 1445, 1101);
INSERT INTO `skill` VALUES (1461, '438754588522666', b'1', '2020-11-03 12:56:10', b'0', NULL, 0, 'Programmation', 'Les pointeurs', 'C', b'1', 1445, 1102);
INSERT INTO `skill` VALUES (1462, '438851941386666', b'1', '2020-11-03 12:57:47', b'0', NULL, 0, 'Language de programmation', 'Les préliminaires', 'Python', b'0', 1445, 1103);
INSERT INTO `skill` VALUES (1463, '438907466589217', b'1', '2020-11-03 12:58:42', b'0', NULL, 0, 'Langues parlées', 'Speaking', 'Anglais', b'0', 1445, 1104);

-- ----------------------------
-- Table structure for study_level
-- ----------------------------
DROP TABLE IF EXISTS `study_level`;
CREATE TABLE `study_level`  (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `label` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `education` int(11) NULL DEFAULT NULL,
  `specified` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_jmdv843pnvonbtfhom3i794u1`(`id_server`) USING BTREE,
  INDEX `FK19p7f1vp5j6un4jr0u5svnvwp`(`education`) USING BTREE,
  CONSTRAINT `FK19p7f1vp5j6un4jr0u5svnvwp` FOREIGN KEY (`education`) REFERENCES `education` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of study_level
-- ----------------------------
INSERT INTO `study_level` VALUES (894, '109175364431700', 'Sixième (6ème)', 'Sixième (6ème)', 438, 0);
INSERT INTO `study_level` VALUES (895, '109175373156700', 'Cinquième (5ème)', 'Cinquième (5ème)', 438, 0);
INSERT INTO `study_level` VALUES (896, '109175375387400', 'Quatrième (4ème)', 'Quatrième (4ème)', 438, 0);
INSERT INTO `study_level` VALUES (897, '109175377541200', 'Troisième (3ème)', 'Troisième (3ème)', 438, 0);
INSERT INTO `study_level` VALUES (898, '109175379615000', 'Première S (Scientifique)', 'Première S (Scientifique)', 438, 0);
INSERT INTO `study_level` VALUES (899, '109175381826000', 'Première ES (Économique et social)', 'Première ES (Économique et social)', 438, 0);
INSERT INTO `study_level` VALUES (900, '109175383832800', 'Première L (Littéraire)', 'Première L (Littéraire)', 438, 0);
INSERT INTO `study_level` VALUES (901, '109175386356800', 'Terminal S (Scientifique)', 'Terminal S (Scientifique)', 438, 0);
INSERT INTO `study_level` VALUES (902, '109175388462200', 'Terminal ES (Économique et social)', 'Terminal ES (Économique et social)', 438, 0);
INSERT INTO `study_level` VALUES (903, '109175391086100', 'Terminal L (Littéraire)', 'Terminal L (Littéraire)', 438, 0);
INSERT INTO `study_level` VALUES (904, '109175393329100', 'Première STMG (Sciences et technologies du management et de la gestion)', 'Première STMG (Sciences et technologies du management et de la gestion)', 438, 0);
INSERT INTO `study_level` VALUES (905, '109175395738300', 'Première STD2A (Sciences et technologies du design et des arts appliqués)', 'Première STD2A (Sciences et technologies du design et des arts appliqués)', 438, 0);
INSERT INTO `study_level` VALUES (906, '109175398259800', 'Première STI2D (sciences et technologies de l\'industrie et du développement durable)', 'Première STI2D (sciences et technologies de l\'industrie et du développement durable)', 438, 0);
INSERT INTO `study_level` VALUES (907, '109175400254800', 'Première STL (sciences et technologies de laboratoire)', 'Première STL (sciences et technologies de laboratoire)', 438, 0);
INSERT INTO `study_level` VALUES (908, '109175402496900', 'Première ST2S (sciences et technologies de la santé et du social)', 'Première ST2S (sciences et technologies de la santé et du social)', 438, 0);
INSERT INTO `study_level` VALUES (909, '109175404587400', 'Première STAV (sciences et technologies de l\'agronomie et du vivant)', 'Première STAV (sciences et technologies de l\'agronomie et du vivant)', 438, 0);
INSERT INTO `study_level` VALUES (910, '109175406961400', 'Première TMD (technique de la musique et de la danse)', 'Première TMD (technique de la musique et de la danse)', 438, 0);
INSERT INTO `study_level` VALUES (911, '109175408990100', 'Première STHR (sciences et technologies de l\'hôtellerie et de la restauration)', 'Première STHR (sciences et technologies de l\'hôtellerie et de la restauration)', 438, 0);
INSERT INTO `study_level` VALUES (912, '109175411165400', 'Terminal STMG (Sciences et technologies du management et de la gestion)', 'Terminal STMG (Sciences et technologies du management et de la gestion)', 438, 0);
INSERT INTO `study_level` VALUES (913, '109175413303400', 'Terminal STD2A (Sciences et technologies du design et des arts appliqués)', 'Terminal STD2A (Sciences et technologies du design et des arts appliqués)', 438, 0);
INSERT INTO `study_level` VALUES (914, '109175415608900', 'Terminal STI2D (sciences et technologies de l\'industrie et du développement durable)', 'Terminal STI2D (sciences et technologies de l\'industrie et du développement durable)', 438, 0);
INSERT INTO `study_level` VALUES (915, '109175418957900', 'Terminal STL (sciences et technologies de laboratoire)', 'Terminal STL (sciences et technologies de laboratoire)', 438, 0);
INSERT INTO `study_level` VALUES (916, '109175420935700', 'Terminal ST2S (sciences et technologies de la santé et du social)', 'Terminal ST2S (sciences et technologies de la santé et du social)', 438, 0);
INSERT INTO `study_level` VALUES (917, '109175422980500', 'Terminal STAV (sciences et technologies de l\'agronomie et du vivant)', 'Terminal STAV (sciences et technologies de l\'agronomie et du vivant)', 438, 0);
INSERT INTO `study_level` VALUES (918, '109175425133000', 'Terminal TMD (technique de la musique et de la danse)', 'Terminal TMD (technique de la musique et de la danse)', 438, 0);
INSERT INTO `study_level` VALUES (919, '109175427187800', 'Terminal STHR (sciences et technologies de l\'hôtellerie et de la restauration)', 'Terminal STHR (sciences et technologies de l\'hôtellerie et de la restauration)', 438, 0);
INSERT INTO `study_level` VALUES (920, '109175429011600', 'Seconde professionnelle CAP', 'Seconde professionnelle CAP', 438, 0);
INSERT INTO `study_level` VALUES (921, '109175431024500', 'Première Professionnelle CAP', 'Première Professionnelle CAP', 438, 0);
INSERT INTO `study_level` VALUES (922, '109175432849500', 'Seconde professionnelle BEP', 'Seconde professionnelle BEP', 438, 0);
INSERT INTO `study_level` VALUES (923, '109175434977000', 'Première Professionnelle BEP', 'Première Professionnelle BEP', 438, 0);
INSERT INTO `study_level` VALUES (924, '109175436954300', 'Seconde CAP', 'Seconde CAP', 438, 0);
INSERT INTO `study_level` VALUES (925, '109175439062800', 'Première CAP', 'Première CAP', 438, 0);
INSERT INTO `study_level` VALUES (926, '109175441318700', 'Autres établissements du secondaire', 'Autres établissements du secondaire', 438, 1);
INSERT INTO `study_level` VALUES (927, '109175443426300', 'Licence 1 (Bac +1)', 'Licence 1 (Bac +1)', 439, 0);
INSERT INTO `study_level` VALUES (928, '109175445325600', 'Licence 2 (Bac+2)', 'Licence 2 (Bac+2)', 439, 0);
INSERT INTO `study_level` VALUES (929, '109175447236600', 'Licence 3 (Bac+3)', 'Licence 3 (Bac+3)', 439, 0);
INSERT INTO `study_level` VALUES (930, '109175449100400', 'Master 1 (Bac+4)', 'Master 1 (Bac+4)', 439, 0);
INSERT INTO `study_level` VALUES (931, '109175451088600', 'Master 2 (Bac+5)', 'Master 2 (Bac+5)', 439, 0);
INSERT INTO `study_level` VALUES (932, '109175453159600', 'Doctorat 1 (Bac +6)', 'Doctorat 1 (Bac +6)', 439, 0);
INSERT INTO `study_level` VALUES (933, '109175455316400', 'Doctorat 2 (Bac +7)', 'Doctorat 2 (Bac +7)', 439, 0);
INSERT INTO `study_level` VALUES (934, '109175457545300', 'Doctorat 3 (Bac +8)', 'Doctorat 3 (Bac +8)', 439, 0);
INSERT INTO `study_level` VALUES (935, '109175459700800', 'Première année (Bac +1)', 'Première année (Bac +1)', 439, 0);
INSERT INTO `study_level` VALUES (936, '109175462112800', 'Deuxième année (Bac +2)', 'Deuxième année (Bac +2)', 439, 0);
INSERT INTO `study_level` VALUES (937, '109175464066900', 'Classes préparatoires : Bac +1 à Bac +2', 'Classes préparatoires : Bac +1 à Bac +2', 439, 0);
INSERT INTO `study_level` VALUES (938, '109175466067700', 'Bachelor (Bac+4)', 'Bachelor (Bac+4)', 439, 0);
INSERT INTO `study_level` VALUES (939, '109175467869900', 'Première année BTS (Bac +1)', 'Première année BTS (Bac +1)', 439, 0);
INSERT INTO `study_level` VALUES (940, '109175470371300', 'Deuxième année BTS (Bac +2)', 'Deuxième année BTS (Bac +2)', 439, 0);
INSERT INTO `study_level` VALUES (941, '109175473998900', 'Première année IUT (Bac +1)', 'Première année IUT (Bac +1)', 439, 0);
INSERT INTO `study_level` VALUES (942, '109175476685800', 'Deuxième année IUT (Bac +2)', 'Deuxième année IUT (Bac +2)', 439, 0);
INSERT INTO `study_level` VALUES (943, '109175479128700', 'Autres à préciser manuellement', 'Autres à préciser manuellement', 439, 1);

-- ----------------------------
-- Table structure for teaching_area
-- ----------------------------
DROP TABLE IF EXISTS `teaching_area`;
CREATE TABLE `teaching_area`  (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `label` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `teaching_area_group` int(11) NULL DEFAULT NULL,
  `education` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_ox8ks35vdkc9nd83in6u23a1i`(`id_server`) USING BTREE,
  INDEX `FKr5km3wq5qmfmu05ttam0ojqu3`(`education`) USING BTREE,
  INDEX `FKh0g8a3pq339qtc9oqllryu034`(`teaching_area_group`) USING BTREE,
  CONSTRAINT `FKh0g8a3pq339qtc9oqllryu034` FOREIGN KEY (`teaching_area_group`) REFERENCES `teaching_area_group` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of teaching_area
-- ----------------------------
INSERT INTO `teaching_area` VALUES (1212, '271089424686400', 'Droit privé et sciences criminelles', 'Droit privé et sciences criminelles', 1184, NULL);
INSERT INTO `teaching_area` VALUES (1213, '271113721384499', 'Droit public', 'Droit public', 1184, NULL);
INSERT INTO `teaching_area` VALUES (1214, '271113727241400', 'Histoire du droit et des institutions', 'Histoire du droit et des institutions', 1184, NULL);
INSERT INTO `teaching_area` VALUES (1215, '271113732057400', 'Science politique', 'Science politique', 1184, NULL);
INSERT INTO `teaching_area` VALUES (1216, '271113739286300', 'Sciences économiques', 'Sciences économiques', 1185, NULL);
INSERT INTO `teaching_area` VALUES (1217, '271113746612000', 'Sciences de gestion', 'Sciences de gestion', 1185, NULL);
INSERT INTO `teaching_area` VALUES (1218, '271113754501300', 'Sciences du langage : linguistique et phonétique générales', 'Sciences du langage : linguistique et phonétique générales', 1187, NULL);
INSERT INTO `teaching_area` VALUES (1219, '271113761244700', 'Langues et littératures anciennes', 'Langues et littératures anciennes', 1187, NULL);
INSERT INTO `teaching_area` VALUES (1220, '271113766148600', 'Langue et littérature françaises', 'Langue et littérature françaises', 1187, NULL);
INSERT INTO `teaching_area` VALUES (1221, '271113770860600', 'Littératures comparées', 'Littératures comparées', 1187, NULL);
INSERT INTO `teaching_area` VALUES (1222, '271113775927299', 'Langues et littératures anglaises et anglo-saxonnes', 'Langues et littératures anglaises et anglo-saxonnes', 1187, NULL);
INSERT INTO `teaching_area` VALUES (1223, '271113781932600', 'Langues et littératures germaniques et scandinaves', 'Langues et littératures germaniques et scandinaves', 1187, NULL);
INSERT INTO `teaching_area` VALUES (1224, '271113787825200', 'Langues et littératures romanes : espagnol, italien, portugais, autres langues romanes', 'Langues et littératures romanes : espagnol, italien, portugais, autres langues romanes', 1187, NULL);
INSERT INTO `teaching_area` VALUES (1225, '271113793234700', 'Langues et littératures arabes, chinoises, japonaises, hébraïques, d\'autres domaines linguistiques', 'Langues et littératures arabes, chinoises, japonaises, hébraïques, d\'autres domaines linguistiques', 1187, NULL);
INSERT INTO `teaching_area` VALUES (1226, '271113798901300', 'Psychologie et Ergonomie', 'Psychologie et Ergonomie', 1188, NULL);
INSERT INTO `teaching_area` VALUES (1227, '271113804698600', 'Philosophie', 'Philosophie', 1188, NULL);
INSERT INTO `teaching_area` VALUES (1228, '271113809936799', 'Architecture et Arts : plastiques, du spectacle, musique, musicologie, esthétique, sciences de l\'art', 'Architecture et Arts : plastiques, du spectacle, musique, musicologie, esthétique, sciences de l\'art', 1188, NULL);
INSERT INTO `teaching_area` VALUES (1229, '271113815584699', 'Sociologie, démographie', 'Sociologie, démographie', 1188, NULL);
INSERT INTO `teaching_area` VALUES (1230, '271113820180900', 'Anthropologie biologique, ethnologie, préhistoire', 'Anthropologie biologique, ethnologie, préhistoire', 1188, NULL);
INSERT INTO `teaching_area` VALUES (1231, '271113825157800', 'Histoire et civilisations : histoire et archéologie des mondes anciens et des mondes médiévaux ; de l\'art', 'Histoire et civilisations : histoire et archéologie des mondes anciens et des mondes médiévaux ; de l\'art', 1188, NULL);
INSERT INTO `teaching_area` VALUES (1232, '271113831766400', 'Histoire et civilisations : histoire des mondes modernes, histoire du monde contemporain ; de l\'art ; de la musique', 'Histoire et civilisations : histoire des mondes modernes, histoire du monde contemporain ; de l\'art ; de la musique', 1188, NULL);
INSERT INTO `teaching_area` VALUES (1233, '271113846370900', 'Géographie physique, humaine, économique et régionale', 'Géographie physique, humaine, économique et régionale', 1188, NULL);
INSERT INTO `teaching_area` VALUES (1234, '271113850729100', 'Aménagement de l\'espace, urbanisme', 'Aménagement de l\'espace, urbanisme', 1188, NULL);
INSERT INTO `teaching_area` VALUES (1235, '271113860620200', 'Sciences de l\'éducation', 'Sciences de l\'éducation', 1189, NULL);
INSERT INTO `teaching_area` VALUES (1236, '271113865892199', 'Sciences de l\'information et de la communication', 'Sciences de l\'information et de la communication', 1189, NULL);
INSERT INTO `teaching_area` VALUES (1237, '271113870471799', 'Épistémologie, histoire des sciences et des techniques', 'Épistémologie, histoire des sciences et des techniques', 1189, NULL);
INSERT INTO `teaching_area` VALUES (1238, '271113876707600', 'Cultures et langues régionales', 'Cultures et langues régionales', 1189, NULL);
INSERT INTO `teaching_area` VALUES (1239, '271113885905299', 'Sciences et techniques des activités physiques et sportives', 'Sciences et techniques des activités physiques et sportives', 1189, NULL);
INSERT INTO `teaching_area` VALUES (1240, '271114162034099', 'Théologie catholique', 'Théologie catholique', 1189, NULL);
INSERT INTO `teaching_area` VALUES (1241, '271114166223699', 'Théologie protestante', 'Théologie protestante', 1189, NULL);
INSERT INTO `teaching_area` VALUES (1242, '271114173406500', 'Mathématiques', 'Mathématiques', 1191, NULL);
INSERT INTO `teaching_area` VALUES (1243, '271114177959800', 'Mathématiques appliquées et applications des mathématiques', 'Mathématiques appliquées et applications des mathématiques', 1191, NULL);
INSERT INTO `teaching_area` VALUES (1244, '271114181117800', 'Informatique', 'Informatique', 1191, NULL);
INSERT INTO `teaching_area` VALUES (1245, '271114186476600', 'Milieux denses et matériaux', 'Milieux denses et matériaux', 1192, NULL);
INSERT INTO `teaching_area` VALUES (1246, '271114191090499', 'Constituants élémentaires', 'Constituants élémentaires', 1192, NULL);
INSERT INTO `teaching_area` VALUES (1247, '271114194242400', 'Milieux dilués et optique', 'Milieux dilués et optique', 1192, NULL);
INSERT INTO `teaching_area` VALUES (1248, '271114200851699', 'Chimie théorique, physique, analytique', 'Chimie théorique, physique, analytique', 1193, NULL);
INSERT INTO `teaching_area` VALUES (1249, '271114207278300', 'Chimie organique, inorganique, industrielle', 'Chimie organique, inorganique, industrielle', 1193, NULL);
INSERT INTO `teaching_area` VALUES (1250, '271114211069600', 'Chimie des matériaux', 'Chimie des matériaux', 1193, NULL);
INSERT INTO `teaching_area` VALUES (1251, '271114218634900', 'Astronomie, astrophysique', 'Astronomie, astrophysique', 1194, NULL);
INSERT INTO `teaching_area` VALUES (1252, '271114223201200', 'Structure et évolution de la Terre et des autres planètes', 'Structure et évolution de la Terre et des autres planètes', 1194, NULL);
INSERT INTO `teaching_area` VALUES (1253, '271114226772600', 'Terre solide : géodynamique des enveloppes supérieures, paléobiosphère', 'Terre solide : géodynamique des enveloppes supérieures, paléobiosphère', 1194, NULL);
INSERT INTO `teaching_area` VALUES (1254, '271114231849399', 'Météorologie, océanographie physique et physique de l\'environnement', 'Météorologie, océanographie physique et physique de l\'environnement', 1194, NULL);
INSERT INTO `teaching_area` VALUES (1255, '271114236534300', 'Mécanique, génie mécanique, génie civil', 'Mécanique, génie mécanique, génie civil', 1195, NULL);
INSERT INTO `teaching_area` VALUES (1256, '271114241062600', 'Génie informatique, automatique et traitement du signal', 'Génie informatique, automatique et traitement du signal', 1195, NULL);
INSERT INTO `teaching_area` VALUES (1257, '271114244299600', 'Énergétique, génie des procédés', 'Énergétique, génie des procédés', 1195, NULL);
INSERT INTO `teaching_area` VALUES (1258, '271114247060500', 'Génie Électrique, Électronique, optronique et systèmes', 'Génie Électrique, Électronique, optronique et systèmes', 1195, NULL);
INSERT INTO `teaching_area` VALUES (1259, '271114252411600', 'Biochimie et biologie moléculaire', 'Biochimie et biologie moléculaire', 1196, NULL);
INSERT INTO `teaching_area` VALUES (1260, '271114256500500', 'Biologie cellulaire', 'Biologie cellulaire', 1196, NULL);
INSERT INTO `teaching_area` VALUES (1261, '271114259727999', 'Physiologie', 'Physiologie', 1196, NULL);
INSERT INTO `teaching_area` VALUES (1262, '271114262400600', 'Biologie des populations et écologie', 'Biologie des populations et écologie', 1196, NULL);
INSERT INTO `teaching_area` VALUES (1263, '271114264985300', 'Biologie des organismes', 'Biologie des organismes', 1196, NULL);
INSERT INTO `teaching_area` VALUES (1264, '271114267563699', 'Neurosciences', 'Neurosciences', 1196, NULL);
INSERT INTO `teaching_area` VALUES (1265, '271114275454300', 'Sciences physico-chimiques et ingénierie appliquée à la santé', 'Sciences physico-chimiques et ingénierie appliquée à la santé', 1198, NULL);
INSERT INTO `teaching_area` VALUES (1266, '271114279555500', 'Sciences du médicament et des autres produits de santé', 'Sciences du médicament et des autres produits de santé', 1198, NULL);
INSERT INTO `teaching_area` VALUES (1267, '271114282405800', 'Sciences biologiques, fondamentales et cliniques', 'Sciences biologiques, fondamentales et cliniques', 1198, NULL);
INSERT INTO `teaching_area` VALUES (1268, '271114286441100', 'Sciences physico-chimiques et ingénierie appliquée à la santé', 'Sciences physico-chimiques et ingénierie appliquée à la santé', 1199, NULL);
INSERT INTO `teaching_area` VALUES (1269, '271114290317800', 'Sciences du médicament et des autres produits de santé', 'Sciences du médicament et des autres produits de santé', 1199, NULL);
INSERT INTO `teaching_area` VALUES (1270, '271114293000200', 'Sciences biologiques, fondamentales et cliniques', 'Sciences biologiques, fondamentales et cliniques', 1199, NULL);
INSERT INTO `teaching_area` VALUES (1271, '271114297156300', 'Maïeutique', 'Maïeutique', 1200, NULL);
INSERT INTO `teaching_area` VALUES (1272, '271114300187300', 'Sciences de la rééducation et de la réadaptation', 'Sciences de la rééducation et de la réadaptation', 1200, NULL);
INSERT INTO `teaching_area` VALUES (1273, '271114303142600', 'Sciences infirmières', 'Sciences infirmières', 1200, NULL);
INSERT INTO `teaching_area` VALUES (1274, '271114309456600', 'Anatomie', 'Anatomie', 1202, NULL);
INSERT INTO `teaching_area` VALUES (1275, '271114314049800', 'Histologie, embryologie et cytogénétique', 'Histologie, embryologie et cytogénétique', 1202, NULL);
INSERT INTO `teaching_area` VALUES (1276, '271114317962399', 'Anatomie et cytologie pathologiques', 'Anatomie et cytologie pathologiques', 1202, NULL);
INSERT INTO `teaching_area` VALUES (1277, '271114322132300', 'Biophysique et médecine nucléaire', 'Biophysique et médecine nucléaire', 1203, NULL);
INSERT INTO `teaching_area` VALUES (1278, '271114325044600', 'Radiologie et imagerie médicale', 'Radiologie et imagerie médicale', 1203, NULL);
INSERT INTO `teaching_area` VALUES (1279, '271114328426800', 'Biochimie et biologie moléculaire', 'Biochimie et biologie moléculaire', 1204, NULL);
INSERT INTO `teaching_area` VALUES (1280, '271114331305000', 'Physiologie', 'Physiologie', 1204, NULL);
INSERT INTO `teaching_area` VALUES (1281, '271114333861000', 'Biologie cellulaire', 'Biologie cellulaire', 1204, NULL);
INSERT INTO `teaching_area` VALUES (1282, '271114336223700', 'Nutrition', 'Nutrition', 1204, NULL);
INSERT INTO `teaching_area` VALUES (1283, '271114339928300', 'Bactériologie - virologie ; hygiène hospitalière', 'Bactériologie - virologie ; hygiène hospitalière', 1205, NULL);
INSERT INTO `teaching_area` VALUES (1284, '271114342670300', 'Parasitologie et mycologie', 'Parasitologie et mycologie', 1205, NULL);
INSERT INTO `teaching_area` VALUES (1285, '271114344776000', 'Maladies infectieuses ; maladies tropicales', 'Maladies infectieuses ; maladies tropicales', 1205, NULL);
INSERT INTO `teaching_area` VALUES (1286, '271114348342700', 'Épidémiologie, économie de la santé et prévention', 'Épidémiologie, économie de la santé et prévention', 1206, NULL);
INSERT INTO `teaching_area` VALUES (1287, '271114351322799', 'Médecine et santé au travail', 'Médecine et santé au travail', 1206, NULL);
INSERT INTO `teaching_area` VALUES (1288, '271114353707899', 'Médecine légale et droit de la santé', 'Médecine légale et droit de la santé', 1206, NULL);
INSERT INTO `teaching_area` VALUES (1289, '271114356332900', 'Biostatistiques, informatique médicale et technologies de communication', 'Biostatistiques, informatique médicale et technologies de communication', 1206, NULL);
INSERT INTO `teaching_area` VALUES (1290, '271114358788200', 'Épistémologie clinique', 'Épistémologie clinique', 1206, NULL);
INSERT INTO `teaching_area` VALUES (1291, '271114362644499', 'Hématologie ; transfusion', 'Hématologie ; transfusion', 1207, NULL);
INSERT INTO `teaching_area` VALUES (1292, '271114365567900', 'Cancérologie ; radiothérapie', 'Cancérologie ; radiothérapie', 1207, NULL);
INSERT INTO `teaching_area` VALUES (1293, '271114367973700', 'Immunologie', 'Immunologie', 1207, NULL);
INSERT INTO `teaching_area` VALUES (1294, '271114370902500', 'Génétique', 'Génétique', 1207, NULL);
INSERT INTO `teaching_area` VALUES (1295, '271114376382000', 'Anesthésiologie - réanimation ; médecine d\'urgence', 'Anesthésiologie - réanimation ; médecine d\'urgence', 1208, NULL);
INSERT INTO `teaching_area` VALUES (1296, '271114392805400', 'Réanimation ; médecine d\'urgence', 'Réanimation ; médecine d\'urgence', 1208, NULL);
INSERT INTO `teaching_area` VALUES (1297, '271114396656500', 'Pharmacologie fondamentale ; pharmacologie clinique', 'Pharmacologie fondamentale ; pharmacologie clinique', 1208, NULL);
INSERT INTO `teaching_area` VALUES (1298, '271114399955099', 'Thérapeutique', 'Thérapeutique', 1208, NULL);
INSERT INTO `teaching_area` VALUES (1299, '271114404238000', 'Neurologie', 'Neurologie', 1209, NULL);
INSERT INTO `teaching_area` VALUES (1300, '271114407988000', 'Neurochirurgie', 'Neurochirurgie', 1209, NULL);
INSERT INTO `teaching_area` VALUES (1301, '271114410482099', 'Psychiatrie d\'adultes', 'Psychiatrie d\'adultes', 1209, NULL);
INSERT INTO `teaching_area` VALUES (1302, '271114412961800', 'Pédopsychiatrie', 'Pédopsychiatrie', 1209, NULL);
INSERT INTO `teaching_area` VALUES (1303, '271114415345100', 'Médecine physique et de réadaptation', 'Médecine physique et de réadaptation', 1209, NULL);
INSERT INTO `teaching_area` VALUES (1304, '271114419226000', 'Rhumatologie', 'Rhumatologie', 1210, NULL);
INSERT INTO `teaching_area` VALUES (1305, '271114422219900', 'Chirurgie orthopédique et traumatologique', 'Chirurgie orthopédique et traumatologique', 1210, NULL);
INSERT INTO `teaching_area` VALUES (1306, '271114424811100', 'Dermato-vénéréologie', 'Dermato-vénéréologie', 1210, NULL);
INSERT INTO `teaching_area` VALUES (1307, '271114427052200', 'Chirurgie plastique, reconstructrice et esthétique ; brûlologie', 'Chirurgie plastique, reconstructrice et esthétique ; brûlologie', 1210, NULL);
INSERT INTO `teaching_area` VALUES (1308, '271114431150300', 'Pneumologie', 'Pneumologie', 1211, NULL);
INSERT INTO `teaching_area` VALUES (1309, '271114434526800', 'Cardiologie', 'Cardiologie', 1211, NULL);
INSERT INTO `teaching_area` VALUES (1310, '271114437075200', 'Chirurgie thoracique et cardiovasculaire', 'Chirurgie thoracique et cardiovasculaire', 1211, NULL);
INSERT INTO `teaching_area` VALUES (1311, '271114439840500', 'Chirurgie vasculaire ; médecine vasculaire', 'Chirurgie vasculaire ; médecine vasculaire', 1211, NULL);
INSERT INTO `teaching_area` VALUES (1320, '271135343007600', 'Gastroentérologie ; hépatologie', 'Gastroentérologie ; hépatologie', 1312, NULL);
INSERT INTO `teaching_area` VALUES (1321, '271135346385300', 'Chirurgie digestive', 'Chirurgie digestive', 1312, NULL);
INSERT INTO `teaching_area` VALUES (1322, '271135348742800', 'Néphrologie', 'Néphrologie', 1312, NULL);
INSERT INTO `teaching_area` VALUES (1323, '271135351447500', 'Urologie', 'Urologie', 1312, NULL);
INSERT INTO `teaching_area` VALUES (1324, '271135355503599', 'Médecine interne ; gériatrie et biologie du vieillissement, addictologie', 'Médecine interne ; gériatrie et biologie du vieillissement, addictologie', 1313, NULL);
INSERT INTO `teaching_area` VALUES (1325, '271135358601700', 'Chirurgie générale', 'Chirurgie générale', 1313, NULL);
INSERT INTO `teaching_area` VALUES (1326, '271135363186799', 'Médecine générale', 'Médecine générale', 1313, NULL);
INSERT INTO `teaching_area` VALUES (1327, '271135367389300', 'Pédiatrie', 'Pédiatrie', 1314, NULL);
INSERT INTO `teaching_area` VALUES (1328, '271135370707500', 'Chirurgie infantile', 'Chirurgie infantile', 1314, NULL);
INSERT INTO `teaching_area` VALUES (1329, '271135373303100', 'Gynécologie-obstétrique ; gynécologie médicale', 'Gynécologie-obstétrique ; gynécologie médicale', 1314, NULL);
INSERT INTO `teaching_area` VALUES (1330, '271135376126600', 'Endocrinologie, diabète et maladies métaboliques', 'Endocrinologie, diabète et maladies métaboliques', 1314, NULL);
INSERT INTO `teaching_area` VALUES (1331, '271135379376499', 'Biologie et médecine du développement et de la reproduction', 'Biologie et médecine du développement et de la reproduction', 1314, NULL);
INSERT INTO `teaching_area` VALUES (1332, '271135383224800', 'Oto-rhino-laryngologie', 'Oto-rhino-laryngologie', 1315, NULL);
INSERT INTO `teaching_area` VALUES (1333, '271135385965200', 'Ophtalmologie', 'Ophtalmologie', 1315, NULL);
INSERT INTO `teaching_area` VALUES (1334, '271135388223900', 'Chirurgie maxillo-faciale et stomatologie', 'Chirurgie maxillo-faciale et stomatologie', 1315, NULL);
INSERT INTO `teaching_area` VALUES (1335, '271135393270100', 'Pédodontie', 'Pédodontie', 1317, NULL);
INSERT INTO `teaching_area` VALUES (1336, '271135396377699', 'Orthopédie dento-faciale', 'Orthopédie dento-faciale', 1317, NULL);
INSERT INTO `teaching_area` VALUES (1337, '271135398461500', 'Prévention, épidémiologie, économie de la santé, odontologie légale', 'Prévention, épidémiologie, économie de la santé, odontologie légale', 1317, NULL);
INSERT INTO `teaching_area` VALUES (1338, '271135401574400', 'Parodontologie', 'Parodontologie', 1318, NULL);
INSERT INTO `teaching_area` VALUES (1339, '271135404234700', 'Chirurgie buccale, pathologie et thérapeutique, anesthésiologie et réanimation', 'Chirurgie buccale, pathologie et thérapeutique, anesthésiologie et réanimation', 1318, NULL);
INSERT INTO `teaching_area` VALUES (1340, '271135406416400', 'Sciences biologiques (biochimie, immunologie, histologie, embryologie, génétique, anatomie pathologique, bactériologie, pharmacologie)', 'Sciences biologiques (biochimie, immunologie, histologie, embryologie, génétique, anatomie pathologique, bactériologie, pharmacologie)', 1318, NULL);
INSERT INTO `teaching_area` VALUES (1341, '271135409921000', 'Odontologie conservatrice, endodontie', 'Odontologie conservatrice, endodontie', 1319, NULL);
INSERT INTO `teaching_area` VALUES (1342, '271135413612100', 'Prothèses', 'Prothèses', 1319, NULL);
INSERT INTO `teaching_area` VALUES (1343, '271135415855199', 'Sciences anatomiques et physiologiques, occlusodontiques, biomatériaux, biophysique, radiologie', 'Sciences anatomiques et physiologiques, occlusodontiques, biomatériaux, biophysique, radiologie', 1319, NULL);

-- ----------------------------
-- Table structure for teaching_area_group
-- ----------------------------
DROP TABLE IF EXISTS `teaching_area_group`;
CREATE TABLE `teaching_area_group`  (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `label` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `teaching_area_set` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_f4h4ji0wfcyp7g7kufslxwbrx`(`id_server`) USING BTREE,
  INDEX `FKqsyfcods699dixtgn4en0q5hd`(`teaching_area_set`) USING BTREE,
  CONSTRAINT `FKqsyfcods699dixtgn4en0q5hd` FOREIGN KEY (`teaching_area_set`) REFERENCES `teaching_area_set` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of teaching_area_group
-- ----------------------------
INSERT INTO `teaching_area_group` VALUES (1184, '271072446331600', 'Groupe 1 : droit et politiques', 'Groupe 1 : droit et politiques', 1183);
INSERT INTO `teaching_area_group` VALUES (1185, '271113736932499', 'Groupe 2 : Sciences économiques et de gestion', 'Groupe 2 : Sciences économiques et de gestion', 1183);
INSERT INTO `teaching_area_group` VALUES (1187, '271113752766300', 'Groupe 3 : Langues', 'Groupe 3 : Langues', 1186);
INSERT INTO `teaching_area_group` VALUES (1188, '271113797190100', 'Groupe 4 : sciences humaines', 'Groupe 4 : sciences humaines', 1186);
INSERT INTO `teaching_area_group` VALUES (1189, '271113858113900', 'Groupe 12 : Sport, Religion, Éducation et Culture', 'Groupe 12 : Sport, Religion, Éducation et Culture', 1186);
INSERT INTO `teaching_area_group` VALUES (1191, '271114171441300', 'Groupe 5 : Mathématiques et Informatique', 'Groupe 5 : Mathématiques et Informatique', 1190);
INSERT INTO `teaching_area_group` VALUES (1192, '271114184748200', 'Groupe 6 : Sciences physiques', 'Groupe 6 : Sciences physiques', 1190);
INSERT INTO `teaching_area_group` VALUES (1193, '271114198578900', 'Groupe 7 : Chimie', 'Groupe 7 : Chimie', 1190);
INSERT INTO `teaching_area_group` VALUES (1194, '271114214887299', 'Groupe 8 : Sciences spatiales et de l’univers', 'Groupe 8 : Sciences spatiales et de l’univers', 1190);
INSERT INTO `teaching_area_group` VALUES (1195, '271114234988900', 'Groupe 9 : Mécanique, informatique, électroniques', 'Groupe 9 : Mécanique, informatique, électroniques', 1190);
INSERT INTO `teaching_area_group` VALUES (1196, '271114250162100', 'Groupe 10 : Biologie et Biochimie', 'Groupe 10 : Biologie et Biochimie', 1190);
INSERT INTO `teaching_area_group` VALUES (1198, '271114273196400', 'Personnels enseignants hospitaliers (bi-appartenants)', 'Personnels enseignants hospitaliers (bi-appartenants)', 1197);
INSERT INTO `teaching_area_group` VALUES (1199, '271114285014999', 'Personnels enseignants-chercheurs (mono-appartenants)', 'Personnels enseignants-chercheurs (mono-appartenants)', 1197);
INSERT INTO `teaching_area_group` VALUES (1200, '271114295731200', 'Autres sections de santé', 'Autres sections de santé', 1197);
INSERT INTO `teaching_area_group` VALUES (1202, '271114307944400', '42 : Morphologie et morphogenèse', '42 : Morphologie et morphogenèse', 1201);
INSERT INTO `teaching_area_group` VALUES (1203, '271114320656300', '43 : Biophysique et imagerie médicale', '43 : Biophysique et imagerie médicale', 1201);
INSERT INTO `teaching_area_group` VALUES (1204, '271114327256000', '44 : Biochimie, biologie cellulaire et moléculaire, physiologie et nutrition', '44 : Biochimie, biologie cellulaire et moléculaire, physiologie et nutrition', 1201);
INSERT INTO `teaching_area_group` VALUES (1205, '271114338260800', '45 : Microbiologie, maladies transmissibles et hygiène', '45 : Microbiologie, maladies transmissibles et hygiène', 1201);
INSERT INTO `teaching_area_group` VALUES (1206, '271114346870500', '46 : Santé publique, environnement et société', '46 : Santé publique, environnement et société', 1201);
INSERT INTO `teaching_area_group` VALUES (1207, '271114361269700', '47 : Cancérologie, génétique, hématologie, immunologie', '47 : Cancérologie, génétique, hématologie, immunologie', 1201);
INSERT INTO `teaching_area_group` VALUES (1208, '271114374788100', '48 : Anesthésiologie, réanimation, médecine d\'urgence, pharmacologie et thérapeutique', '48 : Anesthésiologie, réanimation, médecine d\'urgence, pharmacologie et thérapeutique', 1201);
INSERT INTO `teaching_area_group` VALUES (1209, '271114402951499', '49 : Pathologie nerveuse et musculaire, pathologie mentale, handicap et rééducation', '49 : Pathologie nerveuse et musculaire, pathologie mentale, handicap et rééducation', 1201);
INSERT INTO `teaching_area_group` VALUES (1210, '271114417904599', '50 : Pathologie ostéo-articulaire, dermatologie et chirurgie plastique', '50 : Pathologie ostéo-articulaire, dermatologie et chirurgie plastique', 1201);
INSERT INTO `teaching_area_group` VALUES (1211, '271114429733700', '51 : Pathologie cardiorespiratoire et vasculaire', '51 : Pathologie cardiorespiratoire et vasculaire', 1201);
INSERT INTO `teaching_area_group` VALUES (1312, '271135341222900', '52 : Maladies des appareils digestif et urinaire', '52 : Maladies des appareils digestif et urinaire', 1201);
INSERT INTO `teaching_area_group` VALUES (1313, '271135354104999', '53 : Médecine interne, gériatrie, chirurgie générale et médecine générale', '53 : Médecine interne, gériatrie, chirurgie générale et médecine générale', 1201);
INSERT INTO `teaching_area_group` VALUES (1314, '271135365920000', '54 : Développement et pathologie de l\'enfant, gynécologie-obstétrique, endocrinologie et reproduction', '54 : Développement et pathologie de l\'enfant, gynécologie-obstétrique, endocrinologie et reproduction', 1201);
INSERT INTO `teaching_area_group` VALUES (1315, '271135381933600', '55 : Pathologie de la tête et du cou', '55 : Pathologie de la tête et du cou', 1201);
INSERT INTO `teaching_area_group` VALUES (1317, '271135391691999', '56 : Développement, croissance et prévention', '56 : Développement, croissance et prévention', 1316);
INSERT INTO `teaching_area_group` VALUES (1318, '271135400500200', '57 : Sciences biologiques, médecine et chirurgie buccales', '57 : Sciences biologiques, médecine et chirurgie buccales', 1316);
INSERT INTO `teaching_area_group` VALUES (1319, '271135408468900', '58 : Sciences physiques et physiologiques endodontiques et prothétiques', '58 : Sciences physiques et physiologiques endodontiques et prothétiques', 1316);

-- ----------------------------
-- Table structure for teaching_area_set
-- ----------------------------
DROP TABLE IF EXISTS `teaching_area_set`;
CREATE TABLE `teaching_area_set`  (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `label` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `education` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_9qi7qjgv1nw3ocd1b59ph6ah6`(`id_server`) USING BTREE,
  INDEX `FK2s5sq1scene9139o2ysijp6ij`(`education`) USING BTREE,
  CONSTRAINT `FK2s5sq1scene9139o2ysijp6ij` FOREIGN KEY (`education`) REFERENCES `education` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of teaching_area_set
-- ----------------------------
INSERT INTO `teaching_area_set` VALUES (1183, '271053542993600', 'Droit, Sciences politique, Économiques et Gestion', 'Droit, Sciences politique, Économiques et Gestion', 439);
INSERT INTO `teaching_area_set` VALUES (1186, '271113749381400', 'Lettres et Sciences Humaines', 'Lettres et Sciences Humaines', 439);
INSERT INTO `teaching_area_set` VALUES (1190, '271114168459700', 'Sciences et techniques', 'Sciences et techniques', 439);
INSERT INTO `teaching_area_set` VALUES (1197, '271114269437099', 'Pharmacie', 'Pharmacie', 439);
INSERT INTO `teaching_area_set` VALUES (1201, '271114304988900', 'Médecine', 'Médecine', 439);
INSERT INTO `teaching_area_set` VALUES (1316, '271135389658099', 'Odontologie', 'Odontologie', 439);

-- ----------------------------
-- Table structure for token
-- ----------------------------
DROP TABLE IF EXISTS `token`;
CREATE TABLE `token`  (
  `id` int(11) NOT NULL,
  `id_server` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `created_at` datetime(0) NOT NULL,
  `token` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `account` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_g6cqlajolqvqq1ft3y2wsfquj`(`id_server`) USING BTREE,
  UNIQUE INDEX `UK_pddrhgwxnms2aceeku9s2ewy5`(`token`) USING HASH,
  INDEX `FKck8dkegqatv6k8iq37s50v8dv`(`account`) USING BTREE,
  CONSTRAINT `FKck8dkegqatv6k8iq37s50v8dv` FOREIGN KEY (`account`) REFERENCES `account` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of token
-- ----------------------------
INSERT INTO `token` VALUES (1408, '117315925599600', '2020-10-28 18:16:25', 'eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIxMTMwODMyNjYzNjgwMCIsImV4cCI6MTYwMzkwNzE4NSwiaWF0IjoxNjAzOTA1Mzg1fQ.F9VmM_-NB0GmNinDCIf0T6t3zsa-1TGuMzugnXWTJ4GQ67IgXW_CoGN38OOVdz8FdflVDC8ZLWMKwbuPYInX8w', 1);
INSERT INTO `token` VALUES (1446, '436503204474898', '2020-12-03 13:23:47', 'eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiI0MzY0NzM5ODg3NjAxMTUiLCJleHAiOjE2MDcwMDAwMjcsImlhdCI6MTYwNjk5ODIyN30.2DkMTUyUvVAi9sFDKrZcu_qipALKZ5J-329D3LwF6fHFUHZN19cYKi0Xbud0HrYNCugaA7MdztYHTywjI3uD8g', 1445);
INSERT INTO `token` VALUES (1448, '436738873053681', '2020-11-16 17:07:12', 'eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiI0MzY3MTk4MzM1NzI2MzciLCJleHAiOjE2MDU1NDQ2MzIsImlhdCI6MTYwNTU0MjgzMn0.hr8OscNJASlIY8DVirTAxW-8MKB14lYZBzld2DZwAkhcmV7Ukq_621d-NIcRTkSuGiNTdaTS_q5Vnf0F7wCkCg', 1447);

SET FOREIGN_KEY_CHECKS = 1;
